<?php
declare(strict_types=1);

namespace App\Ability\Event;

use App\Ability\AbilityPartInterface;
use App\Entity\Game\Card;
use App\Entity\Game\CardEvent;
use App\Service\AbilityProcessor;
use App\Service\Game\CardEventProcessor;

trait StandEvent
{
    public function doStand(string $location): void
    {
        if ($this instanceof AbilityPartInterface) {
            $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
            if (
                $player->circleExists($location)
                && $player->getCircle($location) !== null
                && $player->getCircle($location)->getState() === Card::STATE_REST
            ) {
                $targetCard = $player->getCircle($location);
                $targetCard->setState(Card::STATE_STAND);
                $eventCard = $player->findCard($this->getSelfCardId());

                $this->getGame()->getState()->addChatLog(
                    $player->getName() . ' stands ' . $targetCard->getName()
                );

                CardEventProcessor::processCardEvent(
                    (new CardEvent())
                    ->setGame($this->getGame())
                    ->setType(CardEvent::TYPE_ON_STAND)
                    ->setPlayerHash($this->getPlayerHash())
                    ->setSourceCard($eventCard)
                    ->setTargetCards([
                        $targetCard
                    ])
                );
            }
        }
    }
}
