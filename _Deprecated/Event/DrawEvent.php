<?php
declare(strict_types=1);

namespace App\Ability\Event;

use App\Ability\AbilityPartInterface;
use App\Entity\Game\CardEvent;
use App\Service\AbilityProcessor;
use App\Service\Game\CardEventProcessor;

trait DrawEvent
{
    public function doDraw(int $count): void
    {
        if ($this instanceof AbilityPartInterface) {
            $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
            if ($player->getDeck()->getCount() > 0) {
                $cards = $player->getDeck()->drawCards($count);
                $player->getHand()->addCards($cards);
                $eventCard = $player->findCard($this->getEventCardId());
                CardEventProcessor::processCardEvent(
                    (new CardEvent())
                    ->setGame($this->getGame())
                    ->setPlayerHash($this->getPlayerHash())
                    ->setType(CardEvent::TYPE_ON_DRAW)
                    ->setSourceCard($eventCard)
                    ->setTargetCards($cards)
                );
            }
        }
    }
}
