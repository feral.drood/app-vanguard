<?php
declare(strict_types=1);

namespace App\Ability\Event;

use App\Ability\AbilityPartInterface;
use App\Entity\Game\Card;
use App\Entity\Game\CardEvent;
use App\Entity\Game\Player;
use App\Entity\Game\PlayerError;
use App\Entity\Game\TurnEvent;
use App\Service\AbilityProcessor;
use App\Service\ChatHelper;
use App\Service\Game\CardEventProcessor;

trait RetireOpponentRearguardEvent
{
    public function canBeRetired(string $location, $showError = true): bool
    {
        if ($this instanceof AbilityPartInterface) {
            $opponent = $this->getGame()->getState()->getOpposingPlayer($this->getPlayerHash());
            if (
                $opponent->circleExists($location) === true
                && $location !== Player::VANGUARD
                && $opponent->getCircle($location) !== null
            ) {
                $unit = $opponent->getCircle($location);

                if ($unit->hasProperty(Card::PROPERTY_CANNOT_BE_TARGETED)) {
                    if ($showError) {
                        $this->getGame()->getGameState()->setPlayerError(
                            new PlayerError(
                                $this->getPlayerHash(),
                                $unit->getName() . ' cannot be targeted'
                            )
                        );
                    }
                    return false;
                } elseif ($unit->hasProperty(Card::PROPERTY_CANNOT_BE_RETIRED_BY_OPPONENT)) {
                    if ($showError) {
                        $this->getGame()->getGameState()->setPlayerError(
                            new PlayerError(
                                $this->getPlayerHash(),
                                $unit->getName() . ' cannot be retired by opponent'
                            )
                        );
                    }
                    return false;
                }
            }
        }
        return true;
    }

    public function doRetireOpponentRearguard(string $location): void
    {
        if ($this instanceof AbilityPartInterface) {
            $opponent = $this->getGame()->getState()->getOpposingPlayer($this->getPlayerHash());
            if (
                $opponent->circleExists($location) === true
                && $location !== Player::VANGUARD
                && $opponent->getCircle($location) !== null
            ) {
                $unit = $opponent->getCircle($location);
                $eventCard = $opponent->findCard($this->getSelfCardId());

                $unit->reset();
                $opponent->getDrop()->addCard($unit);
                $opponent->setCircle($location, null);

                $this->getGame()->getState()->addChatLog(
                    ChatHelper::possessive($opponent->getName()) . ' ' . $unit->getName(). ' is retired'
                );

                $this->getGame()->getState()->getEventLog()->add(
                    (new TurnEvent())
                    ->setPlayerHash($this->getPlayerHash())
                    ->setType(TurnEvent::EVENT_RETIRE_OPPONENT_REARGUARD)
                    ->setEventCardId($this->getSelfCardId())
                    ->setTargetCardIds([$unit->getId()])
                );

                CardEventProcessor::processCardEvent(
                    (new CardEvent())
                    ->setGame($this->getGame())
                    ->setType(CardEvent::TYPE_ON_OPPONENT_REARGUARD_RETIRE)
                    ->setPlayerHash($opponent->getHash())
                    ->setSourceCard($eventCard)
                );
            }
        }
    }
}
