<?php
declare(strict_types=1);

namespace App\Ability\Event;

use App\Ability\AbilityPartInterface;
use App\Entity\Game\CardEvent;
use App\Entity\Game\Player;
use App\Service\AbilityProcessor;
use App\Service\ChatHelper;
use App\Service\Game\CardEventProcessor;

trait RetireRearguardEvent
{
    public function doRetireRearguard(string $location): void
    {
        if ($this instanceof AbilityPartInterface) {
            $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
            if (
                $player->circleExists($location) === true
                && $location !== Player::VANGUARD
                && $player->getCircle($location) !== null
            ) {
                $unit = $player->getCircle($location);
                $eventCard = $player->findCard($this->getSelfCardId());

                $unit->reset();
                $player->getDrop()->addCard($unit);
                $player->setCircle($location, null);

                $this->getGame()->getState()->addChatLog(
                    ChatHelper::possessive($player->getName()) . ' ' . $unit->getName(). ' is retired'
                );

                CardEventProcessor::processCardEvent(
                    (new CardEvent())
                    ->setGame($this->getGame())
                    ->setType(CardEvent::TYPE_ON_REARGUARD_RETIRE)
                    ->setPlayerHash($this->getPlayerHash())
                    ->setSourceCard($eventCard)
                );
            }
        }
    }
}
