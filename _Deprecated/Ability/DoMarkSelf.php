<?php
declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\AbilityPart;
use App\Ability\ActionInterface;
use App\Entity\PlayerInput;

class DoMarkSelf extends AbilityPart implements ActionInterface
{
    public function processAction(PlayerInput $input): bool
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
        $card = $player->findCard($this->getSelfCardId());
        $player->setBuffer([$card]);

        return true;
    }
}
