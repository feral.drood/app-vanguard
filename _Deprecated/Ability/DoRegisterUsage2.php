<?php
declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\AbilityPart;
use App\Ability\ActionInterface;
use App\Entity\PlayerInput;

class DoRegisterUsage2 extends AbilityPart implements ActionInterface
{
    public function processAction(PlayerInput $input): bool
    {
        $this->getGame()->getState()->getAbilityStack()->addUsedAbility($this->getSelfCardId() . '?2');

        return true;
    }
}
