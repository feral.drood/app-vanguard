<?php
declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\AbilityPart;
use App\Ability\ActionInterface;
use App\Ability\ProcessInterface;
use App\Entity\PlayerInput;

class DoAddOpponentProperty extends AbilityPart implements ProcessInterface
{
    public function process()
    {
        $opponent = $this->getGame()->getState()->getOpposingPlayer($this->getPlayerHash());
        $opponent->addProperty($this->getOption());

        return true;
    }
}
