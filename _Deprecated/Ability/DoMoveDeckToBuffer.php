<?php
declare(strict_types=1);

namespace App\Ability\Action;

use App\Entity\Actions;
use App\Ability\AbilityPart;
use App\Ability\ActionInterface;
use App\Ability\SelectInterface;
use App\Entity\PlayerInput;

class DoMoveDeckToBuffer extends AbilityPart implements ActionInterface
{
    public function processAction(PlayerInput $input): bool
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());

        $removedCards = $player->getDeck()->drawCards(intval($this->getOption()));
        $player->setBuffer($removedCards);

        return true;
    }
}
