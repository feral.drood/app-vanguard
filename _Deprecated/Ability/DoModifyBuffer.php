<?php
declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\AbilityPart;
use App\Ability\ActionInterface;
use App\Entity\Game\Card;
use App\Entity\PlayerInput;

class DoModifyBuffer extends AbilityPart implements ActionInterface
{
    public function processAction(PlayerInput $input): bool
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
        $buffer = $player->getBuffer();

        $length = $this->getOption(self::OPTION_LENGTH, Card::BONUS_LENGTH_BATTLE);
        $power = $this->getOption(Card::BONUS_CRITICAL);
        $critical = $this->getOption(Card::BONUS_CRITICAL);
        $property = $this->getOption(Card::BONUS_PROPERTY);
        $drive = $this->getOption(Card::BONUS_DRIVE);

        foreach ($buffer as $unit) {
            if ($property !== null) {
                $unit->addBonusProperty($property);
            }
            if ($power !== null) {
                $unit->addBonus($length, Card::BONUS_POWER, (int)$power);
            }
            if ($critical !== null) {
                $unit->addBonus($length, Card::BONUS_CRITICAL, (int)$critical);
            }
            if ($drive !== null) {
                $unit->addBonus($length, Card::BONUS_DRIVE, (int)$drive);
            }
        }

        return true;
    }
}
