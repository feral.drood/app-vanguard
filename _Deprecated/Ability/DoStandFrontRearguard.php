<?php
declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\Event\StandEvent;
use App\Entity\Actions;
use App\Entity\Game\Card;
use App\Ability\AbilityPart;
use App\Ability\ActionInterface;
use App\Ability\SelectInterface;
use App\Entity\Game\Player;
use App\Entity\PlayerInput;

class DoStandFrontRearguard extends AbilityPart implements ActionInterface, SelectInterface
{
    use StandEvent;
    public function processAction(PlayerInput $input): bool
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
        $data = $input->getList();
        $data = array_unique($data);
        if (count($data) === 0) {
            return true;
        }
        if (count($data) > intval($this->getOption())) {
            return false;
        }

        foreach ($data as $circleId) {
            if (
                $circleId === null
                || $circleId === Player::VANGUARD
                || !$player->circleExists($circleId)
                || $player->getCircle($circleId) === null
            ) {
                return false;
            }
        }

        foreach ($data as $circleId) {
            $this->doStand($circleId);
        }
        return true;
    }

    public function getActionType(): ?string
    {
        return Actions::SELECT_REARGUARD;
    }

    public function getActionOption()
    {
        return intval($this->getOption());
    }
}
