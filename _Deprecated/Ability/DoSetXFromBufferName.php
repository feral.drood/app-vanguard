<?php
declare(strict_types=1);

namespace App\Ability\Action;

use App\Entity\Game\Card;
use App\Ability\AbilityPart;
use App\Ability\ActionInterface;
use App\Entity\PlayerInput;

class DoSetXFromBufferName extends AbilityPart implements ActionInterface
{
    public function processAction(PlayerInput $input): bool
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
        $names = [];
        $buffer = $player->getBuffer();
        foreach ($buffer as $card) {
            $names[] = $card->getName();
        }

        $names = array_unique($names);
        if (count($names) === 0) {
            $player->setBufferOption('');
        } elseif (count($names) === 1) {
            $player->setBufferOption($names[0]);
        } else {
            $player->setBufferOption($names);
        }

        return true;
    }
}
