<?php
declare(strict_types=1);

namespace App\Ability\Action;

use App\Entity\Game\Card;
use App\Ability\AbilityPart;
use App\Ability\ActionInterface;
use App\Entity\PlayerInput;

class DoSetX extends AbilityPart implements ActionInterface
{
    public function processAction(PlayerInput $input): bool
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
        $player->setBufferOption($this->getOption());

        return true;
    }
}
