<?php
declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\AbilityPart;
use App\Ability\ActionInterface;
use App\Ability\Event\StandEvent;
use App\Entity\Game\Card;
use App\Entity\PlayerInput;

class DoStandSelf extends AbilityPart implements ActionInterface
{
    use StandEvent;

    public function processAction(PlayerInput $input): bool
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
        $circleId = $player->findCircle($this->getSelfCardId());

        if (
            $circleId !== null
            && $player->getCircle($circleId) !== null
        ) {
            $this->doStand($circleId);
        }

        return true;
    }
}
