<?php
declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\AbilityPart;
use App\Ability\ActionInterface;
use App\Entity\PlayerInput;

class DoMoveBufferToSoul extends AbilityPart implements ActionInterface
{
    public function processAction(PlayerInput $input): bool
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
        $player->getSoul()->addCards($player->getBuffer());
        $player->clearBuffer();

        return true;
    }
}
