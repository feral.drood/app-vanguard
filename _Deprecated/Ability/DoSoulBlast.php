<?php
declare(strict_types=1);

namespace App\Ability\Action;

use App\Entity\Actions;
use App\Ability\AbilityPart;
use App\Ability\ActionInterface;
use App\Ability\SelectInterface;
use App\Entity\PlayerInput;

class DoSoulBlast extends AbilityPart implements SelectInterface, ActionInterface
{
    public function processAction(PlayerInput $input): bool
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
        $data = $input->getList();
        if (count($data) !== intval($this->getOption())) {
            return false;
        }

        foreach ($data as $cardId) {
            if (!$player->getSoul()->cardExists($cardId)) {
                return false;
            }
        }

        $removedCards = $player->getSoul()->removeCards($data);
        $player->getDrop()->addCards($removedCards);
        return true;
    }

    public function getActionType(): string
    {
        return Actions::SELECT_SOUL;
    }

    public function getActionOption()
    {
        return intval($this->getOption());
    }
}
