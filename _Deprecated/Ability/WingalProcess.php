<?php
declare(strict_types=1);

namespace App\Ability\Aura;

use App\Entity\Game\Card;
use App\Entity\Game\Player;
use App\Ability\AbilityPart;
use App\Ability\ProcessInterface;

/*
 * [CONT](RC):During your turn, all of your "Blaster Blade" in the same column as this unit get [Power]+5000.
 */
class WingalProcess extends AbilityPart implements ProcessInterface
{
    const NAME = 'Blaster Blade';
    const PAIRS = [
        Player::REARGUARD1 => Player::REARGUARD2,
        Player::VANGUARD => Player::REARGUARD3,
        Player::REARGUARD5 => Player::REARGUARD4
    ];

    public function process()
    {
        $gameState = $this->getGame()->getState();
        $player = $gameState->getPlayer($this->getPlayerHash());
        if ($player->getHash() !== $gameState->getActivePlayerHash()) {
            return;
        }

        /** @var Card $card */
        $card = $this->getOption();

        $circleId = $player->findCircle($card->getId());
        if (array_key_exists($circleId, self::PAIRS)) {
            $buddy = $player->getCircle(self::PAIRS[$circleId]);
        } elseif (in_array($circleId, self::PAIRS, true)) {
            $buddy = $player->getCircle(array_flip(self::PAIRS)[$circleId]);
        } else {
            $buddy = null;
        }

        if ($buddy !== null && strcasecmp($buddy->getName(), self::NAME) === 0) {
            $buddy->addBonus(Card::BONUS_LENGTH_PERMANENT, Card::BONUS_POWER, 5);
        }
    }
}
