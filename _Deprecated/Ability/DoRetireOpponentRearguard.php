<?php
declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\Event\RetireOpponentRearguardEvent;
use App\Entity\Actions;
use App\Ability\AbilityPart;
use App\Ability\ActionInterface;
use App\Ability\SelectInterface;
use App\Entity\Game\PlayerError;
use App\Entity\Game\Player;
use App\Entity\Game\Card;
use App\Entity\PlayerInput;

class DoRetireOpponentRearguard extends AbilityPart implements ActionInterface, SelectInterface
{
    use RetireOpponentRearguardEvent;

    /**
     * @param string|array $data
     * @return bool
     */
    public function processAction(PlayerInput $input): bool
    {
        $data = $input->getList();
        $validChoices = $this->getValidChoices();

        if (count($validChoices) > (int)$this->getOption(self::OPTION_MAX_COUNT, 1)) {
            $selectedChoices = [];

            foreach ($data as $location) {
                if (in_array($location, $validChoices, true)) {
                    $selectedChoices[] = $location;
                }
            }
        } else {
            $selectedChoices = $validChoices;
        }

        if (
            count($selectedChoices) < (int)$this->getOption(self::OPTION_MIN_COUNT, 0)
            || count($data) !== count($selectedChoices)
        ) {
            $this->getGame()->getState()->setPlayerError(
                new PlayerError(
                    $this->getPlayerHash(),
                    'Invalid rearguard selection'
                )
            );
            return false;
        }

        foreach ($selectedChoices as $location) {
            $this->doRetireOpponentRearguard($location);
        }

        return true;
    }

    public function getActionType(): ?string
    {
        if (count($this->getValidChoices()) > (int)$this->getOption(self::OPTION_MAX_COUNT, 1)) {
            return Actions::SELECT_OPPONENT_REARGUARD;
        } else {
            return null;
        }
    }

    public function getActionOption()
    {
        return intval($this->getOption(self::OPTION_MAX_COUNT, 1));
    }

    private function getValidChoices(): array
    {
        $validChoices = [];
        $opponent = $this->getGame()->getState()->getOpposingPlayer($this->getPlayerHash());
        $minGrade = (int)$this->getOption(self::OPTION_MIN_GRADE, 0);
        $maxGrade = (int)$this->getOption(self::OPTION_MAX_GRADE, 4);
        $name = $this->getOption(self::OPTION_NAME);
        $partialName = $this->getOption(self::OPTION_PARTIAL_NAME);
        $location = $this->getOption(self::OPTION_LOCATION);
        $validLocations = null;
        $invalidLocations = null;

        if ($location !== null) {
            if ($location === 'front') {
                $invalidLocations = [
                    Player::REARGUARD2,
                    Player::REARGUARD3,
                    Player::REARGUARD4
                ];
            } elseif ($location === 'back') {
                $validLocations = [
                    Player::REARGUARD2,
                    Player::REARGUARD3,
                    Player::REARGUARD4
                ];
            } elseif ($location === 'column') {
                $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
                $currentLocation = $player->findCircle($this->getSelfCardId());
                if (in_array($currentLocation, [Player::REARGUARD1, Player::REARGUARD2], true)) {
                    $validLocations = [Player::REARGUARD4, Player::REARGUARD5];
                } elseif (in_array($currentLocation, [Player::REARGUARD4, Player::REARGUARD5], true)) {
                    $validLocations = [Player::REARGUARD1, Player::REARGUARD2];
                } elseif (in_array($currentLocation, [Player::REARGUARD3, Player::VANGUARD], true)) {
                    $validLocations = [Player::REARGUARD3];
                } else {
                    $circleId = (int)substr($currentLocation, strlen(Player::REARGUARD));
                    if ($circleId % 2 === 0) {
                        $validLocations = [Player::REARGUARD . ($circleId + 1)];
                    } else {
                        $validLocations = [Player::REARGUARD . ($circleId - 1)];
                    }
                }
            }
        }

        foreach ($opponent->getField() as $location => $unit) {
            if (
                $location !== Player::VANGUARD
                && $unit !== null
            ) {
                if (
                    (
                        $name !== null
                        && $name !== $unit->getName()
                    )
                    || (
                        $partialName !== null
                        && stripos($unit->getName(), $partialName) === false
                    )
                    || $unit->getGrade() < $minGrade
                    || $unit->getGrade() > $maxGrade
                    || !$this->canBeRetired($location, false)
                    || (
                        $validLocations !== null
                        && !in_array($location, $validLocations, true)
                    )
                    || (
                        $invalidLocations !== null
                        && in_array($location, $invalidLocations, true)
                    )
                    || $unit->hasProperty(Card::PROPERTY_CANNOT_BE_TARGETED)
                    || $unit->hasProperty(Card::PROPERTY_CANNOT_BE_RETIRED_BY_OPPONENT)
                ) {
                    continue;
                }
                $validChoices[] = $location;
            }
        }

        return $validChoices;
    }
}
