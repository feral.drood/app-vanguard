<?php
declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\AbilityPart;
use App\Ability\ActionInterface;
use App\Entity\Actions;
use App\Entity\Game\Player;
use App\Ability\SelectInterface;
use App\Entity\Game\PlayerError;
use App\Entity\PlayerInput;

class DoSoulChargeNamedRearguard extends AbilityPart implements ActionInterface, SelectInterface
{
    public function processAction(PlayerInput $input): bool
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
        $choices = $this->getValidChoices();
        $data = $input->getList();

        if (count($choices) < 1) {
            return true;
        } elseif (count($choices) === 1) {
            $data = $choices;
        } else {
            $validCount = 0;
            foreach ($data as $location) {
                if (
                    $location !== Player::VANGUARD
                    && $player->circleExists($location)
                    && $player->getCircle($location) !== null
                    && $player->getCircle($location)->getName() === $this->getOption()
                ) {
                    $validCount++;
                }
            }

            if ($validCount !== 1 || count($data) !== 1) {
                $this->getGame()->getState()->setPlayerError(
                    new PlayerError(
                        $this->getPlayerHash(),
                        'Invalid named rearguard selection'
                    )
                );
                return false;
            }
        }

        foreach ($data as $location) {
            $card = $player->getCircle($location);
            $player->setCircle($location, null);
            $player->getSoul()->addCard($card);
            $this->getGame()->getState()->addChatLog(
                $player->getName() . ' soul charges ' . $card->getName()
            );
        }

        return true;
    }

    public function getActionType(): ?string
    {
        $choices = $this->getValidChoices();

        if (count($choices) > 1) {
            return Actions::SELECT_REARGUARD;
        } else {
            return null;
        }
    }

    /**
     * Return optional parameter for some selects
     *
     * @return mixed
     */
    public function getActionOption()
    {
        return 1;
    }

    private function getValidChoices(): array
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
        $choices = [];
        foreach ($player->getField() as $location => $unit) {
            if (
                $unit !== null
                && $location !== Player::VANGUARD
                && $unit->getName() === $this->getOption()
            ) {
                $choices[] = $location;
            }
        }

        return $choices;
    }
}
