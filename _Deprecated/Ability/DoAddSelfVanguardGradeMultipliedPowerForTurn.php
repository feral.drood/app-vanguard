<?php
declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\ProcessInterface;
use App\Entity\Game\Card;
use App\Ability\AbilityPart;

class DoAddSelfVanguardGradeMultipliedPowerForTurn extends AbilityPart implements ProcessInterface
{
    public function process()
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
        $circleId = $player->findCircle($this->getSelfCardId());

        if ($circleId !== null) {
            $addedPower = $player->getVanguard()->getGrade() * intval($this->getOption());
            $player->getCircle($circleId)->addBonus(Card::BONUS_LENGTH_TURN, Card::BONUS_POWER, $addedPower);
        }
        return true;
    }
}
