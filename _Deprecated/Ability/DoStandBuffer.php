<?php
declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\Event\StandEvent;
use App\Ability\AbilityPart;
use App\Ability\ActionInterface;
use App\Entity\PlayerInput;

class DoStandBuffer extends AbilityPart implements ActionInterface
{
    use StandEvent;

    public function processAction(PlayerInput $input): bool
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());

        foreach ($player->getBuffer() as $card) {
            $location = $player->findCircle($card->getId());
            if ($location !== null) {
                $this->doStand($location);
            }
        }

        return true;
    }
}
