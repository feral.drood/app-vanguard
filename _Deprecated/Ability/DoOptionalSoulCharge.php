<?php
declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\ChoiceInterface;
use App\Ability\AbilityPart;
use App\Ability\ActionInterface;
use App\Entity\PlayerInput;

class DoOptionalSoulCharge extends AbilityPart implements ActionInterface, ChoiceInterface
{
    public function processAction(PlayerInput $input): bool
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());

        if ($input->getValue() === 'soul') {
            $removedCards = $player->getDeck()->drawCards(intval($this->getOption()));
            $player->getSoul()->addCards($removedCards);
        }

        return true;
    }

    public function getChoices(): array
    {
        $count = intval($this->getOption());
        $message = 'Soul charge';
        if ($count > 1) {
            $message .= ' (' . $count . ')';
        }
        return ['soul' => $message];
    }
}
