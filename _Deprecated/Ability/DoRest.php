<?php
declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\AbilityPart;
use App\Ability\ActionInterface;
use App\Entity\Game\Card;
use App\Entity\PlayerInput;

class DoRest extends AbilityPart implements ActionInterface
{
    public function processAction(PlayerInput $input): bool
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
        $circleId = $player->findCircle($this->getSelfCardId());

        if (
            $circleId !== null
            && $player->getCircle($circleId) !== null
        ) {
            $player->getCircle($circleId)->setState(Card::STATE_REST);
        }

        return true;
    }
}
