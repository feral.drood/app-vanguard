<?php
declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\Event\StandEvent;
use App\Entity\Actions;
use App\Entity\Game\Card;
use App\Ability\AbilityPart;
use App\Ability\ActionInterface;
use App\Ability\SelectInterface;
use App\Entity\Game\Player;
use App\Entity\PlayerInput;

class DoStandRearguardAndAddPowerForTurn extends AbilityPart implements ActionInterface, SelectInterface
{
    use StandEvent;

    public function processAction(PlayerInput $input): bool
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
        $circleId = $input->getValue();
        if (
            $circleId !== null
            && $circleId !== Player::VANGUARD
            && $player->circleExists($circleId)
            && $player->getCircle($circleId) !== null
        ) {
            $player->getCircle($circleId)->addBonus(
                Card::BONUS_LENGTH_TURN,
                Card::BONUS_POWER,
                $this->getOption() !== null ? intval($this->getOption()) : 1
            );
            $this->doStand($circleId);

            return true;
        }
        return false;
    }

    public function getActionType(): ?string
    {
        return Actions::SELECT_REARGUARD;
    }

    public function getActionOption()
    {
        return 1;
    }
}
