<?php
declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\Event\StandEvent;
use App\Entity\Actions;
use App\Entity\Game\Card;
use App\Ability\AbilityPart;
use App\Ability\ActionInterface;
use App\Ability\SelectInterface;
use App\Entity\Game\Player;
use App\Entity\Game\PlayerError;
use App\Entity\PlayerInput;

class DoStandOtherRearguard extends AbilityPart implements ActionInterface, SelectInterface
{
    use StandEvent;

    public function processAction(PlayerInput $input): bool
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
        $data = $input->getList();
        $data = array_unique($data);

        $validChoices = $this->getValidChoices();
        $validLocations = [];
        if (count($validChoices) > intval($this->getOption())) {
            foreach ($data as $selectedLocation) {
                if (
                    $selectedLocation !== Player::VANGUARD
                    && $player->circleExists($selectedLocation)
                    && $player->getCircle($selectedLocation) !== null
                    && $player->getCircle($selectedLocation)->getId() !== $this->getSelfCardId()
                ) {
                    $validLocations[] = $selectedLocation;
                }
            }

            if (count($validLocations) > intval($this->getOption())) {
                $this->getGame()->getState()->setPlayerError(
                    new PlayerError(
                        $this->getPlayerHash(),
                        'Invalid rearguard selection'
                    )
                );
                return false;
            }
        } else {
            $validLocations = $validChoices;
        }

        foreach ($validLocations as $location) {
            $this->doStand($location);
        }

        return true;
    }

    public function getActionType(): ?string
    {
        if (count($this->getValidChoices()) > intval($this->getOption())) {
            return Actions::SELECT_REARGUARD;
        } else {
            return null;
        }
    }

    public function getActionOption()
    {
        return intval($this->getOption());
    }

    public function getValidChoices(): array
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
        $validLocations = [];

        foreach ($player->getField() as $location => $unit) {
            if (
                $location !== Player::VANGUARD
                && $unit !== null
                && $unit->getId() !== $this->getSelfCardId()
            ) {
                $validLocations[] = $location;
            }
        }

        return $validLocations;
    }
}
