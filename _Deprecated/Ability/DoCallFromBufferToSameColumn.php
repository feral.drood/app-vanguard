<?php
declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\Event\RetireRearguardEvent;
use App\Ability\AbilityPart;
use App\Ability\ProcessInterface;
use App\Entity\Game\CardEvent;
use App\Entity\Game\Player;
use App\Entity\PlayerInput;
use App\Service\Game\CardEventProcessor;

class DoCallFromBufferToSameColumn extends AbilityPart implements ProcessInterface
{
    use RetireRearguardEvent;

    const PAIRS = [
        Player::REARGUARD1 => Player::REARGUARD2,
        Player::REARGUARD2 => Player::REARGUARD1,
        Player::VANGUARD => Player::REARGUARD3,
        Player::REARGUARD4 => Player::REARGUARD5,
        Player::REARGUARD5 => Player::REARGUARD4,
    ];

    public function process(): bool
    {
        $gameState = $this->getGame()->getState();
        $player = $gameState->getPlayer($this->getPlayerHash());

        $buffer = $player->getBuffer();

        if (count($buffer) !== 1) {
            $player->getDrop()->addCards($buffer);
            $gameState->addChatLog('DoCallFromBufferToSameColumn.Error: buffer count 1 != ' . count($buffer));
            return true;
        } else {
            $card = $buffer[0];
        }

        $cardLocation = $player->findCardLocation($card->getId());
        $selfLocation = $player->findCircle($this->getSelfCardId());
        if (!isset(self::PAIRS[$selfLocation])) {
            $gameState->addChatLog('DoCallFromBufferToSameColumn.Error: invalid self location');
            $player->getDrop()->addCards($buffer);
            return true;
        }

        $targetLocation = self::PAIRS[$selfLocation];

        // retire old rearguard
        if ($player->getCircle($targetLocation) !== null) {
            $this->doRetireRearguard($targetLocation);
        }

        $player->setCircle($targetLocation, $card);

        $gameState->addChatLog(
            $player->getName() . ' calls ' . $card->getName()
        );

        CardEventProcessor::processCardEvent(
            (new CardEvent())
            ->setGame($this->getGame())
            ->setType($cardLocation === 'hand' ? CardEvent::TYPE_ON_PLACE_FROM_HAND : CardEvent::TYPE_ON_PLACE)
            ->setPlayerHash($this->getPlayerHash())
            ->setSourceCard($card)
        );

        return true;
    }
}
