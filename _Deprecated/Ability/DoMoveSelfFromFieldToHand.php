<?php
declare(strict_types=1);

namespace App\Ability\Action;

use App\Entity\Game\Card;
use App\Ability\AbilityPart;
use App\Ability\ActionInterface;
use App\Entity\PlayerInput;
use App\Service\ChatLogHelper;

class DoMoveSelfFromFieldToHand extends AbilityPart implements ActionInterface
{
    public function processAction(PlayerInput $input): bool
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
        $circleId = $player->findCircle($this->getSelfCardId());

        if ($circleId !== null) {
            $card = $player->getCircle($circleId);
            $player->setCircle($circleId, null);
            $player->getHand()->addCard($card);
            $this->getGame()->getState()->addChatLog(
                ChatLogHelper::possessive($player->getName()) . ' ' . $card->getName() . ' is returned to hand'
            );
        }
        return true;
    }
}
