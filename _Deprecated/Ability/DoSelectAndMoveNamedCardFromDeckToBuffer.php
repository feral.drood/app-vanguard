<?php
declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\AbilityPart;
use App\Ability\ActionInterface;
use App\Ability\CardsSelectInterface;
use App\Ability\SelectInterface;
use App\Entity\Actions;
use App\Entity\Game\CardEvent;
use App\Entity\PlayerInput;
use App\Service\AbilityProcessor;
use App\Service\Game\CardEventProcessor;

/**
 * Select named card from deck to active
 */
class DoSelectAndMoveNamedCardFromDeckToBuffer extends AbilityPart implements ActionInterface, SelectInterface, CardsSelectInterface
{
    const MAX_CARDS = 50;

    public function processAction(PlayerInput $input): bool
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
        $data = $input->getValue();
        if ($data === null) {
            return true;
        }

        $topCards = $player->getDeck()->viewCards(self::MAX_CARDS);
        $eventCard = $player->findCard($this->getSelfCardId());
        $card = null;
        foreach ($topCards as $card) {
            if ($card->getId() === $data) {
                break;
            }
        }

        if ($card !== null && strcasecmp($card->getName(), $this->getOption()) === 0) {
            $player->getDeck()->removeCards([$card->getId()]);
            $player->setBuffer([$card]);
        }
        CardEventProcessor::processCardEvent(
            (new CardEvent())
            ->setGame($this->getGame())
            ->setType(CardEvent::TYPE_ON_SEARCH)
            ->setPlayerHash($this->getPlayerHash())
            ->setSourceCard($eventCard)
        );

        return true;
    }

    public function getActionType(): ?string
    {
        return Actions::SELECT_DECK;
    }

    public function getActionOption()
    {
        return 1;
    }

    public function getCardCount(): int
    {
        return self::MAX_CARDS;
    }
}
