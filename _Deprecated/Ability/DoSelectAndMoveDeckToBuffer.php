<?php
declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\AbilityPart;
use App\Ability\ActionInterface;
use App\Ability\CardsSelectInterface;
use App\Ability\SelectInterface;
use App\Entity\Actions;
use App\Entity\Game\CardEvent;
use App\Entity\Game\PlayerError;
use App\Entity\PlayerInput;
use App\Service\Game\CardEventProcessor;

/**
 * Select named/grade/whatever cards from deck to buffer
 */
class DoSelectAndMoveDeckToBuffer extends AbilityPart implements ActionInterface, SelectInterface, CardsSelectInterface
{
    public function processAction(PlayerInput $input): bool
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
        $data = $input->getList();

        $minGrade = (int)$this->getOption(self::OPTION_MAX_GRADE, 0);
        $maxGrade = (int)$this->getOption(self::OPTION_MAX_GRADE, 4);
        $deckCount = (int)$this->getOption(self::OPTION_DECK_COUNT, 50);
        $name = $this->getOption(self::OPTION_NAME);
        $partialName = $this->getOption(self::OPTION_PARTIAL_NAME);

        $topCards = $player->getDeck()->viewCards($deckCount);
        $eventCard = $player->findCard($this->getSelfCardId());
        $selectedCards = [];

        foreach ($topCards as $card) {
            if (
                (
                    $name !== null
                    && $name !== $card->getName()
                )
                || (
                    $partialName !== null
                    && stripos($card->getName(), $partialName) === false
                )
                || $card->getGrade() < $minGrade
                || $card->getGrade() > $maxGrade
                || !in_array($card->getId(), $data, true)
            ) {
                continue;
            }
            $selectedCards[] = $card;
        }

        if (
            count($selectedCards) < (int)$this->getOption(self::OPTION_MIN_COUNT, 0)
            || count($data) !== count($selectedCards)
        ) {
            $this->getGame()->getState()->setPlayerError(
                new PlayerError(
                    $this->getPlayerHash(),
                    'Invalid selection'
                )
            );
            return false;
        }

        CardEventProcessor::processCardEvent(
            (new CardEvent())
            ->setGame($this->getGame())
            ->setType(CardEvent::TYPE_ON_SEARCH)
            ->setPlayerHash($this->getPlayerHash())
            ->setSourceCard($eventCard)
        );

        return true;
    }

    public function getActionType(): ?string
    {
        return Actions::SELECT_DECK;
    }

    public function getActionOption()
    {
        return (int)$this->getOption(self::OPTION_MAX_COUNT, 1);
    }

    public function getCardCount(): int
    {
        return (int)$this->getOption(self::OPTION_DECK_COUNT, 50);
    }
}
