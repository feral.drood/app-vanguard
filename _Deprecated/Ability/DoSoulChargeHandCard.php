<?php
declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\AbilityPart;
use App\Ability\ActionInterface;
use App\Entity\Actions;
use App\Entity\Game\Player;
use App\Ability\SelectInterface;
use App\Entity\Game\PlayerError;
use App\Entity\PlayerInput;
use App\Service\ChatLogHelper;

class DoSoulChargeHandCard extends AbilityPart implements ActionInterface, SelectInterface
{
    public function processAction(PlayerInput $input): bool
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
        $data = $input->getList();

        if ($player->getHand()->getCount() <= intval($this->getOption())) {
            $cards = $player->getHand()->getCards();
            $player->getHand()->setCards([]);
            $player->getSoul()->addCards($cards);
        } else {
            $cards = [];
            foreach ($data as $cardId) {
                $card = $player->getHand()->getCardById($cardId);
                if ($card !== null) {
                    $cards[] = $card;
                }
            }

            if (count($cards) !== intval($this->getOption())) {
                $this->getGame()->getState()->setPlayerError(
                    new PlayerError(
                        $this->getPlayerHash(),
                        'Invalid hand card selection'
                    )
                );
                return false;
            }
            $player->getHand()->removeCards($cards);
            $player->getSoul()->addCards($cards);
        }

        $this->getGame()->getState()->addChatLog(
            $player->getName() .
            ' soul charges ' .
            intval($this->getOption()) .
            ChatLogHelper::plural(intval($this->getOption()), 'card') .
            ' from hand'
        );

        return true;
    }

    public function getActionType(): ?string
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());

        if ($player->getHand()->getCount() > intval($this->getOption())) {
            return Actions::SELECT_HAND;
        } else {
            return null;
        }
    }

    /**
     * Return optional parameter for some selects
     *
     * @return mixed
     */
    public function getActionOption()
    {
        return intval($this->getOption());
    }
}
