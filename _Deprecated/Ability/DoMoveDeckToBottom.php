<?php
declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\ChoiceInterface;
use App\Ability\AbilityPart;
use App\Ability\ActionInterface;
use App\Entity\Game\Deck;
use App\Entity\PlayerInput;

class DoMoveDeckToBottom extends AbilityPart implements ActionInterface
{
    public function processAction(PlayerInput $input): bool
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
        $removedCards = $player->getDeck()->drawCards(intval($this->getOption()));
        $player->getDeck()->addCards($removedCards, Deck::BOTTOM);

        return true;
    }
}
