<?php
declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\AbilityPart;
use App\Ability\ActionInterface;
use App\Entity\Game\Player;
use App\Entity\PlayerInput;

class DoSoulChargeSelf extends AbilityPart implements ActionInterface
{
    public function processAction(PlayerInput $input): bool
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
        $circleId = $player->findCircle($this->getSelfCardId());

        if (
            $circleId !== null
            && $circleId !== Player::VANGUARD
        ) {
            $card = $player->getCircle($circleId);
            $card->reset();
            $player->getSoul()->addCard($card);
            $player->setCircle($circleId, null);
            return true;
        }
        return false;
    }
}
