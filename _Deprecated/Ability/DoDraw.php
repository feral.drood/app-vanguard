<?php
declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\AbilityPart;
use App\Ability\ActionInterface;
use App\Entity\PlayerInput;
use App\Service\ChatLogHelper;

class DoDraw extends AbilityPart implements ActionInterface
{
    public function processAction(PlayerInput $input): bool
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
        if (intval($this->getOption()) < 1) {
            $count = 1;
        } else {
            $count = intval($this->getOption());
        }
        $player->getHand()->addCards($player->getDeck()->drawCards($count));
        $this->getGame()->getState()->addChatLog(
            $player->getName() .
            ' draws ' .
            $count .
            ' ' .
            ChatLogHelper::plural($count, 'card')
        );

        return true;
    }
}
