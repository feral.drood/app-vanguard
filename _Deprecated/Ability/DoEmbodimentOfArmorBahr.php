<?php
declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\SelectInterface;
use App\Entity\Actions;
use App\Ability\AbilityPart;
use App\Ability\ActionInterface;
use App\Entity\Game\Card;
use App\Entity\Game\Player;
use App\Entity\PlayerInput;

/**
 *  choose one of your opponent's grade 2 or less rear-guards in the same column as this unit, retire it,
 * and if the number of your rear-guards is greater than your opponent's, this unit gets [Power]+5000 until end of turn.
 */
class DoEmbodimentOfArmorBahr extends AbilityPart implements ActionInterface, SelectInterface
{
    public function processAction(PlayerInput $input): bool
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
        $opponent = $this->getGame()->getState()->getOpposingPlayer($this->getPlayerHash());
        $circleId = $player->findCircle($this->getSelfCardId());

        $data = $input->getValue();
        if (
            $data === null
            || $circleId === null
            || $data === Player::VANGUARD
            || !$opponent->circleExists($data)
            || $opponent->getCircle($data) === null
            || $opponent->getCircle($data)->getGrade() > 2
        ) {
            return false;
        }

        if (
            (
                in_array($circleId, [Player::REARGUARD1, Player::REARGUARD2], true)
                && in_array($data, [Player::REARGUARD5, Player::REARGUARD4], true)
            ) || (
                in_array($circleId, [Player::REARGUARD4, Player::REARGUARD5], true)
                && in_array($data, [Player::REARGUARD2, Player::REARGUARD1], true)
            ) || (
                $circleId === Player::REARGUARD3
                && $data === Player::REARGUARD3
            )
        ) {
            $opponent->getCircle($data)->reset();
            $opponent->getDrop()->addCard($opponent->getCircle($data));
            $opponent->setCircle($data, null);

            $playerRearguardCount = $opponentRearguardCount = 0;
            foreach ($player->getField() as $location => $unit) {
                if ($location !== Player::VANGUARD && $unit !== null) {
                    $playerRearguardCount++;
                }
            }
            foreach ($opponent->getField() as $location => $unit) {
                if ($location !== Player::VANGUARD && $unit !== null) {
                    $opponentRearguardCount++;
                }
            }

            if ($playerRearguardCount > $opponentRearguardCount) {
                $player->getCircle($circleId)->addBonus(Card::BONUS_LENGTH_TURN, Card::BONUS_POWER, 5);
            }
            return true;
        }
        return false;
    }

    public function getActionType(): ?string
    {
        return Actions::SELECT_OPPONENT_REARGUARD;
    }

    public function getActionOption()
    {
        return 1;
    }
}
