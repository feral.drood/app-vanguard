<?php
declare(strict_types=1);

namespace App\Ability\Action;

use App\Entity\Game\Card;
use App\Ability\AbilityPart;
use App\Ability\ActionInterface;
use App\Entity\PlayerInput;
use App\Service\ChatLogHelper;

class DoMoveSelfFromHandToBuffer extends AbilityPart implements ActionInterface
{
    public function processAction(PlayerInput $input): bool
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
        $card = $player->getHand()->getCardById($this->getSelfCardId());

        if ($card !== null) {
            $player->getHand()->removeCards([$card]);
            $player->setBuffer([$card]);
        }

        return true;
    }
}
