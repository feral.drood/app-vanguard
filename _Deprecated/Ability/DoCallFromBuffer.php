<?php
declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\Event\RetireRearguardEvent;
use App\Entity\Actions;
use App\Ability\AbilityPart;
use App\Ability\ActionInterface;
use App\Entity\Game\CardEvent;
use App\Entity\Game\Player;
use App\Entity\PlayerInput;
use App\Service\Game\CardEventProcessor;

/**
 * Call active card to rearguard
 */
class DoCallFromBuffer extends AbilityPart implements ActionInterface
{
    use RetireRearguardEvent;

    public function processAction(PlayerInput $input): bool
    {
        $gameState = $this->getGame()->getState();
        $player = $gameState->getPlayer($this->getPlayerHash());

        $circle = $input->getValue();
        if ($circle === null || !$player->circleExists($circle) || $circle === Player::VANGUARD) {
            return false;
        }

        $card = $player->getBuffer()[0];
        $cardLocation = $player->findCardLocation($card->getId());

        // retire old rearguard
        if ($player->getCircle($circle) !== null) {
            $this->doRetireRearguard($circle);
        }
        $player->setCircle($circle, $card);

        $gameState->addChatLog(
            $player->getName() . ' calls ' . $card->getName()
        );

        CardEventProcessor::processCardEvent(
            (new CardEvent())
            ->setGame($this->getGame())
            ->setType($cardLocation === 'hand' ? CardEvent::TYPE_ON_PLACE_FROM_HAND : CardEvent::TYPE_ON_PLACE)
            ->setPlayerHash($this->getPlayerHash())
            ->setSourceCard($card)
        );

        return true;
    }

    public function getSelectOption(): array
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
        $bufferCard = $player->getBuffer()[0];

        return [
            'label' => 'Select circle to call ' . $bufferCard->getName(),
            'type' => Actions::SELECT_REARGUARD_CIRCLE,
            'count' => 1,
        ];
    }

    public function getValidChoices(): array
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
        $field = $player->getField();
        unset($field[Player::VANGUARD]);

        return array_keys($field);
    }
}
