<?php
declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\AbilityPart;
use App\Ability\ActionInterface;
use App\Entity\Actions;
use App\Entity\Game\Player;
use App\Ability\SelectInterface;
use App\Entity\Game\PlayerError;
use App\Entity\PlayerInput;

class DoMarkRearguard extends AbilityPart implements ActionInterface, SelectInterface
{
    public function processAction(PlayerInput $input): bool
    {
        $gameState = $this->getGame()->getState();
        $player = $gameState->getPlayer($this->getPlayerHash());
        $validChoices = $this->getValidChoices();
        $data = $input->getList();

        if (count($validChoices) > (int)$this->getOption(self::OPTION_MAX_COUNT, 1)) {
            $selectedChoices = [];

            foreach ($data as $location) {
                if (in_array($location, $validChoices, true)) {
                    $selectedChoices[] = $location;
                }
            }
        } else {
            $selectedChoices = $validChoices;
        }

        if (
            count($selectedChoices) < (int)$this->getOption(self::OPTION_MIN_COUNT, 0)
            || count($data) !== count($selectedChoices)
        ) {
            $this->getGame()->getState()->setPlayerError(
                new PlayerError(
                    $this->getPlayerHash(),
                    'Invalid unit selection'
                )
            );
            return false;
        }

        $player->setBuffer($selectedChoices);

        return true;
    }

    public function getActionType(): ?string
    {
        if (count($this->getValidChoices()) > (int)$this->getOption(self::OPTION_MAX_COUNT, 1)) {
            return Actions::SELECT_REARGUARD;
        } else {
            return null;
        }
    }

    /**
     * Return optional parameter for some selects
     *
     * @return mixed
     */
    public function getActionOption()
    {
        return (int)$this->getOption(self::OPTION_MAX_COUNT, 1);
    }

    private function getValidChoices(): array
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
        $race = $this->getOption(self::OPTION_RACE);
        $minGrade = (int)$this->getOption(self::OPTION_MIN_GRADE, 0);
        $maxGrade = (int)$this->getOption(self::OPTION_MAX_GRADE, 4);
        $name = $this->getOption(self::OPTION_NAME);
        $partialName = $this->getOption(self::OPTION_PARTIAL_NAME);
        $location = $this->getOption(self::OPTION_LOCATION);
        $validLocations = null;
        $invalidLocations = null;

        if ($location !== null) {
            if ($location === 'front') {
                $invalidLocations = [
                    Player::REARGUARD2,
                    Player::REARGUARD3,
                    Player::REARGUARD4
                ];
            } elseif ($location === 'back') {
                $validLocations = [
                    Player::REARGUARD2,
                    Player::REARGUARD3,
                    Player::REARGUARD4
                ];
            } elseif ($location === 'column') {
                $currentLocation = $player->findCircle($this->getSelfCardId());
                $pairs = [
                    Player::REARGUARD1 => Player::REARGUARD2,
                    Player::REARGUARD2 => Player::REARGUARD1,
                    Player::VANGUARD => Player::REARGUARD3,
                    Player::REARGUARD3 => Player::VANGUARD,
                    Player::REARGUARD4 => Player::REARGUARD5,
                    Player::REARGUARD5 => Player::REARGUARD4,
                ];
                if (array_key_exists($currentLocation, $pairs)) {
                    $validLocations = [$pairs[$currentLocation]];
                }
            }
        }

        $validChoices = [];
        foreach ($player->getField() as $location => $unit) {
            if (
                $unit === null
                || $location === Player::VANGUARD
                || (
                    $name !== null
                    && $name !== $unit->getName()
                )
                || (
                    $partialName !== null
                    && stripos($unit->getName(), $partialName) === false
                )
                || $unit->getGrade() < $minGrade
                || $unit->getGrade() > $maxGrade
                || (
                    $validLocations !== null
                    && !in_array($location, $validLocations, true)
                )
                || (
                    $invalidLocations !== null
                    && in_array($location, $invalidLocations, true)
                )
                || (
                    $race !== null
                    && !$unit->isRace($race)
                )
            ) {
                continue;
            }
            $validChoices[] = $location;
        }

        return $validChoices;
    }
}
