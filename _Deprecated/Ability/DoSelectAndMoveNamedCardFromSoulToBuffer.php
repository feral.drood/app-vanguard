<?php
declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\SelectInterface;
use App\Entity\Actions;
use App\Ability\AbilityPart;
use App\Ability\ActionInterface;
use App\Entity\Game\Card;
use App\Entity\Game\Player;
use App\Entity\Game\PlayerError;
use App\Entity\PlayerInput;
use App\Service\AbilityProcessor;

class DoSelectAndMoveNamedCardFromSoulToBuffer extends AbilityPart implements ActionInterface, SelectInterface
{
    public function processAction(PlayerInput $input): bool
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
        $data = $input->getValue();

        $selectedCard = null;
        if ($this->isMultipleChoices()) {
            if (
                $data === null
                || $player->getSoul()->getCardById($data) === null
                || $player->getSoul()->getCardById($data)->getName() !== $this->getOption()
            ) {
                $this->getGame()->getState()->setPlayerError(
                    new PlayerError(
                        $this->getPlayerHash(),
                        'Select ' . $this->getOption() . ' named card from soul'
                    )
                );
                return false;
            } else {
                $selectedCard = $player->getSoul()->getCardById($data);
            }
        } else {
            $cards = $player->getSoul()->getCards();
            foreach ($cards as $card) {
                if (strcasecmp($card->getName(), $this->getOption()) === 0) {
                    $selectedCard = $card;
                    break;
                }
            }
        }

        if ($selectedCard === null) {
            $this->getGame()->getState()->addChatLog('DoSelectAndMoveNamedCardFromSoulToBuffer.Error: missing card');
            return true;
        }

        $player->getSoul()->removeCards([$selectedCard->getId()]);
        $player->setBuffer([$selectedCard]);

        return true;
    }

    public function getActionType(): ?string
    {
        if (!$this->isMultipleChoices()) {
            return null;
        } else {
            return Actions::SELECT_SOUL;
        }
    }

    public function getActionOption()
    {
        return 1;
    }

    public function isMultipleChoices(): bool
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
        $count = 0;

        $cards = $player->getSoul()->getCards();
        foreach ($cards as $card) {
            if (strcasecmp($card->getName(), $this->getOption()) === 0) {
                $count++;
            }
        }

        return $count > 1;
    }
}
