<?php
declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\Event\RetireOpponentRearguardEvent;
use App\Ability\Event\RetireRearguardEvent;
use App\Entity\Actions;
use App\Entity\Game\Player;
use App\Ability\AbilityPart;
use App\Ability\ActionInterface;
use App\Ability\SelectInterface;
use App\Entity\PlayerInput;

class DoSelfRetire extends AbilityPart implements ActionInterface
{
    use RetireRearguardEvent;

    public function processAction(PlayerInput $input): bool
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
        $location = $player->findCircle($this->getSelfCardId());

        if ($location !== null) {
            $this->doRetireRearguard($location);
        }

        return true;
    }
}
