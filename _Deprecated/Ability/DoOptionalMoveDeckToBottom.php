<?php
declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\ChoiceInterface;
use App\Ability\AbilityPart;
use App\Ability\ActionInterface;
use App\Entity\Game\Deck;
use App\Entity\PlayerInput;

class DoOptionalMoveDeckToBottom extends AbilityPart implements ActionInterface, ChoiceInterface
{
    public function processAction(PlayerInput $input): bool
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());

        if ($input->getValue() === 'bottom') {
            $removedCards = $player->getDeck()->drawCards(intval($this->getOption()));
            $player->getDeck()->addCards($removedCards, Deck::BOTTOM);
        }

        return true;
    }

    public function getChoices(): array
    {
        return [
            'top' => 'Leave on top',
            'bottom' => 'Move to bottom',
        ];
    }
}
