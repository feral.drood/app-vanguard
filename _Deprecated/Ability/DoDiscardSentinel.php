<?php
declare(strict_types=1);

namespace App\Ability\Action;

use App\Entity\Actions;
use App\Ability\AbilityPart;
use App\Ability\ActionInterface;
use App\Ability\SelectInterface;
use App\Entity\Game\Card;
use App\Entity\Game\PlayerError;
use App\Entity\PlayerInput;
use App\Service\ChatLogHelper;

class DoDiscardSentinel extends AbilityPart implements SelectInterface, ActionInterface
{
    public function processAction(PlayerInput $input): bool
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
        $data = $input->getList();

        $validChoices = $this->getValidChoices();
        if (count($validChoices) > intval($this->getOption())) {
            $validChoices = [];
            foreach ($data as $cardId) {
                if ($cardId === null) {
                    continue;
                }

                $card = $player->getHand()->getCardById($cardId);

                if (
                    $card !== null
                    && $card->getShield() === 0
                ) {
                    $validChoices[] = $card;
                }
            }

            if (count($validChoices) !== intval($this->getOption())) {
                $this->getGame()->getState()->setPlayerError(
                    new PlayerError(
                        $this->getPlayerHash(),
                        'Invalid sentinel selection'
                    )
                );
                return false;
            }
        }

        $player->getHand()->removeCards($validChoices);
        $player->getDrop()->addCards($validChoices);

        $this->getGame()->getState()->addChatLog(
            $player->getName() . ' discards ' . intval($this->getOption()) . ' ' . ChatLogHelper::plural(intval($this->getOption()), 'card')
        );

        return true;
    }

    public function getActionType(): ?string
    {
        if (count($this->getValidChoices()) > intval($this->getOption())) {
            return Actions::SELECT_HAND_DISCARD;
        } else {
            return null;
        }
    }

    public function getActionOption()
    {
        return intval($this->getOption());
    }

    public function getValidChoices(): array
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
        $cards = $player->getHand()->getCards();
        $validChoices = [];

        foreach ($cards as $card) {
            if ($card->getShield() === 0) {
                $validChoices[] = $card;
            }
        }

        return $validChoices;

    }
}
