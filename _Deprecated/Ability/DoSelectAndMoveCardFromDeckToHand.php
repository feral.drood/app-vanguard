<?php
declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\AbilityPart;
use App\Ability\ActionInterface;
use App\Ability\CardsSelectInterface;
use App\Ability\SelectInterface;
use App\Entity\Actions;
use App\Entity\Game\CardEvent;
use App\Entity\PlayerInput;
use App\Service\AbilityProcessor;
use App\Service\Game\CardEventProcessor;

class DoSelectAndMoveCardFromDeckToHand extends AbilityPart implements ActionInterface, SelectInterface, CardsSelectInterface
{
    public function processAction(PlayerInput $input): bool
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
        $data = $input->getValue();

        $topCards = $player->getDeck()->viewCards(intval($this->getOption()));
        $eventCard = $player->findCard($this->getSelfCardId());
        foreach ($topCards as $card) {
            if ($card->getId() === $data) {
                $player->getDeck()->removeCards([$data]);
                $player->getHand()->addCard($card);
                return true;
            }
        }

        CardEventProcessor::processCardEvent(
            (new CardEvent())
                ->setGame($this->getGame())
                ->setType(CardEvent::TYPE_ON_SEARCH)
                ->setPlayerHash($this->getPlayerHash())
                ->setSourceCard($eventCard)
        );
        return false;
    }

    public function getActionType(): ?string
    {
        return Actions::SELECT_DECK;
    }

    public function getActionOption()
    {
        return 1;
    }

    public function getCardCount(): int
    {
        return intval($this->getOption());
    }
}
