<?php
declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\AbilityConstants;
use App\Ability\AbilityPart;
use App\Ability\ActionInterface;
use App\Ability\CardsSelectInterface;
use App\Ability\SelectInterface;
use App\Entity\Actions;
use App\Entity\Game\PlayerError;
use App\Entity\PlayerInput;

class DoAssistDraw extends AbilityPart implements ActionInterface, CardsSelectInterface
{
    public function processAction(PlayerInput $input): bool
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
        $cardId = $input->getValue();
        $validChoices = $this->getValidChoices();

        if ($cardId === null || count($validChoices) === 0) {
            return true;
        }

        if (!in_array($cardId, $validChoices, true)) {
            $this->getGame()->getState()->setPlayerError(
                new PlayerError(
                    $this->getPlayerHash(),
                    'Invalid selection'
                )
            );

            return false;
        }

        $this->getGame()->getState()->getAbilityStack()->addEffect([
            AbilityConstants::ID => 'assist_discard',
            AbilityConstants::TYPE => 'do_discard',
            AbilityConstants::NAME => 'assist_discard',
            AbilityConstants::PLAYER_HASH => $this->getPlayerHash(),
            AbilityConstants::OPTION => 2
        ]);

        $card = $player->getDeck()->getCardById($cardId);
        $player->getDeck()->removeCards([$cardId]);
        $player->getHand()->addCard($card);

        return true;
    }

    public function getSelectOption(): array
    {
        return [
            'type' => Actions::SELECT_DECK,
            'count' => 1,
        ];
    }

    public function getCardCount(): int
    {
        return 5;
    }

    public function getValidChoices(): array
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
        $topCards = $player->getDeck()->viewCards($this->getCardCount());
        $choices = [];
        $rideGrade = $player->getVanguard()->getGrade() + 1;

        foreach ($topCards as $card) {
            if ($card->getGrade() === $rideGrade) {
                $choices[] = $card->getId();
            }
        }

        return $choices;
    }
}
