<?php
declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\AbilityPart;
use App\Ability\ActionInterface;
use App\Entity\PlayerInput;

class DoNothing extends AbilityPart implements ActionInterface
{
    public function processAction(PlayerInput $input): bool
    {
        return true;
    }
}
