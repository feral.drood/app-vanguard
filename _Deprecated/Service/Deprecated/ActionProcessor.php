<?php

namespace App\Service;

use App\Entity\Game\Card;
use App\Entity\Game\CardEvent;
use App\Entity\Game\Deck;
use App\Entity\Game\Player;
use App\Entity\Game;
use App\Entity\Game\State;
use App\Entity\Game\PlayerError;
use App\Entity\Game\TurnEvent;
use App\Service\Game\ActivatedAbilityProcessor;
use App\Service\Game\CardEventProcessor;
use App\Service\Game\GiftProcessor;
use App\Service\Game\PerfectGuardProcessor;

class ActionProcessor
{
    const BOOST_PAIRS = [
        Player::VANGUARD => Player::REARGUARD3,
        Player::REARGUARD1 => Player::REARGUARD2,
        Player::REARGUARD5 => Player::REARGUARD4
    ];

    private $pusher;
    private $abilityProcessor;

    public function __construct(
        PusherFactory $pusher,
        AbilityProcessor $abilityProcessor
    ) {
        $this->pusher = $pusher;
        $this->abilityProcessor = $abilityProcessor;
    }

    public function processAction(Game $game, string $userHash, $data): bool
    {
        $gameState = $game->getState();
        $trigger = false;

        switch ($gameState->getPhase()) {
            case State::PHASE_VANGUARD:
                $player = $gameState->getPlayer($userHash);
                $vanguardId = is_array($data) ? array_shift($data) : $data;
                if (
                    $vanguardId !== null
                    && $player->getDeck()->cardExists($vanguardId)
                    && $player->getDeck()->getCardById($vanguardId)->getGrade() === 0
                ) {
                    $card = $player->getDeck()->getCardById($vanguardId);
                    $card->setFaceDown(true);
                    $player->getDeck()->removeCards([$vanguardId]);
                    $player->setCircle(Player::VANGUARD, $card);
                    $trigger = true;
                    $gameState->addChatLog($player->getName() . ' selects a vanguard unit');
                }
                break;
            case State::PHASE_MULLIGAN1:
                if ($gameState->getActivePlayerHash() === $userHash) {
                    $player = $gameState->getPlayer($userHash);
                    $removedCards = $player->getHand()->removeCards($data);
                    $player->getDeck()->addCards($removedCards, Deck::BOTTOM);
                    $player->getHand()->addCards($player->getDeck()->drawCards(count($removedCards)));
                    $player->getDeck()->shuffle();
                    $gameState->setPhase(State::PHASE_MULLIGAN2);
                    if (count($removedCards) > 0) {
                        $gameState->addChatLog($player->getName() . ' chooses to replace ' . count($removedCards) . ' cards');
                    } else {
                        $gameState->addChatLog($player->getName() . ' chooses to keep all cards');
                    }
                    $gameState->addChatLog($gameState->getInactivePlayer()->getName() . ' is choosing starting hand');
                    $trigger = true;
                }
                break;
            case State::PHASE_MULLIGAN2:
                if ($gameState->getInactivePlayerHash() === $userHash) {
                    $player1 = $gameState->getActivePlayer();
                    $player2 = $gameState->getInactivePlayer();
                    $removedCards = $player2->getHand()->removeCards($data);
                    $player2->getDeck()->addCards($removedCards, Deck::BOTTOM);
                    $player2->getHand()->addCards($player2->getDeck()->drawCards(count($removedCards)));
                    $player2->getDeck()->shuffle();
                    $gameState->setPhase(State::PHASE_STAND);

                    $player1->getCircle(Player::VANGUARD)->setFaceDown(false);
                    $player2->getCircle(Player::VANGUARD)->setFaceDown(false);
                    $gameState->setCurrentTurn(1);
                    if (count($removedCards) > 0) {
                        $gameState->addChatLog($player2->getName() . ' chooses to replace ' . count($removedCards) . ' cards');
                    } else {
                        $gameState->addChatLog($player2->getName() . ' chooses to keep all cards');
                    }
                    $trigger = true;
                }
                break;
            case State::PHASE_ASSIST:
                if ($gameState->getActivePlayerHash() === $userHash) {
                    $data = is_array($data) ? array_shift($data) : $data;
                    if ($data === 'assist') {
                        $gameState->addChatLog($gameState->getActivePlayer()->getName() . ' chooses to use G Assist');
                        $gameState->getAbilityStack()->addEffect([
                            AbilityProcessor::ID => 'assist_draw',
                            AbilityProcessor::TYPE => 'do_assist_draw',
                            AbilityProcessor::NAME => 'assist_draw',
                            AbilityProcessor::PLAYER_HASH => $gameState->getActivePlayerHash(),
                            AbilityProcessor::OPTION => 1
                        ]);
                        $gameState->getAbilityStack()->addEffect([
                            AbilityProcessor::ID => 'assist_shuffle',
                            AbilityProcessor::TYPE => 'do_deck_shuffle',
                            AbilityProcessor::NAME => 'assist_shuffle',
                            AbilityProcessor::PLAYER_HASH => $gameState->getActivePlayerHash(),
                        ]);
                    }
                    $gameState->setPhase(State::PHASE_RIDE);
                }
                break;
            case State::PHASE_RIDE:
                if ($gameState->getActivePlayerHash() === $userHash) {
                    $player = $gameState->getActivePlayer();
                    $cardId = is_array($data) ? array_shift($data) : $data;
                    if ($cardId !== null && $player->getHand()->cardExists($cardId)) {
                        $card = $player->getHand()->getCardById($cardId);
                        if (
                            $card->getGrade() === $player->getVanguard()->getGrade()
                            || $card->getGrade() === ($player->getVanguard()->getGrade() + 1)
                        ) {
                            $oldVanguard = $player->getVanguard();
                            $gameState->addChatLog($player->getName() . ' rides ' . $card->getName());
                            CardEventProcessor::processCardEvent(
                                (new CardEvent())
                                ->setGame($game)
                                ->setType(CardEvent::TYPE_ON_RIDDEN)
                                ->setPlayerHash($player->getHash())
                                ->setTargetCards([
                                    $oldVanguard
                                ])
                                ->setSourceCard($card)
                            );
                            $player->getHand()->removeCards([$cardId]);
                            $player->getSoul()->addCard($oldVanguard);
                            $player->setVanguard($card);
                            $gameState->getEventLog()->add(
                                (new TurnEvent())
                                    ->setType(TurnEvent::EVENT_RIDE)
                                    ->setPlayerHash($userHash)
                                    ->setEventCardId($card->getId())
                                    ->setTargetCardIds([$oldVanguard->getId()])
                            );
                            CardEventProcessor::processCardEvent(
                                (new CardEvent())
                                ->setGame($game)
                                ->setType(CardEvent::TYPE_ON_PLACE)
                                ->setPlayerHash($userHash)
                                ->setSourceCard($card)
                                ->setTargetCards([
                                    $card
                                ])
                            );
                            if ($card->getGift() !== null) {
                                GiftProcessor::processGift($game, $userHash, $card);
                            }
                            $trigger = true;
                        }
                    }
                    $gameState->setPhase(State::PHASE_MAIN);
                }
                break;
            case State::PHASE_MAIN:
                if ($gameState->getActivePlayerHash() === $userHash) {
                    $player = $gameState->getActivePlayer();
                    $cards = is_array($data) ? $data : [];
                    if (
                        count($cards) === 1
                        && $player->circleExists($cards[0])
                        && $player->getCircle($cards[0]) !== null
                    ) {
                        ActivatedAbilityProcessor::process($game, $userHash, $player->getCircle($cards[0]));
                    } elseif (count($cards) === 2) {
                        $circleId = null;
                        $callId = null;
                        if (
                            $cards[0] !== null
                            && $cards[1] !== null
                            && $player->circleExists($cards[0])
                            && $player->circleExists($cards[1])
                        ) {
                            if (
                                (
                                    in_array($cards[0], [Player::REARGUARD1, Player::REARGUARD2], true)
                                    && in_array($cards[1], [Player::REARGUARD1, Player::REARGUARD2], true)
                                ) || (
                                    in_array($cards[0], [Player::REARGUARD4, Player::REARGUARD5], true)
                                    && in_array($cards[1], [Player::REARGUARD4, Player::REARGUARD5], true)
                                )
                            ) {
                                $temp = $player->getCircle($cards[0]);
                                $player->setCircle($cards[0], $player->getCircle($cards[1]));
                                $player->setCircle($cards[1], $temp);
                                $gameState->addChatLog($player->getName() . ' repositions units within column');
                            }
                        } elseif ($player->circleExists($cards[0])) {
                            $circleId = $cards[0];
                            $callId = $cards[1];
                        } elseif ($player->circleExists($cards[1])) {
                            $circleId = $cards[1];
                            $callId = $cards[0];
                        }

                        if (
                            $circleId !== null
                            && $callId !== null
                            && $player->getHand()->cardExists($callId)
                            && $player->getHand()->getCardById($callId)->getGrade() <= $player->getVanguard()->getGrade()
                        ) {
                            if ($player->getCircle($circleId) !== null) {
                                $player->getDrop()->addCard($player->getCircle($circleId));
                            }
                            $card = $player->getHand()->getCardById($callId);
                            $player->setCircle($circleId, $card);
                            $player->getHand()->removeCards([$callId]);
                            $gameState->addChatLog($player->getName() . ' calls ' . $card->getName());
                            CardEventProcessor::processCardEvent(
                                (new CardEvent())
                                ->setGame($game)
                                ->setType(CardEvent::TYPE_ON_PLACE)
                                ->setPlayerHash($userHash)
                                ->setSourceCard($card)
                                ->setTargetCards([
                                    $card
                                ])
                            );
                        }
                    } else {
                        $gameState->setPhase(State::PHASE_BATTLE);
                    }
                }
                break;
            case State::PHASE_BATTLE:
                if ($gameState->getActivePlayerHash() === $userHash) {
                    $player = $gameState->getActivePlayer();
                    $circles = is_array($data) ? $data : [];
                    if (count($circles) === 2) {
                        if (
                            $player->circleExists($circles[0])
                            && $player->circleExists($circles[1])
                            && $player->getCircle($circles[0]) !== null
                            && $player->getCircle($circles[1]) !== null
                            && $player->getCircle($circles[0])->getState() === Card::STATE_STAND
                            && $player->getCircle($circles[1])->getState() === Card::STATE_STAND
                        ) {
                            if (
                                array_key_exists($circles[0], self::BOOST_PAIRS)
                                && self::BOOST_PAIRS[$circles[0]] === $circles[1]
                                && $player->getCircle($circles[1])->getGrade() <= 1
                            ) {
                                if ($player->getCircle($circles[0])->hasProperty(Card::PROPERTY_CANNOT_ATTACK)) {
                                    $gameState->setPlayerError(
                                        new PlayerError(
                                            $userHash,
                                            $player->getCircle($circles[0])->getName() . ' cannot attack'
                                        )
                                    );
                                } else {
                                    $gameState->setAttacker($circles[0]);
                                    $gameState->setBooster($circles[1]);
                                    $player->getCircle($circles[0])->setAttacker(true);
                                    $player->getCircle($circles[1])->setBooster(true);
                                    $gameState->addChatLog(
                                        $player->getName() .
                                        ' chooses ' .
                                        $player->getCircle($circles[0])->getName() .
                                        ' as attacker and ' .
                                        $player->getCircle($circles[1])->getName() .
                                        ' as booster'
                                    );
                                    $gameState->setPhase(State::PHASE_BATTLE_SELECT_DEFENDER);
                                }
                            } elseif (
                                array_key_exists($circles[1], self::BOOST_PAIRS)
                                && self::BOOST_PAIRS[$circles[1]] === $circles[0]
                                && $player->getCircle($circles[0])->getGrade() <= 1
                            ) {
                                if ($player->getCircle($circles[1])->hasProperty(Card::PROPERTY_CANNOT_ATTACK)) {
                                    $gameState->setPlayerError(
                                        new PlayerError(
                                            $userHash,
                                            $player->getCircle($circles[1])->getName() . ' cannot attack'
                                        )
                                    );
                                } else {
                                    $gameState->setAttacker($circles[1]);
                                    $gameState->setBooster($circles[0]);
                                    $player->getCircle($circles[1])->setAttacker(true);
                                    $player->getCircle($circles[0])->setBooster(true);
                                    $gameState->addChatLog(
                                        $player->getName() .
                                        ' chooses ' .
                                        $player->getCircle($circles[1])->getName() .
                                        ' as attacker and ' .
                                        $player->getCircle($circles[0])->getName() .
                                        ' as booster'
                                    );
                                    $gameState->setPhase(State::PHASE_BATTLE_SELECT_DEFENDER);
                                }
                            }
                        }
                    } elseif (count($circles) === 1) {
                        if (
                            $player->circleExists($circles[0])
                            && $player->isFrontRow($circles[0])
                            && $player->getCircle($circles[0]) !== null
                            && $player->getCircle($circles[0])->getState() === Card::STATE_STAND
                        ) {
                            if ($player->getCircle($circles[0])->hasProperty(Card::PROPERTY_CANNOT_ATTACK)) {
                                $gameState->setPlayerError(
                                    new PlayerError(
                                        $userHash,
                                        $player->getCircle($circles[0])->getName() . ' cannot attack'
                                    )
                                );
                            } else {
                                $gameState->setAttacker($circles[0]);
                                $player->getCircle($circles[0])->setAttacker(true);
                                $gameState->addChatLog(
                                    $player->getName() .
                                    ' chooses ' .
                                    $player->getCircle($circles[0])->getName() .
                                    ' as attacker'
                                );

                                $gameState->setPhase(State::PHASE_BATTLE_SELECT_DEFENDER);
                            }
                        }
                    } else {
                        $gameState->setPhase(State::PHASE_END);
                    }
                }
                break;
            case State::PHASE_BATTLE_SELECT_DEFENDER:
                if ($gameState->getActivePlayerHash() === $userHash) {
                    $player = $gameState->getActivePlayer();
                    $opponent = $gameState->getInactivePlayer();
                    $circleId = is_array($data) ? array_shift($data) : $data;
                    if (
                        $circleId !== null
                        && $opponent->circleExists($circleId)
                        && $opponent->getCircle($circleId) !== null
                        && $opponent->isFrontRow($circleId)
                    ) {
                        if ($opponent->getCircle($circleId)->hasProperty(Card::PROPERTY_CANNOT_BE_ATTACKED)) {
                            $gameState->setPlayerError(
                                new PlayerError(
                                    $userHash,
                                    $opponent->getCircle($circleId)->getName() . ' cannot be attacked'
                                )
                            );
                        } else {
                            $opponent->getCircle($circleId)->setDefender(true);
                            $gameState->setDefender($circleId);
                            $attacker = $player->getCircle($gameState->getAttacker());
                            $attacker->setState(Card::STATE_REST);
                            $gameState->addChatLog(
                                $player->getName() .
                                ' chooses ' .
                                $opponent->getCircle($circleId)->getName() .
                                ' as defender'
                            );

                            $gameState->getEventLog()->add(
                                (new TurnEvent())
                                    ->setType(TurnEvent::EVENT_ATTACK)
                                    ->setPlayerHash($userHash)
                                    ->setEventCardId($attacker->getId())
                                    ->setTargetCardIds([$opponent->getCircle($circleId)->getId()])
                            );

                            CardEventProcessor::processCardEvent(
                                (new CardEvent())
                                ->setGame($game)
                                ->setType(CardEvent::TYPE_ON_ATTACK)
                                ->setPlayerHash($userHash)
                                ->setSourceCard($attacker)
                                ->setTargetCards([
                                    $attacker
                                ])
                            );
                            if ($gameState->getBooster() !== null) {
                                $booster = $player->getCircle($gameState->getBooster());
                                $booster->setState(Card::STATE_REST);
                                CardEventProcessor::processCardEvent(
                                    (new CardEvent())
                                    ->setGame($game)
                                    ->setType(CardEvent::TYPE_ON_BOOST)
                                    ->setPlayerHash($userHash)
                                    ->setSourceCard($booster)
                                    ->setTargetCards([
                                        $booster
                                    ])
                                );
                            }
                            $gameState->setPhase(State::PHASE_BATTLE_SELECT_GUARDIAN);
                        }
                    } elseif ($circleId === null) {
                        $player->getCircle($gameState->getAttacker())->setAttacker(false);
                        $player->getCircle($gameState->getBooster())->setBooster(false);
                        $gameState->setAttacker(null);
                        $gameState->setBooster(null);
                        $gameState->setPhase(State::PHASE_BATTLE);
                    }
                }
                break;
            case State::PHASE_BATTLE_SELECT_GUARDIAN:
                if ($gameState->getInactivePlayerHash() === $userHash) {
                    $opponent = $gameState->getPlayer($userHash);
                    $data = is_array($data) ? $data : [$data];
                    $defenderCount = 0;
                    $handDefenderCount = 0;
                    $errorCount = 0;
                    foreach ($data as $selected) {
                        if (
                            $selected !== null
                            && $opponent->circleExists($selected)
                            && $opponent->isFrontRow($selected)
                            && $selected !== Player::VANGUARD
                            && $selected !== $gameState->getDefender()
                            && $opponent->getCircle($selected) !== null
                            && $opponent->getCircle($selected)->getSkill() === Card::SKILL_INTERCEPT
                            && $opponent->getCircle($selected)->getShield() !== null
                        ) {
                            $defenderCount++;
                        } elseif (
                            $selected !== null
                            && $opponent->getHand()->cardExists($selected)
                            && $opponent->getHand()->getCardById($selected)->getShield() !== null
                        ) {
                            if (
                                $opponent->getHand()->getCardById($selected)->getShield() === 0
                                && $opponent->hasProperty(Player::PROPERTY_CANNOT_CALL_SENTINEL_FROM_HAND_TO_GC)
                            ) {
                                $errorCount++;
                                $gameState->setPlayerError(
                                    new PlayerError(
                                        $userHash,
                                        'Cannot call sentinels from hand to GC'
                                    )
                                );
                                break;
                            } else {
                                $handDefenderCount++;
                                $defenderCount++;
                            }
                        } else {
                            $errorCount++;
                            break;
                        }
                    }

                    if (
                        $opponent->hasProperty(Player::PROPERTY_MUST_CALL_TWO_GUARDIANS_FROM_HAND_TO_GC)
                        && $handDefenderCount === 1
                    ) {
                        $gameState->setPlayerError(
                            new PlayerError(
                                $userHash,
                                'Must call at least two guardians from hand to GC '
                            )
                        );
                        $errorCount++;
                    }

                    if ($errorCount === 0) {
                        foreach ($data as $selected) {
                            if (
                                $selected !== null
                                && $opponent->circleExists($selected)
                                && $opponent->isFrontRow($selected)
                                && $selected !== Player::VANGUARD
                                && $selected !== $gameState->getDefender()
                                && $opponent->getCircle($selected) !== null
                                && $opponent->getCircle($selected)->getSkill() === Card::SKILL_INTERCEPT
                                && $opponent->getCircle($selected)->getShield() !== null
                            ) {
                                $guardian = $opponent->getCircle($selected);
                                $gameState->getGuardians()->addCard($guardian);
                                $opponent->setCircle($selected, null);
                                PerfectGuardProcessor::processPerfectGuard($game, $userHash, $guardian);
                            } elseif (
                                $selected !== null
                                && $opponent->getHand()->cardExists($selected)
                                && $opponent->getHand()->getCardById($selected)->getShield() !== null
                            ) {
                                $guardian = $opponent->getHand()->getCardById($selected);
                                $opponent->getHand()->removeCards([$selected]);
                                $gameState->getGuardians()->addCard($guardian);
                                PerfectGuardProcessor::processPerfectGuard($game, $userHash, $guardian);
                            }
                        }
                        if (count($data) > 0) {
                            $gameState->addChatLog(
                                $opponent->getName() . ' calls ' . count($data) . ' ' . ChatLogHelper::plural(count($data), 'guardian')
                            );
                        }
                        $gameState->setPhase(State::PHASE_BATTLE_COMBAT);
                    }
                }
                break;
        }

        return $trigger;
    }
}
