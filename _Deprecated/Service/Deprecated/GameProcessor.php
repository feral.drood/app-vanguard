<?php

namespace App\Service;

use App\Entity\Game;
use App\Entity\Game\State;
use App\Service\Game\ContinuousAbilityProcessor;

class GameProcessor
{
    private $abilityProcessor;
    private $actionProcessor;
    private $phaseProcessor;

    public function __construct(
        AbilityProcessor $abilityProcessor,
        ActionProcessor $actionProcessor,
        PhaseProcessor $phaseProcessor
    ) {
        $this->abilityProcessor = $abilityProcessor;
        $this->actionProcessor = $actionProcessor;
        $this->phaseProcessor = $phaseProcessor;
    }

    public function sendVanguardSelection(Game $game)
    {
        $gameState = $game->getState();
        $gameState->setPhase(State::PHASE_VANGUARD);
        $this->progressGame($game);
    }

    public function startGame(Game $game)
    {
        $game->getState()->setPhase(State::PHASE_VANGUARD);
        $this->progressGame($game);
    }

    public function processAction(Game $game, $playerHash, $data)
    {
        $gameState = $game->getState();
        if (!$gameState->getAbilityStack()->isEmpty()) {
            $this->abilityProcessor->processAction($game, $playerHash, $data);
        } elseif ($data !== null) {
            $this->actionProcessor->processAction($game, $playerHash, $data);
        }
        if ($gameState->getAbilityStack()->isActionListEmpty()) {
            if ($gameState->getActivePlayer() !== null && $gameState->getActivePlayer()->getTrigger() !== null) {
                $gameState->getActivePlayer()->getHand()->addCard($gameState->getActivePlayer()->getTrigger());
                $gameState->getActivePlayer()->setTrigger(null);
            }
            if ($gameState->getInactivePlayer() !== null && $gameState->getInactivePlayer()->getTrigger() !== null) {
                $gameState->getInactivePlayer()->getDamage()->addCard($gameState->getInactivePlayer()->getTrigger());
                $gameState->getInactivePlayer()->setTrigger(null);
            }
        }
        ContinuousAbilityProcessor::recalculate($game);
        $this->progressGame($game);
    }

    public function progressGame(Game $game)
    {
        $gameState = $game->getState();
        $trigger = false;
        while ($trigger === false) {
            if (!$gameState->getAbilityStack()->isEmpty()) {
                $trigger = $this->abilityProcessor->progressGame($game);
                if ($gameState->getAbilityStack()->isActionListEmpty()) {
                    if ($gameState->getActivePlayer()->getTrigger() !== null) {
                        $gameState->getActivePlayer()->getHand()->addCard($gameState->getActivePlayer()->getTrigger());
                        $gameState->getActivePlayer()->setTrigger(null);
                    }
                    if ($gameState->getInactivePlayer()->getTrigger() !== null) {
                        $gameState->getInactivePlayer()->getDamage()->addCard($gameState->getInactivePlayer()->getTrigger());
                        $gameState->getInactivePlayer()->setTrigger(null);
                    }
                }
            } else {
                $this->checkWinCondition($game);
                $trigger = $this->phaseProcessor->progressGame($game);;
            }
            ContinuousAbilityProcessor::recalculate($game);
        }
    }

    public function checkWinCondition(Game $game): void
    {
        $gameState = $game->getState();
        if ($gameState->getPhase() === State::PHASE_WIN) {
            return;
        }
        $player1 = $game->getUser1Hash() !== null ? $gameState->getPlayer($game->getUser1Hash()) : null;
        $player2 = $game->getUser2Hash() !== null ? $gameState->getPlayer($game->getUser2Hash()) : null;

        if (
            $player1 !== null
            && (
                $player1->getDeck()->getCount() === 0
                || $player1->getDamage()->getCount() >= 6
            )
        ) {
            $gameState
                ->setPhase(State::PHASE_WIN)
                ->setActivePlayerHash($player2->getHash())
            ;
//            $game->setStatus(Game::GAME_STATUS_DONE);
        }
        if (
            $player2
            && (
                $player2->getDeck()->getCount() === 0
                || $player2->getDamage()->getCount() >= 6
            )
        ) {
            $gameState
                ->setPhase(State::PHASE_WIN)
                ->setActivePlayerHash($player1->getHash())
            ;
//            $game->setStatus(Game::GAME_STATUS_DONE);
        }
    }
}