<?php
declare(strict_types=1);

namespace App\Controller;

use App\Repository\GameRepository;
use App\Service\Authorization;
use Symfony\Component\HttpFoundation\JsonResponse;

class EquipGaugeController
{
    private $gameRepository;
    private $authorization;

    public function __construct(
        GameRepository $gameRepository,
        Authorization $authorization
    ) {
        $this->gameRepository = $gameRepository;
        $this->authorization = $authorization;
    }

    public function getCards($location)
    {
        $userHash = $this->authorization->getUserHash();
        list($userId, $gameId) = $this->authorization->decodeHash($userHash);
        $userId = strval($userId);

        $game = $this->gameRepository->getOneById($gameId);
        if ($game === null) {
            return new JsonResponse([], JsonResponse::HTTP_FORBIDDEN);
        }

        if ($game->getUser1Id() !== $userId && $game->getUser2Id() !== $userId) {
            return new JsonResponse([], JsonResponse::HTTP_FORBIDDEN);
        }

        $player = $game->getState()->getPlayer($userHash);
        if (
            $location !== null
            && $player->circleExists($location)
            && $player->getCircle($location) !== null
        ) {
            $cards = $player->getCircle($location)->getEquipGauge();
            $output = [];
            foreach ($cards as $card) {
                $output[$card->getId()] = $card->serializeForUpdate();
            }

            return new JsonResponse($output);
        } else {
            return new JsonResponse([]);
        }
    }
}
