<?php
declare(strict_types=1);

namespace App\Ability\Provider;

use App\Ability\AbilityPartInterface;
use App\Ability\ProviderInterface;

class getCircleCount extends AbilityPartInterface implements ProviderInterface
{
    public function getName(): string
    {
        return 'get_circle_count';
    }

    public function getValue()
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
        $type = $this->getOption('type');

        $count = 0;
        $field = $player->getField();
        switch ($type) {
            case 'rearguard':
            default:
        }

        return $count;
    }
}
