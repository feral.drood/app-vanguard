<?php
declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\Event\RetireOpponentRearguardEvent;
use App\Ability\Event\RetireRearguardEvent;
use App\Ability\ProcessInterface;
use App\Entity\Actions;
use App\Entity\Game\Player;
use App\Ability\AbilityPartInterface;
use App\Ability\ActionInterface;
use App\Ability\SelectInterface;
use App\Entity\PlayerInput;

class DoSelfRetire extends AbilityPartInterface implements ProcessInterface
{
    use RetireRearguardEvent;

    public function getName(): string
    {
        return 'do_self_retire';
    }

    public function process(): bool
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
        $location = $player->findCircle($this->getSelfCardId());

        if ($location !== null) {
            $this->doRetireRearguard($location);
        }

        return true;
    }
}
