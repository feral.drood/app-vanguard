<?php
declare(strict_types=1);

namespace App\Ability\Condition;

use App\Ability\AbilityPartInterface;
use App\Ability\ConditionInterface;
use App\Entity\Game\TurnEvent;

class IsRiddenGradeGte extends AbilityPartInterface implements ConditionInterface
{
    public function getName(): string
    {
        return 'is_ridden_grade_gte';
    }

    public function isPassed(): bool
    {
        $gameLog = $this->getGame()->getState()->getEventLog();
        $rideEvent = $gameLog->getFirst(TurnEvent::EVENT_RIDE, $this->getPlayerHash());
        if ($rideEvent === null) {
            return false;
        } else {
            $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
            $riddenCard = $player->findCard($rideEvent->getTargetCardIds()[0]);
            if ($riddenCard === null) {
                return false;
            } else {
                return $riddenCard->getGrade() >= intval($this->getOption());
            }
        }
    }
}
