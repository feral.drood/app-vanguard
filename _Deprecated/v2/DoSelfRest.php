<?php
declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\AbilityPartInterface;
use App\Ability\ActionInterface;
use App\Ability\ProcessInterface;
use App\Entity\Game\Card;
use App\Entity\PlayerInput;

class DoSelfRest extends AbilityPartInterface implements ProcessInterface
{
    public function getName(): string
    {
        return 'do_self_rest';
    }

    public function process(): bool
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
        $card = $player->findCard($this->getSelfCardId());
        $card->setState(Card::STATE_REST);

        return true;
    }
}
