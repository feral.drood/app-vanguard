<?php
declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\Event\RetireOpponentRearguardEvent;
use App\Ability\Event\RetireRearguardEvent;
use App\Ability\ProcessInterface;
use App\Ability\AbilityPartInterface;

class DoRetireBuffer extends AbilityPartInterface implements ProcessInterface
{
    use RetireRearguardEvent;
    use RetireOpponentRearguardEvent;

    public function getName(): string
    {
        return 'do_retire_buffer';
    }

    public function process(): bool
    {
        $playerOption = $this->getOption(self::OPTION_PLAYER, self::OPTION_PLAYER);
        if ($playerOption === self::OPTION_PLAYER) {
            $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
        } else {
            $player = $this->getGame()->getState()->getOpposingPlayer($this->getPlayerHash());
        }

        $cards = $player->getBuffer();
        foreach ($cards as $card) {
            $location = $player->findCardLocation($card->getId());
            if ($playerOption === self::OPTION_PLAYER) {
                $this->doRetireRearguard($location);
            } else {
                $this->doRetireOpponentRearguard($location);
            }
        }

        return true;
    }
}
