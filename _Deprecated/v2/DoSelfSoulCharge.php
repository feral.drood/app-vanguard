<?php
declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\AbilityPartInterface;
use App\Ability\ProcessInterface;
use App\Entity\Game\Player;

class DoSelfSoulCharge extends AbilityPartInterface implements ProcessInterface
{
    public function getName(): string
    {
        return 'do_self_soul_charge';
    }

    public function process(): bool
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
        $circleId = $player->findCircle($this->getSelfCardId());

        if (
            $circleId !== null
            && $circleId !== Player::VANGUARD
        ) {
            $card = $player->getCircle($circleId);
            $card->reset();
            $player->getSoul()->addCard($card);
            $player->setCircle($circleId, null);

            $this->getGame()->getState()->addChatLog(
                $player->getName() .
                ' soul charges ' .
                $card->getName()
            );
        }

        return true;
    }
}
