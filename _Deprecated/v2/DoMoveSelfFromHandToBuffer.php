<?php
declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\ProcessInterface;
use App\Ability\AbilityPartInterface;
use App\Entity\PlayerInput;

class DoMoveSelfFromHandToBuffer extends AbilityPartInterface implements ProcessInterface
{
    public function getName(): string
    {
        return 'do_move_self_from_hand_to_buffer';
    }

    public function process(): bool
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
        $card = $player->getHand()->getCardById($this->getSelfCardId());

        if ($card !== null) {
            $player->getHand()->removeCards([$card]);
            $player->setBuffer([$card]);
        }

        return true;
    }
}
