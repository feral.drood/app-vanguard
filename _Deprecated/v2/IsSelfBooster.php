<?php
declare(strict_types=1);

namespace App\Ability\Condition;

use App\Ability\AbilityPartInterface;
use App\Ability\ConditionInterface;

class IsSelfBooster extends AbilityPartInterface implements ConditionInterface
{
    public function getName(): string
    {
        return 'is_self_booster';
    }

    public function isPassed(): bool
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
        $booster = $this->getGame()->getState()->getBooster();
        if (
            $booster !== null
            && $player->circleExists($booster)
            && $player->getCircle($booster)->getId() === $this->getSelfCardId()
        ) {
            return true;
        } else {
            return false;
        }
    }
}
