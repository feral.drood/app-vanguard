<?php
declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\ActionInterface;
use App\Ability\DeckSelectInterface;
use App\Ability\ProcessInterface;
use App\Ability\AbilityPartInterface;
use App\Entity\Actions;
use App\Entity\Game\PlayerError;
use App\Entity\PlayerInput;

class DoMoveDeckToBuffer extends AbilityPartInterface implements ProcessInterface, ActionInterface, DeckSelectInterface
{
    public function getName(): string
    {
        return 'do_move_deck_to_buffer';
    }

    public function processAction(PlayerInput $input): bool
    {
        $gameState = $this->getGame()->getState();
        $selectedCards = $input->getList();
        $minCount = $this->getOption(self::OPTION_MIN_COUNT, 0);
        
        $validCards = $this->getValidChoices();
        $passedChoices = [];

        foreach ($selectedCards as $cardId) {
            if (in_array($cardId, $validCards, true)) {
                $passedChoices[] = $cardId;
            }
        }

        if (
            count($passedChoices) !== count($selectedCards)
            || count($passedChoices) < $minCount
        ) {
            $gameState->setPlayerError(
                new PlayerError(
                    $this->getPlayerHash(),
                    'Invalid selection'
                )
            );

            return false;
        }

        $player = $gameState->getPlayer($this->getPlayerHash());
        $removedCards = $player->getDeck()->removeCards($passedChoices);
        $player->setBuffer($removedCards);

        return true;
    }

    public function process()
    {
        $this->processAction(
            new PlayerInput(
                $this->getPlayerHash(),
                $this->getValidChoices()
            )
        );
    }

    public function getSelectOption(): ?array
    {
        $validCount = count($this->getValidChoices());
        $maxCount = $this->getOption(self::OPTION_MAX_COUNT);

        if ($validCount > $maxCount) {
            return [
                'type' => Actions::SELECT_DECK,
                'count' => $maxCount
            ];
        } else {
            return null;
        }
    }

    public function getCardCount(): int
    {
        return $this->getOption(self::OPTION_DECK_COUNT, 50);
    }

    public function getValidChoices(): array
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());

        $race = $this->getOption(self::OPTION_RACE);
        $minGrade = (int)$this->getOption(self::OPTION_MIN_GRADE, 0);
        $maxGrade = (int)$this->getOption(self::OPTION_MAX_GRADE, 4);
        $name = $this->getOption(self::OPTION_NAME);
        $partialName = $this->getOption(self::OPTION_PARTIAL_NAME);

        $cards = $player->getDeck()->viewCards($this->getCardCount());
        $choices = [];
        foreach ($cards as $card) {
            if (
                (
                    $name !== null
                    && $name !== $card->getName()
                )
                || (
                    $partialName !== null
                    && stripos($card->getName(), $partialName) === false
                )
                || $card->getGrade() < $minGrade
                || $card->getGrade() > $maxGrade
                || (
                    $race !== null
                    && !$card->isRace($race)
                )
            ) {
                continue;
            }
            $choices[] = $card->getId();
        }

        return $choices;
    }
}
