<?php
declare(strict_types=1);

namespace App\Ability\Condition;

use App\Ability\AbilityPartInterface;
use App\Ability\ConditionInterface;
use App\Entity\Game\TurnEvent;

class IsEventCheck extends AbilityPartInterface implements ConditionInterface
{
    public function getName(): string
    {
        return 'is_event_check';
    }

    public function isPassed(): bool
    {
        $event = $this->getOption('event');
        $attackCount = $this->getGame()->getState()->getEventLog()->count($event);

        if (
            $this->getOption(self::OPTION_MIN_COUNT) !== null
            && $this->getOption(self::OPTION_MIN_COUNT) > $attackCount
        ) {
            return false;
        }

        if (
            $this->getOption(self::OPTION_MAX_COUNT) !== null
            && $this->getOption(self::OPTION_MAX_COUNT) < $attackCount
        ) {
            return false;
        }

        return true;
    }
}
