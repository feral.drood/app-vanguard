<?php
declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\ChoiceInterface;
use App\Ability\AbilityPartInterface;
use App\Ability\ActionInterface;
use App\Ability\ProcessInterface;
use App\Entity\Game\Deck;
use App\Entity\PlayerInput;

class DoMoveDeckToBottom extends AbilityPartInterface implements ProcessInterface
{
    public function getName(): string
    {
        return 'do_move_deck_to_bottom';
    }

    public function process()
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
        $removedCards = $player->getDeck()->drawCards(intval($this->getOption()));
        $player->getDeck()->addCards($removedCards, Deck::BOTTOM);

        return true;
    }
}
