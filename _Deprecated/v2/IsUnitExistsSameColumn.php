<?php
declare(strict_types=1);

namespace App\Ability\Condition;

use App\Ability\AbilityPartInterface;
use App\Ability\ConditionInterface;
use App\Entity\Game;
use App\Entity\Game\Player;

class IsUnitExistsSameColumn extends AbilityPartInterface implements ConditionInterface
{
    const PAIRS = [
        Player::VANGUARD => Player::REARGUARD3,
        Player::REARGUARD1 => Player::REARGUARD2,
        Player::REARGUARD2 => Player::REARGUARD1,
        Player::REARGUARD4 => Player::REARGUARD5,
        Player::REARGUARD5 => Player::REARGUARD4,
    ];

    public function getName(): string
    {
        return 'is_unit_exists_same_column';
    }

    public function isPassed(): bool
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());

        $selfLocation = $player->findCircle($this->getSelfCardId());
        if (
            $selfLocation === null
            || !array_key_exists($selfLocation, self::PAIRS)
            || $player->getCircle(self::PAIRS[$selfLocation]) === null
        ) {
            return false;
        }

        return true;
    }
}
