<?php
declare(strict_types=1);

namespace App\Ability\Condition;

use App\Ability\AbilityPartInterface;
use App\Ability\ConditionInterface;

class IsSelfAttacker extends AbilityPartInterface implements ConditionInterface
{
    public function getName(): string
    {
        return 'is_self_attacker';
    }

    public function isPassed(): bool
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
        $attacker = $this->getGame()->getState()->getAttacker();
        if (
            $attacker !== null
            && $player->circleExists($attacker)
            && $player->getCircle($attacker)->getId() === $this->getSelfCardId()
        ) {
            return true;
        } else {
            return false;
        }
    }
}
