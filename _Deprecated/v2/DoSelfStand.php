<?php
declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\AbilityPartInterface;
use App\Ability\ActionInterface;
use App\Ability\Event\StandEvent;
use App\Ability\ProcessInterface;
use App\Entity\Game\Card;
use App\Entity\PlayerInput;

class DoSelfStand extends AbilityPartInterface implements ProcessInterface
{
    use StandEvent;

    public function getName(): string
    {
        return 'do_self_stand';
    }

    public function process()
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
        $circleId = $player->findCircle($this->getSelfCardId());

        if (
            $circleId !== null
            && $player->getCircle($circleId) !== null
        ) {
            $this->doStand($circleId);
        }

        return true;
    }
}
