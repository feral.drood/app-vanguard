<?php
declare(strict_types=1);

namespace App\Ability\Condition;

use App\Ability\AbilityPartInterface;
use App\Ability\ActionInterface;
use App\Ability\ConditionInterface;
use App\Ability\SelectInterface;
use App\Entity\Game\TurnEvent;
use App\Exception\MisconfiguredAbilityException;
use App\Service\Game\AbilityUnpacker;

class IsComparison extends AbilityPartInterface implements ConditionInterface
{
    const COMPARISON_GT = 'gt';
    const COMPARISON_GTE = 'gte';
    const COMPARISON_LT = 'lt';
    const COMPARISON_LTE = 'lte';
    const COMPARISON_EQ = 'eq';

    public function getName(): string
    {
        return 'is_comparison';
    }

    public function isPassed(): bool
    {
        $operator = $this->getOption('operator');
        $selectData = $this->getOption('conditions', []);
        if (count($selectData) !== 2) {
            throw new MisconfiguredAbilityException('Invalid child condition count');
        }

        /** @var AbilityPartInterface|ActionInterface $select1 */
        $select1 = AbilityUnpacker::createAbilityPartFromStack(
            $this->getGame(),
            $selectData[0]
        );
        $select1
            ->setPlayerHash($this->getPlayerHash())
            ->setSelfCardId($this->getSelfCardId())
            ->setEventCardId($this->getEventCardId())
        ;
        $select1Count = count($select1->getValidChoices());

        /** @var AbilityPartInterface|ActionInterface $select2 */
        $select2 = AbilityUnpacker::createAbilityPartFromStack(
            $this->getGame(),
            $selectData[1]
        );
        $select2
            ->setPlayerHash($this->getPlayerHash())
            ->setSelfCardId($this->getSelfCardId())
            ->setEventCardId($this->getEventCardId())
        ;
        $select2Count = count($select2->getValidChoices());

        switch ($operator) {
            case self::COMPARISON_GT:
                return $select1Count > $select2Count;
                break;
            case self::COMPARISON_GTE:
                return $select1Count >= $select2Count;
                break;
            case self::COMPARISON_LT:
                return $select1Count < $select2Count;
                break;
            case self::COMPARISON_LTE:
                return $select1Count <= $select2Count;
                break;
            case self::COMPARISON_EQ:
                return $select1Count === $select2Count;
                break;
        }

        throw new MisconfiguredAbilityException('Invalid operator');
    }
}
