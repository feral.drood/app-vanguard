<?php
declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\ProcessInterface;
use App\Ability\AbilityPartInterface;
use App\Entity\PlayerInput;
use App\Service\ChatHelper;

class DoMoveSelfFromFieldToHand extends AbilityPartInterface implements ProcessInterface
{
    public function getName(): string
    {
        return 'do_move_self_from_field_to_hand';
    }

    public function process(): bool
    {
        $player = $this->getGame()->getState()->getPlayer($this->getPlayerHash());
        $circleId = $player->findCircle($this->getSelfCardId());

        if ($circleId !== null) {
            $card = $player->getCircle($circleId);
            $player->setCircle($circleId, null);
            $player->getHand()->addCard($card);
            $this->getGame()->getState()->addChatLog(
                ChatHelper::possessive($player->getName()) . ' ' . $card->getName() . ' is returned to hand'
            );
        }
        return true;
    }
}
