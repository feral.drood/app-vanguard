<?php
declare(strict_types=1);

namespace App\DependencyInjection;

use App\Service\Phases\PhaseManager;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class PhaseCompilerPass  implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        // always first check if the primary service is defined
        if (!$container->has(PhaseManager::class)) {
            return;
        }

        $definition = $container->findDefinition(PhaseManager::class);

        // find all service IDs with the app.mail_transport tag
        $taggedServices = $container->findTaggedServiceIds('app.phase');

        foreach ($taggedServices as $id => $tags) {
            // add the transport service to the TransportChain service
            $definition->addMethodCall('addPhase', [new Reference($id)]);
        }
    }
}
