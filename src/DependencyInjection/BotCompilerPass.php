<?php

declare(strict_types=1);

namespace App\DependencyInjection;

use App\Bot\BotManager;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class BotCompilerPass  implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        // always first check if the primary service is defined
        if (!$container->has(BotManager::class)) {
            return;
        }

        $definition = $container->findDefinition(BotManager::class);
        $taggedServices = $container->findTaggedServiceIds('app.bot');

        foreach ($taggedServices as $id => $tags) {
            // add the transport service to the TransportChain service
            $definition->addMethodCall('addBot', [new Reference($id)]);
        }
    }
}
