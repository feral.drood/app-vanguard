<?php

declare(strict_types=1);

namespace App\DependencyInjection;

use App\Service\AbilityPartManager;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class AbilityPartCompilerPass  implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->has(AbilityPartManager::class)) {
            return;
        }

        $definition = $container->findDefinition(AbilityPartManager::class);
        $taggedActionServices = $container->findTaggedServiceIds('app.action_ability_part');
        $taggedConditionServices = $container->findTaggedServiceIds('app.condition_ability_part');

        foreach ($taggedActionServices as $id => $tags) {
            $definition->addMethodCall('addActionAbilityPartProcessor', [new Reference($id)]);
        }
        foreach ($taggedConditionServices as $id => $tags) {
            $definition->addMethodCall('addConditionAbilityPartProcessor', [new Reference($id)]);
        }
    }
}
