<?php

namespace App\Controller;

use App\Entity\CardBlueprint;
use App\Repository\CardBlueprintRepository;
use App\Repository\GameRepository;
use App\Service\ConstructedDeckUnpacker;
use App\Service\Phases\PhaseManager;
use Doctrine\DBAL\Platforms\MySqlPlatform;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use App\Ability\Abilities;

class DebugController
{
    private $cardRepository;
    private $constructedDeckLoader;
    private $gameRepository;
    private $entityManager;
    private $router;
    private $requestStack;

    public function __construct(
        CardBlueprintRepository $cardRepository,
        ConstructedDeckUnpacker $constructedDeckLoader,
        GameRepository $gameRepository,
        EntityManagerInterface $entityManager,
        UrlGeneratorInterface $router,
        RequestStack $requestStack
    ) {
        $this->gameRepository =  $gameRepository;
        $this->constructedDeckLoader = $constructedDeckLoader;
        $this->entityManager = $entityManager;
        $this->router = $router;
        $this->cardRepository = $cardRepository;
        $this->requestStack = $requestStack;
    }

    public function main()
    {
        $games = $this->gameRepository->findAll();

        $html = '<html><body>';
        $html .= 'Games: <br>';
        foreach ($games as $game) {
            $html .= '<a href="'.$this->router->generate('debug_view', ['gameId' => $game->getId()]).'">'. $game->getId(). " </a> ";
            $html .= '(<a href="'.$this->router->generate('debug_delete', ['gameId' => $game->getId()]).'">del</a>)<br>';
        }
        if (count($games) === 0) {
            $html .= 'list empty<br>';
        }
        $html .= '</body></html>';

        return new Response($html);
    }

    public function viewGame($gameId)
    {
        $game = $this->gameRepository->getOneById($gameId);

        $className = 'App\Type\GameStateType'; # set classname here
        $serialized = sprintf('O:%d:"%s":0:{}', strlen($className), $className);
        $gameStateType = unserialize($serialized);

        $html = '<html><body>';
        $html .= '<pre>'.$gameStateType->convertToDatabaseValue($game->getState(), new MySqlPlatform()).'</pre>';
        $html .= '</body></html>';

        return new Response($html);
    }

    public function deleteGame($gameId)
    {
        $game = $this->gameRepository->getOneById($gameId);
        $this->entityManager->remove($game);
        $this->entityManager->flush();

        return new RedirectResponse($this->router->generate('debug_main'));
    }
}
