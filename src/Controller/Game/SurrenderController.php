<?php

declare(strict_types=1);

namespace App\Controller\Game;

use App\Entity\Game;
use App\Entity\GameLog;
use App\Entity\PlayerInput;
use App\Entity\Update\GameUpdate;
use App\Entity\User;
use App\Repository\GameRepository;
use App\Service\AbilityPartManager;
use App\Service\Game\ActionProcessor;
use App\Service\HashGenerator;
use App\Service\GameStateUpdatesProvider;
use App\Service\Phases\Phases;
use App\Service\PusherClient;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Actions;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;

class SurrenderController
{
    private $gameRepository;
    private $hashGenerator;
    private $updateManager;
    private $actionProcessor;
    private $pusherClient;
    private $entityManager;
    private $abilityPartManager;

    public function __construct(
        GameRepository $gameRepository,
        HashGenerator $hashGenerator,
        GameStateUpdatesProvider $updateManager,
        ActionProcessor $actionProcessor,
        PusherClient $pusherClient,
        EntityManagerInterface $entityManager,
        AbilityPartManager $abilityPartManager
    ) {
        $this->gameRepository = $gameRepository;
        $this->hashGenerator = $hashGenerator;
        $this->updateManager = $updateManager;
        $this->actionProcessor = $actionProcessor;
        $this->pusherClient = $pusherClient;
        $this->entityManager = $entityManager;
        $this->abilityPartManager = $abilityPartManager;
    }

    public function processSurrender(Security $security, Request $request, string $gameHash): JsonResponse
    {
        /** @var User $user */
        $user = $security->getUser();
        if ($user === null) {
            return new JsonResponse(null, JsonResponse::HTTP_FORBIDDEN);
        }

        $gameId = $this->hashGenerator->decodeHash($gameHash);
        if ($gameId === null) {
            return new JsonResponse(null, JsonResponse::HTTP_BAD_REQUEST);
        }

        $game = $this->gameRepository->getOneById($gameId);
        if ($game === null || $game->getStatus() !== Game::STATUS_ACTIVE) {
            return new JsonResponse(null, JsonResponse::HTTP_NOT_FOUND);
        }

        if ($game->getUser1Id() !== $user->getId() && $game->getUser2Id() !== $user->getId()) {
            return new JsonResponse([], JsonResponse::HTTP_FORBIDDEN);
        }

        $game->getState()
            ->setPhase(Phases::PHASE_FINISHED)
            ->addChatLog($user->getName() . ' surrenders')
        ;
        $updates = [
            new GameUpdate(
                'game.finished',
                null,
                true
            ),
            new GameUpdate(
                'chat.log',
                null,
                [$user->getName() . ' surrenders']
            )
        ];
        $game->setStatus(Game::STATUS_DONE);
        $game->setState(clone $game->getState());

        $this->entityManager->flush();

        $this->pusherClient->sendUpdates($gameHash, $updates);

        return new JsonResponse([
            'type' => Actions::MULTIPLE_UPDATES,
            'data' => [],
        ]);
    }
}
