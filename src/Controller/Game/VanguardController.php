<?php
declare(strict_types=1);

namespace App\Controller\Game;

use App\Entity\User;
use App\Repository\GameRepository;
use App\Service\HashGenerator;
use App\Service\Phases\Phases;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Security;

class VanguardController
{
    private $gameRepository;
    private $hashGenerator;

    public function __construct(
        GameRepository $gameRepository,
        HashGenerator $hashGenerator
    ) {
        $this->gameRepository = $gameRepository;
        $this->hashGenerator = $hashGenerator;
    }

    public function getVanguardList(Security $security, string $gameHash, string $playerHash): JsonResponse
    {
        /** @var User $user */
        $user = $security->getUser();
        if ($user === null) {
            return new JsonResponse(null, JsonResponse::HTTP_FORBIDDEN);
        }

        $gameId = $this->hashGenerator->decodeHash($gameHash);
        $game = $this->gameRepository->getOneById($gameId);
        if ($game === null) {
            return new JsonResponse(null, JsonResponse::HTTP_NOT_FOUND);
        }
        if (
            $this->hashGenerator->decodeHash($playerHash) !== $user->getId()
        ) {
            return new JsonResponse(null, JsonResponse::HTTP_FORBIDDEN);
        }

        if ($game->getState()->getPhase() !== Phases::PHASE_VANGUARD) {
            return new JsonResponse(null, JsonResponse::HTTP_CONFLICT);
        }

        $player = $game->getState()->getPlayer($playerHash);
        $cards = $player->getDeck()->getCards();
        $pureIds = [];
        $output = [];
        foreach ($cards as $card) {
            $pureId = explode('#', $card->getId())[0];
            if (
                $card->getGrade() === 0
                && !in_array($pureId, $pureIds, true)
            ) {
                $output[$card->getId()] = $card->serializeForUpdate();
                $pureIds[] = $pureId;
            }
        }

        return new JsonResponse($output);
    }
}
