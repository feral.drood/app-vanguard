<?php
declare(strict_types=1);

namespace App\Controller\Game;

use App\Entity\Game\State;
use App\Entity\Update\GameUpdate;
use App\Entity\User;
use App\Repository\GameRepository;
use App\Service\Game\AbilityStackProcessor;
use App\Service\Game\PhaseProcessor;
use App\Service\HashGenerator;
use App\Service\GameStateUpdatesProvider;
use App\Service\Phases\Phases;
use App\Service\PusherClient;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Actions;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Security;

class StatusController
{
    private $gameRepository;
    private $hashGenerator;
    private $updateManager;
    private $abilityStackProcessor;
    private $phaseProcessor;
    private $entityManager;
    private $pusherFactory;

    public function __construct(
        GameRepository $gameRepository,
        HashGenerator $hashGenerator,
        GameStateUpdatesProvider $updateManager,
        AbilityStackProcessor $abilityStackProcessor,
        PhaseProcessor $phaseProcessor,
        EntityManagerInterface $entityManager,
        PusherClient $pusherFactory
    ) {
        $this->gameRepository = $gameRepository;
        $this->hashGenerator = $hashGenerator;
        $this->updateManager = $updateManager;
        $this->abilityStackProcessor = $abilityStackProcessor;
        $this->phaseProcessor = $phaseProcessor;
        $this->entityManager = $entityManager;
        $this->pusherFactory = $pusherFactory;
    }

    public function getStatus(Security $security, string $gameHash): Response
    {
        /** @var User $user */
        $user = $security->getUser();
        if ($user === null) {
            return new Response(null, Response::HTTP_FORBIDDEN);
        }

        $gameId = $this->hashGenerator->decodeHash($gameHash);
        if ($gameId === null) {
            return new Response(null, Response::HTTP_BAD_REQUEST);
        }

        $game = $this->gameRepository->getOneById($gameId);
        if ($game === null) {
            return new Response(null, Response::HTTP_NOT_FOUND);
        }
//        if ($game->getUser1Id() !== $user->getId() && $game->getUser2Id() !== $user->getId()) {
//            return new JsonResponse([], JsonResponse::HTTP_FORBIDDEN);
//        }

        $originalGameState = new State();
        $playerHash = $this->hashGenerator->encodeHash($user->getId());
//        $game->addLog($playerHash . ' reconnected');
        $updates = $this->updateManager->getUpdates($originalGameState, $game->getState());
        if (
            $user->getId() === $game->getUser1Id()
            || $user->getId() === $game->getUser2Id()
        ) {
            $updates = array_merge($updates, $this->updateManager->getPlayerHandUpdate($originalGameState, $game->getState(), $playerHash));
            if (!$game->getState()->getAbilityStack()->isEmpty()) {
                $this->abilityStackProcessor->progressState($game->getState());
            } else {
                $this->phaseProcessor->progressState($game->getState());
            }
            $stateUpdateMessages = $game->getState()->getUpdateMessages();
            if (count($stateUpdateMessages) > 0) {
                foreach ($stateUpdateMessages as $updateMessage) {
                    if ($updateMessage->getPlayerHash() === $playerHash) {
                        $updates[] = $updateMessage;
                    } else {
                        continue;
                    }
                }
            }
        }

        if ($game->getState()->getPhase() === Phases::PHASE_FINISHED) {
            $updates[] = new GameUpdate(
                'game.finished',
                null,
                true
            );
        }
        $updateMessages = [];
        foreach ($updates as $update) {
            $updateMessages[] = $update->getData();
        }

        return new JsonResponse([
            'type' => Actions::MULTIPLE_UPDATES,
            'data' => $updateMessages
        ]);
    }
}
