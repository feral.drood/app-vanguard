<?php

declare(strict_types=1);

namespace App\Controller\Game;

use App\Entity\Game;
use App\Entity\GameLog;
use App\Entity\PlayerInput;
use App\Entity\Update\GameUpdate;
use App\Entity\User;
use App\Repository\GameRepository;
use App\Service\AbilityPartManager;
use App\Service\Game\ActionProcessor;
use App\Service\HashGenerator;
use App\Service\GameStateUpdatesProvider;
use App\Service\Phases\Phases;
use App\Service\PusherClient;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Actions;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;

class ActionController
{
    private $gameRepository;
    private $hashGenerator;
    private $updateManager;
    private $actionProcessor;
    private $pusherClient;
    private $entityManager;
    private $abilityPartManager;

    public function __construct(
        GameRepository $gameRepository,
        HashGenerator $hashGenerator,
        GameStateUpdatesProvider $updateManager,
        ActionProcessor $actionProcessor,
        PusherClient $pusherClient,
        EntityManagerInterface $entityManager,
        AbilityPartManager $abilityPartManager
    ) {
        $this->gameRepository = $gameRepository;
        $this->hashGenerator = $hashGenerator;
        $this->updateManager = $updateManager;
        $this->actionProcessor = $actionProcessor;
        $this->pusherClient = $pusherClient;
        $this->entityManager = $entityManager;
        $this->abilityPartManager = $abilityPartManager;
    }

    public function getActionDialogUpdate(Security $security, string $gameHash, string $playerHash): JsonResponse
    {
        /** @var User $user */
        $user = $security->getUser();
        if ($user === null || $this->hashGenerator->encodeHash($user->getId()) !== $playerHash) {
            return new JsonResponse(null, JsonResponse::HTTP_FORBIDDEN);
        }

        $gameId = $this->hashGenerator->decodeHash($gameHash);
        if ($gameId === null) {
            return new JsonResponse(null, JsonResponse::HTTP_BAD_REQUEST);
        }

        $game = $this->gameRepository->getOneById($gameId);
        if ($game === null) {
            return new JsonResponse(null, JsonResponse::HTTP_NOT_FOUND);
        }

        $state = $game->getState();
        $abilityStack = $state->getAbilityStack();
        if (!$abilityStack->isEmpty()) {
            if ($abilityStack->getCostCount() > 0) {
                $abilityPartData = $abilityStack->getCost();
            } else {
                $abilityPartData = $abilityStack->getEffect();
            }

            $abilityPartProcessor = $this->abilityPartManager->getActionAbilityPartProcessor($abilityPartData->getType());
            $output = $abilityPartProcessor->getPrivateUpdateData($state, $abilityPartData);
            if ($output !== null) {
                return new JsonResponse($output->getData());
            }
        }

        return new JsonResponse([]);
    }

    public function processAction(Security $security, Request $request, string $gameHash): JsonResponse
    {
        /** @var User $user */
        $user = $security->getUser();
        if ($user === null) {
            return new JsonResponse(null, JsonResponse::HTTP_FORBIDDEN);
        }

        $gameId = $this->hashGenerator->decodeHash($gameHash);
        if ($gameId === null) {
            return new JsonResponse(null, JsonResponse::HTTP_BAD_REQUEST);
        }

        $game = $this->gameRepository->getOneById($gameId);
        if ($game === null) {
            return new JsonResponse(null, JsonResponse::HTTP_NOT_FOUND);
        }

        if ($game->getUser1Id() !== $user->getId() && $game->getUser2Id() !== $user->getId()) {
            return new JsonResponse([], JsonResponse::HTTP_FORBIDDEN);
        }

        $inputData = json_decode($request->getContent(), true, 3);
        $playerHash = $this->hashGenerator->encodeHash($user->getId());
        $input = new PlayerInput(
            $playerHash,
            $inputData
        );

        $originalGameState = $game->getState();
        $game->setState(clone $originalGameState);

        $this->actionProcessor->processPlayerInput($game->getState(), $input);

        $updates = $this->updateManager->getUpdates($originalGameState, $game->getState());

        if ($game->getState()->getPhase() === Phases::PHASE_FINISHED) {
            $game->setStatus(Game::STATUS_DONE);
        }

        $playerError = $game->getState()->getPlayerError();
        if ($playerError !== null) {
            $errorUpdate = new GameUpdate(
                Actions::PLAYER_ERROR,
                $playerHash,
                $playerError->getMessage()
            );

            return new JsonResponse([
                'type' => Actions::MULTIPLE_UPDATES,
                'data' => [
                    $errorUpdate->getData(),
                ],
            ]);
        }
        $updates[] = new GameUpdate(
            Actions::ACTION_CLEAR,
            $playerHash,
            null
        );

        $gameLog = (new GameLog())
            ->setGameId($gameId)
            ->setPlayerHash($playerHash)
            ->setInputData($inputData)
            ->setBeforeState($originalGameState)
            ->setAfterState($game->getState())
            ->setMessageData($updates)
        ;
        $this->entityManager->persist($gameLog);

        $this->entityManager->flush();
        $updates = array_merge($updates, $game->getState()->getUpdateMessages());

        $this->pusherClient->sendUpdates($gameHash, $updates);

        return new JsonResponse([
            'type' => Actions::MULTIPLE_UPDATES,
            'data' => [],
        ]);
    }
}
