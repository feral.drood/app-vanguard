<?php
declare(strict_types=1);

namespace App\Controller\Game;

use App\Entity\Game\Card;
use App\Entity\User;
use App\Repository\GameRepository;
use App\Service\HashGenerator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Security;

class SoulController
{
    private $hashGenerator;
    private $gameRepository;

    public function __construct(
        HashGenerator $hashGenerator,
        GameRepository $gameRepository
    ) {
        $this->hashGenerator = $hashGenerator;
        $this->gameRepository = $gameRepository;
    }

    public function getSoulCards(Security $security, string $gameHash, string $playerHash): JsonResponse
    {
        /** @var User $user */
        $user = $security->getUser();
        if ($user === null) {
            return new JsonResponse(null, JsonResponse::HTTP_FORBIDDEN);
        }

        $gameId = $this->hashGenerator->decodeHash($gameHash);
        $game = $this->gameRepository->getOneById($gameId);
        if ($game === null) {
            return new JsonResponse(null, JsonResponse::HTTP_NOT_FOUND);
        }

        $player = $game->getState()->getPlayer($playerHash);
        if ($player === null) {
            return new JsonResponse(null, JsonResponse::HTTP_NOT_FOUND);
        }

        $output = [];
        $cards = $player->getSoul()->getCards();
        foreach ($cards as $card) {
            $output[$card->getId()] = $card->serializeForUpdate(Card::ZONE_SOUL);
        }

        return new JsonResponse($output);
    }
}
