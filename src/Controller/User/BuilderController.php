<?php
declare(strict_types=1);

namespace App\Controller\User;

use App\Repository\CardBlueprintRepository;
use Symfony\Component\HttpFoundation\JsonResponse;

class BuilderController
{
    private $cardRepository;

    public function __construct(
        CardBlueprintRepository $cardRepository
    ) {
        $this->cardRepository = $cardRepository;
    }

    public function getClans(): JsonResponse
    {
        $clans = $this->cardRepository->getAllClans();
        return new JsonResponse($clans);
    }

    public function getClan(string $clan): JsonResponse
    {
        $cards = $this->cardRepository->getAllByClan($clan);

        $output = [];
        foreach ($cards as $card) {
            $output[] = [
                'id' => $card->getId(),
                'name' => $card->getName(),
                'grade' => $card->getGrade(),
                'power' => $card->getPower(),
                'shield' => $card->getShield(),
                'trigger' => $card->getTrigger(),
                'image' => $card->getImage(),
                'description' => $card->getDescription(),
            ];
        }

        return new JsonResponse($output);
    }
}
