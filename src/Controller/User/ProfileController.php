<?php

declare(strict_types=1);

namespace App\Controller\User;

use App\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Security;

class ProfileController
{
    public function getProfile(Security $security)
    {
        /** @var User $user */
        $user = $security->getUser();
        if ($user === null) {
            return new JsonResponse(null, JsonResponse::HTTP_FORBIDDEN);
        }

        $output = [];
        $output['player_name'] = $user->getName();
        $output['player_picture'] = $user->getPicture();

        return new JsonResponse($output);
    }
}
