<?php

declare(strict_types=1);

namespace App\Controller\User;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Security;

class NameController
{
    const NAME = 'name';

    private $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    ) {
        $this->entityManager = $entityManager;
    }

    public function getNames(Security $security): JsonResponse
    {
        /** @var User $user */
        $user = $security->getUser();
        if ($user === null) {
            return new JsonResponse(null, JsonResponse::HTTP_FORBIDDEN);
        }

        return new JsonResponse([
            'names' => $this->getValidNames($user),
        ]);
    }

    public function updateName(Security $security, RequestStack $requestStack): JsonResponse
    {
        /** @var User $user */
        $user = $security->getUser();
        if ($user === null) {
            return new JsonResponse(null, JsonResponse::HTTP_FORBIDDEN);
        }

        $request = $requestStack->getCurrentRequest();
        $content = $request->getContent();
        $json = json_decode($content, true);
        if (
            $json === null
            || !isset($json[self::NAME])
        ) {
            return new JsonResponse(['error' => 'missing_value'], JsonResponse::HTTP_BAD_REQUEST);
        }

        if (!in_array($json[self::NAME], $this->getValidNames($user), true)) {
            return new JsonResponse(['error' => 'invalid_value'], JsonResponse::HTTP_BAD_REQUEST);
        }

        $user->setName($json[self::NAME]);
        $this->entityManager->flush();

        return new JsonResponse([
            'user_name' => $user->getName(),
        ]);
    }

    private function getValidNames(User $user): array
    {
        $names = [];
        if ($user->getFacebookName() !== null) {
            $names[] = $user->getFacebookName();
            $names = array_merge($names, explode(' ', $user->getFacebookName()));
        }
        if ($user->getGoogleName() !== null) {
            $names[] = $user->getGoogleName();
            $names = array_merge($names, explode(' ', $user->getGoogleName()));
        }
        $names = array_filter($names);
        $names = array_unique($names);

        // Exception for Dummy user
        if (count($names) === 0) {
            $names[] = $user->getName();
        }

        return $names;
    }
}
