<?php
declare(strict_types=1);

namespace App\Controller\User;

use App\Entity\Game\Card;
use App\Entity\ConstructedDeck;
use App\Entity\User;
use App\Repository\CardBlueprintRepository;
use App\Repository\ConstructedDeckRepository;
use App\Service\HashGenerator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;

class DeckController
{
    private $hashGenerator;
    private $deckRepository;
    private $cardRepository;
    private $entityManager;

    public function __construct(
        HashGenerator $hashGenerator,
        ConstructedDeckRepository $deckRepository,
        CardBlueprintRepository $cardRepository,
        EntityManagerInterface $entityManager
    ) {
        $this->hashGenerator = $hashGenerator;
        $this->deckRepository = $deckRepository;
        $this->cardRepository = $cardRepository;
        $this->entityManager = $entityManager;
    }

    public function getDecks(Security $security): JsonResponse
    {
        /** @var User $user */
        $user = $security->getUser();
        if ($user === null) {
            return new JsonResponse(null, JsonResponse::HTTP_FORBIDDEN);
        }

        $decks = $this->deckRepository->getAllPrivateByUserId($user->getId());
        $output = [];
        foreach ($decks as $deck) {
            $output[] = [
                'id' => $this->hashGenerator->encodeHash($deck->getId()),
                'name' => $deck->getName(),
                'clan' => $deck->getClan(),
            ];
        }

        return new JsonResponse($output);
    }

    public function getDeck(Security $security, string $deckHash): JsonResponse
    {
        /** @var User $user */
        $user = $security->getUser();
        if ($user === null) {
            return new JsonResponse(null, JsonResponse::HTTP_FORBIDDEN);
        }

        $deckId = $this->hashGenerator->decodeHash($deckHash);
        if ($deckId === null) {
            return new JsonResponse(['error' => 'invalid_data'], JsonResponse::HTTP_BAD_REQUEST);
        }

        /** @var ConstructedDeck $deck */
        $deck = $this->deckRepository->find($deckId);
        if (
            $deck === null
            || $deck->getUserId() !== $user->getId()
        ) {
            return new JsonResponse(null, JsonResponse::HTTP_NOT_FOUND);
        }

        return new JsonResponse([
            'id' => $deckHash,
            'name' => $deck->getName(),
            'clan' => $deck->getClan(),
            'cards' => $deck->getCards(),
        ]);
    }

    public function deleteDeck(Security $security, Request $request, string $deckHash): JsonResponse
    {
        /** @var User $user */
        $user = $security->getUser();
        if ($user === null) {
            return new JsonResponse(null, JsonResponse::HTTP_FORBIDDEN);
        }

        $deckId = $this->hashGenerator->decodeHash($deckHash);
        if ($deckId === null) {
            return new JsonResponse(null, JsonResponse::HTTP_NOT_FOUND);
        }

        $deck = $this->deckRepository->getOneByid($deckId);

        if ($deck->getUserId() === $user->getId()) {
            $this->entityManager->remove($deck);
            $this->entityManager->flush();

            return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);
        }

        return new JsonResponse(null, JsonResponse::HTTP_BAD_REQUEST);
    }

    public function addDeck(Security $security, Request $request, string $deckHash = null): JsonResponse
    {
        /** @var User $user */
        $user = $security->getUser();
        if ($user === null) {
            return new JsonResponse(null, JsonResponse::HTTP_FORBIDDEN);
        }

        $content = $request->getContent();
        $json = json_decode($content, true);
        if (
            $json === null
            || !isset($json['name'])
            || !isset($json['clan'])
            || !isset($json['cards'])
        ) {
            return new JsonResponse(null, JsonResponse::HTTP_BAD_REQUEST);
        }

        $deckId = null;
        if ($deckHash !== null) {
            $deckId = $this->hashGenerator->decodeHash($deckHash);
        }

        $deck = null;
        if ($deckId !== null) {
            $deck = $this->deckRepository->getOneByid($deckId);

            if ($deck->getUserId() !== $user->getId()) {
                $deck = null;
            }
        }

        if ($deck === null) {
            $deck = (new ConstructedDeck())
                ->setUserId($user->getId())
                ->setClan($json['clan'])
            ;
            $this->entityManager->persist($deck);
        }
        $json['name'] = trim(preg_replace("/[^a-zA-Z0-9 ]+/", "", $json['name']));
        $deck->setName($json['name']);

        $validName = true;
        $sentinelCount = 0;
        $triggerCount = 0;
        $healTriggerCount = 0;
        $cardCount = 0;

        if (
            strlen($json['name']) <3
            || strlen($json['name']) > 20
        ) {
            $validName = false;
        }
        $cards = [];
        foreach ($json['cards'] as $cardId => $cardNumber) {
            $cardNumber = (int)$cardNumber;
            $cardBlueprint = $this->cardRepository->getOneById($cardId);
            if (
                $cardBlueprint === null
                || $cardBlueprint->getClan() !== $deck->getClan()
                || $cardNumber < 1
                || $cardNumber > 4
            ) {
                continue;
            }

            if ($cardBlueprint->getShield() === 0) {
                $sentinelCount += $cardNumber;
            }
            if ($cardBlueprint->getTrigger() !== null) {
                $triggerCount += $cardNumber;
            }
            if ($cardBlueprint->getTrigger() === Card::TRIGGER_HEAL) {
                $healTriggerCount += $cardNumber;
            }
            $cardCount += $cardNumber;

            $cards[$cardBlueprint->getId()] = (int)$cardNumber;
        }
        $deck->setCards($cards);

        if (
            !$validName
            || $sentinelCount > 4
            || $healTriggerCount > 4
            || $triggerCount !== 16
            || $cardCount !== 50
        ) {
            return new JsonResponse(['error' => 'invalid_data'], JsonResponse::HTTP_BAD_REQUEST);
        }

        $this->entityManager->flush();

        return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);
    }
}
