<?php

declare(strict_types=1);

namespace App\Controller\User;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Security;

class PictureController
{
    const PICTURE = 'picture';

    private $entityManager;
    private $defaultPictures;

    public function __construct(
        EntityManagerInterface $entityManager
    ) {
        $this->entityManager = $entityManager;
        $this->defaultPictures = [
             '/images/avatar/male1.png',
             '/images/avatar/male2.png',
             '/images/avatar/male3.png',
             '/images/avatar/male4.png',
             '/images/avatar/female1.png',
             '/images/avatar/female2.png',
             '/images/avatar/female3.png',
             '/images/avatar/female4.png',
        ];
    }

    public function getPictures(Security $security): JsonResponse
    {
        /** @var User $user */
        $user = $security->getUser();
        if ($user === null) {
            return new JsonResponse(null, JsonResponse::HTTP_FORBIDDEN);
        }

        return new JsonResponse([
            'pictures' => $this->getValidPictures($user),
        ]);
    }

    public function updatePicture(Security $security, RequestStack $requestStack): JsonResponse
    {
        /** @var User $user */
        $user = $security->getUser();
        if ($user === null) {
            return new JsonResponse(null, JsonResponse::HTTP_FORBIDDEN);
        }

        $request = $requestStack->getCurrentRequest();
        $content = $request->getContent();
        $json = json_decode($content, true);
        if (
            $json === null
            || !isset($json[self::PICTURE])
        ) {
            return new JsonResponse(['error' => 'missing_value'], JsonResponse::HTTP_BAD_REQUEST);
        }

        if (!in_array($json[self::PICTURE], $this->getValidPictures($user), true)) {
            return new JsonResponse(['error' => 'invalid_value'], JsonResponse::HTTP_BAD_REQUEST);
        }

        $user->setPicture($json[self::PICTURE]);
        $this->entityManager->flush();

        return new JsonResponse([
            'user_picture' => $user->getPicture(),
        ]);
    }

    private function getValidPictures(User $user): array
    {
        $pictures = [];
        if ($user->getFacebookPicture() !== null) {
            $pictures[] = $user->getFacebookPicture();
        }
        if ($user->getGooglePicture() !== null) {
            $pictures[] = $user->getGooglePicture();
        }
        $pictures = array_filter($pictures);
        $pictures = array_merge($pictures, $this->defaultPictures);
        $pictures = array_unique($pictures);

        return $pictures;
    }
}
