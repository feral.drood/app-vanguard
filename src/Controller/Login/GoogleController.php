<?php

declare(strict_types=1);

namespace App\Controller\Login;

use App\Repository\UserRepository;
use App\Service\Client\GoogleClient;
use App\Service\UserCurrentGameDataProvider;
use App\Service\UserTokenUpdater;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class GoogleController
{
    private $client;
    private $userRepository;
    private $entityManager;
    private $userTokenUpdater;
    private $currentGameDataProvider;

    public function __construct(
        GoogleClient $client,
        UserRepository $userRepository,
        EntityManagerInterface $entityManager,
        UserTokenUpdater $userTokenUpdater,
        UserCurrentGameDataProvider $currentGameDataProvider
    ) {
        $this->client = $client;
        $this->userRepository = $userRepository;
        $this->entityManager = $entityManager;
        $this->userTokenUpdater = $userTokenUpdater;
        $this->currentGameDataProvider = $currentGameDataProvider;
    }

    public function login(Request $request)
    {
        $content = $request->getContent();
        if (empty($content)) {
            return new JsonResponse(['error' => 'missing_data'], JsonResponse::HTTP_BAD_REQUEST);
        }

        $user = $this->client->getUser($content);
        if ($user === null) {
            return new JsonResponse(['error' => 'invalid_data'], JsonResponse::HTTP_BAD_REQUEST);
        }

        $savedUser = $this->userRepository->findOneByGoogleId($user->getGoogleId());
        if ($savedUser === null) {
            $savedUser = $this->userRepository->findOneByEmail($user->getEmail());
        }

        if ($savedUser === null) {
            $this->entityManager->persist($user);
        } else {
            $savedUser
                ->setGoogleId($user->getGoogleId())
                ->setGoogleName($user->getGoogleName())
                ->setGooglePicture($user->getGooglePicture())
            ;
            if (
                $savedUser->getPicture() === null
                && $user->getGooglePicture() !== null
            ) {
                $savedUser->setPicture($user->getGooglePicture());
            }
            $user = $savedUser;
        }

        $this->userTokenUpdater->updateToken($user);
        $this->entityManager->flush();

        $output = $this->currentGameDataProvider->getUserCurrentGameData($user);

        return new JsonResponse($output);
    }
}
