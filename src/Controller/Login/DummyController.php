<?php

declare(strict_types=1);

namespace App\Controller\Login;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Service\UserCurrentGameDataProvider;
use App\Service\UserTokenUpdater;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class DummyController
{
    const DUMMY_EMAIL = 'dummy@feral.lt';
    const DUMMY_NAME = 'Dummy';

    private $userRepository;
    private $entityManager;
    private $userTokenUpdater;
    private $currentGameDataProvider;

    public function __construct(
        UserRepository $userRepository,
        EntityManagerInterface $entityManager,
        UserTokenUpdater $userTokenUpdater,
        UserCurrentGameDataProvider $currentGameDataProvider
    ) {
        $this->userRepository = $userRepository;
        $this->entityManager = $entityManager;
        $this->userTokenUpdater = $userTokenUpdater;
        $this->currentGameDataProvider = $currentGameDataProvider;
    }

    public function login(string $email, string $name)
    {
//        $user = $this->userRepository->findOneByEmail(self::DUMMY_EMAIL);
        $user = $this->userRepository->findOneByEmail($email);

        if ($user === null) {
            $user = (new User())
                ->setName($name)
                ->setEmail($email)
                ->setPicture('/images/avatar/male1.png')
            ;
            $this->entityManager->persist($user);
        }
        $this->userTokenUpdater->updateToken($user);
        $this->entityManager->flush();

        $output = $this->currentGameDataProvider->getUserCurrentGameData($user);

        return new JsonResponse($output);
    }
}
