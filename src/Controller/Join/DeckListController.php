<?php
declare(strict_types=1);

namespace App\Controller\Join;

use App\Entity\User;
use App\Repository\ConstructedDeckRepository;
use Hashids\Hashids;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Security;

class DeckListController
{
    private $constructedDeckRepository;
    private $hashGenerator;

    public function __construct(
        ConstructedDeckRepository $constructedDeckRepository,
        Hashids $hashGenerator
    ) {
        $this->constructedDeckRepository = $constructedDeckRepository;
        $this->hashGenerator = $hashGenerator;
    }

    public function getDeckList(Security $security): Response
    {
        /** @var User $user */
        $user = $security->getUser();
        if ($user === null) {
            return new Response(null, JsonResponse::HTTP_FORBIDDEN);
        }

        $decks = $this->constructedDeckRepository->getAllPublicByUserId($user->getId());
        $output = [];
        foreach ($decks as $deck) {
            $output[] = [
                'id' => $this->hashGenerator->encode($deck->getId()),
                'name' => $deck->getName(),
                'clan' => $deck->getClan(),
            ];
        }

        return new JsonResponse($output);
    }
}
