<?php

declare(strict_types=1);

namespace App\Controller\Join;

use App\Entity\User;
use App\Repository\GameRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Game;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Security;

class CancelController
{
    private $entityManager;
    private $gameRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        GameRepository $gameRepository
    ) {
        $this->entityManager = $entityManager;
        $this->gameRepository = $gameRepository;
    }

    public function cancel(Security $security): Response
    {
        /** @var User $user */
        $user = $security->getUser();
        if ($user === null) {
            return new Response(null, Response::HTTP_FORBIDDEN);
        }

        $game = $this->gameRepository->getOneActiveByUserId($user->getId());
        if (
            $game === null
            || $game->getStatus() !== Game::STATUS_ACTIVE
        ) {
            return new Response(null, Response::HTTP_NOT_FOUND);
        }

        if (
            $user->getId() === $game->getUser1Id()
            && $game->getUser2Id() === null
        ) {
            $game
                ->setStatus(Game::STATUS_DONE)
            ;
        }
        $this->entityManager->flush();

        return new Response(null, Response::HTTP_NO_CONTENT);
    }
}
