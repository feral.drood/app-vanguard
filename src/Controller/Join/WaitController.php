<?php
declare(strict_types=1);

namespace App\Controller\Join;

use App\Entity\User;
use App\Repository\GameRepository;
use App\Service\UserCurrentGameDataProvider;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Security;

class WaitController
{
    private $gameRepository;
    private $currentGameDataProvider;

    public function __construct(
        GameRepository $gameRepository,
        UserCurrentGameDataProvider $currentGameDataProvider
    ) {
        $this->gameRepository = $gameRepository;
        $this->currentGameDataProvider = $currentGameDataProvider;
    }

    public function getStatus(Security $security): Response
    {
        /** @var User $user */
        $user = $security->getUser();
        if ($user === null) {
            return new Response(null, JsonResponse::HTTP_FORBIDDEN);
        }

        $game = $this->gameRepository->getOneActiveByUserId($user->getId());
        if ($game === null) {
            return new Response(null, Response::HTTP_NOT_FOUND);
        }

        return new JsonResponse($this->currentGameDataProvider->getGameData($game, $user));
    }
}
