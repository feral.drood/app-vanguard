<?php

declare(strict_types=1);

namespace App\Controller\Join;

use App\Entity\User;
use App\Repository\ConstructedDeckRepository;
use App\Repository\GameRepository;
use App\Bot\BotManager;
use App\Service\ConstructedDeckUnpacker;
use App\Service\HashGenerator;
use App\Service\Phases\Phases;
use App\Service\UserCurrentGameDataProvider;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Game;
use App\Entity\Game\Player;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Security;

class JoinController
{
    private const TYPE_TRAINING = 'training';

    private $entityManager;
    private $gameRepository;
    private $currentGameDataProvider;
    private $botManager;
    private $constructedDeckRepository;
    private $constructedDeckUnpacker;
    private $hashGenerator;

    public function __construct(
        EntityManagerInterface $entityManager,
        GameRepository $gameRepository,
        UserCurrentGameDataProvider $currentGameDataProvider,
        BotManager $botManager,
        ConstructedDeckRepository $constructedDeckRepository,
        ConstructedDeckUnpacker $constructedDeckUnpacker,
        HashGenerator $hashGenerator
    ) {
        $this->entityManager = $entityManager;
        $this->gameRepository = $gameRepository;
        $this->currentGameDataProvider = $currentGameDataProvider;
        $this->botManager = $botManager;
        $this->constructedDeckRepository = $constructedDeckRepository;
        $this->constructedDeckUnpacker = $constructedDeckUnpacker;
        $this->hashGenerator = $hashGenerator;
    }

    public function getBots(Security $security): JsonResponse
    {
        /** @var User $user */
        $user = $security->getUser();
        if ($user === null) {
            return new JsonResponse(null, JsonResponse::HTTP_FORBIDDEN);
        }

        $bots = $this->botManager->getBots();
        $output = [];
        foreach ($bots as $bot) {
            $output[] = [
                'name' => $bot->getName(),
                'avatar' => $bot->getAvatar(),
                'clan' => $bot->getClan(),
            ];
        }

        return new JsonResponse([
            'bots' => $output,
        ]);
    }

    public function getActiveGames(Security $security): JsonResponse
    {
        /** @var User $user */
        $user = $security->getUser();
        if ($user === null) {
            return new JsonResponse(null, JsonResponse::HTTP_FORBIDDEN);
        }

        $game = $this->gameRepository->getOneActiveByUserId($user->getId());
        if ($game !== null) {
            return new JsonResponse($this->currentGameDataProvider->getGameData($game, $user));
        }

        $output = [];
        $games = $this->gameRepository->getActiveGames();
        foreach ($games as $game) {
            if ($game->getUser2Id() === null) {
                continue;
            }
            $player0_hash = null;
            $player0_name = null;
            $player0_avatar = null;
            $player1_hash = null;
            $player1_name = null;
            $player1_avatar = null;
            foreach ($game->getState()->getPlayers() as $player) {
                if ($player0_name === null) {
                    $player0_hash = $player->getHash();
                    $player0_name = $player->getName();
                    $player0_avatar = $player->getAvatar();
                } else {
                    $player1_hash = $player->getHash();
                    $player1_name = $player->getName();
                    $player1_avatar = $player->getAvatar();
                }
            }
            $output[] = [
                'hash' => $this->hashGenerator->encodeHash($game->getId()),
                'player0_hash' => $player0_hash,
                'player0_name' => $player0_name,
                'player0_avatar' => $player0_avatar,
                'player1_hash' => $player1_hash,
                'player1_name' => $player1_name,
                'player1_avatar' => $player1_avatar,
            ];
        }

        return new JsonResponse([
            'games' => $output,
        ]);
    }

    public function join(Security $security, RequestStack $requestStack, string $type): JsonResponse
    {
        /** @var User $user */
        $user = $security->getUser();
        if ($user === null) {
            return new JsonResponse(null, JsonResponse::HTTP_FORBIDDEN);
        }

        $game = $this->gameRepository->getOneActiveByUserId($user->getId());
        if ($game !== null) {
            return new JsonResponse($this->currentGameDataProvider->getGameData($game, $user));
        }

        $request = $requestStack->getCurrentRequest();
        $content = $request->getContent();
        $json = json_decode($content, true);
        if (
            $json === null
            || !array_key_exists('deck', $json)
            || (
                $type === self::TYPE_TRAINING
                && !array_key_exists('name', $json)
            )
        ) {
            return new JsonResponse(['error' => 'invalid_data'], JsonResponse::HTTP_BAD_REQUEST);
        }
        $deckId = $this->hashGenerator->decodeHash($json['deck']);
        if ($deckId === null) {
            return new JsonResponse(['error' => 'invalid_data'], JsonResponse::HTTP_BAD_REQUEST);
        }

        $deck = $this->constructedDeckRepository->getOneByid($deckId);
        if ($deck === null) {
            return new JsonResponse(['error' => 'not_found'], JsonResponse::HTTP_NOT_FOUND);
        }

        if (
            $deck->getUserId() !== $user->getId()
            && $deck->getUserId() !== null
        ) {
            return new JsonResponse(['error' => 'foreign_data'], JsonResponse::HTTP_FORBIDDEN);
        }

        if ($type === self::TYPE_TRAINING) {
            $bot = null;
            if ($json['name'] === null) {
                $bot = $this->botManager->getRandomBot();
            } else {
                $bot = $this->botManager->getBot($json['name']);
            }

            if ($bot === null) {
                return new JsonResponse(['error' => 'not_found'], JsonResponse::HTTP_NOT_FOUND);
            }

            $game = (new Game())
                ->setUser1Id($user->getId())
                ->setUser2Id(0)
            ;

            // Bot
            $player = (new Player())
                ->setHash(BotManager::BOT_HASH)
                ->setName($bot->getName())
                ->setAvatar($bot->getAvatar())
            ;
            $player->getDeck()->setCards(
                $this->constructedDeckUnpacker->unpack($bot->getCards())
            );
            $game->getState()->addPlayer($player);
            $game->getState()->addChatLog($bot->getName() . ' joined the game');
        } else {
            $game = $this->gameRepository->getOneWaiting($user);
            if ($game === null) {
                $game = (new Game())
                    ->setUser1Id($user->getId())
                ;
            } else {
                $game->setUser2Id($user->getId());
            }
        }

        // Player
        $playerHash = $this->hashGenerator->encodeHash($user->getId());
        $player = (new Player())
            ->setHash($playerHash)
            ->setName($user->getName())
            ->setAvatar($user->getPicture())
        ;
        $player->getDeck()->setCards(
            $this->constructedDeckUnpacker->unpack($deck->getCards())
        );
        $game->getState()->addPlayer($player);
        $game->getState()->addChatLog($user->getName() . ' joined the game');

        if ($game->getUser2Id() !== null) {
            $game->getState()->setPhase(Phases::PHASE_VANGUARD);
        }

        $game->setState(clone $game->getState());
        $this->entityManager->persist($game);
        $this->entityManager->flush();

        return new JsonResponse($this->currentGameDataProvider->getGameData($game, $user));
    }
}
