<?php

declare(strict_types=1);

namespace App\Type;

use App\Entity\Game\State;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

class GameStateType extends Type
{
    const GAME_STATE_TYPE = 'game_state'; // modify to match your type name

    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    public function asd() {

    }

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        $a = array();
        return $platform->getJsonTypeDeclarationSQL($fieldDeclaration);
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        $value = json_decode($value, true);

        return (new State())->deserializeFromDatabase($value);
    }

    /**
     * @param State $value
     *
     * @return false|mixed|string
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return json_encode($value->serializeForDatabase(), JSON_PRETTY_PRINT);
    }

    public function getName()
    {
        return self::GAME_STATE_TYPE;
    }
}
