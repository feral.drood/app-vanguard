<?php

namespace App\Command;

use App\Entity\CardBlueprint;
use App\Entity\ConstructedDeck;
use App\Entity\Game\Protect;
use App\Repository\CardBlueprintRepository;
use App\Repository\ConstructedDeckRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ImportDeckCommand extends Command
{
    const TYPE = 'type';
    const DECK = 'deck';
    const NAME = 'name';
    const CLAN = 'clan';
    const CARDS = 'cards';

    private $cardRepository;
    private $constructedDeckRepository;
    private $entityManager;

    public function __construct(
        CardBlueprintRepository $cardRepository,
        ConstructedDeckRepository $constructedDeckRepository,
        EntityManagerInterface $entityManager
    ) {
        $this->cardRepository = $cardRepository;
        $this->constructedDeckRepository = $constructedDeckRepository;
        $this->entityManager = $entityManager;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('app:import-deck')
            ->setDescription('Import deck json')
            ->setHelp('deck file format should be {"type": "deck", "name": "X", "cards": ["CARD1" => 1, "CARD2" => 2]}')
            ->addArgument('file', InputOption::VALUE_REQUIRED, 'Card deck file')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $setFile = $input->getArgument('file');

        if (!is_file($setFile) || !is_readable($setFile) ) {
            $output->writeln('Invalid file');
            return;
        }

        $content = file_get_contents($setFile);
        $json = json_decode($content, true);
        if (
            !is_array($json)
            || !key_exists(self::TYPE, $json)
            || $json[self::TYPE] !== self::DECK
            || !key_exists(self::NAME, $json)
            || !key_exists(self::CARDS, $json)
            || !key_exists(self::CLAN, $json)
            || !is_array($json[self::CARDS])
        ) {
            $output->writeln('Invalid file');
            return;
        }

        $deck = $this->constructedDeckRepository->getOnePublicByName($json[self::NAME]);
        if ($deck === null) {
            $deck = (new ConstructedDeck())
                ->setName($json[self::NAME])
                ->setClan($json[self::CLAN])
            ;
            $this->entityManager->persist($deck);
            $output->writeln('Creating ' . $json[self::NAME] . ' deck');
        } else {
            $deck->setClan($json[self::CLAN]);
            $output->writeln('Loading ' . $json[self::NAME] . ' deck');
        }

        $cards = [];
        foreach ($json[self::CARDS] as $cardId => $cardCount) {
            $card = $this->cardRepository->getOneById($cardId);

            if ($card === null) {
                $output->writeln('Card ' . $cardId . ' not found');
                die(1);
            } else {
                $output->writeln('Adding ' . $card->getId() . ' x ' . $cardCount);
                $cards[$cardId] = $cardCount;
            }
        }
        $deck->setCards($cards);

        $output->writeln('Done');
        $this->entityManager->flush();
    }
}
