<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\CardBlueprint;
use App\Entity\Game;
use App\Entity\Game\CardList;
use App\Entity\Game\Card;
use App\Repository\CardBlueprintRepository;
use App\Repository\GameRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ImportSetCommand extends Command
{
    private $cardRepository;
    private $gameRepository;
    private $entityManager;

    /**
     * @var Game[]
     */
    private $games;

    public function __construct(
        CardBlueprintRepository $cardRepository,
        GameRepository $gameRepository,
        EntityManagerInterface $entityManager
    ) {
        parent::__construct();

        $this->cardRepository = $cardRepository;
        $this->gameRepository = $gameRepository;
        $this->entityManager = $entityManager;
        $this->games = [];
    }

    protected function configure()
    {
        $this->setName('app:import-set')
            ->setDescription('Import card json')
            ->addArgument('file', InputOption::VALUE_REQUIRED, 'Card set file')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $setFile = $input->getArgument('file');

        if (!is_file($setFile) || !is_readable($setFile) ) {
            $output->writeln('Invalid file');

            return;
        }

        $content = file_get_contents($setFile);
        $json = json_decode($content, true);
        if (
            !is_array($json)
            || !isset($json['type'])
            || !isset($json['cards'])
            || $json['type'] !== 'set'
        ) {
            $output->writeln('Invalid file');

            return;
        }

        $this->games = $this->gameRepository->getActiveGames();

        foreach ($json['cards'] as $cardData) {
            $card = $this->cardRepository->getOneById($cardData['id']);

            if ($card === null) {
                $card = $this->cardRepository->getOneByName($cardData['name']);
                if ($card !== null) {
                    $output->writeln('Duplicate card name found, aborting');
                    $output->writeln(
                        'Name: '
                        . $cardData['name']
                        . ' Old: '
                        . $card->getId()
                        . ' New: '
                        . $cardData['id']
                    );

                    return;
                }

                $card = new CardBlueprint();
                $card->setId($cardData['id']);
                $this->entityManager->persist($card);
                $output->writeln('Inserting ' . $cardData['id']);
            } else {
                $output->writeln('Updating ' . $cardData['id']);
            }

            $card
                ->setName($cardData['name'])
                ->setGrade($cardData['grade'])
                ->setImage($cardData['image'])
            ;

            if (isset($cardData['clan'])) {
                $card->setClan($cardData['clan']);
            }
            if (isset($cardData['power'])) {
                $card->setPower($cardData['power']);
            }
            if (isset($cardData['skill'])) {
                $card->setSkill($cardData['skill']);
            }
            if (isset($cardData['critical'])) {
                $card->setCritical($cardData['critical']);
            }
            if (isset($cardData['type'])) {
                $card->setType($cardData['type']);
            } else {
                if (isset($cardData['trigger'])) {
                    $card->setType(Card::TYPE_TRIGGER_UNIT);
                } else {
                    $card->setType(Card::TYPE_NORMAL_UNIT);
                }
            }

            if (isset($cardData['description'])) {
                $card->setDescription($cardData['description']);
            } else {
                $card->setDescription(null);
            }
            if (isset($cardData['shield'])) {
                $card->setShield($cardData['shield']);
            } else {
                $card->setShield(null);
            }
            if (isset($cardData['race'])) {
                if (is_array($cardData['race'])) {
                    $card->setRace(implode(' / ', $cardData['race']));
                } else {
                    $card->setRace($cardData['race']);
                }
            } else {
                $card->setRace('Undefined');
            }
            if (isset($cardData['gift'])) {
                $card->setGift($cardData['gift']);
            } else {
                $card->setGift(null);
            }
            if (isset($cardData['trigger'])) {
                $card->setTrigger($cardData['trigger']);
            } else {
                $card->setTrigger(null);
            }
            if (isset($cardData['abilities'])) {
                $card->setAbilities($cardData['abilities']);
            } else {
                $card->setAbilities([]);
            }

            foreach ($this->games as $game) {
                $state = $game->getState();
                foreach ($state->getPlayers() as $player) {
                    $this->updateCardList($player->getHand(), $card);
                    $this->updateCardList($player->getDeck(), $card);
                    $this->updateCardList($player->getDrop(), $card);
                    $this->updateCardList($player->getSoul(), $card);
                    $this->updateCardList($player->getDamage(), $card);
                    if ($player->getTrigger() !== null) {
                        list($cardId) = explode('#', $player->getTrigger()->getId());
                        if ($cardId === $card->getId()) {
                            $this->updateCard($player->getTrigger(), $card);
                        }
                    }
                    foreach ($player->getField()->getCircles() as $circleId => $unit) {
                        if ($unit === null) {
                            continue;
                        }
                        list($cardId) = explode('#', $unit->getId());
                        if ($cardId === $card->getId()) {
                            $this->updateCard($unit, $card);
                        }
                    }
                    // buffer?
                }
            }
        }

        foreach ($this->games as $game) {
            $game->setState(clone $game->getState());
        }

        $this->entityManager->flush();
        $output->writeln('Done');
    }

    private function updateCardList(CardList $cardList, CardBlueprint $cardBlueprint)
    {
        foreach ($cardList->getCards() as $card) {
            list($cardId) = explode('#', $card->getId());
            if ($cardId === $cardBlueprint->getId()) {
                $this->updateCard($card, $cardBlueprint);
            }
        }
    }

    private function updateCard(Card $card, CardBlueprint $cardBlueprint)
    {
        $card
            ->setType($cardBlueprint->getType())
            ->setName($cardBlueprint->getName())
            ->setGrade($cardBlueprint->getGrade())
            ->setSkill($cardBlueprint->getSkill())
            ->setPower($cardBlueprint->getPower())
            ->setCritical($cardBlueprint->getCritical())
            ->setClan($cardBlueprint->getClan())
            ->setImage($cardBlueprint->getImage())
            ->setDescription($cardBlueprint->getDescription())
            ->setShield($cardBlueprint->getShield())
            ->setRace($cardBlueprint->getRace())
            ->setGift($cardBlueprint->getGift())
            ->setTrigger($cardBlueprint->getTrigger())
            ->setAbilities($cardBlueprint->getAbilities())
        ;
    }
}
