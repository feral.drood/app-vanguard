<?php

namespace App\Command;

use App\Repository\GameLogRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Repository\GameRepository;

class RollbackActionCommand extends Command
{
    private $gameRepository;
    private $gameLogRepository;
    private $entityManager;

    public function __construct(
        GameRepository $gameRepository,
        GameLogRepository $gameLogRepository,
        EntityManagerInterface $entityManager
    ) {
        parent::__construct();

        $this->gameRepository = $gameRepository;
        $this->gameLogRepository = $gameLogRepository;
        $this->entityManager = $entityManager;
    }

    protected function configure()
    {
        $this->setName('app:rollback-action')
            ->setDescription('Rollback last game action')
            ->addArgument('game', InputArgument::REQUIRED, 'Game id')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $gameId = (int)$input->getArgument('game');
        $game = $this->gameRepository->getOneById($gameId);
        if ($game === null) {
            $output->writeln('Unable to load game');
            return;
        }

        $gameLog = $this->gameLogRepository->getLatestByGameId($gameId);
        if ($gameLog !== null) {
            $game->setState(clone $gameLog->getBeforeState());
            $output->writeln('State restored');
            $this->entityManager->remove($gameLog);
        }

        $this->entityManager->flush();
        $output->writeln('done');
    }
}
