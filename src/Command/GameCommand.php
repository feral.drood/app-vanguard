<?php

namespace App\Command;

use App\Entity\Game\Deck;
use App\Entity\Game\Card;
use App\Repository\CardBlueprintRepository;
use App\Repository\ConstructedDeckRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Repository\GameRepository;

class GameCommand extends Command
{

    const TRIGGER = 'trigger';
    const DAMAGE = 'damage';
    const GUARDIAN = 'guardian';
    const GUARDIANS = 'guardians';
    const FIELD = 'field';
    const HAND = 'hand';
    const DROP = 'drop';
    const DECK = 'deck';
    const SOUL = 'soul';

    const LIST = 'list';
    const VIEW = 'view';
    const ADD = 'add';
    const SET = 'set';
    const INSERT = 'insert';
    const REMOVE = 'remove';

    private $gameRepository;
    private $cardRepository;
    private $constructedDeckRepository;
    private $entityManager;

    public function __construct(
        GameRepository $gameRepository,
        CardBlueprintRepository $cardRepository,
        ConstructedDeckRepository $constructedDeckRepository,
        EntityManagerInterface $entityManager
    ) {
        $this->gameRepository = $gameRepository;
        $this->cardRepository = $cardRepository;
        $this->constructedDeckRepository = $constructedDeckRepository;
        $this->entityManager = $entityManager;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('app:game')
            ->setDescription('Modify game in progress for easier testing')
            ->addArgument('game', InputArgument::REQUIRED, 'Game id')
            ->addArgument('action', InputArgument::REQUIRED, 'list/view/add/remove')
            ->addArgument('zone', InputArgument::REQUIRED, 'playerHash.hand/damage/deck/drop/soul/guardian/field')
            ->addArgument('card', InputArgument::OPTIONAL, 'Card id')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $game = $this->gameRepository->getOneById($input->getArgument('game'));
        if ($game === null) {
            $output->writeln('Unable to load game');
            return;
        }
        $action = $input->getArgument('action');
        if (!in_array($action, [self::LIST, self::VIEW, self::ADD, self::SET, self::INSERT, self::REMOVE], true)) {
            $output->writeln('Unrecognized action');
            return;
        }
        $zone = $input->getArgument('zone');
        if (strpos($zone, '.') !== false) {
            list($playerHash, $zone) = explode('.', $zone);
        } else {
            $playerHash = '';
        }

        if ($playerHash === 'active') {
            $playerHash = $game->getState()->getActivePlayerHash();
        } elseif ($playerHash === 'inactive') {
            $playerHash = $game->getState()->getInactivePlayerHash();
        }

        if (
            !in_array($zone, [self::GUARDIAN, self::GUARDIANS], true)
            && !$game->getState()->getPlayer($playerHash)
        ) {
            $output->writeln('Invalid zone (missing player hash)');
            return;
        }

        switch ($action) {
            case self::LIST:
                switch ($zone) {
                    case self::HAND:
                        $this->listCards($output, $game->getState()->getPlayer($playerHash)->getHand());
                        break;
                    case self::DAMAGE:
                        $this->listCards($output, $game->getState()->getPlayer($playerHash)->getDamage());
                        break;
                    case self::DROP:
                        $this->listCards($output, $game->getState()->getPlayer($playerHash)->getDrop());
                        break;
                    case self::SOUL:
                        $this->listCards($output, $game->getState()->getPlayer($playerHash)->getSoul());
                        break;
                    case self::DECK:
                        $this->listCards($output, $game->getState()->getPlayer($playerHash)->getDeck());
                        break;
                    case self::FIELD:
                        foreach ($game->getState()->getPlayer($playerHash)->getField() as $location => $unit) {
                            $output->writeln($location . ' - ' . ($unit === null ? 'none' : $unit->getId . ' ' . $unit->getName()));
                        }
                        break;
                    case self::GUARDIAN:
                    case self::GUARDIANS:
                        $this->listCards($output, $game->getState()->getGuardians());
                        break;
                    default:
                        $output->writeln('Unknown zone');
                }
                break;
            case self::VIEW:
                switch ($zone) {
                    case self::TRIGGER:
                        if ($game->getState()->getPlayer($playerHash)->getTrigger() === null) {
                            $output->writeln('Trigger zone is empty');
                        } else {
                            $output->writeln($game->getState()->getPlayer($playerHash)->getTrigger()->getId() . ' ' . $game->getState()->getPlayer($playerHash)->getTrigger()->getName());
                        }
                        break;
                    default:
                        $output->writeln('Unknown zone');
                }
                break;
            case self::INSERT:
            case self::ADD:
            case self::SET:
                $cardId = $input->getArgument('card');
                if (!empty($cardId)) {
                    $pureId = explode('#', $cardId)[0];
                    $blueprint = $this->cardRepository->getOneById($pureId);
//                    $output->writeln(print_r($blueprint, true));
                    $card = (new Card())
                        ->setId($cardId)
                        ->setType($blueprint->getType())
                        ->setImage($blueprint->getImage())
                        ->setTrigger($blueprint->getTrigger())
                        ->setGift($blueprint->getGift())
                        ->setDescription($blueprint->getDescription())
                        ->setGrade($blueprint->getGrade())
                        ->setSkill($blueprint->getSkill())
                        ->setPower($blueprint->getPower())
                        ->setShield($blueprint->getShield())
                        ->setCritical($blueprint->getCritical())
                        ->setName($blueprint->getName())
                        ->setClan($blueprint->getClan())
                        ->setRace($blueprint->getRace())
                        ->setAbilities($blueprint->getAbilities())
                    ;
                } else {
                    $output->writeln('Missing card');
                    return;
                }
                if ($action === self::ADD) {
                    $location = 'bottom';
                } else {
                    $location = 'top';
                }
                switch ($zone) {
                    case self::HAND:
                        $game->getState()->getPlayer($playerHash)->getHand()->addCard($card, $location);
                        break;
                    case self::DAMAGE:
                        $game->getState()->getPlayer($playerHash)->getDamage()->addCard($card, $location);
                        break;
                    case self::DROP:
                        $game->getState()->getPlayer($playerHash)->getDrop()->addCard($card, $location);
                        break;
                    case self::SOUL:
                        $game->getState()->getPlayer($playerHash)->getSoul()->addCard($card, $location);
                        break;
                    case self::DECK:
                        $game->getState()->getPlayer($playerHash)->getDeck()->addCard($card, $location);
                        break;
                    case self::GUARDIAN:
                    case self::GUARDIANS:
                        $game->getState()->getGuardians()->addCard($card, $location);
                        break;
                    case self::TRIGGER:
                        $game->getState()->getPlayer($playerHash)->setTrigger($card);
                        break;
                    default:
                        if ($game->getState()->getPlayer($playerHash)->getField()->circleExists($zone)) {
                            $game->getState()->getPlayer($playerHash)->getField()->setCircleCard($zone, $card);
                        } else {
                            $output->writeln('Unknown zone');
                        }
                }
                break;
            case self::REMOVE:
                $cardId = $input->getArgument('card');
                switch ($zone) {
                    case self::HAND:
                        $game->getState()->getPlayer($playerHash)->getHand()->removeCardsByIds([$cardId]);
                        break;
                    case self::DAMAGE:
                        $game->getState()->getPlayer($playerHash)->getDamage()->removeCardsByIds([$cardId]);
                        break;
                    case self::DROP:
                        $game->getState()->getPlayer($playerHash)->getDrop()->removeCardsByIds([$cardId]);
                        break;
                    case self::SOUL:
                        $game->getState()->getPlayer($playerHash)->getSoul()->removeCardsByIds([$cardId]);
                        break;
                    case self::DECK:
                        $game->getState()->getPlayer($playerHash)->getDeck()->removeCardsByIds([$cardId]);
                        break;
                    case self::GUARDIAN:
                    case self::GUARDIANS:
                        $game->getState()->getGuardians()->removeCardsByIds([$cardId]);
                        break;
                    case self::TRIGGER:
                        $game->getState()->getPlayer($playerHash)->setTrigger(null);
                        break;
                    default:
                        if ($game->getState()->getPlayer($playerHash)->circleExists($zone)) {
                            $game->getState()->getPlayer($playerHash)->setCircle($zone, null);
                        } else {
                            $output->writeln('Unknown zone');
                        }
                }
                break;
            default:
                $output->writeln('Unknown action');
        }


        $game->setState(clone $game->getState());
        $this->entityManager->flush();
        $output->writeln('done');
    }

    private function listCards(OutputInterface $output, Deck $deck)
    {
        foreach ($deck->getCards() as $card) {
            $output->writeln($card->getId() . ' ' .$card->getName());
        }
        if ($deck->getCount() === 0) {
            $output->writeln('List empty');
        }
    }
}
