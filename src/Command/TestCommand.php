<?php

namespace App\Command;

use App\Ability\Abilities;
use App\Ability\AbilityConstants;
use App\Bot\BotManager;
use App\Entity\CardBlueprint;
use App\Repository\CardBlueprintRepository;
use App\Service\HashGenerator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TestCommand extends Command
{
    private $service;

    public function __construct(
        HashGenerator $service
    ) {
        parent::__construct();
        $this->service = $service;
    }

    protected function configure()
    {
        $this->setName('app:test')
            ->setDescription('Test')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln($this->service->decodeHash('BOT'));
        $output->writeln('done');
    }
}
