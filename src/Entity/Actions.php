<?php

namespace App\Entity;

final class Actions
{
    const GAME_WIN = 'game.win';
    const GAME_UPDATE = 'game.update';
    const MULTIPLE_UPDATES = 'game.updates';

    const CHAT_LOG = 'chat.log';
    const HAND_SIZE = 'hand.size';
    const HAND_UPDATE_REQUIRED = 'hand.update';
    const OPPONENT_CARDS = 'opponent.cards';
    const HAND_CARDS = 'hand.cards';
    const DAMAGE_CARDS = 'damage.cards';
    const PLAYER_NAME = 'player.name';
    const PLAYER_AVATAR = 'player.avatar';
    const PLAYER_ERROR = 'player.error';

    const DECK_COUNT = 'deck.count';
    const SOUL_COUNT = 'soul.count';
    const BIND_COUNT = 'bind.count';
    const DROP_COUNT = 'drop.count';
    const DROP_CARD = 'drop.card';

    const CARD_VANGUARD = 'card.vanguard';
    const CARD_REARGUARD1 = 'card.rearguard1';
    const CARD_REARGUARD2 = 'card.rearguard2';
    const CARD_REARGUARD3 = 'card.rearguard3';
    const CARD_REARGUARD4 = 'card.rearguard4';
    const CARD_REARGUARD5 = 'card.rearguard5';
    const CARD_GUARDIANS = 'card.guardians';
    const CARD_TRIGGER = 'card.trigger';

    const ACTION_DIALOG = 'action.dialog';
    const ACTION_CLEAR = 'action.clear';
    const ACTION_SELECT = 'action.select';
    const ACTION_CARDS = 'action.cards';
    const ACTION_OPTION = 'action.option';
    const ACTION_CHOICE = 'action.choice';

    const ZONE_VANGUARD = 'vanguard';
    const ZONE_DROP = 'drop';
    const ZONE_SOUL = 'soul';
    const ZONE_BIND = 'bind';

    const SELECT_MULLIGAN = 'mulligan';
    const SELECT_ATTACKER = 'attacker';
    const SELECT_VANGUARD = 'vanguard';
    const SELECT_DEFENDER = 'defender';
    const SELECT_GUARDIAN = 'guardian';
    const SELECT_UNIT = 'unit';
    const SELECT_UNIT_POWER = 'unit_power';
    const SELECT_UNIT_CRITICAL = 'unit_critical';
    const SELECT_RIDE = 'ride';
    const SELECT_MAIN = 'main';
    const SELECT_BATTLE = 'battle';
    const SELECT_REARGUARD_CIRCLE = 'rearguard_circle';
    const SELECT_HAND = 'hand';
    const SELECT_HAND_DISCARD = 'hand_discard';
    const SELECT_HAND_OR_SOUL = 'hand_or_soul';
    const SELECT_HAND_OR_SOUL_NAMED = 'hand_or_soul_named';
    const SELECT_HAND_OR_SOUL_TO_REARGUARD = 'hand_or_soul_to_rearguard';
    const SELECT_CIRCLE = 'circle';
    const SELECT_CIRCLE_FORCE_GIFT = 'circle_force_gift';
    const SELECT_DAMAGE = 'damage';
    const SELECT_DROP = 'drop';
    const SELECT_DECK = 'deck';
    const SELECT_SOUL = 'soul';
    const SELECT_REARGUARD = 'rearguard';

    const SELECT_OPPONENT_FRONT_REARGUARD = 'opponent_front_rearguard';
    const SELECT_OPPONENT_BACK_REARGUARD = 'opponent_back_rearguard';
    const SELECT_OPPONENT_FRONT_UNIT = 'opponent_front_unit';
    const SELECT_OPPONENT_REARGUARD = 'opponent_rearguard';
    const SELECT_OPPONENT_UNIT = 'opponent_unit';

    /**
     * @param string $type
     * @param string $player
     * @param mixed $data
     * @return array
     */
    static function create($type, $player, $data)
    {
        $update = [
            'type' => $type,
            'player' => $player,
            'data' => $data
        ];

        return $update;
    }

    static function field($location)
    {
        return 'card.' . $location;
    }
}
