<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", unique=true)
     */
    private $token;

    /**
     * @ORM\Column(type="string", unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", unique=true, nullable=true)
     */
    private $googleId;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $googleName;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $googlePicture;

    /**
     * @ORM\Column(type="string", unique=true, nullable=true)
     */
    private $facebookId;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $facebookName;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $facebookPicture;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $picture;

    public function getId(): int
    {
        return $this->id;
    }

    public function setFacebookId(string $facebookId = null): self
    {
        $this->facebookId = $facebookId;

        return $this;
    }

    public function getFacebookId(): ?string
    {
        return $this->facebookId;
    }

    public function getFacebookName(): ?string
    {
        return $this->facebookName;
    }

    public function setFacebookName(string $facebookName = null): self
    {
        $this->facebookName = $facebookName;

        return $this;
    }

    public function getFacebookPicture(): ?string
    {
        return $this->facebookPicture;
    }

    public function setFacebookPicture(string $facebookPicture = null): self
    {
        $this->facebookPicture = $facebookPicture;

        return $this;
    }

    public function setGoogleId(string $googleId = null): self
    {
        $this->googleId = $googleId;

        return $this;
    }

    public function getGoogleId(): ?string
    {
        return $this->googleId;
    }

    public function getGoogleName(): ?string
    {
        return $this->googleName;
    }

    public function setGoogleName(string $googleName = null): self
    {
        $this->googleName = $googleName;

        return $this;
    }

    public function getGooglePicture(): ?string
    {
        return $this->googlePicture;
    }

    public function setGooglePicture(string $googlePicture = null): self
    {
        $this->googlePicture = $googlePicture;

        return $this;
    }

    public function setToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function getUsername(): string
    {
        return $this->token;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPicture(): ?string
    {
        return $this->picture;
    }

    public function setPicture(string $picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    public function getRoles(): array
    {
        return ['ROLE_USER'];
    }

    /**
     * @see UserInterface
     */
    public function getPassword()
    {
        // not needed for apps that do not check user passwords
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed for apps that do not check user passwords
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }
}
