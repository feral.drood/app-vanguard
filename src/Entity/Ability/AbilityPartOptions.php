<?php

declare(strict_types=1);

namespace App\Entity\Ability;

final class AbilityPartOptions
{
    public const NAME = 'name';
    public const PLAYER = 'player';
    public const PLAYER_OPPONENT = 'opponent';
    public const PARTIAL_NAME = 'partial_name';
    public const RACE = 'race';

    const STORED_VALUE = 'stored_value';

    public const CARD = 'card';
    public const CARD_SELF = 'self';
    public const CARD_EVENT = 'event';

    public const STATE = 'state';
    public const STATE_REST = 'rest';
    public const STATE_STAND = 'stand';

    public const TITLE = 'title';
    public const SILENT = 'silent';

    public const FACE = 'face';
    public const FACE_UP = 'up';
    public const FACE_DOWN = 'down';
    public const DELETED = 'deleted';

    public const GRADE = 'grade';
    public const GRADE_CALL = 'call';
    public const MIN_GRADE = 'min_grade';
    public const MAX_GRADE = 'max_grade';
    public const COUNT = 'count';
    public const MIN_COUNT = 'min_count';
    public const MAX_COUNT = 'max_count';
    public const SHIELD = 'shield';
    public const MIN_SHIELD = 'min_shield';
    public const MAX_SHIELD = 'max_shield';

    public const LENGTH = 'length';
    public const PROPERTY = 'property';

    public const LOCATION = 'location';
    public const LOCATION_OPEN = 'open';
    public const LOCATION_ATTACKER = 'attacker';
    public const LOCATION_DEFENDER = 'defender';
    public const LOCATION_BOOSTER = 'booster';
    public const LOCATION_TRIGGER = 'trigger';
    public const LOCATION_HAND = 'hand';
    public const LOCATION_BUFFER = 'buffer';
    public const LOCATION_FRONT_UNIT = 'front_unit';
    public const LOCATION_FRONT_REARGUARD = 'front_rearguard';
    public const LOCATION_BACK_REARGUARD = 'back_rearguard';
    public const LOCATION_BACK = 'back';
    public const LOCATION_COLUMN = 'column';
    public const LOCATION_VANGUARD = 'vanguard';
    public const LOCATION_REARGUARD = 'rearguard';
    public const LOCATION_FIELD = 'field';
    public const LOCATION_EQUIP_GAUGE = 'equip_gauge';
    public const LOCATION_DROP = 'drop';
    public const LOCATION_BIND = 'bind';
    public const LOCATION_DECK = 'deck';
    public const LOCATION_DAMAGE = 'damage';
    public const LOCATION_SOUL = 'soul';
    public const LOCATION_GUARDIAN = 'guardian';

    public const ZONE = 'zone';

    public const LOCATION_VALID_LOCATIONS = 'valid_locations';
    public const LOCATION_INVALID_LOCATIONS = 'invalid_locations';

    public const EVENT = 'event';

    public const INTENT = 'intent';
    public const INTENT_RETIRE = 'retire';
    public const INTENT_DISCARD = 'discard';

//    const OPTION_CONDITIONS = 'conditions';
//    const OPTION_EFFECTS = 'effects';
//    const OPTION_CHOICE = 'choice';
//    const OPTION_LABEL = 'label';
//    const OPTION_DECK_COUNT = 'deck_count';

//
//    const OPTION_TRIGGER = 'trigger';
//    const OPTION_TRIGGER_ANY = 'any';
}
