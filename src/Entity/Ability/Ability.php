<?php
declare(strict_types=1);

namespace App\Entity\Ability;

class Ability
{
    public const TYPE_AUTO = 'auto';
    public const TYPE_ACT = 'act';
    public const TYPE_CONT = 'cont';

    private const ID = 'id';
    private const NAME = 'name';
    private const TYPE = 'type';
    private const EVENT = 'event';
    private const CONDITIONS = 'conditions';
    private const COSTS = 'costs';
    private const EFFECTS = 'effects';
    private const PLAYER_HASH = 'player_hash';
    private const SELF_CARD_ID = 'self_card_id';
    private const EVENT_CARD_ID = 'event_card_id';

    private $data;

    public function __construct(
        array $data = null
    ) {
        $this->data = $data ?? [];
    }

    public function setType(string $type): self
    {
        $this->data[self::TYPE] = $type;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->data[self::TYPE] ?? null;
    }

    public function setEvent(?string $event): self
    {
        $this->data[self::EVENT] = $event;

        return $this;
    }

    public function getEvent(): ?string
    {
        return $this->data[self::EVENT];
    }

    public function setId(string $id): self
    {
        $this->data[self::ID] = $id;

        return $this;
    }

    public function getId(): ?string
    {
        return $this->data[self::ID] ?? null;
    }

    public function setName(string $name): self
    {
        $this->data[self::NAME] = $name;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->data[self::NAME] ?? null;
    }

    public function setPlayerHash(string $playerHash): self
    {
        $this->data[self::PLAYER_HASH] = $playerHash;

        return $this;
    }

    public function getPlayerHash(): ?string
    {
        return $this->data[self::PLAYER_HASH] ?? null;
    }

    public function setSelfCardId(string $id): self
    {
        $this->data[self::SELF_CARD_ID] = $id;

        return $this;
    }

    public function getSelfCardId(): ?string
    {
        return $this->data[self::SELF_CARD_ID] ?? null;
    }

    public function setEventCardId(string $id): self
    {
        $this->data[self::EVENT_CARD_ID] = $id;

        return $this;
    }

    public function getEventCardId(): ?string
    {
        return $this->data[self::EVENT_CARD_ID] ?? null;
    }

    public function setConditions(array $conditions): self
    {
        $this->data[self::CONDITIONS] = $conditions;

        return $this;
    }

    /**
     * @param AbilityPart|array $condition
     * @return $this
     */
    public function addCondition($condition): self
    {
        $this->data[self::CONDITIONS][] = $condition instanceof AbilityPart ? $condition->getData() : $condition;

        return $this;
    }

    public function getConditions(): array
    {
        return $this->data[self::CONDITIONS] ?? [];
    }

    public function setCosts(array $costs): self
    {
        $this->data[self::COSTS] = $costs;

        return $this;
    }

    /**
     * @param AbilityPart|array $cost
     * @return $this
     */
    public function addCost($cost): self
    {
        $this->data[self::COSTS][] = $cost instanceof AbilityPart ? $cost->getData() : $cost;

        return $this;
    }

    public function getCosts(): array
    {
        return $this->data[self::COSTS] ?? [];
    }

    public function setEffects(array $effects): self
    {
        $this->data[self::EFFECTS] = $effects;

        return $this;
    }

    /**
     * @param AbilityPart|array $effect
     * @return $this
     */
    public function addEffect($effect): self
    {
        $this->data[self::EFFECTS][] = $effect instanceof AbilityPart ? $effect->getData() : $effect;

        return $this;
    }

    public function getEffects(): array
    {
        return $this->data[self::EFFECTS] ?? [];
    }

    public function getData(): array
    {
        return $this->data;
    }
}
