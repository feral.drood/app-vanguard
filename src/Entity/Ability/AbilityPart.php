<?php

declare(strict_types=1);

namespace App\Entity\Ability;

class AbilityPart
{
    private const TYPE = 'type';
    private const PLAYER_HASH = 'player_hash';
    private const OPTIONS = 'options';
    private const SELF_CARD = 'self_card';
    private const EVENT_CARD = 'event_card';

    private $data;

    public function __construct(
        array $data = null
    ) {
        $this->data = $data ?? [];
    }

    public function setType(string $type): self
    {
        $this->data[self::TYPE] = $type;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->data[self::TYPE] ?? null;
    }

    public function getSelfCardId(): ?string
    {
        return $this->data[self::SELF_CARD] ?? null;
    }

    public function setSelfCardId(?string $selfCardId): self
    {
        $this->data[self::SELF_CARD] = $selfCardId;

        return $this;
    }

    public function getEventCardId(): ?string
    {
        return $this->data[self::EVENT_CARD] ?? null;
    }

    public function setEventCardId(?string $eventCardId): self
    {
        $this->data[self::EVENT_CARD] = $eventCardId;

        return $this;
    }

    public function setPlayerHash(?string $playerHash): self
    {
        $this->data[self::PLAYER_HASH] = $playerHash;

        return $this;
    }

    public function getPlayerHash(): ?string
    {
        return $this->data[self::PLAYER_HASH] ?? null;
    }

    public function setOptions(array $options): self
    {
        $this->data[self::OPTIONS] = $options;

        return $this;
    }

    /**
     * @param string $option
     * @param mixed $value
     *
     * @return $this
     */
    public function addOption(string $option, $value): self
    {
        if (!isset($this->data[self::OPTIONS])) {
            $this->data[self::OPTIONS] = [];
        }
        $this->data[self::OPTIONS][$option] = $value;

        return $this;
    }

    /**
     * @param string $option
     *
     * @return mixed
     */
    public function getOption(string $option)
    {
        return $this->data[self::OPTIONS][$option] ?? null;
    }

    public function getOptions(): array
    {
        return $this->data[self::OPTIONS] ?? [];
    }

    public function getData(): array
    {
        return $this->data;
    }
}
