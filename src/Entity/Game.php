<?php

declare(strict_types=1);

namespace App\Entity;

use App\Entity\Game\State;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GameRepository")
 */
class Game
{
    const STATUS_ACTIVE = 'active';
    const STATUS_DONE = 'done';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $status;

    /**
     * @ORM\Column(type="integer")
     */
    private $user1Id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $user2Id;

    /**
     * @ORM\Column(type="game_state")
     */
    private $state;

    public function __construct()
    {
        $this->status = self::STATUS_ACTIVE;
        $this->state = new State();
        $this->user1Id = null;
        $this->user2Id = null;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getState(): State
    {
        return $this->state;
    }

    public function setState(State $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getUser1Id(): int
    {
        return $this->user1Id;
    }

    public function setUser1Id(int $userId): self
    {
        $this->user1Id = $userId;

        return $this;
    }

    public function getUser2Id(): ?int
    {
        return $this->user2Id;
    }

    public function setUser2Id(int $userId = null): self
    {
        $this->user2Id = $userId;

        return $this;
    }
}
