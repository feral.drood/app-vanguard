<?php
declare(strict_types=1);

namespace App\Entity;

class PlayerInput
{
    private $playerHash;
    private $data;

    public function __construct(
        string $playerHash,
        array $data
    ) {
        $this->playerHash = $playerHash;
        $this->data = $data;
    }

    public function getPlayerHash(): string
    {
        return $this->playerHash;
    }

    public function getValue(): ?string
    {
        if (count($this->data) > 0) {
            $value = array_values($this->data);
            while (is_array($value)) {
                $value = array_shift($value);
            }

            return $value;
        }

        return null;
    }

    public function getList(): array
    {
        return $this->data;
    }
}
