<?php
declare(strict_types=1);

namespace App\Entity\Game;

class PlayerError
{
    private $playerHash;
    private $message;

    public function __construct(
        string $playerHash,
        string $message
    ) {
        $this->playerHash = $playerHash;
        $this->message = $message;
    }

    public function getPlayerHash(): string
    {
        return $this->playerHash;
    }

    public function setPlayerHash(string $playerHash): PlayerError
    {
        $this->playerHash = $playerHash;
        return $this;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setMessage(string $message): PlayerError
    {
        $this->message = $message;
        return $this;
    }
}
