<?php
declare(strict_types=1);

namespace App\Entity\Game;

class Deck extends CardList
{
    const TOP = 'top';
    const BOTTOM = 'bottom';

    /**
     * @param int $count
     * @return Card[]
     */
    public function drawCards(int $count): array
    {
        return array_splice($this->cards, 0, $count);
    }

    public function addCard(Card $card, string $location = self::TOP)
    {
        if ($location === self::TOP) {
            array_unshift($this->cards, $card);
        } else {
            array_push($this->cards, $card);
        }
    }

    public function addCards(array $cards, string $location = self::TOP): void
    {
        foreach ($cards as $card) {
            $this->addCard($card, $location);
        }
    }

    public function shuffle()
    {
        // shuffle for the shuffle god!
        shuffle($this->cards);
        shuffle($this->cards);
        shuffle($this->cards);

        return $this;
    }
}
