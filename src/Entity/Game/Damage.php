<?php
declare(strict_types=1);

namespace App\Entity\Game;

class Damage extends CardList
{
    public function serializeForUpdate(): array
    {
        $data = [];
        foreach ($this->getCards() as $card) {
            $data[$card->getId()] = $card->serializeForUpdate(Card::ZONE_DAMAGE);
        }
        return $data;
    }
}
