<?php
declare(strict_types=1);

namespace App\Entity\Game;

class Player
{
    const PROPERTY_CANNOT_CALL_SENTINEL_FROM_HAND_TO_GC = 'cannot_call_sentinel_from_hand_to_gc';
    const PROPERTY_MUST_CALL_TWO_GUARDIANS_FROM_HAND_TO_GC = 'must_call_two_guardians_from_hand_to_gc';

    private $id;
    private $name;
    private $avatar;
    private $deck;
    private $drop;
    private $soul;
    private $bind;
    private $hand;
    private $field;
    private $damage;
    private $trigger;
    private $giftTypes; // accel => accel1, force => force2,
    private $giftCircleCount; // force => vanguard, rearguard1, accel => rearguard3
    private $giftCircleType;
    private $properties;
    private $buffer;
    private $guardians;

    public function __construct()
    {
        $this->deck = new Deck();
        $this->hand = new Hand();
        $this->drop = new Drop();
        $this->soul = new Deck();
        $this->bind = new Deck();
        $this->damage = new Damage();
        $this->field = new Field();
        $this->giftTypes = [];
        $this->giftCircleCount = [];
        $this->giftCircleType = [];
        $this->properties = [];
        $this->buffer = new Buffer();
        $this->guardians = new Guardians();
        $this->tokenId = 0;
    }

    public function __clone () {
        $this->deck = clone $this->deck;
        $this->hand = clone $this->hand;
        $this->drop = clone $this->drop;
        $this->soul = clone $this->soul;
        $this->bind = clone $this->bind;
        $this->damage = clone $this->damage;
        $this->field = clone $this->field;
        if ($this->trigger !== null) {
            $this->trigger = clone $this->trigger;
        }
        $this->buffer = clone $this->buffer;
        $this->guardians = clone $this->guardians;
    }

    public function setHash(string $id): Player
    {
        $this->id = $id;
        return $this;
    }

    public function getHash(): ?string
    {
        return $this->id;
    }

    public function getDeck(): Deck
    {
        return $this->deck;
    }

    public function getDrop(): Drop
    {
        return $this->drop;
    }

    public function getHand(): Hand
    {
        return $this->hand;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): Player
    {
        $this->name = $name;
        return $this;
    }

    public function setAvatar(string $avatar = null): Player
    {
        $this->avatar = $avatar;
        return $this;
    }

    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    public function getSoul(): Deck
    {
        return $this->soul;
    }

    public function getBind(): Deck
    {
        return $this->bind;
    }

    public function getField(): Field
    {
        return $this->field;
    }

    public function getGuardians(): Guardians
    {
        return $this->guardians;
    }

    public function getNextTokenId(): int
    {
        $this->tokenId++;

        return $this->tokenId;
    }

    /**
     * Add additional rearguard circle (Accel Gift)
     */
//    public function addCircle(): string
//    {
//        $circle = self::REARGUARD . count($this->field);
//        $this->field[$circle] = null;
//
//        return $circle;
//    }

    /**
     * Get unit positioned at varguard/rearguard1/rearguard2/.. circles
     * @param string $circle
     * @return Card
     */
//    public function getCircle(string $circle): ?Card
//    {
//        return $this->field[$circle];
//    }

    /**
     * @param $circle
     * @return bool
     */
//    public function circleExists($circle): bool
//    {
//        return array_key_exists($circle, $this->field);
//    }

//    public function isFrontRow($circleId): bool
//    {
//        return !in_array($circleId, [self::REARGUARD2, self::REARGUARD3, self::REARGUARD4], true);
//    }

//    public function findCircle($cardId): ?string
//    {
//        foreach ($this->field as $circle => $unit) {
//            if ($unit !== null && $unit->getId() === $cardId) {
//                return $circle;
//            }
//        }
//        return null;
//    }

    /**
     * @param string $circle
     * @param Card $card
     * @return $this
     */
//    public function setCircle($circle, $card)
//    {
//        $this->field[$circle] = $card;
//        return $this;
//    }

    /**
     * @return Card[]
     */
//    public function getField()
//    {
//        return $this->field;
//    }

    /**
     * @param array $field
     * @return $this
     */
//    public function setField($field)
//    {
//        $this->field = $field;
//        return $this;
//    }

    public function getDamage(): Damage
    {
        return $this->damage;
    }

    public function getTrigger(): ?Card
    {
        return $this->trigger;
    }

    public function setTrigger(Card $trigger = null): Player
    {
        $this->trigger = $trigger;
        return $this;
    }

    public function setGiftType(string $gift, ?string $type): self
    {
        $this->giftTypes[$gift] = $type;

        return $this;
    }

    public function getGiftType(string $gift): ?string
    {
        return $this->giftTypes[$gift] ?? null;
    }

    public function getGiftCircles(): array
    {
        return $this->giftCircleType;
    }

    public function addGiftCircle(string $gift, string $location): self
    {
        $this->giftCircleType[$location] = $gift;
        $this->giftCircleCount[$location] = $this->giftCircleCount[$location] ?? 0;
        $this->giftCircleCount[$location]++;

        return $this;
    }

    public function getGiftCircleType(string $location): ?string
    {
        return $this->giftCircleType[$location] ?? null;
    }

    public function getGiftCircleCount(string $location): ?int
    {
        return $this->giftCircleCount[$location] ?? 0;
    }

    public function addProperty(string $property): Player
    {
        $this->properties[] = $property;
        return $this;
    }

    public function clearProperties(): void
    {
        $this->properties = [];
    }

    public function hasProperty(string $property): bool
    {
        return in_array($property, $this->properties, true);
    }

    public function getBuffer(): Buffer
    {
        return $this->buffer;
    }

    public function getCardById(string $cardId): ?Card
    {
        if (($card = $this->deck->getCardById($cardId)) !== null) {
            return $card;
        }
        if (($card = $this->drop->getCardById($cardId)) !== null) {
            return $card;
        }
        if (($card = $this->hand->getCardById($cardId)) !== null) {
            return $card;
        }
        if (($card = $this->soul->getCardById($cardId)) !== null) {
            return $card;
        }
        if (($card = $this->bind->getCardById($cardId)) !== null) {
            return $card;
        }
        if (($card = $this->damage->getCardById($cardId)) !== null) {
            return $card;
        }
        if (($card = $this->field->getCardById($cardId)) !== null) {
            return $card;
        }
        if (($card = $this->field->getCircleCard($cardId)) !== null) {
            return $card;
        }
        if (($card = $this->field->getCardById($cardId)) !== null) {
            return $card;
        }
        if (($card = $this->buffer->getCardById($cardId)) !== null) {
            return $card;
        }
        if ($this->getTrigger() !== null && $this->getTrigger()->getId() === $cardId) {
            return $this->getTrigger();
        }

        return $this->guardians->getCardById($cardId);
    }

    public function getZoneByCardId(string $cardId): ?string
    {
        if ($this->deck->getCardById($cardId) !== null) {
            return Card::ZONE_DECK;
        }
        if ($this->drop->getCardById($cardId) !== null) {
            return Card::ZONE_DROP;
        }
        if ($this->hand->getCardById($cardId) !== null) {
            return Card::ZONE_HAND;
        }
        if ($this->soul->getCardById($cardId) !== null) {
            return Card::ZONE_SOUL;
        }
        if ($this->bind->getCardById($cardId) !== null) {
            return Card::ZONE_BIND;
        }
        if ($this->damage->getCardById($cardId) !== null) {
            return Card::ZONE_DAMAGE;
        }
        if ($this->guardians->getCardById($cardId) !== null) {
            return Card::ZONE_GUARDIAN;
        }
        if ($this->buffer->getCardById($cardId) !== null) {
            return Card::ZONE_BUFFER;
        }
        return $this->field->getCircleByCardId($cardId);
    }

    public function serializeForDatabase()
    {
        $data = [
            'id' => $this->getHash(),
            'name' => $this->getName(),
            'deck' => $this->getDeck()->serializeForDatabase(),
            'hand' => $this->getHand()->serializeForDatabase(),
            'drop' => $this->getDrop()->serializeForDatabase(),
            'soul' => $this->getSoul()->serializeForDatabase(),
            'bind' => $this->bind->serializeForDatabase(),
            'damage' => $this->getDamage()->serializeForDatabase(),
            'field' => $this->field->serializeForDatabase(),
            'guardians' => $this->guardians->serializeForDatabase(),
            'buffer' => $this->buffer->serializeForDatabase(),
            'gift_types' => $this->giftTypes,
            'gift_circle_type' => $this->giftCircleType,
            'gift_circle_count' => $this->giftCircleCount,
        ];
        if ($this->tokenId > 0) {
            $data['token_id'] = $this->tokenId;
        }
        if ($this->avatar !== null) {
            $data['avatar'] = $this->avatar;
        }
        if ($this->getTrigger() !== null) {
            $data['trigger'] = $this->getTrigger()->serializeForDatabase();
        }
        if (count($this->properties) > 0) {
            $data['properties'] = $this->properties;
        }

        return $data;
    }

    public function deserializeFromDatabase($data)
    {
        $this->id = $data['id'];
        $this->name = $data['name'];
        if (array_key_exists('avatar', $data)) {
            $this->avatar = $data['avatar'];
        }
        $this->giftTypes = $data['gift_types'];
        $this->giftCircleType = $data['gift_circle_type'] ?? [];
        $this->giftCircleCount = $data['gift_circle_count'] ?? [];
        $this->deck->deserializeFromDatabase($data['deck']);
        $this->hand->deserializeFromDatabase($data['hand']);
        $this->drop->deserializeFromDatabase($data['drop']);
        $this->soul->deserializeFromDatabase($data['soul']);
        $this->bind->deserializeFromDatabase($data['bind'] ?? []);
        $this->damage->deserializeFromDatabase($data['damage']);
        $this->field->deserializeFromDatabase($data['field']);
        $this->guardians->deserializeFromDatabase($data['guardians']);
        $this->buffer->deserializeFromDatabase($data['buffer']);
        if (isset($data['token_id'])) {
            $this->tokenId = $data['token_id'];
        }
        if (isset($data['trigger'])) {
            $this->trigger = (new Card())->deserializeFromDatabase($data['trigger']);
        }
        if (isset($data['properties'])) {
            $this->properties = $data['properties'];
        }

        return $this;
    }
}
