<?php
declare(strict_types=1);

namespace App\Entity\Game;

class Field
{
    const VANGUARD = 'vanguard';
    const REARGUARD = 'rearguard';
    const REARGUARD1 = 'rearguard1';
    const REARGUARD2 = 'rearguard2';
    const REARGUARD3 = 'rearguard3';
    const REARGUARD4 = 'rearguard4';
    const REARGUARD5 = 'rearguard5';

    private $circles;

    public function __construct()
    {
        $this->circles = [
            self::VANGUARD => null,
            self::REARGUARD1 => null,
            self::REARGUARD2 => null,
            self::REARGUARD3 => null,
            self::REARGUARD4 => null,
            self::REARGUARD5 => null,
        ];
    }

    public function __clone()
    {
        foreach ($this->circles as $circle => $card) {
            if ($card !== null) {
                $this->circles[$circle] = clone $card;
            }
        }
    }

    /**
     * @return Card[]
     */
    public function getCircles(): array
    {
        return $this->circles;
    }

    public function getVanguard(): ?Card
    {
        return $this->circles[self::VANGUARD];
    }

    /**
     * Add additional rearguard circle (Accel Gift)
     */
    public function addCircle(): string
    {
        $circle = self::REARGUARD . count($this->circles);
        $this->circles[$circle] = null;

        return $circle;
    }

    public function getCircleCard(string $circle): ?Card
    {
        if (array_key_exists($circle, $this->circles)) {
            return $this->circles[$circle];
        }

        return $this->getCardById($circle);
    }

    public function circleExists(string $circle): bool
    {
        return array_key_exists($circle, $this->circles);
    }

    public function isFrontRow(string $circleId): bool
    {
        return !in_array($circleId, [self::REARGUARD2, self::REARGUARD3, self::REARGUARD4], true);
    }

    public function getCircleByCardId(string $cardId): ?string
    {
        foreach ($this->circles as $circle => $unit) {
            if ($unit !== null && $unit->getId() === $cardId) {
                return $circle;
            }
        }

        return null;
    }

    public function getCardById(string $cardId): ?Card
    {
        /** @var Card $card */
        foreach ($this->circles as $card) {
            if ($card !== null && $card->getId() === $cardId) {
                return $card;
            }
//            if (count($card->getEquipGauge()) > 0) {
//                foreach ($card->getEquipGauge() as $equipCard) {
//                    if ($equipCard->getId() === $cardId) {
//                        return $equipCard;
//                    }
//                }
//            }
        }

        return null;
    }


    public function setCircleCard(string $circle, Card $card = null): Field
    {
        $this->circles[$circle] = $card;

        return $this;
    }

    public function serializeForDatabase(): array
    {
        $data = [];

        /**
         * @var string $location
         * @var Card $card
         */
        foreach ($this->circles as $circle => $card) {
            $data[$circle] = ($card === null ? null : $card->serializeForDatabase());
        }

        return $data;
    }

    public function deserializeFromDatabase(array $data)
    {
        foreach ($data as $circle => $cardData) {
            $this->circles[$circle] = ($cardData === null ? null : (new Card())->deserializeFromDatabase($cardData));
        }
    }
}
