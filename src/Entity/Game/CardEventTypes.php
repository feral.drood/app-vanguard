<?php
declare(strict_types=1);

namespace App\Entity\Game;

final class CardEventTypes
{
    public const EVENT_PREFIX = 'game.';

    public const BEFORE_RIDE = 'before_ride';
    public const AFTER_RIDE = 'after_ride';

    public const BEFORE_CALL = 'before_call';
    public const AFTER_CALL = 'after_call';

    public const BEFORE_CALL_FROM_DECK = 'before_call_from_deck';
    public const AFTER_CALL_FROM_DECK = 'after_call_from_deck';

    public const BEFORE_CALL_FROM_SOUL = 'before_call_from_soul';
    public const AFTER_CALL_FROM_SOUL = 'after_call_from_soul';

    public const BEFORE_CALL_FROM_HAND = 'before_call_from_hand';
    public const AFTER_CALL_FROM_HAND = 'after_call_from_hand';

    public const BEFORE_PLACE = 'before_place';
    public const AFTER_PLACE = 'after_place';

    public const BEFORE_PLACE_FROM_DECK = 'before_place_from_deck';
    public const AFTER_PLACE_FROM_DECK = 'after_place_from_deck';

    public const BEFORE_PLACE_FROM_SOUL = 'before_place_from_soul';
    public const AFTER_PLACE_FROM_SOUL = 'after_place_from_soul';

    public const BEFORE_PLACE_FROM_HAND = 'before_place_from_hand';
    public const AFTER_PLACE_FROM_HAND = 'after_place_from_hand';

    public const BEFORE_RETIRE = 'before_retire';
    public const AFTER_RETIRE = 'after_retire';

    public const BEFORE_DRIVE_CHECK = 'before_drive_check';
    public const AFTER_DRIVE_CHECK = 'after_drive_check';

    public const BEFORE_DAMAGE_CHECK = 'before_damage_check';
    public const AFTER_DAMAGE_CHECK = 'after_damage_check';

    public const BEFORE_ATTACK = 'before_attack';
    public const BEFORE_BOOST = 'before_boost';

    public const AFTER_HIT = 'after_hit';
    public const AFTER_MISS = 'after_miss';
    public const AFTER_ATTACK = 'after_attack';

    public const AFTER_LOOK = 'after_look';
    public const AFTER_REVEAL = 'after_reveal';
    public const AFTER_SEARCH = 'after_search';

//    public const BEFORE_DRAW = 'before_draw';
    public const AFTER_DRAW = 'after_draw';

    public const PHASE_BATTLE_START = 'phase_battle_start';
    public const PHASE_END_START = 'phase_end_start';

//    const TYPE_ON_ATTACK_OTHER = 'on_attack_other';
//    const TYPE_ON_BOOST = 'on_boost';
//    const TYPE_ON_DRAW = 'on_draw';
//    const TYPE_ON_STAND = 'on_stand';
//    const TYPE_ON_STAND_OTHER = 'on_stand_other';
//    const TYPE_ON_HIT = 'on_hit';
//    const TYPE_ON_HIT_OTHER = 'on_hit_other';
//    const TYPE_ON_MISS = 'on_miss';
//    const TYPE_ON_MISS_OTHER = 'on_miss_other';
//    const TYPE_ON_DRIVE_CHECK = 'on_drive_check';
//    const TYPE_ON_DAMAGE_CHECK = 'on_damage_check';
//    const TYPE_ON_SEARCH = 'on_search';
//    const TYPE_ON_REARGUARD_RETIRE = 'on_rearguard_retire';
//    const TYPE_ON_REARGUARD_RETIRE_OTHER = 'on_rearguard_retire_other';
//    const TYPE_ON_OPPONENT_REARGUARD_RETIRE = 'on_opponent_rearguard_retire';
//    const TYPE_ON_EQUIP_GAUGE_RETIRE = 'on_equip_gauge_retire';
}
