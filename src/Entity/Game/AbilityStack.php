<?php

declare(strict_types=1);

namespace App\Entity\Game;

use App\Entity\Ability\Ability;
use App\Entity\Ability\AbilityPart;

class AbilityStack
{
    private const QUEUE = 'queue';
    private const COSTS = 'costs';
    private const EFFECTS = 'effects';
    private const USED_ABILITIES = 'used_abilities';

    private $queue = [[]];
    private $costs = [];
    private $effects = [];
    private $usedAbilities = [];

    /**
     * @param Ability|array $ability
     * @return self
     */
    public function addAbility($ability): self
    {
        $last = count($this->queue) - 1;
        $this->queue[$last][] = $ability instanceof Ability ? $ability->getData() : $ability;

        return $this;
    }

    public function getAbility(int $index): Ability
    {
        return new Ability($this->queue[0][$index]);
    }

    public function getAbilities(): array
    {
        while (count($this->queue) > 1 && count($this->queue[0]) === 0) {
            array_splice($this->queue,  0, 1);
        }

        return $this->queue[0];
    }

    public function removeAbility(int $index): void
    {
        $last = count($this->queue) - 1;
        if (count($this->queue[$last]) > 0) {
            $this->queue[] = [];
        }

        array_splice($this->queue[0], $index, 1);
    }

    /**
     * @param AbilityPart|array $cost
     * @return self
     */
    public function addCost($cost): self
    {
        $this->costs[] = $cost instanceof AbilityPart ? $cost->getData() : $cost;
        return $this;
    }

    public function setCosts(array $costs): self
    {
        $this->costs = $costs;
        return $this;
    }

    public function getCostCount(): int
    {
        return count($this->costs);
    }

    public function getCost(): ?AbilityPart
    {
        if (count($this->costs) > 0) {
            return new AbilityPart(array_slice($this->costs, 0, 1)[0]);
        }

        return null;
    }

    public function removeCost(): void
    {
        if (count($this->costs) > 0) {
            array_splice($this->costs, 0, 1);
        }
    }

    /**
     * @param AbilityPart|array $effect
     * @return AbilityStack
     */
    public function insertEffect($effect): self
    {
        $effectData = $effect instanceof AbilityPart ? $effect->getData() : $effect;

        if (count($this->effects) > 0) {
            array_splice($this->effects, 1, 0, [$effectData]);
        } else {
            $this->effects[] = $effectData;
        }

        return $this;
    }

    /**
     * @param AbilityPart|array $effect
     * @return AbilityStack
     */
    public function addEffect($effect): AbilityStack
    {
        $this->effects[] = $effect instanceof AbilityPart ? $effect->getData() : $effect;

        return $this;
    }

    public function setEffects(array $effects): AbilityStack
    {
        $this->effects = $effects;
        return $this;
    }

    public function getEffectCount(): int
    {
        return count($this->effects);
    }

    public function getEffect(): ?AbilityPart
    {
        if (count($this->effects) > 0) {
            return new AbilityPart(array_slice($this->effects, 0, 1)[0]);
        }
        return null;
    }

    public function removeEffect(): void
    {
        if (count($this->effects) > 0) {
            array_splice($this->effects, 0, 1);
        }
    }


    public function isEmpty(): bool
    {
        $abilityCount = 0;
        foreach ($this->queue as $group) {
            $abilityCount += count($group);
        }

        return
            count($this->costs) === 0
            && count($this->effects) === 0
            && $abilityCount === 0
        ;
    }

    public function isActionListEmpty(): bool
    {
        return count($this->costs) === 0 && count($this->effects) === 0;
    }

    public function isUsedAbility(string $abilityId): bool
    {
        return in_array($abilityId, $this->usedAbilities, true);
    }

    public function resetUsedAbilities(): void
    {
        $this->usedAbilities = [];
    }

    public function addUsedAbility(string $abilityId): AbilityStack
    {
        $this->usedAbilities[] = $abilityId;
        return $this;
    }

    public function serializeForDatabase(): array
    {
        return [
            self::QUEUE => $this->queue,
            self::COSTS => $this->costs,
            self::EFFECTS => $this->effects,
            self::USED_ABILITIES => $this->usedAbilities,
        ];
    }

    public function deserializeFromDatabase(array $data): AbilityStack
    {
        $this->queue = $data[self::QUEUE] ?? [];
        $this->costs = $data[self::COSTS] ?? [];
        $this->effects = $data[self::EFFECTS] ?? [];
        $this->usedAbilities = $data[self::USED_ABILITIES] ?? [];

        return $this;
    }

}
