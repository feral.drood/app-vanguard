<?php
declare(strict_types=1);

namespace App\Entity\Game;

class Hand extends CardList
{
    public function serializeForUpdate(): array
    {
        $data = [];
        foreach ($this->getCards() as $card) {
            if ($card->getName() === 'Protect') {
                $data[] = $card->serializeForUpdate(Card::ZONE_HAND);
            } else {
                $data[] = [
                    'image' => null,
                ];
            }
        }
        return $data;
    }
}
