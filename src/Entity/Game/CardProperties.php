<?php

declare(strict_types=1);

namespace App\Entity\Game;

final class CardProperties
{
    public const RACE_ASTRAL_DEITY = 'Astral Deity';

    public const CANNOT_ATTACK = 'cannot_attack';
    public const CANNOT_BE_ATTACKED = 'cannot_be_attacked';
    public const CANNOT_BE_TARGETED = 'cannot_be_targeted';
    public const CANNOT_BE_HIT = 'cannot_be_hit';
    public const CANNOT_BE_RETIRED_BY_OPPONENT = 'cannot_be_retired_by_opponent';
    public const CANNOT_BE_RETIRED_BY_CARD_ABILITIES = 'cannot_be_retired_by_card_abilities';
    public const CAN_ATTACK_ON_FIRST_TURN = 'can_attack_on_first_turn';
    public const ONE_GRADE_LOWER_TO_CALL = 'one_grade_lower_to_call';
    public const ASTRAL_PLANE_OPENED = 'astral_plane_opened';
    public const DISCARD_AS_GRADE3 = 'discard_as_grade3';
    public const CANNOT_RIDE = 'cannot_ride';
}
