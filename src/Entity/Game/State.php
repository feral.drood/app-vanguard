<?php
declare(strict_types=1);

namespace App\Entity\Game;

use App\Entity\Update\UpdateDataInterface;

class State
{

    /**
     * @var Player[]
     */
    private $players = [];

    /**
     * @var string
     */
    private $currentPlayer;

    /**
     * @var int
     */
    private $currentTurn;

    /**
     * @var string
     */
    private $phase;

    /**
     * @var string|null
     */
    private $nextPhase;

    /**
     * @var string|null
     */
    private $attacker;

    /**
     * @var string|null
     */
    private $defender;

    /**
     * @var string|null
     */
    private $booster;

    /**
     * @var AbilityStack
     */
    private $stack;

    /**
     * @var CardEventLog
     */
    private $cardEventLog;

    /**
     * @var array
     */
    private $chatLog;

    /**
     * @var PlayerError|null
     */
    private $playerError;

    /**
     * @var array
     */
    private $updateMessages = [];

    public function __construct()
    {
        $this->currentTurn = 0;
        $this->stack = new AbilityStack();
        $this->cardEventLog = new CardEventLog();
        $this->chatLog = [];
    }

    public function __clone () {
        foreach ($this->players as $key => $player) {
            $this->players[$key] = clone $player;
        }
        $this->stack = clone $this->stack;
    }

    public function addPlayer(Player $player): State
    {
        $this->players[] = $player;
        return $this;
    }

    /**
     * @return Player[]
     */
    public function getPlayers(): array
    {
        return $this->players;
    }

    public function getPlayer(string $userHash): ?Player
    {
        foreach ($this->players as $player) {
            if ($player->getHash() === $userHash) {
                return $player;
            }
        }
        return null;
    }

    public function getActivePlayer(): ?Player
    {
        if ($this->currentPlayer === null)
            return null;
        return $this->getPlayer($this->currentPlayer);
    }

    public function getActivePlayerHash(): ?string
    {
        return $this->currentPlayer;
    }

    public function getInactivePlayerHash(): ?string
    {
        return $this->getOpposingPlayerHash($this->currentPlayer);
    }

    public function getOpposingPlayer(string $playerHash): ?Player
    {
        foreach ($this->players as $player) {
            if ($player->getHash() !== $playerHash) {
                return $player;
            }
        }
        return null;
    }

    public function getOpposingPlayerHash(string $playerHash): ?string
    {
        foreach ($this->players as $player) {
            if ($player->getHash() !== $playerHash) {
                return $player->getHash();
            }
        }
        return null;
    }

    public function getInactivePlayer(): ?Player
    {
        foreach ($this->players as $player) {
            if ($player->getHash() !== $this->currentPlayer) {
                return $player;
            }
        }
        return null;
    }

    public function setActivePlayerHash(string $currentPlayer = null): State
    {
        $this->currentPlayer = $currentPlayer;
        return $this;
    }

    public function getPhase(): ?string
    {
        return $this->phase;
    }

    public function setPhase(string $phase = null): State
    {
        $this->phase = $phase;
        return $this;
    }

    public function getNextPhase(): ?string
    {
        return $this->nextPhase;
    }

    public function setNextPhase(?string $nextPhase): self
    {
        $this->nextPhase = $nextPhase;

        return $this;
    }

    public function getCurrentTurn(): int
    {
        return $this->currentTurn;
    }

    public function setCurrentTurn(int $currentTurn = null): State
    {
        $this->currentTurn = $currentTurn;
        return $this;
    }

    public function getAttacker(): ?string
    {
        return $this->attacker;
    }

    public function setAttacker(string $attacker = null): State
    {
        $this->attacker = $attacker;
        return $this;
    }

    public function getDefender(): ?string
    {
        return $this->defender;
    }

    public function setDefender(string $defender = null): State
    {
        $this->defender = $defender;
        return $this;
    }

    public function getBooster(): ?string
    {
        return $this->booster;
    }

    public function setBooster(string $booster = null): State
    {
        $this->booster = $booster;
        return $this;
    }

    public function addChatLog(string $log): State
    {
        $this->chatLog[] = $log;

        return $this;
    }

    public function getChatLog(): array
    {
        return $this->chatLog;
    }

    public function getLogCount(): int
    {
        return count($this->chatLog);
    }

    public function getCardEventLog(): CardEventLog
    {
        return $this->cardEventLog;
    }

//    public function isAttackSuccessful(): bool
//    {
//        if ($this->getInactivePlayer()->getCircle($this->getDefender())->hasProperty(Card::PROPERTY_CANNOT_BE_HIT)) {
//            return false;
//        }
//        $attackerPower = $this->getActivePlayer()->getCircle($this->getAttacker())->getPower();
//        $attackerPower += ($this->getBooster() !== null ? $this->getActivePlayer()->getCircle($this->getBooster())->getPower() : 0);
//        $defenderPower = $this->getInactivePlayer()->getCircle($this->getDefender())->getPower();
//        $defenderPower += $this->guardians->getShieldTotal();
//
//        return $attackerPower >= $defenderPower;
//    }

    public function getAbilityStack(): AbilityStack
    {
        return $this->stack;
    }

    public function setPlayerError(PlayerError $playerError = null): State
    {
        $this->playerError = $playerError;
        return $this;
    }

    public function getPlayerError(): ?PlayerError
    {
        return $this->playerError;
    }

    public function serializeForDatabase(): array
    {
        $output = [
            'players' => [],
            'current_turn' => $this->getCurrentTurn(),
            'current_player' => $this->getActivePlayerHash(),
            'phase' => $this->getPhase(),
            'next_phase' => $this->getNextPhase(),
            'attacker' => $this->getAttacker(),
            'defender' => $this->getDefender(),
            'booster' => $this->getBooster(),
            'stack' => $this->stack->serializeForDatabase(),
            'card_event_log' => $this->cardEventLog->serializeForDatabase(),
        ];
        if (count($this->chatLog) > 0) {
            $output['chat_log'] = $this->chatLog;
        }
        foreach ($this->getPlayers() as $player) {
            $output['players'][] = $player->serializeForDatabase();
        }

        return $output;
    }

    public function deserializeFromDatabase($data): State
    {
        $this
            ->setCurrentTurn($data['current_turn'])
            ->setActivePlayerHash($data['current_player'])
            ->setPhase($data['phase'])
            ->setNextPhase($data['next_phase'] ?? null)
            ->setAttacker($data['attacker'])
            ->setDefender($data['defender'])
            ->setBooster($data['booster'])
        ;
        foreach($data['players'] as $playerData) {
            $this->players[] = (new Player())->deserializeFromDatabase($playerData);
        }
        if (isset($data['stack'])) {
            $this->stack->deserializeFromDatabase($data['stack']);
        }
        if (isset($data['chat_log'])) {
            $this->chatLog = $data['chat_log'];
        }
        if (isset($data['card_event_log'])) {
            $this->cardEventLog->deserializeFromDatabase($data['card_event_log']);
        }

        return $this;
    }

    public function addUpdateMessage(UpdateDataInterface $message): self
    {
        $this->updateMessages[] = $message;

        return $this;
    }

    /**
     * @return UpdateDataInterface[]
     */
    public function getUpdateMessages(): array
    {
        return $this->updateMessages;
    }
}
