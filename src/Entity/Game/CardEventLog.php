<?php

declare(strict_types=1);

namespace App\Entity\Game;

class CardEventLog
{
    private $data;

    public function __construct(
        array $data = null
    ) {
        $this->data = $data ?? [];
    }

    public function clear(): void
    {
        $this->data = [];
    }

    public function addEvents(array $events): self
    {
        foreach ($events as $event) {
            $this->data[$event] = $this->data[$event] ?? 0;
            $this->data[$event]++;
        }

        return $this;
    }

    public function add(CardEvent $cardEvent): CardEventLog
    {
        $this->data[$cardEvent->getType()] = $this->data[$cardEvent->getType()] ?? 0;
        $this->data[$cardEvent->getType()]++;

        return $this;
    }

//    public function getFirst(string $type, string $playerHash = null): ?TurnEvent
//    {
//        foreach ($this->events as $event) {
//            if ($event[self::TYPE] === $type) {
//                if ($playerHash === null || $event[self::PLAYER_HASH] === $playerHash) {
//                    return (new TurnEvent())
//                        ->setType($event[self::TYPE])
//                        ->setPlayerHash($event[self::PLAYER_HASH])
//                        ->setEventCardId($event[self::EVENT_CARD])
//                        ->setTargetCardIds($event[self::TARGET_CARDS])
//                    ;
//                }
//            }
//        }
//
//        return null;
//    }
//
//    public function getLast(string $type, string $playerHash = null): ?TurnEvent
//    {
//        $events = array_reverse($this->events);
//        foreach ($events as $event) {
//            if ($event[self::TYPE] === $type) {
//                if ($playerHash === null || $event[self::PLAYER_HASH] === $playerHash) {
//                    return (new TurnEvent())
//                        ->setType($event[self::TYPE])
//                        ->setPlayerHash($event[self::PLAYER_HASH])
//                        ->setEventCardId($event[self::EVENT_CARD])
//                        ->setTargetCardIds($event[self::TARGET_CARDS])
//                        ;
//                }
//            }
//        }
//
//        return null;
//    }

    public function getCount(string $type): ?int
    {
        return $this->data[$type] ?? null;
    }
//
//    public function count(string $type, string $playerHash = null): int
//    {
//        $count = 0;
//        foreach ($this->events as $event) {
//            if (
//                $event[self::TYPE] === $type
//                && ($playerHash === null || $event[self::PLAYER_HASH] === $playerHash)
//            ) {
//                $count++;
//            }
//        }
//
//        return $count;
//    }

    public function serializeForDatabase(): array
    {
        return $this->data;
    }

    public function deserializeFromDatabase(array $data): self
    {
        $this->data = $data;

        return $this;
    }
}
