<?php

declare(strict_types=1);

namespace App\Entity\Game;

class Card
{
    public const TYPE_NORMAL_UNIT = 'normal_unit';
    public const TYPE_TRIGGER_UNIT = 'trigger_unit';
    public const TYPE_TOKEN_UNIT = 'blitz_order';
    public const TYPE_NORMAL_ORDER = 'normal_order';
    public const TYPE_BLITZ_ORDER = 'blitz_order';
    public const TYPE_PROTECT = 'protect';

    const TRIGGER_CRITICAL = 'critical';
    const TRIGGER_DRAW = 'draw';
    const TRIGGER_HEAL = 'heal';
    const TRIGGER_FRONT = 'front';

    const PERFECT_SHIELD = 0;

    const GIFT_FORCE = 'force';
    const GIFT_PROTECT = 'protect';
    const GIFT_ACCEL = 'accel';

    const STATE_STAND = 'stand';
    const STATE_REST = 'rest';

    const BONUS_LENGTH_TURN = 'turn';
    const BONUS_LENGTH_BATTLE = 'battle';
    const BONUS_LENGTH_PERMANENT = 'permanent';

    const BONUS_POWER = 'power';
    const BONUS_CRITICAL = 'critical';
    const BONUS_SHIELD = 'shield';
    const BONUS_DRIVE = 'drive';
    const BONUS_PROPERTY = 'property';
    const BONUS_DAMAGE = 'damage';

    const ZONE_GUARDIAN = 'guardian';
    const ZONE_TRIGGER = 'trigger';
    const ZONE_DAMAGE = 'damage';
    const ZONE_VIEW = 'view';
    const ZONE_SOUL = 'soul';
    const ZONE_BIND = 'bind';
    const ZONE_EQUIP_GAUGE = 'equip_gauge';
    const ZONE_DROP = 'drop';
    const ZONE_DECK = 'deck';
    const ZONE_HAND = 'hand';
    const ZONE_FIELD = 'field';
    const ZONE_BUFFER = 'buffer';

    const SKILL_BOOST = 'boost';
    const SKILL_INTERCEPT = 'intercept';
    const SKILL_TWIN_DRIVE = 'twin-drive';
    const SKILL_TRIPLE_DRIVE = 'triple-drive';

    const PROPERTY_CANNOT_ATTACK = 'cannot_attack';
    const PROPERTY_CANNOT_BE_ATTACKED = 'cannot_be_attacked';
    const PROPERTY_CANNOT_BE_TARGETED = 'cannot_be_targeted';
    const PROPERTY_CANNOT_BE_HIT = 'cannot_be_hit';
    const PROPERTY_CANNOT_BE_RETIRED_BY_OPPONENT = 'cannot_be_retired_by_opponent';
    const PROPERTY_CAN_ATTACK_ON_FIRST_TURN = 'can_attack_on_first_turn';
    const PROPERTY_ONE_GRADE_LOWER_TO_CALL = 'one_grade_lower_to_call';

    private const ID = 'id';
    private const TYPE = 'type';
    private const NAME = 'name';
    private const DESCRIPTION = 'description';
    private const GRADE = 'grade';
    private const SKILL = 'skill';
    private const POWER = 'power';
    private const CRITICAL = 'critical';
    private const SHIELD = 'shield';
    private const CLAN = 'clan';
    private const RACE = 'race';
    private const RACES = 'races';
    private const GIFT = 'gift';
    private const TRIGGER = 'trigger';
    private const IMAGE = 'image';
    private const ABILITIES = 'abilities';
    private const STATE = 'state';
    private const BONUS = 'bonus';
    private const ATTACKER = 'attacker';
    private const DEFENDER = 'defender';
    private const BOOSTER = 'booster';
    private const DELETED = 'deleted';
    private const FACE = 'face';

    private $data;

    public function __construct(
        array $data = null
    ) {
        $this->data = $data ?? [];
        $this->data[self::BONUS] = $this->data[self::BONUS] ?? [];
    }

    public function setType(string $type = null): self
    {
        $this->data[self::TYPE] = $type;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->data[self::TYPE];
    }

    public function getId(): string
    {
        return $this->data[self::ID];
    }

    public function setId(string $id): self
    {
       $this->data[self::ID] = $id;

       return $this;
    }

    public function getName(): string
    {
        return $this->data[self::NAME];
    }

    public function setName(string $name): self
    {
        $this->data[self::NAME] = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->data[self::DESCRIPTION] ?? null;
    }

    public function setDescription(string $description = null): self
    {
        $this->data[self::DESCRIPTION] = $description;

        return $this;
    }

    public function getGrade(): ?int
    {
        return $this->data[self::GRADE] ?? null;
    }

    public function setGrade(int $grade = null): self
    {
        $this->data[self::GRADE] = $grade;

        return $this;
    }

    public function getDrive(): int
    {
        $skill = $this->data[self::SKILL] ?? null;
        switch ($skill) {
            case self::SKILL_BOOST:
            case self::SKILL_INTERCEPT:
                $drive = 1;
                break;
            case self::SKILL_TWIN_DRIVE:
                $drive = 2;
                break;
            case self::SKILL_TRIPLE_DRIVE:
                $drive = 3;
                break;
            default:
                $drive = 0;
        }

        foreach ($this->data[self::BONUS] as $length => $bonus) {
            if (array_key_exists(self::BONUS_DRIVE, $bonus)) {
                $drive += $bonus[self::BONUS_DRIVE];
            }
        }

        if ($drive < 0) {
            $drive = 0;
        }

        return $drive;
    }

    public function getPower(): ?int
    {
        $power = $this->data[self::POWER] ?? null;
        if (
            $this->isDeleted()
            && $power !== null
        ) {
            $power = 0;
        }
        foreach ($this->data[self::BONUS] as $length => $bonus) {
            if (array_key_exists(self::BONUS_POWER, $bonus)) {
                $power += $bonus[self::BONUS_POWER];
            }
        }

        return $power;
    }

    public function setPower(?int $power): self
    {
        $this->data[self::POWER] = $power;

        return $this;
    }

    public function getCritical(): ?int
    {
        $critical = $this->data[self::CRITICAL] ?? null;
        foreach ($this->data[self::BONUS] as $length => $bonus) {
            if (array_key_exists(self::BONUS_CRITICAL, $bonus)) {
                $critical += $bonus[self::BONUS_CRITICAL];
            }
        }

        return $critical;
    }

    public function setCritical(?int $critical): self
    {
        $this->data[self::CRITICAL] = $critical;

        return $this;
    }

    public function getShield(): ?int
    {
        $shield = $this->data[self::SHIELD] ?? null;
        foreach ($this->data[self::BONUS] as $length => $bonus) {
            if (array_key_exists(self::BONUS_SHIELD, $bonus)) {
                $shield += $bonus[self::BONUS_SHIELD];
            }
        }

        return $shield;
    }

    public function setShield(?int $shield): self
    {
        $this->data[self::SHIELD] = $shield;

        return $this;
    }

    public function getGift(): ?string
    {
        return $this->data[self::GIFT] ?? null;
    }

    public function setGift(?string $gift): self
    {
        $this->data[self::GIFT] = $gift;

        return $this;
    }

    public function getTrigger(): ?string
    {
        return $this->data[self::TRIGGER] ?? null;
    }

    public function setTrigger(string $trigger = null): self
    {
        $this->data[self::TRIGGER] = $trigger;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->data[self::IMAGE] ?? null;
    }

    public function setImage(string $image = null): self
    {
        $this->data[self::IMAGE] = $image;

        return $this;
    }

    public function getClan(): ?string
    {
        return $this->data[self::CLAN] ?? null;
    }

    public function setClan(?string $clan): self
    {
        $this->data[self::CLAN] = $clan;

        return $this;
    }

    public function getState(): string
    {
        return $this->data[self::STATE] ?? self::STATE_STAND;
    }

    public function getDamage(): int
    {
        $damage = $this->getCritical();
        foreach ($this->data[self::BONUS] as $length => $bonus) {
            if (array_key_exists(self::BONUS_DAMAGE, $bonus)) {
                $damage += $bonus[self::BONUS_DAMAGE];
            }
        }

        return $damage;
    }

    public function setState(string $state): self
    {
        $this->data[self::STATE] = $state;

        return $this;
    }

    public function isAttacker(): bool
    {
        return $this->data[self::ATTACKER] ?? false;
    }

    public function setAttacker(bool $attacker): self
    {
        $this->data[self::ATTACKER] = $attacker;

        return $this;
    }

    public function isDefender(): bool
    {
        return $this->data[self::DEFENDER] ?? false;
    }

    public function setDefender(bool $defender): self
    {
        $this->data[self::DEFENDER] = $defender;

        return $this;
    }

    public function isBooster(): bool
    {
        return $this->data[self::BOOSTER] ?? false;
    }

    public function setBooster(bool $booster): self
    {
        $this->data[self::BOOSTER] = $booster;

        return $this;
    }

    public function addBonus(string $length, string $type, int $value): self
    {
        if (!isset($this->data[self::BONUS][$length])) {
            $this->data[self::BONUS][$length] = [];
        }
        if (!isset($this->data[self::BONUS][$length][$type])) {
            $this->data[self::BONUS][$length][$type] = $value;
        } else {
            $this->data[self::BONUS][$length][$type] += $value;
        }

        return $this;
    }

    public function resetBonus(string $length): void
    {
        if (array_key_exists($length, $this->data[self::BONUS])) {
            unset($this->data[self::BONUS][$length]);
        }
    }

    public function addBonusProperty(string $property): self
    {
        if (!isset($this->data[self::BONUS][self::BONUS_LENGTH_PERMANENT])) {
            $this->data[self::BONUS][self::BONUS_LENGTH_PERMANENT] = [];
        }
        if (!isset($this->data[self::BONUS][self::BONUS_LENGTH_PERMANENT][self::BONUS_PROPERTY])) {
            $this->data[self::BONUS][self::BONUS_LENGTH_PERMANENT][self::BONUS_PROPERTY] = [$property];
        } else {
            $this->data[self::BONUS][self::BONUS_LENGTH_PERMANENT][self::BONUS_PROPERTY][] = $property;
        }

        return $this;
    }

    public function hasProperty(string $property): bool
    {
        return
            isset($this->data[self::BONUS][self::BONUS_LENGTH_PERMANENT][self::BONUS_PROPERTY])
            && in_array($property, $this->data[self::BONUS][self::BONUS_LENGTH_PERMANENT][self::BONUS_PROPERTY], true)
        ;
    }

    public function isFaceDown(): bool
    {
        return $this->data[self::FACE] ?? false;
    }

    public function setFaceDown(bool $faceDown): self
    {
        $this->data[self::FACE] = $faceDown;

        return $this;
    }

    public function isDeleted(): bool
    {
        return $this->data[self::DELETED] ?? false;
    }

    public function setDeleted(bool $deleted): self
    {
        $this->data[self::DELETED] = $deleted;

        return $this;
    }

    public function reset(): void
    {
        $this->data[self::BONUS] = [];
        $this->data[self::ATTACKER] = false;
        $this->data[self::DEFENDER] = false;
        $this->data[self::BOOSTER] = false;
        $this->data[self::FACE] = false;
        $this->data[self::STATE] = self::STATE_STAND;
    }

    public function getSkill(): ?string
    {
        return $this->data[self::SKILL] ?? null;
    }

    public function setSkill(?string $skill): self
    {
        $this->data[self::SKILL] = $skill;

        return $this;
    }

    public function getRace(): ?string
    {
        return $this->data[self::RACE] ?? null;
    }

    public function setRace(?string $race): self
    {
        $races = explode(' / ', $race);
        if (count($races) > 1) {
            $this->data[self::RACES] = $races;
        } else {
            $this->data[self::RACE] = $race;
        }

        return $this;
    }

    public function getRaces(): array
    {
        return $this->data[self::RACES] ?? [];
    }

    public function getAbilities(): array
    {
        return $this->data[self::ABILITIES] ?? [];
    }

    public function hasAbilities(): bool
    {
        return count($this->data[self::ABILITIES] ?? []) > 0;
    }

    public function setAbilities(array $abilities): Card
    {
        $this->data[self::ABILITIES] = $abilities;

        return $this;
    }

    public function serializeForUpdate(string $zone = null): array
    {
//        } elseif ($zone === self::ZONE_GUARDIAN) {
//            return [
//                "id" => $this->getId(),
//                "name" => $this->getName(),
//                "description" => $this->getDescription(),
//                "image" => $this->getImage(),
//                "shield" => $this->getShield(),
//            ];
        if (in_array($zone, [self::ZONE_TRIGGER, self::ZONE_DAMAGE], true)) {
//        } elseif (in_array($zone, [self::ZONE_TRIGGER, self::ZONE_DAMAGE, self::ZONE_VIEW], true)) {
            return [
                "id" => $this->getId(),
                "name" => $this->getName(),
                "description" => $this->getDescription(),
                "image" => ($this->isFaceDown() ? null : $this->getImage()),
            ];
//            if (!$this->isFaceDown()) {
//                $output["image"] = $this->getImage();
//            }
//            return $output;
        } elseif ($this->isFaceDown()) {
            return [
                'grade' => $this->getGrade(),
                'image' => null,
            ];
        }
        $output = [
            "id" => $this->getId(),
            "name" => $this->getName(),
            "image" => null,
        ];
        if (!$this->isDeleted()) {
            $output['image'] = $this->getImage();
            $output['description'] = $this->getDescription();
        }
        if ($this->getSkill() !== null) {
            $output['skill'] = $this->getSkill();
        }
        if ($this->getGrade() !== null) {
            $output['grade'] = $this->getGrade();
        }
        if ($this->getPower() !== null) {
            $output['power'] = $this->getPower();
        }
        if ($this->getCritical() !== null) {
            $output['critical'] = $this->getCritical();
        }

        if ($this->getState() === Card::STATE_REST) {
            $output['state'] = $this->getState();
        }

        if ($this->getGift() !== null) {
            $output['gift'] = $this->getGift();
        }

        if ($this->getShield() !== null) {
            $output['shield'] = $this->getShield();
        }
        if ($this->isDefender()) {
            $output['defender'] = true;
        }
        if ($this->isAttacker()) {
            $output['attacker'] = true;
        }
        if ($this->isBooster()) {
            $output['booster'] = true;
        }

        return $output;
    }

    public function serializeForDatabase(): array
    {
        return $this->data;
    }

    public function deserializeFromDatabase(array $data): Card
    {
        $this->data = $data;

        return $this;
    }
}
