<?php

declare(strict_types=1);

namespace App\Entity\Game;

use Symfony\Contracts\EventDispatcher\Event;

class CardEvent extends Event
{
    private $state;
    private $type;
    private $playerHash;
    private $card;
    private $targetCards;

    public function __construct(
        State $state,
        string $type,
        string $playerHash,
        Card $card = null,
        array $targetCards = []
    ) {
        $this->state = $state;
        $this->type = $type;
        $this->playerHash = $playerHash;
        $this->card = $card;
        $this->targetCards = $targetCards;
    }

    public function getState(): State
    {
        return $this->state;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getPlayerHash(): string
    {
        return $this->playerHash;
    }

    public function setCard(?Card $card): self
    {
        $this->card = $card;

        return $this;
    }

    public function getCard(): ?Card
    {
        return $this->card;
    }

    /**
     * @param Card[] $targetCards
     * @return $this
     */
    public function setTargetCards(array $targetCards): self
    {
        $this->targetCards = $targetCards;

        return $this;
    }

    /**
     * @return Card[]
     */
    public function getTargetCards(): array
    {
        return $this->targetCards;
    }
}
