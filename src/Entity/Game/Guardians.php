<?php
declare(strict_types=1);

namespace App\Entity\Game;

class Guardians extends CardList
{
    public function getShieldTotal(): int
    {
        $shield = 0;
        foreach ($this->cards as $guardian) {
            $shield += $guardian->getShield();
        }
        return $shield;
    }

    public function serializeForUpdate(): array
    {
        $output = [];
        foreach ($this->getCards() as $card) {
            $output[$card->getId()] = $card->serializeForUpdate(Card::ZONE_GUARDIAN);
        }
        return $output;
    }
}
