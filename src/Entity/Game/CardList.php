<?php
declare(strict_types=1);

namespace App\Entity\Game;

class CardList
{
    /** @var Card[] */
    protected $cards = [];

    public function __clone () {
        foreach ($this->cards as $key => $card) {
            $this->cards[$key] = clone $card;
        }
    }

    public function setCards(array $cards)
    {
        $this->cards = $cards;
    }

    /**
     * @return Card[]
     */
    public function getCards(): array
    {
        return $this->cards;
    }

    public function getCardById(string $cardId): ?Card
    {
        foreach ($this->cards as $card) {
            if ($card->getId() === $cardId) {
                return $card;
            }
        }

        return null;
    }

    /**
     * @param int $count
     * @return Card[]
     */
    public function viewCards(int $count): array
    {
        return array_slice($this->cards, 0, $count);
    }

    public function addCard(Card $card)
    {
        $this->cards[] = $card;
    }

    /**
     * @param Card[] $cards
     */
    public function addCards(array $cards)
    {
        $this->cards = array_merge($this->cards, $cards);
    }

    public function getCount(): int
    {
        return count($this->cards);
    }

    /**
     * @param string[] $cardIds
     * @return Card[]
     */
    public function removeCardsByIds(array $cardIds): array
    {
        $removedCards = [];
        $keptCards = [];
        foreach ($this->cards as $card) {
            if (in_array($card->getId(), $cardIds, true)) {
                $removedCards[] = $card;
            } else {
                $keptCards[] = $card;
            }
        }
        $this->cards = $keptCards;

        return $removedCards;
    }

    public function serializeForUpdate(): array
    {
        return [
            'count' => count($this->cards)
        ];
    }

    public function serializeForDatabase(): array
    {
        $data = [];
        foreach ($this->cards as $card) {
            $data[] = $card->serializeForDatabase();
        }

        return $data;
    }

    /**
     * @param array $data
     * @return CardList
     */
    public function deserializeFromDatabase(array $data)
    {
        foreach ($data as $cardData) {
            $this->cards[] = (new Card())->deserializeFromDatabase($cardData);
        }

        return $this;
    }
}
