<?php

declare(strict_types=1);

namespace App\Entity\Game;

class Buffer
{
    public const VALUE = 'value';
    public const CARDS = 'cards';
    public const MARKED = 'marked';
    public const ORIGIN = 'origin';

    private $data;

    public function __construct()
    {
        $this->data = [
            self::MARKED => [],
            self::VALUE => [],
            self::CARDS => [],
            self::ORIGIN => [],
        ];
    }

    public function __clone()
    {
        foreach ($this->data[self::CARDS] as $bufferId => $cardList) {
            foreach ($cardList as $cardId => $card) {
                $this->data[self::CARDS][$bufferId][$cardId] = clone $card;
            }
        }
    }

    public function clear(string $bufferId = null): void
    {
        $this->clearValue($bufferId);
        $this->clearCards($bufferId);
        $this->clearOrigin($bufferId);
        $this->clearMarked($bufferId);
    }

    public function clearValue(string $bufferId = null): void
    {
        if ($bufferId === null) {
            $this->data[self::VALUE] = [];
            return;
        }

        if (isset($this->data[self::VALUE][$bufferId])) {
            unset($this->data[self::VALUE][$bufferId]);
        }
    }

    public function clearCards(string $bufferId = null): void
    {
        if ($bufferId === null) {
            $this->data[self::CARDS] = [];
            return;
        }

        if (isset($this->data[self::CARDS][$bufferId])) {
            unset($this->data[self::CARDS][$bufferId]);
        }
    }

    public function clearOrigin(string $bufferId = null): void
    {
        if ($bufferId === null) {
            $this->data[self::ORIGIN] = [];
            return;
        }

        if (isset($this->data[self::CARDS][$bufferId])) {
            unset($this->data[self::CARDS][$bufferId]);
        }
    }

    public function clearMarked(string $bufferId = null): void
    {
        if ($bufferId === null) {
            $this->data[self::MARKED] = [];
            return;
        }

        if (isset($this->data[self::MARKED][$bufferId])) {
            unset($this->data[self::MARKED][$bufferId]);
        }
    }

    public function setValue(string $bufferId, $value): self
    {
        $this->data[self::VALUE][$bufferId] = $value;

        return $this;
    }

    /**
     * @param string $bufferId
     * @return mixed|null
     */
    public function getValue(string $bufferId)
    {
        return $this->data[self::VALUE][$bufferId] ?? null;
    }

    public function setMarked(string $bufferId, $value): self
    {
        $this->data[self::MARKED][$bufferId] = $value;

        return $this;
    }

    /**
     * @param string $bufferId
     * @return mixed|null
     */
    public function getMarked(string $bufferId)
    {
        return $this->data[self::MARKED][$bufferId] ?? null;
    }

    public function addCard(string $bufferId, Card $card): self
    {
        if (!isset($this->data[self::CARDS][$bufferId])) {
            $this->data[self::CARDS][$bufferId] = [];
        }
        $this->data[self::CARDS][$bufferId][$card->getId()] = $card;

        return $this;
    }

    public function setOrigin(string $bufferId, string $origin): self
    {
        $this->data[self::ORIGIN][$bufferId] = $origin;

        return $this;
    }

    public function getOrigin(string $bufferId): ?string
    {
        return $this->data[self::ORIGIN][$bufferId] ?? null;
    }

    /**
     * @param string $bufferId
     * @param Card[] $cards
     * @return Buffer
     */
    public function setCards(string $bufferId, array $cards): self
    {
        if (!isset($this->data[self::CARDS][$bufferId])) {
            $this->data[self::CARDS][$bufferId] = [];
        }
        foreach ($cards as $card) {
            $this->data[self::CARDS][$bufferId][$card->getId()] = $card;
        }

        return $this;
    }

    public function getCards(string $bufferId): array
    {
        return $this->data[self::CARDS][$bufferId] ?? [];
    }

    public function removeCardById(string $bufferId, string $cardId): ?Card
    {
        if (isset($this->data[self::CARDS][$bufferId][$cardId])) {
            $card = $this->data[self::CARDS][$bufferId][$cardId];
            unset($this->data[self::CARDS][$bufferId][$cardId]);

            return $card;
        }

        return null;
    }

    public function getCardById(string $searchId): ?Card
    {
        foreach ($this->data[self::CARDS] as $bufferId => $cardData) {
            foreach ($cardData as $cardId => $card) {
                if ($searchId === $card->getId()) {
                    return $card;
                }
            }
        }

        return null;
    }

    public function serializeForDatabase(): array
    {
        $output = $this->data;

        foreach ($this->data[self::CARDS] as $bufferId => $cardList) {
            /**
             * @var string $cardId
             * @var Card $card
             */
            foreach ($cardList as $cardId => $card) {
                $output[self::CARDS][$bufferId][$cardId] = $card->serializeForDatabase();
            }
        }

        return $output;
    }

    public function deserializeFromDatabase(array $data): Buffer
    {
        $this->data = $data;

        foreach ($this->data[self::CARDS] as $bufferId => $cardList) {
            foreach ($cardList as $cardId => $card) {
                $this->data[self::CARDS][$bufferId][$cardId] = (new Card())->deserializeFromDatabase($card);
            }
        }

        return $this;
    }
}
