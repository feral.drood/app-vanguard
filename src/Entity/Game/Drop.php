<?php

declare(strict_types=1);

namespace App\Entity\Game;

class Drop extends CardList
{
    public function getCardUpdate(): ?array
    {
        if (count($this->cards) > 0) {
            /** @var Card $card */
            $card = $this->cards[count($this->cards) - 1];

            return [
                'image' => $card->getImage(),
                'name' => $card->getName(),
                'description' => $card->getDescription(),
            ];
        }

        return null;
    }

    public function serializeForUpdate(): array
    {
        $data = [];

        if ($this->getCount() > 0) {
            $cards = $this->viewCards(1);
            if (count($cards) === 1 && $cards[0] !== null) {
                $data['image'] = $cards[0]->getImage();
                $data['name'] = $cards[0]->getName();
                $data['description'] = $cards[0]->getDescription();
            }
        }

        return $data;
    }
}
