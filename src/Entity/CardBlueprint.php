<?php
declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CardBlueprintRepository")
 */
class CardBlueprint
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="string")
     */
    private $type;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $grade;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $skill;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $power;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $critical;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $shield;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $clan;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $race;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $gift;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $triggerEffect;

    /**
     * @ORM\Column(type="string")
     */
    private $image;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     */
    private $abilities;

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): CardBlueprint
    {
        $this->id = $id;
        return $this;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): CardBlueprint
    {
        $this->name = $name;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description = null): CardBlueprint
    {
        $this->description = $description;
        return $this;
    }

    public function getGrade(): ?int
    {
        return $this->grade;
    }

    public function setGrade(int $grade = null): CardBlueprint
    {
        $this->grade = $grade;
        return $this;
    }

    public function getPower(): ?int
    {
        return $this->power;
    }

    public function setPower(int $power = null): CardBlueprint
    {
        $this->power = $power;
        return $this;
    }

    public function getCritical(): ?int
    {
        return $this->critical;
    }

    public function setCritical(int $critical): CardBlueprint
    {
        $this->critical = $critical;
        return $this;
    }

    public function getShield(): ?int
    {
        return $this->shield;
    }

    public function setShield(int $shield = null): CardBlueprint
    {
        $this->shield = $shield;
        return $this;
    }

    public function getClan(): ?string
    {
        return $this->clan;
    }

    public function setClan(string $clan): CardBlueprint
    {
        $this->clan = $clan;
        return $this;
    }

    public function getGift(): ?string
    {
        return $this->gift;
    }

    public function setGift(string $gift = null): self
    {
        $this->gift = $gift;
        return $this;
    }

    public function getTrigger(): ?string
    {
        return $this->triggerEffect;
    }

    public function setTrigger(string $trigger = null): CardBlueprint
    {
        $this->triggerEffect = $trigger;
        return $this;
    }

    public function getImage(): string
    {
        return $this->image;
    }

    public function setImage(string $image): CardBlueprint
    {
        $this->image = $image;
        return $this;
    }

    public function getSkill(): ?string
    {
        return $this->skill;
    }

    public function setSkill(?string $skill): CardBlueprint
    {
        $this->skill = $skill;
        return $this;
    }

    public function getRace(): ?string
    {
        return $this->race;
    }

    public function setRace(string $race): CardBlueprint
    {
        $this->race = $race;
        return $this;
    }

    public function getAbilities(): array
    {
        return $this->abilities;
    }

    public function setAbilities(array $abilities): CardBlueprint
    {
        $this->abilities = $abilities;
        return $this;
    }
}
