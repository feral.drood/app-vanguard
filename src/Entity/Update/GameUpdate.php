<?php

declare(strict_types=1);

namespace App\Entity\Update;

class GameUpdate implements UpdateDataInterface
{
    private const TYPE = 'type';
    private const PLAYER = 'player';
    private const DATA = 'data';

    private $update = [];

    /**
     * @param string $type
     * @param string|null $playerHash
     * @param mixed $data
     */
    public function __construct(
        string $type,
        ?string $playerHash,
        $data
    ) {
        $this->update = [
            self::TYPE => $type,
            self::PLAYER => $playerHash,
            self::DATA => ($data instanceof UpdateDataInterface ? $data->getData() : $data)
        ];
    }

    public function getPlayerHash(): ?string
    {
        return $this->update[self::PLAYER];
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->update;
    }
}