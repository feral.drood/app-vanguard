<?php

declare(strict_types=1);

namespace App\Entity\Update;

interface UpdateDataInterface
{
    /**
     * Return data to fill in update action.data field
     *
     * @return string|null|array
     */
    public function getData();
}
