<?php

declare(strict_types=1);

namespace App\Entity\Update;

use App\Entity\Actions;

class DialogUpdate extends GameUpdate
{
    private const DATA = 'data';

    private const LOAD_ZONE = 'load_zone';
    private const LOAD_PLAYER_HASH = 'load_player_hash';
    private const LOAD_LOCATION = 'load_location';
    private const TITLE = 'title';
    private const TYPE = 'type';
    private const TYPE_ACTION = 'action';
    private const OPTIONS = 'options';
    private const ACTIONS = 'actions';
    private const CARDS = 'cards';

    public const OPTION_MIN_COUNT = 'min_count';
    public const OPTION_MAX_COUNT = 'max_count';

    private $data;

    public function __construct(
        ?string $playerHash
    ) {
        parent::__construct(Actions::ACTION_DIALOG, $playerHash, null);

        $this->data = [
            self::TYPE => self::TYPE_ACTION,
            self::ACTIONS => [
                'send' => 'Done'
            ]
        ];
    }

    /**
     * @param string|array $title
     * @return $this
     */
    public function setTitle($title): self
    {
        $this->data[self::TITLE] = $title;

        return $this;
    }

    public function setLoadZone(?string $zone): self
    {
        $this->data[self::LOAD_ZONE] = $zone;

        return $this;
    }

    public function setType(string $type): self
    {
        $this->data[self::TYPE] = $type;

        return $this;
    }

    public function setCards(array $cards): self
    {
        $this->data[self::CARDS] = $cards;

        return $this;
    }

    public function addOption(string $option, $value): self
    {
        if (!key_exists(self::OPTIONS, $this->data)) {
            $this->data[self::OPTIONS] = [];
        }

        $this->data[self::OPTIONS][$option] = $value;

        return $this;
    }

    public function setActions(array $actions): self
    {
        $this->data[self::ACTIONS] = $actions;

        return $this;
    }

    public function getData(): array
    {
        $output = parent::getData();

        $output[self::DATA] = $this->data;

        return $output;
    }
}
