<?php
declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GameLogRepository")
 */
class GameLog
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $gameId;

    /**
     * @ORM\Column(type="game_state")
     */
    private $beforeState;

    /**
     * @ORM\Column(type="game_state")
     */
    private $afterState;

    /**
     * @ORM\Column(type="string")
     */
    private $playerHash;

    /**
     * @ORM\Column(type="json")
     */
    private $inputData;

    /**
     * @ORM\Column(type="json")
     */
    private $messageData;

    public function getId(): int
    {
        return $this->id;
    }

    public function getGameId(): int
    {
        return $this->gameId;
    }

    public function setGameId(int $gameId): self
    {
        $this->gameId = $gameId;

        return $this;
    }

    public function getPlayerHash(): string
    {
        return $this->playerHash;
    }

    public function setPlayerHash(string $playerHash): self
    {
        $this->playerHash = $playerHash;

        return $this;
    }

    public function setInputData(array $inputData): self
    {
        $this->inputData = $inputData;

        return $this;
    }

    public function getInputData(): array
    {
        return $this->inputData;
    }

    public function getMessageData(): array
    {
        return $this->messageData;
    }

    public function setMessageData(array $messageData): self
    {
        $this->messageData = $messageData;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getBeforeState()
    {
        return $this->beforeState;
    }

    /**
     * @param mixed $beforeState
     * @return GameLog
     */
    public function setBeforeState($beforeState)
    {
        $this->beforeState = $beforeState;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAfterState()
    {
        return $this->afterState;
    }

    /**
     * @param mixed $afterState
     * @return GameLog
     */
    public function setAfterState($afterState)
    {
        $this->afterState = $afterState;
        return $this;
    }
}
