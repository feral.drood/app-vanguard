<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Game\Card;
use App\Entity\Game\CardProperties;
use App\Entity\Game\Field;
use App\Entity\Game\Player;
use App\Entity\Game\State;
use App\Entity\Actions;
use App\Entity\Update\GameUpdate;

class GameStateUpdatesProvider
{
    public function getUpdates(State $originalState, State $modifiedState): array
    {
        $updates = [];
        foreach ($modifiedState->getPlayers() as $modifiedPlayer) {
            $originalPlayer = $originalState->getPlayer($modifiedPlayer->getHash());
            if ($originalPlayer === null) {
                $originalPlayer = new Player();
            }
            if ($originalPlayer->getName() !== $modifiedPlayer->getName()) {
                $updates[] = new GameUpdate(
                    Actions::PLAYER_NAME,
                    $modifiedPlayer->getHash(),
                    $modifiedPlayer->getName()
                );
            }
            if ($originalPlayer->getAvatar() !== $modifiedPlayer->getAvatar()) {
                $updates[] = new GameUpdate(
                    Actions::PLAYER_AVATAR,
                    $modifiedPlayer->getHash(),
                    $modifiedPlayer->getAvatar()
                );
            }
            if ($originalPlayer->getDeck()->getCount() !== $modifiedPlayer->getDeck()->getCount()) {
                $updates[] = new GameUpdate(
                    Actions::DECK_COUNT,
                    $modifiedPlayer->getHash(),
                    $modifiedPlayer->getDeck()->getCount()
                );
            }
            if ($originalPlayer->getSoul()->getCount() !== $modifiedPlayer->getSoul()->getCount()) {
                $updates[] = new GameUpdate(
                    Actions::SOUL_COUNT,
                    $modifiedPlayer->getHash(),
                    $modifiedPlayer->getSoul()->getCount()
                );
            }
            if ($originalPlayer->getDrop()->getCount() !== $modifiedPlayer->getDrop()->getCount()) {
                $updates[] = new GameUpdate(
                    Actions::DROP_COUNT,
                    $modifiedPlayer->getHash(),
                    $modifiedPlayer->getDrop()->getCount()
                );
            }
            if ($originalPlayer->getBind()->getCount() !== $modifiedPlayer->getBind()->getCount()) {
                $updates[] = new GameUpdate(
                    Actions::BIND_COUNT,
                    $modifiedPlayer->getHash(),
                    $modifiedPlayer->getBind()->getCount()
                );
            }
            if ($originalPlayer->getDrop()->getCardUpdate() !== $modifiedPlayer->getDrop()->getCardUpdate()) {
                $updates[] = new GameUpdate(
                    Actions::DROP_CARD,
                    $modifiedPlayer->getHash(),
                    $modifiedPlayer->getDrop()->getCardUpdate()
                );
            }
            if ($originalPlayer->getHand()->serializeForDatabase() !== $modifiedPlayer->getHand()->serializeForDatabase()) {
                $updates[] = new GameUpdate(
                    Actions::HAND_UPDATE_REQUIRED,
                    $modifiedPlayer->getHash(),
                    true
                );
            }
            if ($originalPlayer->getDamage()->serializeForUpdate() !== $modifiedPlayer->getDamage()->serializeForUpdate()) {
                $updates[] = new GameUpdate(
                    Actions::DAMAGE_CARDS,
                    $modifiedPlayer->getHash(),
                    $modifiedPlayer->getDamage()->serializeForUpdate()
                );
            }
            if ($modifiedPlayer->getTrigger() === null) {
                $modifiedTrigger = null;
            } else {
                $modifiedTrigger = $modifiedPlayer->getTrigger()->serializeForUpdate(Card::ZONE_TRIGGER);
            }
            if ($originalPlayer->getTrigger() === null) {
                $originalTrigger = null;
            } else {
                $originalTrigger = $originalPlayer->getTrigger()->serializeForUpdate(Card::ZONE_TRIGGER);
            }
            if ($originalTrigger !== $modifiedTrigger) {
                $updates[] = new GameUpdate(
                    Actions::CARD_TRIGGER,
                    $modifiedPlayer->getHash(),
                    $modifiedTrigger
                );
            }

            foreach ($originalPlayer->getField()->getCircles() as $location => $unit) {
                $originalUnit = [];
                if ($unit !== null) {
                    $originalUnit = $unit->serializeForUpdate();
                }
                if ($originalPlayer->getGiftCircleType($location) !== null) {
                    $originalUnit[
                        $originalPlayer->getGiftType(
                            $originalPlayer->getGiftCircleType($location)
                        )
                    ] = $originalPlayer->getGiftCircleCount(
                        $location
                    );
                }
                if ($originalPlayer->getHash() === $originalState->getActivePlayerHash()) {
                    if ($originalState->getAttacker() === $location) {
                        $originalUnit['attacker'] = true;
                    }
                    if ($originalState->getBooster() === $location) {
                        $originalUnit['booster'] = true;
                    }
                } else {
                    if ($originalState->getDefender() === $location) {
                        $originalUnit['defender'] = true;
                    }
                }
                if (
                    $location === Field::REARGUARD3
                    && $originalPlayer->getField()->getVanguard() !== null
                    && $originalPlayer->getField()->getVanguard()->hasProperty(CardProperties::ASTRAL_PLANE_OPENED)
                ) {
                    $originalUnit['astral_plane'] = true;
                }

                $modifiedUnit = [];
                if ($modifiedPlayer->getField()->getCircleCard($location) !== null) {
                    $modifiedUnit = $modifiedPlayer->getField()->getCircleCard($location)->serializeForUpdate();
                }
                if ($modifiedPlayer->getGiftCircleType($location) !== null) {
                    $modifiedUnit[
                        $modifiedPlayer->getGiftType(
                            $modifiedPlayer->getGiftCircleType($location)
                        )
                    ] = $modifiedPlayer->getGiftCircleCount(
                        $location
                    );
                }
                if ($modifiedPlayer->getHash() === $modifiedState->getActivePlayerHash()) {
                    if ($modifiedState->getAttacker() === $location) {
                        $modifiedUnit['attacker'] = true;
                    }
                    if ($modifiedState->getBooster() === $location) {
                        $modifiedUnit['booster'] = true;
                    }
                } else {
                    if ($modifiedState->getDefender() === $location) {
                        $modifiedUnit['defender'] = true;
                    }
                }
                if (
                    $location === Field::REARGUARD3
                    && $modifiedPlayer->getField()->getVanguard() !== null
                    && $modifiedPlayer->getField()->getVanguard()->hasProperty(CardProperties::ASTRAL_PLANE_OPENED)
                ) {
                    $modifiedUnit['astral_plane'] = true;
                }

                if ($modifiedUnit !== $originalUnit) {
                    $updates[] = new GameUpdate(
                        Actions::field($location),
                        $modifiedPlayer->getHash(),
                        $modifiedUnit
                    );
                }
            }

            foreach ($modifiedPlayer->getField()->getCircles() as $location => $unit) {
                if (!$originalPlayer->getField()->circleExists($location)) {
                    if ($unit === null) {
                        $modifiedUnit = null;
                    } else {
                        $modifiedUnit = $unit->serializeForUpdate();
                    }
                    $updates[] = new GameUpdate(
                        Actions::field($location),
                        $modifiedPlayer->getHash(),
                        $modifiedUnit
                    );
                }
            }

            if ($originalPlayer->getGuardians()->serializeForUpdate() !== $modifiedPlayer->getGuardians()->serializeForUpdate()) {
                $updates[] = new GameUpdate(
                    Actions::CARD_GUARDIANS,
                    $modifiedPlayer->getHash(),
                    $modifiedPlayer->getGuardians()->serializeForUpdate()
                );
            }
        }
        if ($originalState->getLogCount() !== $modifiedState->getLogCount()) {
            $updates[] = new GameUpdate(
                Actions::CHAT_LOG,
                null,
                array_slice($modifiedState->getChatLog(), $originalState->getLogCount())
            );
        }

//        if ($modifiedState->getPlayerError() !== null) {
//            $updates[] = new GameUpdate(
//                Actions::PLAYER_ERROR,
//                $modifiedState->getPlayerError()->getPlayerHash(),
//                $modifiedState->getPlayerError()->getMessage()
//            );
//        }

        return $updates;
    }

    public function getPlayerHandUpdate(State $originalState, State $modifiedState, string $playerHash): array
    {
        $originalPlayer = $originalState->getPlayer($playerHash);
        $modifiedPlayer = $modifiedState->getPlayer($playerHash);
        $updates = [];
        if (
            $originalPlayer === null
            || $originalPlayer->getHand()->serializeForDatabase() !== $modifiedPlayer->getHand()->serializeForDatabase()
        ) {
//            $updates[] = new GameUpdate(
//                Actions::HAND_UPDATE_REQUIRED,
//                $playerHash,
//                false
//            );
            $data = [];
            foreach ($modifiedPlayer->getHand()->getCards() as $card) {
                $data[$card->getId()] = $card->serializeForUpdate();
            }
            $updates[] = new GameUpdate(
                Actions::HAND_CARDS,
                $playerHash,
                $data
            );
        }

        return $updates;
    }

    private function buildGiftArray(array $gifts): array
    {
        $output = [];
        foreach ($gifts as $gift => $circles) {
            foreach ($circles as $circle) {
                $output[$circle] = $output[$circle] ?? 1;
                $output[$circle]++;
            }
        }

        return $output;
    }
}
