<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Game\Card;
use App\Repository\CardBlueprintRepository;

class ConstructedDeckUnpacker
{
    private $cardRepository;

    public function __construct(
        CardBlueprintRepository $cardRepository
    ) {
        $this->cardRepository = $cardRepository;
    }

    /**
     * @param array $deckCards
     * @return Card[]
     */
    public function unpack(array $deckCards): array
    {
        $cards = [];
        $blueprints = $this->cardRepository->getAllByIdList(array_keys($deckCards));
        foreach ($blueprints as $blueprint) {
            $card = (new Card())
                ->setId($blueprint->getId())
                ->setType($blueprint->getType())
                ->setImage($blueprint->getImage())
                ->setTrigger($blueprint->getTrigger())
                ->setGift($blueprint->getGift())
                ->setGrade($blueprint->getGrade())
                ->setSkill($blueprint->getSkill())
                ->setPower($blueprint->getPower())
                ->setShield($blueprint->getShield())
                ->setCritical($blueprint->getCritical())
                ->setName($blueprint->getName())
                ->setDescription($blueprint->getDescription())
                ->setClan($blueprint->getClan())
                ->setRace($blueprint->getRace())
                ->setAbilities($blueprint->getAbilities())
            ;
            for ($c=1; $c<=$deckCards[$blueprint->getId()]; $c++) {
                $cards[] = (clone $card)->setId($blueprint->getId() . '#' . $c);
            }
        }

        return $cards;
    }
}
