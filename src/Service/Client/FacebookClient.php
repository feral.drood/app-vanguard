<?php
declare(strict_types=1);

namespace App\Service\Client;

use App\Entity\User;
use Exception;

class FacebookClient
{
    const FACEBOOK_API_URL = 'https://graph.facebook.com/me?fields=id,name,email,picture&access_token=%s';

    private $facebookToken;

    public function __construct(
        string $facebookToken
    ) {
        $this->facebookToken = $facebookToken;
    }

    public function getUser(string $token): ?User
    {
        try {
            $content = file_get_contents(sprintf(self::FACEBOOK_API_URL, $token));
            $json = json_decode($content, true);
        } catch (Exception $exception) {
            return null;
        }

        if (
            $json === null
            || empty($json['id'])
            || empty($json['name'])
            || empty($json['email'])
        ) {
            return null;
        }

        $user = (new User())
            ->setEmail($json['email'])
            ->setFacebookId($json['id'])
            ->setFacebookName($json['name'])
            ->setName($json['name'])
        ;

        if (
            isset($json['picture'])
            && isset($json['picture']['data'])
            && !$json['picture']['data']['is_silhouette']
        ) {
            $user
                ->setPicture($json['picture']['data']['url'])
                ->setFacebookPicture($json['picture']['data']['url'])
            ;
        }

        return $user;
    }
}
