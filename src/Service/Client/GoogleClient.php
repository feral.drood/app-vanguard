<?php
declare(strict_types=1);

namespace App\Service\Client;

use App\Entity\User;
use Exception;

class GoogleClient
{
    const GOOGLE_API_URL = 'https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=%s';

    public function getUser(string $token): ?User
    {
        try {
            $content = file_get_contents(
                sprintf(self::GOOGLE_API_URL, $token),
                false,
                stream_context_create([
                    "ssl" => [
                        "verify_peer" => false,
                        "verify_peer_name" => false,
                    ],
                ])
            );
            $json = json_decode($content, true);
        } catch (Exception $exception) {
            return null;
        }

        if (
            $json === null
            || empty($json['sub'])
            || empty($json['name'])
            || empty($json['email'])
        ) {
            return null;
        }

        $user = (new User())
            ->setEmail($json['email'])
            ->setGoogleId($json['sub'])
            ->setGoogleName($json['name'])
            ->setName($json['name'])
        ;

        if (isset($json['picture'])) {
            $user
                ->setPicture($json['picture'])
                ->setGooglePicture($json['picture'])
            ;
        }

        return $user;
    }
}
