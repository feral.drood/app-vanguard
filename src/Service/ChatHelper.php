<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Game\Card;

class ChatHelper
{
    public function possessive(string $string): string
    {
        return $string . '\'' . (mb_strtolower(mb_substr($string, -1)) === 's' ? 's' : '');
    }

    public function format_bonus($bonus) : string
    {
        if (intval($bonus) >= 0) {
            $bonus = '+' . $bonus;
        }

        return (string)$bonus;
    }

    public function plural($quantity, string $singular, string $plural = null): string
    {
        $quantity = intval($quantity);
        if (
            $quantity === 1
            || !strlen($singular)
        ) {
            return $singular;
        }
        if ($plural !== null) {
            return $plural;
        }

        $last_letter = strtolower($singular[strlen($singular)-1]);
        switch($last_letter) {
            case 'y':
                return substr($singular,0,-1) . 'ies';
            case 's':
                return $singular . 'es';
            default:
                return $singular . 's';
        }
    }

    public function generateCardLink(Card $card): string
    {
        return '[{' . $card->getName() . '},{' . $card->getImage() . '},{' . $card->getDescription() . '}]';
    }
}
