<?php

declare(strict_types=1);

namespace App\Service\Phases;

use App\Entity\Game\Card;
use App\Entity\Game\CardProperties;
use App\Entity\Game\State;
use App\Entity\Game\Player;
use App\Entity\Game\Field;
use App\Entity\Game\PlayerError;
use App\Entity\PlayerInput;
use App\Entity\Actions;
use App\Entity\Update\DialogUpdate;
use App\Entity\Update\GameUpdate;
use App\Service\ChatHelper;
use App\Service\Game\PhaseSwitcher;

class BattlePhase implements PhaseInterface
{
    const BOOST_PAIRS = [
        Field::VANGUARD => Field::REARGUARD3,
        Field::REARGUARD1 => Field::REARGUARD2,
        Field::REARGUARD5 => Field::REARGUARD4
    ];

    private $chatHelper;
    private $phaseSwitcher;

    public function __construct(
        ChatHelper $chatHelper,
        PhaseSwitcher $phaseSwitcher
    ) {
        $this->chatHelper = $chatHelper;
        $this->phaseSwitcher = $phaseSwitcher;
    }

    public function getPhase(): string
    {
        return Phases::PHASE_BATTLE;
    }

    public function processPlayerInput(State $gameState, PlayerInput $input): bool
    {
        if ($gameState->getActivePlayerHash() === $input->getPlayerHash()) {
            $player = $gameState->getActivePlayer();
            $playerField = $player->getField();
            $circles = $input->getList();
            if (count($circles) === 2) {
                if ($circles[0] === null || $circles[1] === null) {
                    $gameState->setPlayerError(
                        new PlayerError(
                            $player->getHash(),
                            'Invalid selection. Something went wrong?'
                        )
                    );

                    return false;
                }

                if (
                    array_key_exists($circles[1], self::BOOST_PAIRS)
                    && self::BOOST_PAIRS[$circles[1]] === $circles[0]
                ) {
                    $circles = array_reverse($circles);
                }

                $circle0Card = $playerField->getCircleCard($circles[0]);
                $circle1Card = $playerField->getCircleCard($circles[1]);

                if (
                    $circle0Card === null
                    || $circle1Card === null
                ) {
                    $gameState->setPlayerError(
                        new PlayerError(
                            $player->getHash(),
                            'Missing unit on selected field'
                        )
                    );

                    return false;
                }

                if (
                    $circle0Card->getState() !== Card::STATE_STAND
                    || $circle1Card->getState() !== Card::STATE_STAND
                ) {
                    $gameState->setPlayerError(
                        new PlayerError(
                            $player->getHash(),
                            'Attacking units must the standing'
                        )
                    );

                    return false;
                }

                if (
                    !array_key_exists($circles[0], self::BOOST_PAIRS)
                    || self::BOOST_PAIRS[$circles[0]] !== $circles[1]
                ) {
                    $gameState->setPlayerError(
                        new PlayerError(
                            $player->getHash(),
                            'Only unit in the same column can boost'
                        )
                    );

                    return false;
                }

                if ($circle1Card->getSkill() !== Card::SKILL_BOOST) {
                    $gameState->setPlayerError(
                        new PlayerError(
                            $player->getHash(),
                            'Boosting unit must have a boost skill. Well, Duh'
                        )
                    );

                    return false;
                }

                if ($circle0Card->hasProperty(Card::PROPERTY_CANNOT_ATTACK)) {
                    $gameState->setPlayerError(
                        new PlayerError(
                            $player->getHash(),
                            'Unit cannot attack due to restriction'
                        )
                    );

                    return false;
                }

                $gameState->setAttacker($circles[0]);
                $gameState->setBooster($circles[1]);
                $circle0Card->setAttacker(true);
                $circle1Card->setBooster(true);

                $gameState->addChatLog(
                    $player->getName()
                    . ' chooses '
                    . $this->chatHelper->generateCardLink($circle0Card)
                    . ' as attacker and '
                    . $this->chatHelper->generateCardLink($circle1Card)
                    . ' as booster'
                );

                $gameState->setPhase(Phases::PHASE_BATTLE_SELECT_DEFENDER);
            } elseif (count($circles) === 1) {
                if ($circles[0] === null) {
                    $gameState->setPlayerError(
                        new PlayerError(
                            $player->getHash(),
                            'Invalid selection. Something went wrong?'
                        )
                    );

                    return false;
                }

                $circleCard = $playerField->getCircleCard($circles[0]);

                if ($circleCard === null) {
//                    if ($circles[0] === null) {
                        $gameState->setPlayerError(
                            new PlayerError(
                                $player->getHash(),
                                'Not an active unit on field'
                            )
                        );

                        return false;
//                    }
                }

                if ($circleCard->getState() !== Card::STATE_STAND) {
                    $gameState->setPlayerError(
                        new PlayerError(
                            $player->getHash(),
                            'Attacking unit must the standing'
                        )
                    );

                    return false;
                }


                if (
                    ($circleCard = $playerField->getCircleCard($circles[0])) !== null
                    && (
                        $playerField->isFrontRow($circles[0])
                        || (
                            $circles[0] === Field::REARGUARD3
                            && $circleCard->getRace() === CardProperties::RACE_ASTRAL_DEITY
                        )
                    )
                    && $circleCard->getState() === Card::STATE_STAND
                ) {
                    if ($circleCard->hasProperty(Card::PROPERTY_CANNOT_ATTACK)) {
                        $gameState->setPlayerError(
                            new PlayerError(
                                $input->getPlayerHash(),
                                $circleCard->getName() . ' cannot attack'
                            )
                        );

                        return false;
                    } else {
                        $gameState->setAttacker($circles[0]);
                        $circleCard->setAttacker(true);
                        $gameState->addChatLog(
                            $player->getName() .
                            ' chooses ' .
                            $this->chatHelper->generateCardLink($circleCard) .
                            ' as attacker'
                        );

                        $gameState->setPhase(Phases::PHASE_BATTLE_SELECT_DEFENDER);
                    }
                }
            } else {
                $this->phaseSwitcher->setNextPhase($gameState, Phases::PHASE_END);
            }
        }

        return true;
    }

    public function progressState(State $gameState): bool
    {
        $validAttacker = false;
        $player = $gameState->getActivePlayer();
        $playerField = $player->getField();
        foreach ($playerField->getCircles() as $circleId => $unit) {
            if (
                $unit !== null
                && $playerField->isFrontRow($circleId)
                && $unit->getState() === Card::STATE_STAND
            ) {
                if (
                    $gameState->getCurrentTurn() > 1
                    || $unit->hasProperty(Card::PROPERTY_CAN_ATTACK_ON_FIRST_TURN)
                ) {
                    $validAttacker = true;
                }
//                } else {
//                    if ($unit->hasProperty(Card::PROPERTY_CAN_ATTACK_ON_FIRST_TURN)) {
//                        $validAttacker = true;
//                    } else {
//                        $unit->addBonusProperty(Card::PROPERTY_CANNOT_ATTACK);
//                    }
//                }
            }
        }
        if ($validAttacker) {
            $gameState
                ->addUpdateMessage(
                    (new DialogUpdate($player->getHash()))
                    ->setTitle([
                        'Battle Phase',
                        'Select attacker and (optional) booster'
                    ])
                    ->addOption(DialogUpdate::OPTION_MAX_COUNT, 2)
                )
            ;

            return false;
        } else {
            $this->phaseSwitcher->setNextPhase($gameState, Phases::PHASE_END);
        }

        return true;
    }
}
