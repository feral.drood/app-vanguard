<?php

declare(strict_types=1);

namespace App\Service\Phases;

use Exception;

class PhaseManager
{
    private $phases;

    public function __construct()
    {
        $this->phases = [];
    }

    public function addPhase(PhaseInterface $phase)
    {
        $this->phases[$phase->getPhase()] = $phase;
    }

    /**
     * @param string $phase
     * @return PhaseInterface|null
     * @throws Exception
     */
    public function getPhase(string $phase): ?PhaseInterface
    {
        if (array_key_exists($phase, $this->phases)) {
            return $this->phases[$phase];
        } else {
            throw new Exception('Missing phase ' . $phase);
        }
    }
}
