<?php

declare(strict_types=1);

namespace App\Service\Phases;

use App\Entity\Game\Deck;
use App\Entity\Game\Field;
use App\Entity\Game\State;
use App\Entity\Game\Card;
use App\Entity\PlayerInput;
use App\Service\ChatHelper;
use App\Service\Game\PhaseSwitcher;

class EndPhase implements PhaseInterface
{
    private $chatHelper;
    private $phaseSwitcher;

    public function __construct(
        ChatHelper $chatHelper,
        PhaseSwitcher $phaseSwitcher
    ) {
        $this->chatHelper = $chatHelper;
        $this->phaseSwitcher = $phaseSwitcher;
    }

    public function getPhase(): string
    {
        return Phases::PHASE_END;
    }

    public function processPlayerInput(State $game, PlayerInput $input): bool
    {
        return true;
    }

    public function progressState(State $gameState): bool
    {
        $player = $gameState->getActivePlayer();
        $opponent = $gameState->getInactivePlayer();

        $rearguard3 = $player->getField()->getCircleCard(Field::REARGUARD3);
        if (
            $rearguard3 !== null
            && $rearguard3->getRace() === 'Astral Deity'
        ) {
            $player->getField()->setCircleCard(Field::REARGUARD3, null);
            $player->getDeck()->addCard($rearguard3, Deck::BOTTOM);
            $gameState->addChatLog(
                $player->getName()
                . ' puts '
                . $this->chatHelper->generateCardLink($rearguard3)
                . ' on bottom of the deck'
            );
        }
        foreach ($player->getField()->getCircles() as $circleId => $unit) {
            if ($unit !== null) {
                $unit->resetBonus(Card::BONUS_LENGTH_TURN);
                $unit->setDeleted(false);
            }
        }
        foreach ($opponent->getField()->getCircles() as $circleId => $unit) {
            if ($unit !== null) {
                $unit->resetBonus(Card::BONUS_LENGTH_TURN);
            }
        }
        $opponent->clearProperties();
        $player->clearProperties();
        $gameState->getAbilityStack()->resetUsedAbilities();
        $gameState->getCardEventLog()->clear();

        $this->phaseSwitcher->setNextPhase($gameState, Phases::PHASE_START);

        return true;
    }
}
