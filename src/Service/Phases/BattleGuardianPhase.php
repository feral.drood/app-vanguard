<?php

declare(strict_types=1);

namespace App\Service\Phases;

use App\Entity\Ability\Ability;
use App\Entity\Ability\AbilityPart;
use App\Entity\Ability\AbilityPartOptions;
use App\Entity\Game\Card;
use App\Entity\Game\State;
use App\Entity\Game\Player;
use App\Entity\Game\Field;
use App\Entity\Game\PlayerError;
use App\Entity\PlayerInput;
use App\Entity\Update\DialogUpdate;
use App\Entity\Update\GameUpdate;
use App\Service\ChatHelper;
use App\Entity\Actions;
use App\Service\Game\PerfectGuardProcessor;

class BattleGuardianPhase implements PhaseInterface
{
    private $chatHelper;

    public function __construct(
        ChatHelper $chatHelper
    ) {
        $this->chatHelper = $chatHelper;
    }

    public function getPhase(): string
    {
        return Phases::PHASE_BATTLE_SELECT_GUARDIAN;
    }

    public function processPlayerInput(State $gameState, PlayerInput $input): bool
    {
        if ($gameState->getInactivePlayerHash() === $input->getPlayerHash()) {
            $opponent = $gameState->getPlayer($input->getPlayerHash());
            $data = $input->getList();
            $defenderCount = 0;
            $handDefenderCount = 0;
            $errorCount = 0;
            $field = $opponent->getField();
            foreach ($data as $selected) {
                if (
                    $selected !== null
                    && ($selectedCard = $field->getCircleCard($selected)) !== null
                    && $field->isFrontRow($selected)
                    && $selected !== Field::VANGUARD
                    && $selected !== $gameState->getDefender()
                    && $selectedCard->getSkill() === Card::SKILL_INTERCEPT
                    && $selectedCard->getShield() !== null
                ) {
                    $defenderCount++;
                } elseif (
                    $selected !== null
                    && ($selectedCard = $opponent->getHand()->getCardById($selected)) !== null
                    && in_array(
                        $selectedCard->getType(),
                        [
                            Card::TYPE_BLITZ_ORDER,
                            Card::TYPE_NORMAL_ORDER,
                        ],
                        true
                    )
                ) {
                    // do nothing
                } elseif (
                    $selected !== null
                    && ($selectedCard = $opponent->getHand()->getCardById($selected)) !== null
                    && $selectedCard->getShield() !== null
                ) {
                    if (
                        $opponent->getHand()->getCardById($selected)->getShield() === 0
                        && $opponent->hasProperty(Player::PROPERTY_CANNOT_CALL_SENTINEL_FROM_HAND_TO_GC)
                    ) {
                        $gameState->setPlayerError(
                            new PlayerError(
                                $input->getPlayerHash(),
                                'Cannot call sentinels from hand to GC'
                            )
                        );

                        return false;
                    } else {
                        $handDefenderCount++;
                        $defenderCount++;
                    }
                } else {
                    $gameState->setPlayerError(
                        new PlayerError(
                            $input->getPlayerHash(),
                            'Invalid selection'
                        )
                    );

                    return false;
                }
            }

            if (
                $opponent->hasProperty(Player::PROPERTY_MUST_CALL_TWO_GUARDIANS_FROM_HAND_TO_GC)
                && $handDefenderCount === 1
            ) {
                $gameState->setPlayerError(
                    new PlayerError(
                        $input->getPlayerHash(),
                        'Must call at least two guardians from hand to GC '
                    )
                );

                return false;
            }

            if ($errorCount === 0) {
                foreach ($data as $selected) {
                    if (
                        $selected !== null
                        && ($selectedCard = $field->getCircleCard($selected)) !== null
                        && $field->isFrontRow($selected)
                        && $selected !== Field::VANGUARD
                        && $selected !== $gameState->getDefender()
                        && $selectedCard->getSkill() === Card::SKILL_INTERCEPT
                        && $selectedCard->getShield() !== null
                    ) {
                        $opponent->getGuardians()->addCard($selectedCard);
                        $field->setCircleCard($selected, null);
                        $this->checkForPerfectGuardAbility($gameState, $input->getPlayerHash(), $selectedCard);
                    } elseif (
                        $selected !== null
                        && ($selectedCard = $opponent->getHand()->getCardById($selected)) !== null
                        && $selectedCard->getType() === Card::TYPE_BLITZ_ORDER
                    ) {
                        $gameState->addChatLog(
                            $opponent->getName()
                            . ' uses '
                            . $selectedCard->getName()
                        );
                        $opponent->getField()->getCircleCard($gameState->getDefender())->addBonus(
                            Card::BONUS_LENGTH_BATTLE,
                            Card::BONUS_POWER,
                            5
                        );
                        $opponent->getHand()->removeCardsByIds([$selected]);
                    } elseif (
                        $selected !== null
                        && ($selectedCard = $opponent->getHand()->getCardById($selected)) !== null
                        && $selectedCard->getType() === Card::TYPE_NORMAL_ORDER
                    ) {
                        $ability = (new Ability())
                            ->setPlayerHash($opponent->getHash())
                            ->setId($selectedCard->getId() . '?1')
                            ->setName($selectedCard->getName())
                            ->setEventCardId($selectedCard->getId())
                            ->setSelfCardId($selectedCard->getId())
                            ->setCosts([
                                (new AbilityPart())
                                ->setType('do_discard')
                                ->addOption('count', 2)
                            ])
                            ->setEffects([
                                (new AbilityPart())
                                ->setType('do_mark_card')
                                ->addOption(AbilityPartOptions::CARD, AbilityPartOptions::LOCATION_DEFENDER),
                                (new AbilityPart())
                                ->setType('do_modify_buffer')
                                ->addOption(AbilityPartOptions::LENGTH, Card::BONUS_LENGTH_BATTLE)
                                ->addOption(Card::BONUS_POWER, 20),
                                (new AbilityPart())
                                ->setType('do_clear_buffer')
                            ])
                        ;

                        $gameState->getAbilityStack()->addAbility($ability);
//                        $gameState->addChatLog(
//                            $opponent->getName()
//                            . ' uses '
//                            . $selectedCard->getName()
//                        );
//                        $opponent->getField()->getCircleCard($gameState->getDefender())->addBonus(
//                            Card::BONUS_LENGTH_BATTLE,
//                            Card::BONUS_POWER,
//                            20
//                        );
                        $opponent->getHand()->removeCardsByIds([$selected]);
                    } elseif (
                        $selected !== null
                        && ($selectedCard = $opponent->getHand()->getCardById($selected)) !== null
                        && $selectedCard->getShield() !== null
                    ) {
                        $opponent->getHand()->removeCardsByIds([$selected]);
                        $opponent->getGuardians()->addCard($selectedCard);
                        $this->checkForPerfectGuardAbility($gameState, $input->getPlayerHash(), $selectedCard);
                    }
                }
                if (count($data) > 0) {
                    $gameState->addChatLog(
                        $opponent->getName()
                        . ' calls '
                        . count($data)
                        . ' '
                        . $this->chatHelper->plural(count($data), 'guardian')
                    );
                }
                $gameState->setPhase(Phases::PHASE_BATTLE_COMBAT);

                return true;
            }
        }

        return false;
    }

    private function checkForPerfectGuardAbility(State $state, string $playerHash, Card $card)
    {
        if ($card->getShield() === 0) {
            $state->getAbilityStack()->addAbility(
                (new Ability())
                    ->setType('auto')
                    ->setId($card->getId() . '?pg')
                    ->setSelfCardId($card->getId())
                    ->setEventCardId($card->getId())
                    ->setPlayerHash($playerHash)
                    ->setName($card->getName() . ' (PG)')
                    ->addCondition(
                        (new AbilityPart())
                            ->setType('is_card_list_check')
                            ->addOption(AbilityPartOptions::LOCATION, AbilityPartOptions::LOCATION_HAND)
                            ->addOption(AbilityPartOptions::MIN_COUNT, 1)
                    )
                    ->addCost(
                        (new AbilityPart())
                            ->setType('do_discard')
                            ->addOption(AbilityPartOptions::COUNT, 1)
                    )
                    ->addEffect(
                        (new AbilityPart())
                            ->setType('do_perfect_guard')
                    )
            );
        }
    }

    public function progressState(State $state): bool
    {
        $state
            ->addUpdateMessage(
                (new DialogUpdate($state->getInactivePlayerHash()))
                ->setTitle('Select guardians')
            )
        ;

        return false;
    }
}
