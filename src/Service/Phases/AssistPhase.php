<?php

declare(strict_types=1);

namespace App\Service\Phases;

use App\Entity\Ability\AbilityPartOptions;
use App\Entity\Ability\AbilityPart;
use App\Entity\Actions;
use App\Entity\Game\Card;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Entity\Update\DialogUpdate;
use App\Entity\Update\GameUpdate;

class AssistPhase implements PhaseInterface
{
    public function getPhase(): string
    {
        return Phases::PHASE_ASSIST;
    }

    public function processPlayerInput(State $gameState, PlayerInput $input): bool
    {
        if ($gameState->getActivePlayerHash() === $input->getPlayerHash()) {
            $data = $input->getValue();
            if ($data === 'assist') {
                $gameState->addChatLog($gameState->getActivePlayer()->getName() . ' chooses to use G Assist');
                $updateCards = [];
                $handCards = $gameState->getActivePlayer()->getHand()->getCards();
                foreach ($handCards as $card) {
                    $updateCards[$card->getId()] = $card->serializeForUpdate(Card::ZONE_VIEW);
                }
                $gameState->addUpdateMessage(
                    (new DialogUpdate($gameState->getInactivePlayerHash()))
                    ->setType('view')
                    ->setTitle('Opponent hand cards')
                    ->setCards($updateCards)
                    ->setActions([
                        'close' => 'Close'
                    ])
                );
                $gameState->getAbilityStack()->addEffect(
                    (new AbilityPart())
                    ->setType('do_assist_draw')
                    ->setPlayerHash($input->getPlayerHash())
                    ->addOption(AbilityPartOptions::COUNT, 2)
                );
                $gameState->getAbilityStack()->addEffect(
                    (new AbilityPart())
                        ->setType('do_deck_shuffle')
                        ->setPlayerHash($input->getPlayerHash())
                );
            }
            $gameState->setPhase(Phases::PHASE_RIDE);

            return true;
        }

        return false;
    }

    public function progressState(State $state): bool
    {
        $state->addUpdateMessage(
            (new DialogUpdate($state->getActivePlayerHash()))
            ->setTitle('Do you want to use G Assist?')
            ->setActions([
                'assist' => 'Yes',
                'no' => 'No'
            ])
        );

        return false;
    }
}
