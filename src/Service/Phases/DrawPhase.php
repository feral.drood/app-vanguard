<?php

declare(strict_types=1);

namespace App\Service\Phases;

use App\Entity\Game\State;
use App\Entity\Game\Field;
use App\Entity\PlayerInput;

class DrawPhase implements PhaseInterface
{
    public function getPhase(): string
    {
        return Phases::PHASE_DRAW;
    }

    public function processPlayerInput(State $game, PlayerInput $input): bool
    {
        return true;
    }

    public function progressState(State $gameState): bool
    {
        $player = $gameState->getActivePlayer();
        $player->getHand()->addCards($player->getDeck()->drawCards(1));
        $gameState->addChatLog($player->getName() . ' draws 1 card');
        if ($player->getField()->getVanguard()->getGrade() < 3 && $player->getHand()->getCount() >= 2) {
            $validRideCount = 0;
            $nextGrade = $player->getField()->getVanguard()->getGrade() + 1;
            foreach ($player->getHand()->getCards() as $card) {
                if ($card->getGrade() === $nextGrade) {
                    $validRideCount++;
                }
            }
            if ($validRideCount === 0) {
                $gameState->setPhase(Phases::PHASE_ASSIST);
            } else {
                $gameState->setPhase(Phases::PHASE_RIDE);
            }
        } else {
            $gameState->setPhase(Phases::PHASE_RIDE);
        }

        return true;
    }
}
