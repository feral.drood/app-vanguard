<?php

declare(strict_types=1);

namespace App\Service\Phases;

use App\Entity\Game\State;
use App\Entity\Game\Card;
use App\Entity\Game\Field;
use App\Entity\PlayerInput;

class BattleEndPhase implements PhaseInterface
{
    private $battleAttackHelper;

    public function __construct(
        BattleAttackHelper $battleAttackHelper
    ) {
        $this->battleAttackHelper = $battleAttackHelper;
    }

    public function getPhase(): string
    {
        return Phases::PHASE_BATTLE_END;
    }

    public function processPlayerInput(State $state, PlayerInput $input): bool
    {
        return true;
    }

    public function progressState(State $gameState): bool
    {
        $player = $gameState->getActivePlayer();
        $playerField = $player->getField();
        $opponent = $gameState->getInactivePlayer();
        $opponentField = $opponent->getField();
        $attacker = $gameState->getAttacker();
        $defender = $gameState->getDefender();
        $booster = $gameState->getBooster();

        $playerField->getCircleCard($attacker)->setAttacker(false);
        $opponentField->getCircleCard($defender)->setDefender(false);
        if ($booster !== null) {
            $playerField->getCircleCard($booster)->setBooster(false);
        }
        if ($this->battleAttackHelper->isAttackSuccessful($gameState) && $defender !== Field::VANGUARD) {
            $opponent->getDrop()->addCard($opponentField->getCircleCard($defender));
            $opponentField->setCircleCard($defender, null);
        }

        $gameState
            ->setAttacker(null)
            ->setDefender(null)
            ->setBooster(null)
        ;
        foreach ($opponent->getGuardians()->getCards() as $guardian) {
            $guardian->reset();
        }
        $opponent->getDrop()->addCards($opponent->getGuardians()->getCards());
        $opponent->getGuardians()->setCards([]);
        /** @var Card $card */
        foreach ($playerField->getCircles() as $card) {
            if ($card !== null) {
                $card->resetBonus(Card::BONUS_LENGTH_BATTLE);
            }
        }
        /** @var Card $card */
        foreach ($opponentField->getCircles() as $card) {
            if ($card !== null) {
                $card->resetBonus(Card::BONUS_LENGTH_BATTLE);
            }
        }
        $opponent->clearProperties();
        $gameState->setPhase(Phases::PHASE_BATTLE);

        return true;
    }
}
