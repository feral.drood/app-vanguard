<?php

declare(strict_types=1);

namespace App\Service\Phases;

use App\Entity\Actions;
use App\Entity\Game\State;
use App\Entity\Game\Player;
use App\Entity\Game\Field;
use App\Entity\PlayerInput;
use App\Entity\Update\DialogUpdate;
use App\Entity\Update\GameUpdate;

class VanguardPhase implements PhaseInterface
{
    public function getPhase(): string
    {
        return Phases::PHASE_VANGUARD;
    }

    public function processPlayerInput(State $state, PlayerInput $input): bool
    {
        $player = $state->getPlayer($input->getPlayerHash());
        $playerField = $player->getField();
        $vanguardId = $input->getValue();
        if (
            $vanguardId !== null
            && ($card = $player->getDeck()->getCardById($vanguardId)) !== null
            && $card->getGrade() === 0
        ) {
            $card->setFaceDown(true);
            $player->getDeck()->removeCardsByIds([$vanguardId]);
            $playerField->setCircleCard(Field::VANGUARD, $card);
            $state->addChatLog($player->getName() . ' chooses a vanguard');
        }

        return true;
    }

    public function progressState(State $state): bool
    {
        $updates = [];
        foreach ($state->getPlayers() as $player) {
            if ($player->getField()->getVanguard() === null) {
                $updates[] =
                    (new DialogUpdate($player->getHash()))
                        ->setTitle('Choose your vanguard')
                        ->setLoadZone('vanguard')
                        ->setType('vanguard')
                        ->addOption(DialogUpdate::OPTION_MIN_COUNT, 1)
                        ->addOption(DialogUpdate::OPTION_MAX_COUNT, 1)
                ;
            }
        }

        if (count($updates) > 0) {
            foreach ($updates as $updateMessage) {
                $state->addUpdateMessage($updateMessage);
            }

            return false;
        }
        $currentPlayer = $state->getPlayers()[mt_rand(0, 1)];
        $state->setActivePlayerHash($currentPlayer->getHash());
        $state->setPhase(Phases::PHASE_MULLIGAN1);

        $player1 = $state->getActivePlayer();
        $player2 = $state->getInactivePlayer();

        $player1->getDeck()->shuffle();
        $player2->getDeck()->shuffle();

        $player1->getHand()->addCards($player1->getDeck()->drawCards(5));
        $player2->getHand()->addCards($player2->getDeck()->drawCards(5));

        $state->setCurrentTurn(1);
        $state->addChatLog($currentPlayer->getName() . ' is choosing starting hand');

        return true;
    }
}
