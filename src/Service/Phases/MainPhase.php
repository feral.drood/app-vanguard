<?php

declare(strict_types=1);

namespace App\Service\Phases;

use App\Entity\Actions;
use App\Entity\Game\Card;
use App\Entity\Game\CardEvent;
use App\Entity\Game\CardEventTypes;
use App\Entity\Game\CardProperties;
use App\Entity\Game\State;
use App\Entity\Game\PlayerError;
use App\Entity\Game\Field;
use App\Entity\PlayerInput;
use App\Entity\Update\DialogUpdate;
use App\Entity\Update\GameUpdate;
use App\Service\ChatHelper;
use App\Service\Game\ActivatedAbilityProcessor;
use App\Service\Game\CardEventDispatcher;

class MainPhase implements PhaseInterface
{
    private $chatHelper;
    private $eventDispatcher;
    private $activatedAbilityProcessor;

    public function __construct(
        ChatHelper $chatHelper,
        CardEventDispatcher $eventDispatcher,
        ActivatedAbilityProcessor $activatedAbilityProcessor
    ) {
        $this->chatHelper = $chatHelper;
        $this->eventDispatcher = $eventDispatcher;
        $this->activatedAbilityProcessor = $activatedAbilityProcessor;
    }

    public function getPhase(): string
    {
        return Phases::PHASE_MAIN;
    }

    public function processPlayerInput(State $gameState, PlayerInput $input): bool
    {
        $player = $gameState->getActivePlayer();
        $playerField = $player->getField();
        if ($player->getHash() === $input->getPlayerHash()) {
            $cards = $input->getList();

            if (
                count($cards) === 1
                && $playerField->getCircleCard($cards[0]) !== null
            ) {
                return $this
                    ->activatedAbilityProcessor
                    ->process(
                        $gameState,
                        $player->getHash(),
                        $playerField->getCircleCard(
                            $cards[0]
                        )
                    )
                ;
            } elseif (count($cards) === 2) {
                $circleId = null;
                $callId = null;

                if (
                    $playerField->circleExists($cards[0])
                    && $playerField->circleExists($cards[1])
                ) {
                    if (
                        (
                            in_array($cards[0], [Field::REARGUARD1, Field::REARGUARD2], true)
                            && in_array($cards[1], [Field::REARGUARD1, Field::REARGUARD2], true)
                        ) || (
                            in_array($cards[0], [Field::REARGUARD4, Field::REARGUARD5], true)
                            && in_array($cards[1], [Field::REARGUARD4, Field::REARGUARD5], true)
                        )
                    ) {
                        $card = $playerField->getCircleCard($cards[0]);
                        $playerField->setCircleCard($cards[0], $playerField->getCircleCard($cards[1]));
                        $playerField->setCircleCard($cards[1], $card);
                        $gameState->addChatLog($player->getName() . ' repositions units within a column');

                        return true;
                    }

                    $gameState->setPlayerError(new PlayerError(
                        $input->getPlayerHash(),
                        'Both fields must be rear-guard fields and in same column'
                    ));

                    return false;
                } elseif (
                    $cards[0] === Field::VANGUARD
                    || $cards[1] === Field::VANGUARD
                ) {
                    $gameState->setPlayerError(new PlayerError(
                        $input->getPlayerHash(),
                        'Card cannot be called to vanguard circle'
                    ));

                    return false;
                } else {
                    if (
                        $playerField->circleExists($cards[0])
                        && $player->getHand()->getCardById($cards[1]) !== null
                    ) {
                        $callCircle = $cards[0];
                        $callCardId = $cards[1];
                    } elseif (
                        $playerField->circleExists($cards[1])
                        && $player->getHand()->getCardById($cards[0]) !== null
                    ) {
                        $callCircle = $cards[1];
                        $callCardId = $cards[0];
                    } else {
                        $gameState->setPlayerError(new PlayerError(
                            $input->getPlayerHash(),
                            'Eh? Something is wrong'
                        ));

                        return false;
                    }

                    $callCard = $player->getHand()->getCardById($callCardId);
                    if (
                        !in_array(
                            $callCard->getType(),
                            [
                                Card::TYPE_NORMAL_UNIT,
                                Card::TYPE_TRIGGER_UNIT
                            ],
                            true
                        )
                    ) {
                        $gameState->setPlayerError(new PlayerError(
                            $input->getPlayerHash(),
                            'Card must be a normal or trigger unit to be called'
                        ));

                        return false;
                    }

                    $callCardGrade = $callCard->getGrade();
                    if ($callCard->hasProperty(Card::PROPERTY_ONE_GRADE_LOWER_TO_CALL)) {
                        $callCardGrade--;
                    }
                    if ($callCard->getRace() === CardProperties::RACE_ASTRAL_DEITY) {
                        $callCardGrade = 0;
                    }
                    if ($callCardGrade > $playerField->getVanguard()->getGrade()) {
                        $gameState->setPlayerError(new PlayerError(
                            $input->getPlayerHash(),
                            'Vanguard grade too low'
                        ));

                        return false;
                    }

                    if (
                        $callCard->getRace() === CardProperties::RACE_ASTRAL_DEITY
                        && (
                            $callCircle !== Field::REARGUARD3
                            || !$player->getField()->getVanguard()->hasProperty(CardProperties::ASTRAL_PLANE_OPENED)
                        )
                    ) {
                        $gameState->setPlayerError(new PlayerError(
                            $input->getPlayerHash(),
                            'Astral Deity can be called only to Astral Plane'
                        ));

                        return false;
                    }

                    $retireCard = $playerField->getCircleCard($callCircle);
                    if ($retireCard !== null) {
                        $this->eventDispatcher->dispatch(
                            new CardEvent(
                                $gameState,
                                CardEventTypes::BEFORE_RETIRE,
                                $player->getHash(),
                                $retireCard
                            )
                        );
                        $retireCard->reset();
                        $player->getDrop()->addCard($retireCard);
                        $playerField->setCircleCard($callCircle, null);
                        $this->eventDispatcher->dispatch(
                            new CardEvent(
                                $gameState,
                                CardEventTypes::AFTER_RETIRE,
                                $player->getHash(),
                                $retireCard
                            )
                        );
                    }
                    $this->eventDispatcher->dispatch(
                        new CardEvent(
                            $gameState,
                            CardEventTypes::BEFORE_CALL_FROM_HAND,
                            $player->getHash(),
                            $callCard
                        )
                    );
                    $playerField->setCircleCard($callCircle, $callCard);
                    $player->getHand()->removeCardsByIds([$callCardId]);
                    $this->eventDispatcher->dispatch(
                        new CardEvent(
                            $gameState,
                            CardEventTypes::AFTER_CALL_FROM_HAND,
                            $player->getHash(),
                            $callCard
                        )
                    );
                    $gameState->addChatLog(
                        $player->getName()
                        . ' calls '
                        . $this->chatHelper->generateCardLink($callCard)
                        . ' from hand'
                    );
                }
            } else {
                $gameState->setPhase(Phases::PHASE_BATTLE);
                $this->eventDispatcher->dispatch(
                    new CardEvent(
                        $gameState,
                        CardEventTypes::PHASE_BATTLE_START,
                        $gameState->getActivePlayerHash()
                    )
                );
            }
        }

        return true;
    }

    public function progressState(State $state): bool
    {
        $state
            ->addUpdateMessage(
                (new DialogUpdate($state->getActivePlayerHash()))
                    ->setTitle([
                        'Main phase',
                        '1. Select valid card from hand and a field circle to call it',
                        '2. Select two field circles in same column to switch places',
                        '3. Select unit to use activated ability'
                    ])
                    ->addOption(DialogUpdate::OPTION_MAX_COUNT, 2)
            )
        ;

        return false;
    }
}
