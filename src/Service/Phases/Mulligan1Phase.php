<?php

declare(strict_types=1);

namespace App\Service\Phases;

use App\Entity\Game\Deck;
use App\Entity\Game\State;
use App\Entity\Game\PlayerError;
use App\Entity\PlayerInput;
use App\Entity\Update\DialogUpdate;
use App\Service\ChatHelper;

class Mulligan1Phase implements PhaseInterface
{
    private $chatHelper;

    public function __construct(
        ChatHelper $chatHelper
    ) {
        $this->chatHelper = $chatHelper;
    }

    public function getPhase(): string
    {
        return Phases::PHASE_MULLIGAN1;
    }

    protected function getCurrentPlayerHash(State $state): string
    {
        return $state->getActivePlayerHash();
    }

    public function processPlayerInput(State $gameState, PlayerInput $input): bool
    {
        if ($this->getCurrentPlayerHash($gameState) === $input->getPlayerHash()) {
            $player = $gameState->getPlayer($input->getPlayerHash());

            $selections = $input->getList();
            $validCards = [];
            foreach ($selections as $cardId) {
                if (
                    $player->getHand()->getCardById($cardId) !== null
                ) {
                    $validCards[] = $cardId;
                }
            }

            $cardCount = count($validCards);
            if (count($selections) !== $cardCount) {
                $gameState->setPlayerError(
                    new PlayerError($input->getPlayerHash(), 'Invalid selection')
                );

                return false;
            }

            $removedCards = $player->getHand()->removeCardsByIds($validCards);
            $player->getDeck()->addCards($removedCards, Deck::BOTTOM);
            $player->getHand()->addCards($player->getDeck()->drawCards($cardCount));
            $player->getDeck()->shuffle();
            if ($cardCount > 0) {
                $gameState->addChatLog(
                    sprintf(
                        '%s chooses to replace %d %s',
                        $player->getName(),
                        $cardCount,
                        $this->chatHelper->plural($cardCount, 'card')
                    )
                );
            } else {
                $gameState->addChatLog($player->getName() . ' chooses to keep all cards');
            }

            $this->internalProgressState($gameState);
        }

        return true;
    }

    protected function internalProgressState(State $state)
    {
        $state->setPhase(Phases::PHASE_MULLIGAN2);
        $state->addChatLog($state->getInactivePlayer()->getName() . ' is choosing starting hand');
    }

    public function progressState(State $state): bool
    {
        $state
            ->addUpdateMessage(
                (new DialogUpdate($this->getCurrentPlayerHash($state)))
                    ->setTitle('Choose up to 5 cards to replace')
                    ->addOption(DialogUpdate::OPTION_MAX_COUNT, 5)
            )
        ;

        return false;
    }
}
