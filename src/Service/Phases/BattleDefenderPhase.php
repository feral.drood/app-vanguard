<?php
declare(strict_types=1);

namespace App\Service\Phases;

use App\Entity\Game\Card;
use App\Entity\Game\CardEvent;
use App\Entity\Game\CardEventTypes;
use App\Entity\Game\State;
use App\Entity\Game\PlayerError;
use App\Entity\PlayerInput;
use App\Entity\Update\DialogUpdate;
use App\Service\ChatHelper;
use App\Service\Game\CardEventDispatcher;

class BattleDefenderPhase implements PhaseInterface
{
    private $chatHelper;
    private $eventDispatcher;

    public function __construct(
        ChatHelper $chatHelper,
        CardEventDispatcher $eventDispatcher
    ) {
        $this->chatHelper = $chatHelper;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function getPhase(): string
    {
        return Phases::PHASE_BATTLE_SELECT_DEFENDER;
    }

    public function processPlayerInput(State $gameState, PlayerInput $input): bool
    {
        if ($gameState->getActivePlayerHash() === $input->getPlayerHash()) {
            $player = $gameState->getActivePlayer();
            $opponent = $gameState->getInactivePlayer();
            $opponentField = $opponent->getField();
            $circleId = $input->getValue();
            if (
                $circleId !== null
                && ($circleCard = $opponent->getField()->getCircleCard($circleId)) !== null
                && $opponent->getField()->isFrontRow($circleId)
            ) {
                if ($circleCard->hasProperty(Card::PROPERTY_CANNOT_BE_ATTACKED)) {
                    $gameState->setPlayerError(
                        new PlayerError(
                            $input->getPlayerHash(),
                            $opponentField->getCircleCard($circleId)->getName() . ' cannot be attacked'
                        )
                    );
                } else {
                    $circleCard->setDefender(true);
                    $gameState->setDefender($circleId);
                    $attacker = $player->getField()->getCircleCard($gameState->getAttacker());
                    $attacker->setState(Card::STATE_REST);
                    $gameState->addChatLog(
                        $player->getName()
                        . ' chooses '
                        . $this->chatHelper->generateCardLink($opponentField->getCircleCard($circleId))
                        . ' as defender'
                    );

                    $this->eventDispatcher->dispatch(
                        new CardEvent(
                            $gameState,
                            CardEventTypes::BEFORE_ATTACK,
                            $player->getHash(),
                            $attacker
                        )
                    );
                    if ($gameState->getBooster() !== null) {
                        $booster = $player->getField()->getCircleCard($gameState->getBooster());
                        $booster->setState(Card::STATE_REST);
                        $this->eventDispatcher->dispatch(
                            new CardEvent(
                                $gameState,
                                CardEventTypes::BEFORE_BOOST,
                                $player->getHash(),
                                $booster
                            )
                        );
                    }
                    $gameState->setPhase(Phases::PHASE_BATTLE_SELECT_GUARDIAN);
                }
            } elseif ($circleId === null) {
                $player->getField()->getCircleCard($gameState->getAttacker())->setAttacker(false);
                if ($gameState->getBooster() !== null) {
                    $player->getField()->getCircleCard($gameState->getBooster())->setBooster(false);
                }
                $gameState->addChatLog(
                    $player->getName() .
                    ' cancels attack'
                );
                $gameState->setAttacker(null);
                $gameState->setBooster(null);
                $gameState->setPhase(Phases::PHASE_BATTLE);
            }
        }

        return true;
    }

    public function progressState(State $state): bool
    {
        $state
            ->addUpdateMessage(
                (new DialogUpdate($state->getActivePlayerHash()))
                ->setTitle('Select defender')
                ->addOption(DialogUpdate::OPTION_MAX_COUNT, 1)
            )
        ;

        return false;
    }
}
