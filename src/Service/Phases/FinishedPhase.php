<?php

declare(strict_types=1);

namespace App\Service\Phases;

use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Entity\Update\GameUpdate;

class FinishedPhase implements PhaseInterface
{
    public function getPhase(): string
    {
        return Phases::PHASE_FINISHED;
    }

    public function processPlayerInput(State $game, PlayerInput $input): bool
    {
        return true;
    }

    public function progressState(State $gameState): bool
    {
        return false;
    }
}
