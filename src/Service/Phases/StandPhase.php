<?php

declare(strict_types=1);

namespace App\Service\Phases;

use App\Entity\Game\Card;
use App\Entity\Game\State;
use App\Entity\PlayerInput;

class StandPhase implements PhaseInterface
{
    public function getPhase(): string
    {
        return Phases::PHASE_STAND;
    }

    public function processPlayerInput(State $state, PlayerInput $input): bool
    {
        return true;
    }

    public function progressState(State $state): bool
    {
        $player = $state->getActivePlayer();
        $fieldLocations = $player->getField()->getCircles();
        foreach ($fieldLocations as $location => $unit) {
            if (
                $unit !== null
                && $unit->getState() === Card::STATE_REST
            ) {
                $unit->setState(Card::STATE_STAND);
            }
        }
        $state->addChatLog(
            $player->getName() . ' stands all units'
        );
        $state->setPhase(Phases::PHASE_DRAW);

        return true;
    }
}
