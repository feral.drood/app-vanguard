<?php

declare(strict_types=1);

namespace App\Service\Phases;

use App\Entity\Game\Deck;
use App\Entity\Game\Field;
use App\Entity\Game\State;
use App\Entity\Game\Card;
use App\Entity\PlayerInput;
use App\Service\ChatHelper;

class StartPhase implements PhaseInterface
{
    private $chatHelper;

    public function __construct(
        ChatHelper $chatHelper
    ) {
        $this->chatHelper = $chatHelper;
    }

    public function getPhase(): string
    {
        return Phases::PHASE_START;
    }

    public function processPlayerInput(State $game, PlayerInput $input): bool
    {
        return true;
    }

    public function progressState(State $state): bool
    {
        $state
            ->setCurrentTurn($state->getCurrentTurn() + 1)
            ->setPhase(Phases::PHASE_STAND)
            ->setActivePlayerHash(
                $state->getCurrentTurn() > 0
                ? $state->getInactivePlayerHash()
                : $state->getActivePlayerHash()
            )
        ;
        $state->addChatLog(
            '--- Turn ' . $state->getCurrentTurn()
        );

        return true;
    }
}
