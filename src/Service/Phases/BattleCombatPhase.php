<?php

declare(strict_types=1);

namespace App\Service\Phases;

use App\Entity\Game\CardEvent;
use App\Entity\Game\CardEventTypes;
use App\Entity\Game\State;
use App\Entity\Game\Field;
use App\Entity\PlayerInput;
use App\Service\Game\CardEventDispatcher;

class BattleCombatPhase implements PhaseInterface
{
    private $battleAttackHelper;
    private $eventDispatcher;

    public function __construct(
        BattleAttackHelper $battleAttackHelper,
        CardEventDispatcher $eventDispatcher
    ) {
        $this->battleAttackHelper = $battleAttackHelper;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function getPhase(): string
    {
        return Phases::PHASE_BATTLE_COMBAT;
    }

    public function processPlayerInput(State $state, PlayerInput $input): bool
    {
        return true;
    }

    public function progressState(State $gameState): bool
    {
//        $this->eventDispatcher->dispatch(
//            new CardEvent(
//                $gameState,
//                CardEventTypes::BEFORE_ATTACK,
//                $gameState->getActivePlayerHash(),
//                $gameState->getActivePlayer()->getField()->getCircleCard($gameState->getAttacker())
//            )
//        );

        $attackSuccessful = $this->battleAttackHelper->isAttackSuccessful($gameState);
        if (
            (
                $gameState->getAttacker() === Field::VANGUARD
                || $gameState->getAttacker() === Field::REARGUARD3
            )
            && $gameState->getActivePlayer()->getField()->getCircleCard($gameState->getAttacker())->getDrive() > 0
        ) {
            $gameState->setPhase(Phases::PHASE_BATTLE_DRIVE);
        } elseif ($attackSuccessful && $gameState->getDefender() === Field::VANGUARD) {
            $gameState->setPhase(Phases::PHASE_BATTLE_DAMAGE);
        } elseif ($attackSuccessful) {
            $gameState->setPhase(Phases::PHASE_BATTLE_HIT);
        } else {
            $gameState->setPhase(Phases::PHASE_BATTLE_MISS);
        }

        return true;
    }
}
