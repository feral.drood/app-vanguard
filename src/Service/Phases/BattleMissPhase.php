<?php

declare(strict_types=1);

namespace App\Service\Phases;

use App\Entity\Game\CardEvent;
use App\Entity\Game\CardEventTypes;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Service\Game\CardEventDispatcher;

class BattleMissPhase implements PhaseInterface
{
    private $eventDispatcher;
    private $attackHelper;

    public function __construct(
        CardEventDispatcher $eventDispatcher,
        BattleAttackHelper $attackHelper
    ) {
        $this->eventDispatcher = $eventDispatcher;
        $this->attackHelper = $attackHelper;
    }

    public function getPhase(): string
    {
        return Phases::PHASE_BATTLE_MISS;
    }

    public function processPlayerInput(State $state, PlayerInput $input): bool
    {
        return true;
    }

    public function progressState(State $state): bool
    {
        $state->addChatLog('Attack fails: ' . $this->attackHelper->getCombatResult($state));
        $player = $state->getActivePlayer();
        $this->eventDispatcher->dispatch(
            new CardEvent(
                $state,
                CardEventTypes::AFTER_MISS,
                $player->getHash(),
                $player->getField()->getCircleCard($state->getAttacker())
            )
        );
        $this->eventDispatcher->dispatch(
            new CardEvent(
                $state,
                CardEventTypes::AFTER_ATTACK,
                $player->getHash(),
                $player->getField()->getCircleCard($state->getAttacker())
            )
        );

        $state->setPhase(Phases::PHASE_BATTLE_END);

        return true;
    }
}
