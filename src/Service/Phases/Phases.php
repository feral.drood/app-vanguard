<?php
declare(strict_types=1);

namespace App\Service\Phases;

final class Phases
{
    const PHASE_VANGUARD = 'vanguard';

    const PHASE_START = 'start';

    const PHASE_MULLIGAN1 = 'mulligan1';
    const PHASE_MULLIGAN2 = 'mulligan2';

    const PHASE_STAND = 'stand';
    const PHASE_DRAW = 'draw';
    const PHASE_ASSIST = 'assist';

    const PHASE_RIDE = 'ride';

    const PHASE_MAIN = 'main';

    const PHASE_BATTLE = 'battle';
    const PHASE_BATTLE_SELECT_DEFENDER = 'battle_defender';
    const PHASE_BATTLE_SELECT_GUARDIAN = 'battle_guardian';

    const PHASE_BATTLE_COMBAT = 'battle_combat';
    const PHASE_BATTLE_DRIVE = 'battle_drive';
    const PHASE_BATTLE_DAMAGE = 'battle_damage';
    const PHASE_BATTLE_HIT = 'battle_hit';
    const PHASE_BATTLE_MISS = 'battle_miss';

    const PHASE_BATTLE_END = 'battle_end';

    const PHASE_END = 'end';

    const PHASE_FINISHED = 'finished';
}
