<?php

declare(strict_types=1);

namespace App\Service\Phases;

use App\Entity\Game\Card;
use App\Entity\Game\CardEvent;
use App\Entity\Game\CardEventTypes;
use App\Entity\Game\CardProperties;
use App\Entity\Game\State;
use App\Entity\Game\Field;
use App\Entity\Game\PlayerError;
use App\Entity\PlayerInput;
use App\Entity\Update\DialogUpdate;
use App\Service\ChatHelper;
use App\Service\Game\CardEventDispatcher;

class RidePhase implements PhaseInterface
{
    private $chatHelper;
    private $eventDispatcher;

    public function __construct(
        ChatHelper $chatHelper,
        CardEventDispatcher $eventDispatcher
    ) {
        $this->chatHelper = $chatHelper;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function getPhase(): string
    {
        return Phases::PHASE_RIDE;
    }

    public function processPlayerInput(State $gameState, PlayerInput $input): bool
    {
        $player = $gameState->getActivePlayer();
        if ($player->getHash() === $input->getPlayerHash()) {
            $selectionCardId = $input->getValue();
            if ($selectionCardId === null) {
                $gameState->setPhase(Phases::PHASE_MAIN);
            } elseif (
                $player->getHand()->getCardById($selectionCardId) !== null
            ) {
                $card = $player->getHand()->getCardById($selectionCardId);
                $oldVanguard = $player->getField()->getVanguard();
                if (
                    !in_array(
                        $card->getType(),
                        [
                            Card::TYPE_NORMAL_UNIT,
                            Card::TYPE_TRIGGER_UNIT,
                        ],
                        true
                    )
                ) {
                    $gameState->setPlayerError(
                        new PlayerError($player->getHash(), 'Only normal and trigger units can be ridden')
                    );

                    return false;
                } elseif ($card->hasProperty(CardProperties::CANNOT_RIDE)) {
                    $gameState->setPlayerError(
                        new PlayerError($player->getHash(), 'Selected card cannot be ridden')
                    );

                    return false;
                } elseif (
                    $card->getGrade() === $oldVanguard->getGrade()
                    || $card->getGrade() === ($oldVanguard->getGrade() + 1)
                ) {
                    $gameState->addChatLog(
                        $player->getName()
                        . ' rides '
                        . $this->chatHelper->generateCardLink($card)
                    );
                    $this->eventDispatcher->dispatch(
                        new CardEvent(
                            $gameState,
                            CardEventTypes::BEFORE_RIDE,
                            $input->getPlayerHash(),
                            $card,
                            [$oldVanguard]
                        )
                    );
                    $player->getHand()->removeCardsByIds([$selectionCardId]);
                    $oldVanguard->reset();
                    $player->getSoul()->addCard($oldVanguard);
                    $player->getField()->setCircleCard(Field::VANGUARD, $card);
                    $this->eventDispatcher->dispatch(
                        new CardEvent(
                            $gameState,
                            CardEventTypes::AFTER_RIDE,
                            $input->getPlayerHash(),
                            $card,
                            [$oldVanguard]
                        )
                    );
//                    if ($card->getGift() !== null) {
//                        GiftProcessor::processGift($game, $input->getPlayerHash(), $card);
//                    }
                    $gameState->setPhase(Phases::PHASE_MAIN);
                } else {
                    $gameState->setPlayerError(
                        new PlayerError($player->getHash(), 'Incorrect grade of selected card')
                    );

                    return false;
                }
            } else {
                $gameState->setPlayerError(
                    new PlayerError($player->getHash(), 'Selected card not found in hand')
                );

                return false;
            }
        }

        return true;
    }

    public function progressState(State $gameState): bool
    {
        $player = $gameState->getActivePlayer();
        $vanguardGrade = $player->getField()->getVanguard()->getGrade();
        $rideAvailable = false;
        foreach ($player->getHand()->getCards() as $card) {
            if (
                $card->getGrade() === $vanguardGrade
                || $card->getGrade() === $vanguardGrade + 1
            ) {
                $rideAvailable = true;
                break;
            }
        }
        if ($rideAvailable) {
            $gameState
                ->addUpdateMessage(
                    (new DialogUpdate($gameState->getActivePlayerHash()))
                    ->setTitle([
                        'Ride Phase',
                        'Select a card from hand to ride'
                    ])
                    ->addOption(DialogUpdate::OPTION_MAX_COUNT, 1)
                )
            ;

            return false;
        } else {
            $gameState->setPhase(Phases::PHASE_MAIN);
        }

        return true;
    }
}
