<?php

declare(strict_types=1);

namespace App\Service\Phases;

use App\Entity\Game\CardEvent;
use App\Entity\Game\CardEventTypes;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Service\Game\CardEventDispatcher;

class BattleHitPhase implements PhaseInterface
{
    private $attackHelper;
    private $eventDispatcher;

    public function __construct(
        BattleAttackHelper $attackHelper,
        CardEventDispatcher $eventDispatcher
    ) {
        $this->attackHelper = $attackHelper;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function getPhase(): string
    {
        return Phases::PHASE_BATTLE_HIT;
    }

    public function processPlayerInput(State $gameState, PlayerInput $input): bool
    {
        return true;
    }

    public function progressState(State $state): bool
    {
        $state->addChatLog('Attack hits: ' . $this->attackHelper->getCombatResult($state));
        $player = $state->getActivePlayer();
        $this->eventDispatcher->dispatch(
            new CardEvent(
                $state,
                CardEventTypes::AFTER_HIT,
                $player->getHash(),
                $player->getField()->getCircleCard($state->getAttacker())
            )
        );
        $this->eventDispatcher->dispatch(
            new CardEvent(
                $state,
                CardEventTypes::AFTER_ATTACK,
                $player->getHash(),
                $player->getField()->getCircleCard($state->getAttacker())
            )
        );

        $state->setPhase(Phases::PHASE_BATTLE_END);

        return true;
    }
}
