<?php

declare(strict_types=1);

namespace App\Service\Phases;

use App\Entity\Game\Card;
use App\Entity\Game\CardEvent;
use App\Entity\Game\CardEventTypes;
use App\Entity\Game\State;
use App\Entity\Game\Field;
use App\Entity\PlayerInput;
use App\Service\ChatHelper;
use App\Service\Game\CardEventDispatcher;

class BattleDrivePhase implements PhaseInterface
{
    private $battleAttackHelper;
    private $eventDispatcher;
    private $chatHelper;

    public function __construct(
        BattleAttackHelper $battleAttackHelper,
        CardEventDispatcher $eventDispatcher,
        ChatHelper $chatHelper
    ) {
        $this->battleAttackHelper = $battleAttackHelper;
        $this->eventDispatcher = $eventDispatcher;
        $this->chatHelper = $chatHelper;
    }

    public function getPhase(): string
    {
        return Phases::PHASE_BATTLE_DRIVE;
    }

    public function processPlayerInput(State $state, PlayerInput $input): bool
    {
        return true;
    }

    private function continueState(State $state): void
    {
        $player = $state->getActivePlayer();
        if ($player->getTrigger() !== null) {
            $triggerUnit = $player->getTrigger();

            // Move trigger to soul if attacker is Astral Deity
            if ($state->getAttacker() === Field::REARGUARD3) {
                $player->getSoul()->addCard($triggerUnit);
            } else {
                $player->getHand()->addCard($triggerUnit);
            }
            $player->setTrigger(null);

            $this->eventDispatcher->dispatch(
                new CardEvent(
                    $state,
                    CardEventTypes::AFTER_DRIVE_CHECK,
                    $player->getHash(),
                    $triggerUnit
                )
            );
        }

        $isAttackSuccessful = $this->battleAttackHelper->isAttackSuccessful($state);
        $attacker = $player->getField()->getCircleCard($state->getAttacker());
        $attacker->addBonus(Card::BONUS_LENGTH_BATTLE, Card::BONUS_DRIVE, -1);

        if ($attacker->getDrive() > 0) {
            return;
        } elseif ($isAttackSuccessful && $state->getDefender() === Field::VANGUARD) {
            $state->setPhase(Phases::PHASE_BATTLE_DAMAGE);
        } elseif ($isAttackSuccessful) {
            $state->setPhase(Phases::PHASE_BATTLE_HIT);
        } else {
            $state->setPhase(Phases::PHASE_BATTLE_MISS);
        }
    }

    public function progressState(State $gameState): bool
    {
        $player = $gameState->getActivePlayer();

        if ($player->getTrigger() !== null) {
            $this->continueState($gameState);

            return true;
        }

        $cards = $player->getDeck()->drawCards(1);
        if (count($cards) < 1) {
            $this->continueState($gameState);

            return true;
        }

        $triggerUnit = array_shift($cards);
        $gameState->addChatLog(
            $player->getName()
            . ' drive checks '
            . $this->chatHelper->generateCardLink($triggerUnit)
        );
        $player->setTrigger($triggerUnit);
        $this->eventDispatcher->dispatch(
            new CardEvent(
                $gameState,
                CardEventTypes::BEFORE_DRIVE_CHECK,
                $player->getHash(),
                $triggerUnit
            )
        );

        return true;
    }
}
