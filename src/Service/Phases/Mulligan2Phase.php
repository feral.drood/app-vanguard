<?php

declare(strict_types=1);

namespace App\Service\Phases;

use App\Entity\Game\State;

class Mulligan2Phase extends Mulligan1Phase
{
    public function getPhase(): string
    {
        return Phases::PHASE_MULLIGAN2;
    }

    public function getCurrentPlayerHash(State $state): string
    {
        return $state->getInactivePlayerHash();
    }

    protected function internalProgressState(State $state)
    {
        $state->getActivePlayer()->getField()->getVanguard()->setFaceDown(false);
        $state->getInactivePlayer()->getField()->getVanguard()->setFaceDown(false);

        $state->setPhase(Phases::PHASE_START);
    }
}
