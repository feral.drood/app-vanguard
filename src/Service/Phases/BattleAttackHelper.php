<?php

declare(strict_types=1);

namespace App\Service\Phases;

use App\Entity\Game\Card;
use App\Entity\Game\State;

class BattleAttackHelper
{
    public function getAttackerPower(State $state): int
    {
        $attackingPlayer = $state->getActivePlayer();
        $attacker = $state->getActivePlayer()->getField()->getCircleCard($state->getAttacker());
        $attackerPower = $attacker->getPower();
        if ($state->getBooster() !== null) {
            $attackerPower += $attackingPlayer->getField()->getCircleCard($state->getBooster())->getPower();
        }

        return $attackerPower;
    }

    public function getDefenderPower(State $state): int
    {
        $defendingPlayer = $state->getInactivePlayer();
        $defender = $state->getInactivePlayer()->getField()->getCircleCard($state->getDefender());
        $defenderPower = $defender->getPower();
        $defenderPower += $defendingPlayer->getGuardians()->getShieldTotal();

        return $defenderPower;
    }

    public function getCombatResult(State $state): string
    {
        $attackingPlayer = $state->getActivePlayer();
        $defendingPlayer = $state->getInactivePlayer();
        $attacker = $state->getActivePlayer()->getField()->getCircleCard($state->getAttacker());
        $defender = $state->getInactivePlayer()->getField()->getCircleCard($state->getDefender());

        if ($defender->hasProperty(Card::PROPERTY_CANNOT_BE_HIT)) {
            return 'Target cannot be hit';
        }

        $attackerPower = $attacker->getPower();
        if ($state->getBooster() !== null) {
            $attackerPower += $attackingPlayer->getField()->getCircleCard($state->getBooster())->getPower();
        }
        $defenderPower = $defender->getPower();
        $defenderPower += $defendingPlayer->getGuardians()->getShieldTotal();

        return $attackerPower . ' vs ' . $defenderPower;
    }

    public function isAttackSuccessful(State $state): bool
    {
        $attackingPlayer = $state->getActivePlayer();
        $defendingPlayer = $state->getInactivePlayer();
        $attacker = $state->getActivePlayer()->getField()->getCircleCard($state->getAttacker());
        $defender = $state->getInactivePlayer()->getField()->getCircleCard($state->getDefender());

        if ($defender->hasProperty(Card::PROPERTY_CANNOT_BE_HIT)) {
            return false;
        }
        $attackerPower = $attacker->getPower();
        if ($state->getBooster() !== null) {
            $attackerPower += $attackingPlayer->getField()->getCircleCard($state->getBooster())->getPower();
        }
        $defenderPower = $defender->getPower();
        $defenderPower += $defendingPlayer->getGuardians()->getShieldTotal();

        return $attackerPower >= $defenderPower;
    }
}
