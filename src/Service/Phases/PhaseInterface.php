<?php

declare(strict_types=1);

namespace App\Service\Phases;

use App\Entity\Game\State;
use App\Entity\PlayerInput;

interface PhaseInterface
{
    /**
     * Return phase key
     *
     * @return string
     */
    public function getPhase(): string;

    /**
     * Process user action
     *
     * @param State $state
     * @param PlayerInput $input
     * @return bool
     */
    public function processPlayerInput(State $state, PlayerInput $input): bool;

    /**
     * Progress game  without player input, return if player input is required
     *
     * @param State $state
     *
     * @return bool
     */
    public function progressState(State $state): bool;
}
