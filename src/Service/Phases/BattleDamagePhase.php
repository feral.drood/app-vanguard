<?php

declare(strict_types=1);

namespace App\Service\Phases;

use App\Entity\Game\CardEvent;
use App\Entity\Game\CardEventTypes;
use App\Entity\Game\Field;
use App\Entity\Game\State;
use App\Entity\Game\Card;
use App\Entity\PlayerInput;
use App\Service\ChatHelper;
use App\Service\Game\CardEventDispatcher;

class BattleDamagePhase implements PhaseInterface
{
    private $eventDispatcher;
    private $chatHelper;

    public function __construct(
        CardEventDispatcher $eventDispatcher,
        ChatHelper $chatHelper
    ) {
        $this->eventDispatcher = $eventDispatcher;
        $this->chatHelper = $chatHelper;
    }

    public function getPhase(): string
    {
        return Phases::PHASE_BATTLE_DAMAGE;
    }

    public function processPlayerInput(State $state, PlayerInput $input): bool
    {
        return true;
    }

    private function continueState(State $state): void
    {
        $opponent = $state->getInactivePlayer();
        $triggerUnit = $opponent->getTrigger();

        if ($triggerUnit !== null) {
            $opponent->getDamage()->addCard($triggerUnit);
            $opponent->setTrigger(null);

            $this->eventDispatcher->dispatch(
                new CardEvent(
                    $state,
                    CardEventTypes::AFTER_DAMAGE_CHECK,
                    $opponent->getHash(),
                    $triggerUnit
                )
            );
        }

        $attacker = $state->getActivePlayer()->getField()->getCircleCard($state->getAttacker());
        $attacker->addBonus(Card::BONUS_LENGTH_BATTLE, Card::BONUS_DAMAGE, -1);
        if ($attacker->getDamage() === 0) {
            $state->setPhase(Phases::PHASE_BATTLE_HIT);
        }
    }

    public function progressState(State $gameState): bool
    {
        $opponent = $gameState->getInactivePlayer();

        if ($opponent->getTrigger() !== null) {
            $this->continueState($gameState);

            return true;
        }

        $cards = $opponent->getDeck()->drawCards(1);
        if (count($cards) < 1) {
            $this->continueState($gameState);

            return true;
        }

        $triggerUnit = array_shift($cards);
        $gameState->addChatLog(
            $opponent->getName() . ' damage checks ' . $this->chatHelper->generateCardLink($triggerUnit)
        );

        $opponent->setTrigger($triggerUnit);
        $this->eventDispatcher->dispatch(
            new CardEvent(
                $gameState,
                CardEventTypes::BEFORE_DAMAGE_CHECK,
                $opponent->getHash(),
                $triggerUnit
            )
        );

        return true;
    }
}
