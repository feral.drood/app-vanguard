<?php
declare(strict_types=1);

namespace App\Service;

use App\Entity\Game;
use App\Entity\User;
use App\Repository\GameRepository;

class UserCurrentGameDataProvider
{
    private $hashGenerator;
    private $gameRepository;

    public function __construct(
        HashGenerator $hashGenerator,
        GameRepository $gameRepository
    ) {
        $this->hashGenerator = $hashGenerator;
        $this->gameRepository = $gameRepository;
    }

    public function getUserCurrentGameData(User $user): array
    {
        $output = [
            'user_hash' => $this->hashGenerator->encodeHash($user->getId()),
            'user_token' => $user->getToken(),
            'user_name' => $user->getName(),
            'user_picture' => $user->getPicture(),
        ];
        $currentGame = $this->gameRepository->getOneActiveByUserId($user->getId());
        if ($currentGame !== null) {
            $output = array_merge($output, $this->getGameData($currentGame, $user));
        }

        return $output;
    }

    public function getGameData(Game $game, User $user): array
    {
        $output = [
            'game_hash' => $this->hashGenerator->encodeHash($game->getId()),
        ];
        if (
            $game->getUser1Id() === $user->getId()
            || $game->getUser2Id() !== $user->getId()
        ) {
            $output['player_hash'] = $this->hashGenerator->encodeHash($game->getUser1Id());
            if ($game->getUser2Id() === 0) {
                $output['opponent_hash'] = 'BOT';
            } elseif ($game->getUser2Id() !== null) {
                $output['opponent_hash'] = $this->hashGenerator->encodeHash($game->getUser2Id());
            }
        } else {
            $output['player_hash'] = $this->hashGenerator->encodeHash($game->getUser2Id());
            if ($game->getUser1Id() === 0) {
                $output['opponent_hash'] = 'BOT';
            } elseif ($game->getUser1Id() !== null) {
                $output['opponent_hash'] = $this->hashGenerator->encodeHash($game->getUser1Id());
            }
        }

        return $output;
    }
}
