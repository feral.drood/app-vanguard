<?php
declare(strict_types=1);

namespace App\Service;

use Hashids\Hashids;

class HashGenerator
{
    private $hashids;

    public function __construct(
        Hashids $hashids
    ) {
        $this->hashids = $hashids;
    }

    public function decodeHash(string $hash): ?int
    {
        $result = $this->hashids->decode($hash);
        if (count($result) > 0) {
            return $result[0];
        }

        return null;
    }

    public function encodeHash(int $id): string
    {
        return $this->hashids->encode($id);
    }
}
