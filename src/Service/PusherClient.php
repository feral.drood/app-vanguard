<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Update\UpdateDataInterface;
use Pusher\Pusher;

class PusherClient
{
    private const CHANNEL_PREFIX = 'game-';
    private const PUSHER_EVENT = 'game.update';

    private const MESSAGE_TYPE = 'type';
    private const MESSAGE_TYPE_UPDATES = 'game.updates';
    private const MESSAGE_DATA = 'data';

    private $authKey;
    private $secret;
    private $appId;
    private $options;
    private $client;

    public function __construct(
        $authKey,
        $secret,
        $appId,
        $options
    ) {
        $this->authKey = $authKey;
        $this->secret = $secret;
        $this->appId = $appId;
        $this->options = $options;
    }

    private function getClient(): Pusher
    {
        if ($this->client === null) {
            $this->client = new Pusher($this->authKey, $this->secret, $this->appId, $this->options);
        }

        return $this->client;
    }

    public function sendUpdates(string $gameHash, array $updates)
    {
        $data = [];
        foreach ($updates as $update) {
            $data[] = ($update instanceof UpdateDataInterface ? $update->getData() : $data);
        }

        if (count($data) > 0) {
            $this->getClient()->trigger(
                self::CHANNEL_PREFIX . $gameHash,
                self::PUSHER_EVENT,
                [
                    self::MESSAGE_TYPE => self::MESSAGE_TYPE_UPDATES,
                    self::MESSAGE_DATA => $data,
                ]
            );
        }
    }
}
