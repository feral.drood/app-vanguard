<?php

declare(strict_types=1);

namespace App\Service\Game;

use App\Entity\Ability\Ability;
use App\Entity\Ability\AbilityPart;
use App\Entity\Game\PlayerError;
use App\Entity\Game\State;
use App\Entity\Game\Card;
use App\Service\AbilityPartManager;

class ActivatedAbilityProcessor
{
    private $abilityPartManager;

    public function __construct(
        AbilityPartManager $abilityPartManager
    ) {
        $this->abilityPartManager = $abilityPartManager;
    }

    public function process(State $state, string $playerHash, Card $card)
    {
        if ($card->hasAbilities()
            && $state->getAbilityStack()->isEmpty()
        ) {
            foreach ($card->getAbilities() as $key => $abilityData) {
                $ability = new Ability($abilityData);
                if (
                    $ability->getType() === Ability::TYPE_ACT
                ) {
                    $conditionsPassed = true;
                    foreach ($ability->getConditions() as $conditionData) {
                        $conditionPart = (new AbilityPart($conditionData))
                            ->setPlayerHash($playerHash)
                            ->setEventCardId($card->getId())
                            ->setSelfCardId($card->getId())
                        ;
                        $conditionProcessor = $this->abilityPartManager->getConditionAbilityPartProcessor(
                            $conditionPart->getType()
                        );

                        $conditionsPassed = $conditionsPassed && $conditionProcessor->isPassed($state, $conditionPart);
                    }

                    if ($conditionsPassed) {
                        $ability
                            ->setId($card->getId() . '?' . $key)
                            ->setName($card->getName() . ' (ACT)')
                            ->setPlayerHash($playerHash)
                            ->setSelfCardId($card->getId())
                            ->setEventCardId($card->getId())
                        ;
                        $state->getAbilityStack()->addAbility($ability);

                        return true;
                    }
                }
            }
        }

        $state->setPlayerError(
            new PlayerError(
                $playerHash,
                'Unit has no activated abilities or conditions are not met'
            )
        );

        return false;
    }
}
