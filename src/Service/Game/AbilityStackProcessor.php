<?php

declare(strict_types=1);

namespace App\Service\Game;

use App\Entity\Ability\Ability;
use App\Entity\Ability\AbilityPart;
use App\Entity\Game\PlayerError;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Entity\Update\DialogUpdate;
use App\Service\AbilityPartManager;

class AbilityStackProcessor
{
    private const PASS_ACTION = 'pass';
    private const PASS_TITLE = 'Pass';

    private $abilityPartManager;

    public function __construct(
        AbilityPartManager $abilityPartManager
    )
    {
        $this->abilityPartManager = $abilityPartManager;
    }

    public function processPlayerInput(State $state, PlayerInput $playerInput): bool
    {
        $abilityStack = $state->getAbilityStack();

        if ($abilityStack->getCostCount() > 0) {
            if (empty($playerInput->getList())) {
                $this->abortCurrentAbility($state, $playerInput);

                return true;
            }
            $abilityPartData = $abilityStack->getCost();
            if ($abilityPartData->getPlayerHash() !== $playerInput->getPlayerHash()) {
                $state->setPlayerError(
                    new PlayerError(
                        $playerInput->getPlayerHash(),
                        'Player input mismatch'
                    )
                );

                return false;
            }
            $abilityPart = $this->abilityPartManager->getActionAbilityPartProcessor($abilityPartData->getType());
            if ($abilityPart->processPlayerInput($state, $abilityPartData, $playerInput)) {
                $abilityStack->removeCost();

                return true;
            }
        } elseif ($abilityStack->getEffectCount() > 0) {
            $abilityPartData = $abilityStack->getEffect();
            if ($abilityPartData->getPlayerHash() !== $playerInput->getPlayerHash()) {
                $state->setPlayerError(
                    new PlayerError(
                        $playerInput->getPlayerHash(),
                        'Player input mismatch'
                    )
                );

                return false;
            }
            $abilityPart = $this->abilityPartManager->getActionAbilityPartProcessor($abilityPartData->getType());
            if ($abilityPart->processPlayerInput($state, $abilityPartData, $playerInput)) {
                $abilityStack->removeEffect();

                return true;
            }
        } else {
            $abilities = $abilityStack->getAbilities();
            $mandatoryCount = 0;
            foreach ($abilities as $index => $unpackedAbilityData) {
                $abilityData = new Ability($unpackedAbilityData);
                if (
                    $abilityData->getPlayerHash() === $playerInput->getPlayerHash()
                    && $abilityData->getId() === $playerInput->getValue()
                ) {
                    $state->addChatLog(
                        $state->getPlayer($playerInput->getPlayerHash())->getName()
                        . ' activates '
                        . $abilityData->getName()
                        . ' ability'
                    );
                    $this->unpackAbility($state, $abilityData);
                    $abilityStack->removeAbility($index);

                    return true;
                }
                if (count($abilityData->getCosts()) === 0) {
                    $mandatoryCount++;
                }
            }
            if (
                $playerInput->getValue() === self::PASS_ACTION
                && $mandatoryCount === 0
            ) {
                $foundAbility = true;
                while ($foundAbility) {
                    $foundAbility = false;
                    $abilities = $abilityStack->getAbilities();
                    for ($c = 0; $c < count($abilities); $c++) {
                        $ability = $abilityStack->getAbility($c);
                        if ($ability->getPlayerHash() === $playerInput->getPlayerHash()) {
                            $abilityStack->removeAbility($c);
                            $foundAbility = true;
                            break;
                        }
                    }
                }

                return true;
            }
        }

        return false;
    }

    public function progressState(State $state): bool
    {
        $abilityStack = $state->getAbilityStack();

        if ($abilityStack->getCostCount() > 0) {
            $abilityPartData = $abilityStack->getCost();
            $abilityPart = $this->abilityPartManager->getActionAbilityPartProcessor($abilityPartData->getType());
            if ($abilityPart->progressState($state, $abilityPartData)) {
                $abilityStack->removeCost();

                return true;
            }

            $state->addUpdateMessage(
                $abilityPart->getPublicUpdateData($state, $abilityPartData)
            );
        } elseif ($abilityStack->getEffectCount() > 0) {
            $abilityPartData = $abilityStack->getEffect();
            $abilityPart = $this->abilityPartManager->getActionAbilityPartProcessor($abilityPartData->getType());
            if ($abilityPart->progressState($state, $abilityPartData)) {
                $abilityStack->removeEffect();

                return true;
            }

            $state->addUpdateMessage(
                $abilityPart->getPublicUpdateData($state, $abilityPartData)
            );
        } else {
            $abilities = $abilityStack->getAbilities();
            $mandatoryCount = [
                $state->getActivePlayerHash() => 0,
                $state->getInactivePlayerHash() => 0,
            ];
            $actions = [];
            foreach ($abilities as $unpackedAbilityData) {
                $abilityData = new Ability($unpackedAbilityData);
                if (!isset($actions[$abilityData->getPlayerHash()])) {
                    $actions[$abilityData->getPlayerHash()] = [];
                }
                if (count($abilityData->getCosts()) === 0) {
                    $mandatoryCount[$abilityData->getPlayerHash()]++;
                }
                $actions[$abilityData->getPlayerHash()][$abilityData->getId()] = $abilityData->getName();
            }

            if (isset($actions[$state->getActivePlayerHash()])) {
                if (
                    $mandatoryCount[$state->getActivePlayerHash()] === 1
                    && count($actions[$state->getActivePlayerHash()]) === 1
                ) {
                    $this->unpackAbilityById(
                        $state,
                        array_keys($actions[$state->getActivePlayerHash()])[0]
                    );

                    return true;
                }
                if ($mandatoryCount[$state->getActivePlayerHash()] === 0) {
                    $actions[$state->getActivePlayerHash()][self::PASS_ACTION] = self::PASS_TITLE;
                }
                $state->addUpdateMessage(
                    (new DialogUpdate($state->getActivePlayerHash()))
                    ->setTitle('Choose ability')
                    ->setActions(
                        $actions[$state->getActivePlayerHash()]
                    )
                );
            } else {
                $opponentHash = $state->getInactivePlayerHash();
                if (
                    $mandatoryCount[$opponentHash] === 1
                    && count($actions[$opponentHash]) === 1
                ) {
                    $this->unpackAbilityById(
                        $state,
                        array_keys($actions[$opponentHash])[0]
                    );

                    return true;
                }
                if ($mandatoryCount[$state->getActivePlayerHash()] === 0) {
                    $actions[$state->getActivePlayerHash()][self::PASS_ACTION] = self::PASS_TITLE;
                }
                $state->addUpdateMessage(
                    (new DialogUpdate($state->getInactivePlayerHash()))
                        ->setTitle('Choose ability')
                        ->setActions(
                            $actions[$opponentHash]
                        )
                );
            }
        }

        return false;
    }

    private function unpackAbilityById(State $state, string $abilityId) {
        $abilities = $state->getAbilityStack()->getAbilities();

        foreach ($abilities as $index => $abilityData) {
            $ability = new Ability($abilityData);
            if ($ability->getId() === $abilityId) {
                $this->unpackAbility($state, $ability);
                $state->getAbilityStack()->removeAbility($index);
                $state->addChatLog(
                    $state->getPlayer($ability->getPlayerHash())->getName()
                    . ' activates '
                    . $ability->getName()
                    . ' ability'
                );
            }
        }
    }

    private function unpackAbility(State $state, Ability $abilityData)
    {
        $abilityStack = $state->getAbilityStack();

        $costs = $abilityData->getCosts();
        foreach ($costs as $costData) {
            $abilityStack->addCost(
                (new AbilityPart($costData))
                ->setPlayerHash($abilityData->getPlayerHash())
                ->setEventCardId($abilityData->getEventCardId())
                ->setSelfCardId($abilityData->getSelfCardId())
            );
        }

        $effects = $abilityData->getEffects();
        foreach ($effects as $effectData) {
            $abilityStack->addEffect(
                (new AbilityPart($effectData))
                    ->setPlayerHash($abilityData->getPlayerHash())
                    ->setEventCardId($abilityData->getEventCardId())
                    ->setSelfCardId($abilityData->getSelfCardId())
            );
        }
    }

    private function abortCurrentAbility(State $state, PlayerInput $playerInput)
    {
        $stack = $state->getAbilityStack();
        $cardSelfId = null;
        $cardEventId = null;

        while ($cost = $stack->getCost()) {
            if ($cost->getPlayerHash() !== $playerInput->getPlayerHash()) {
                break;
            }

            if ($cardSelfId === null) {
                $cardSelfId = $cost->getSelfCardId();
                $cardEventId = $cost->getEventCardId();
            }

            if (
                $cost->getEventCardId() !== $cardEventId
                || $cost->getSelfCardId() !== $cardSelfId
            ) {
                break;
            }

            $stack->removeCost();
        }

        while ($effect = $stack->getEffect()) {
            if ($effect->getPlayerHash() !== $playerInput->getPlayerHash()) {
                break;
            }

            if (
                $effect->getEventCardId() !== $cardEventId
                || $effect->getSelfCardId() !== $cardSelfId
            ) {
                break;
            }

            $stack->removeEffect();
        }

        if ($cardSelfId !== null) {
            $state->addChatLog($state->getPlayer($playerInput->getPlayerHash())->getName() . ' aborts queued ability');
        }
    }
}
