<?php

declare(strict_types=1);

namespace App\Service\Game;

use App\Entity\PlayerInput;
use App\Bot\BotManager;
use App\Entity\Game\State;

class BotProcessor
{
    private $manager;

    public function __construct(
        BotManager $manager
    ) {
        $this->manager = $manager;
    }

    public function progressState(State $state): ?PlayerInput
    {
        $player = $state->getPlayer(BotManager::BOT_HASH);
        if ($player !== null) {
            $bot = $this->manager->getBot($player->getName());

            return $bot->progressState(BotManager::BOT_HASH, $state);
        }

        return null;
    }
}
