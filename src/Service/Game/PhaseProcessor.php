<?php

declare(strict_types=1);

namespace App\Service\Game;

use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Service\Phases\PhaseManager;

class PhaseProcessor
{
    private $phaseManager;

    public function __construct(
        PhaseManager $phaseManager
    ) {
        $this->phaseManager = $phaseManager;
    }

    public function processPlayerInput(State $state, PlayerInput $playerInput)
    {
        $this->phaseManager->getPhase(
            $state->getPhase()
        )
        ->processPlayerInput(
            $state,
            $playerInput
        );
    }

    public function progressState(State $state): bool
    {
        return $this->phaseManager->getPhase(
           $state->getPhase()
        )->progressState($state);
    }
}
