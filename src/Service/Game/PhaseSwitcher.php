<?php

declare(strict_types=1);

namespace App\Service\Game;

use App\Entity\Game\CardEvent;
use App\Entity\Game\State;

class PhaseSwitcher
{
    private const EVENT_PREFIX = 'phase_';
    private const EVENT_START = '_start';
    private const EVENT_END = '_end';

    private $eventDispatcher;

    public function __construct(
        CardEventDispatcher $eventDispatcher
    ) {
        $this->eventDispatcher = $eventDispatcher;
    }

    public function setNextPhase(State $state, string $nextPhase): void
    {
        $state->setNextPhase($nextPhase);
        $this->eventDispatcher->dispatch(
            new CardEvent(
                $state,
                self::EVENT_PREFIX . $state->getPhase() . self::EVENT_END,
                $state->getActivePlayerHash()
            )
        );
    }

    public function switchPhase(State $state): bool
    {
        $state
            ->setPhase($state->getNextPhase())
            ->setNextPhase(null)
        ;
        $this->eventDispatcher->dispatch(
            new CardEvent(
                $state,
                self::EVENT_PREFIX . $state->getPhase() . self::EVENT_START,
                $state->getActivePlayerHash()
            )
        );

        return true;
    }
}
