<?php

declare(strict_types=1);

namespace App\Service\Game;

use App\Entity\Ability\Ability;
use App\Entity\Ability\AbilityPart;
use App\Entity\Game\CardProperties;
use App\Entity\Game\Deck;
use App\Entity\Game\Field;
use App\Entity\Game\Player;
use App\Entity\Game\State;
use App\Entity\Game\Card;
use App\Service\AbilityPartManager;
use App\Service\ChatHelper;

class ContinuousAbilityProcessor
{
    private const ACCEL1 = 'accel1';
    private const ACCEL2 = 'accel2';
    private const FORCE1 = 'force1';
    private const FORCE2 = 'force2';
    private const PROTECT2 = 'protect2';

    private $abilityPartManager;
    private $chatHelper;

    public function __construct(
        AbilityPartManager $abilityPartManager,
        ChatHelper $chatHelper
    ) {
        $this->abilityPartManager = $abilityPartManager;
        $this->chatHelper = $chatHelper;
    }

    public function recalculate(State $state)
    {
        foreach ($state->getPlayers() as $player) {
            $fieldCircles = $player->getField()->getCircles();
            foreach ($fieldCircles as $circle => $unit) {
                if ($unit !== null) {
                    $unit->resetBonus(Card::BONUS_LENGTH_PERMANENT);
                }
            }
            $handCards = $player->getHand()->getCards();
            foreach ($handCards as $handCard) {
                $handCard->resetBonus(Card::BONUS_LENGTH_PERMANENT);
            }
        }

        foreach ($state->getPlayers() as $player) {
            $fieldCircles = $player->getField()->getCircles();
            foreach ($player->getGiftCircles() as $circle => $gift) {
                $unit = $fieldCircles[$circle];
                if ($unit === null) {
                    continue;
                }

                if (
                    $gift === Card::GIFT_ACCEL
                    && $player->getGiftType(Card::GIFT_ACCEL) === self::ACCEL1
                    && $player->getHash() === $state->getActivePlayerHash()
                ) {
                    $unit->addBonus(Card::BONUS_LENGTH_PERMANENT, Card::BONUS_POWER, 10);
                } elseif (
                    $gift === Card::GIFT_ACCEL
                    && $player->getGiftType(Card::GIFT_ACCEL) === self::ACCEL2
                    && $player->getHash() === $state->getActivePlayerHash()
                ) {
                    $unit->addBonus(Card::BONUS_LENGTH_PERMANENT, Card::BONUS_POWER, 5);
                } elseif (
                    $gift === Card::GIFT_FORCE
                    && $player->getGiftType(Card::GIFT_FORCE) === self::FORCE1
                    && $player->getHash() === $state->getActivePlayerHash()
                ) {
                    $unit->addBonus(Card::BONUS_LENGTH_PERMANENT, Card::BONUS_POWER, 10);
                } elseif (
                    $gift === Card::GIFT_FORCE
                    && $player->getGiftType(Card::GIFT_FORCE) === self::FORCE2
                    && $player->getHash() === $state->getActivePlayerHash()
                ) {
                    $unit->addBonus(Card::BONUS_LENGTH_PERMANENT, Card::BONUS_CRITICAL, 1);
                } elseif (
                    $gift === Card::GIFT_PROTECT
                    && $player->getGiftType(Card::GIFT_PROTECT) === self::PROTECT2
                ) {
                    $unit->addBonus(Card::BONUS_LENGTH_PERMANENT, Card::BONUS_POWER, 5);
                }
            }

            foreach ($fieldCircles as $circle => $unit) {
                if ($unit !== null) {
                    if ($unit->hasAbilities() && !$unit->isDeleted()) {
                        $this->processCardAbilities($state, $player, $unit);
                    }
                }
            }

            $handCards = $player->getHand()->getCards();
            foreach ($handCards as $card) {
                if ($card->hasAbilities()) {
                    $this->processCardAbilities($state, $player, $card);
                }
            }

            // Astral plane support
            $vanguard = $player->getField()->getVanguard();
            $rearguard3 = $player->getField()->getCircleCard(Field::REARGUARD3);
            if ($vanguard !== null ) {
                if ($vanguard->hasProperty(CardProperties::ASTRAL_PLANE_OPENED)) {
                    if (
                        $rearguard3 !== null
                        && $rearguard3->getRace() !== 'Astral Deity'
                    ) {
                        $player->getField()->setCircleCard(Field::REARGUARD3, null);
                        $rearguard3->reset();
                        $player->getDrop()->addCard($rearguard3);
                        $state->addChatLog(
                            $player->getName()
                            . ' retires '
                            . $this->chatHelper->generateCardLink($rearguard3)
                            . ' due to open Astral Plane'
                        );
                    }
                } else {
                    if (
                        $rearguard3 !== null
                        && $rearguard3->getRace() === 'Astral Deity'
                    ) {
                        $player->getField()->setCircleCard(Field::REARGUARD3, null);
                        $rearguard3->reset();
                        $player->getDrop()->addCard($rearguard3);
                        $state->addChatLog(
                            $player->getName()
                            . ' retires '
                            . $this->chatHelper->generateCardLink($rearguard3)
                            . ' due to closed Astral Plane'
                        );
                    }
                }
            }
        }
    }

    private function processCardAbilities(State $state, Player $player, Card $card)
    {
        foreach ($card->getAbilities() as $abilityData) {
            $ability = new Ability($abilityData);
            if ($ability->getType() === Ability::TYPE_CONT) {
                $conditionsPassed = true;
                $conditions = $ability->getConditions();
                foreach ($conditions as $conditionData) {
                    $conditionPart = (new AbilityPart($conditionData))
                        ->setPlayerHash($player->getHash())
                        ->setSelfCardId($card->getId())
                        ->setEventCardId($card->getId());
                    $conditionProcessor = $this->abilityPartManager->getConditionAbilityPartProcessor(
                        $conditionPart->getType()
                    );

                    $conditionsPassed =
                        $conditionsPassed
                        && $conditionProcessor->isPassed(
                            $state,
                            $conditionPart
                        )
                    ;
                }

                if ($conditionsPassed) {
                    $effects = $ability->getEffects();

                    foreach ($effects as $effectData) {
                        $effectPart = (new AbilityPart($effectData))
                            ->setPlayerHash($player->getHash())
                            ->setSelfCardId($card->getId())
                            ->setEventCardId($card->getId())
                        ;
                        $effectProcessor = $this->abilityPartManager->getActionAbilityPartProcessor(
                            $effectPart->getType()
                        );
                        $effectProcessor->progressState($state, $effectPart);
                    }
                }
            }
        }
    }
}
