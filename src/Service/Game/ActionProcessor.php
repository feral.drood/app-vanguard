<?php

declare(strict_types=1);

namespace App\Service\Game;

use App\Entity\Game\PlayerError;
use App\Entity\Game\State;
use App\Entity\PlayerInput;

class ActionProcessor
{
    private $phaseProcessor;
    private $abilityStackProcessor;
    private $continuousAbilityProcessor;
    private $winConditionProcessor;
    private $botProcessor;
    private $phaseSwitcher;

    public function __construct(
        PhaseProcessor $phaseProcessor,
        AbilityStackProcessor $abilityStackProcessor,
        ContinuousAbilityProcessor $continuousAbilityProcessor,
        WinConditionProcessor $winConditionProcessor,
        BotProcessor $botProcessor,
        PhaseSwitcher $phaseSwitcher
    ) {
        $this->phaseProcessor = $phaseProcessor;
        $this->abilityStackProcessor = $abilityStackProcessor;
        $this->continuousAbilityProcessor = $continuousAbilityProcessor;
        $this->winConditionProcessor = $winConditionProcessor;
        $this->botProcessor = $botProcessor;
        $this->phaseSwitcher = $phaseSwitcher;
    }

    public function processPlayerInput(State $state, PlayerInput $input)
    {
        $botCounter = 0;
        $originalInput = $input;

        while ($input !== null) {
            if (!$state->getAbilityStack()->isEmpty()) {
                $this->abilityStackProcessor->processPlayerInput($state, $input);
            } else {
                $this->phaseProcessor->processPlayerInput($state, $input);
            }

            $continue = true;
            while ($continue) {
                if (!$state->getAbilityStack()->isEmpty()) {
                    $continue = $this->abilityStackProcessor->progressState($state);
                } elseif ($state->getNextPhase() !== null) {
                    $continue = $this->phaseSwitcher->switchPhase($state);
                } else {
                    $continue = $this->phaseProcessor->progressState($state);
//                    $continue = $continue && $state->getAbilityStack()->isEmpty();
                }
            }
            $this->continuousAbilityProcessor->recalculate($state);

            if ($this->winConditionProcessor->checkWinCondition($state)) {
                $input = null;
            } else {
                $botCounter++;
                $input = $this->botProcessor->progressState($state);
                if ($botCounter > 50) {
                    $input = null;
                    $state->setPlayerError(
                        new PlayerError(
                            $originalInput->getPlayerHash(),
                            'Bot got stuck :('
                        )
                    );
                }
            }
        }
    }
}
