<?php

declare(strict_types=1);

namespace App\Service\Game;

use App\Entity\Game\CardEvent;
use App\Entity\Game\CardEventTypes;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class CardEventDispatcher
{
    private $eventDispatcher;

    public function __construct(
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->eventDispatcher = $eventDispatcher;
    }

    public function dispatch(CardEvent $cardEvent)
    {
        $this->eventDispatcher->dispatch(
            $cardEvent,
            CardEventTypes::EVENT_PREFIX . $cardEvent->getType()
        );
    }
}
