<?php

declare(strict_types=1);

namespace App\Service\Game;

use App\Entity\Game\State;
use App\Entity\Update\GameUpdate;
use App\Service\Phases\Phases;

class WinConditionProcessor
{
    public function checkWinCondition(State $state): bool
    {
        if ($state->getPhase() === Phases::PHASE_FINISHED) {
            return true;
        }

        foreach ($state->getPlayers() as $player) {
            if (
                $player->getDeck()->getCount() === 0
                || $player->getDamage()->getCount() >= 6
                || (
                    $state->getAbilityStack()->isEmpty()
                    && $player->getField()->getVanguard() === null
                    && $state->getCurrentTurn() > 0
                )
            ) {
                $state->setPhase(Phases::PHASE_FINISHED);
                $state->addChatLog(
                    $state->getOpposingPlayer($player->getHash())->getName()
                    . ' wins'
                );
                $state->addUpdateMessage(new GameUpdate(
                    'game.finished',
                    null,
                    true
                ));

                return true;
            }
        }

        return false;
    }
}
