<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\User;
use App\Repository\UserRepository;

class UserTokenUpdater
{
    private $userRepository;

    public function __construct(
        UserRepository $userRepository
    ) {
        $this->userRepository = $userRepository;
    }

    public function updateToken(
        User $user
    ) {
        $generated = false;

        while (!$generated) {
            $token = $this->generateToken();

            $checkUser = $this->userRepository->findOneByToken($token);
            if ($checkUser === null) {
                $user->setToken($token);
                $generated = true;
            }
        }
    }

    private function generateToken(): string
    {
        $data = random_bytes(16);
        $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10

        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }
}
