<?php

declare(strict_types=1);

namespace App\Service;

use App\Ability\ActionAbilityPartProcessorInterface;
use App\Ability\ConditionAbilityPartProcessorInterface;

class AbilityPartManager
{
    private $actionAbilityParts;
    private $conditionAbilityParts;

    public function __construct()
    {
        $this->actionAbilityParts = [];
        $this->conditionAbilityParts = [];
    }

    public function addActionAbilityPartProcessor(ActionAbilityPartProcessorInterface $abilityPart)
    {
        $this->actionAbilityParts[$abilityPart->getName()] = $abilityPart;
    }

    public function getActionAbilityPartProcessor(string $name): ActionAbilityPartProcessorInterface
    {
        return $this->actionAbilityParts[$name];
    }

    public function addConditionAbilityPartProcessor(ConditionAbilityPartProcessorInterface $abilityPart)
    {
        $this->conditionAbilityParts[$abilityPart->getName()] = $abilityPart;
    }

    public function getConditionAbilityPartProcessor(string $name): ConditionAbilityPartProcessorInterface
    {
        return $this->conditionAbilityParts[$name];
    }
}
