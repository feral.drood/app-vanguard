<?php

declare(strict_types=1);

namespace App\Bot;

class BotManager
{
    public const BOT_HASH = 'BOT';

    private $bots;

    public function __construct()
    {
        $this->bots = [];
    }

    public function addBot(BotInterface $bot)
    {
        $this->bots[$bot->getName()] = $bot;
    }

    public function getBot(string $name): ?BotInterface
    {
        if (array_key_exists($name, $this->bots)) {
            return $this->bots[$name];
        }

        return null;
    }

    /**
     * @return BotInterface[]
     */
    public function getBots(): array
    {
        return $this->bots;
    }

    public function getRandomBot(): ?BotInterface
    {
        if (count($this->bots) > 0) {
            return $this->bots[array_rand($this->bots)];
        }

        return null;
    }
}
