<?php

declare(strict_types=1);

namespace App\Bot;

use Exception;

class BrainException extends Exception
{
}
