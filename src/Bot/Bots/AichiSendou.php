<?php

declare(strict_types=1);

namespace App\Bot\Bots;

use App\Bot\BotInterface;
use App\Entity\Ability\AbilityPartOptions;
use App\Entity\Game\Card;
use App\Entity\Game\Field;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Service\AbilityPartManager;
use App\Service\Phases\BattleAttackHelper;
use App\Service\Phases\Phases;

class AichiSendou implements BotInterface
{
    private $abilityPartManager;
    private $selectedDefender;
    private $battleAttackHelper;

    public function __construct(
        AbilityPartManager $abilityPartManager,
        BattleAttackHelper $battleAttackHelper
    ) {
        $this->abilityPartManager = $abilityPartManager;
        $this->battleAttackHelper = $battleAttackHelper;
    }

    public function getAvatar(): string
    {
        return '/images/bots/aichisendou.png';
    }

    public function getClan(): string
    {
        return 'Royal Paladin';
    }
    public function getCards(): array
    {
        return [
            "V-TD01/001" => 4,
            "V-TD01/002" => 4,
            "V-TD01/003" => 4,
            "V-TD01/004" => 4,
            "V-TD01/006" => 3,
            "V-TD01/007" => 3,
            "V-TD01/008" => 4,
            "V-TD01/009" => 4,
            "V-TD01/010" => 3,
            "V-TD01/011" => 1,
            "V-TD01/012" => 4,
            "V-TD01/013" => 4,
            "V-TD01/014" => 4,
            "V-TD01/015" => 4
        ];
    }

    public function getName(): string
    {
        return 'Aichi Sendou';
    }

    /**
     * @param string $playerHash
     * @param State $state
     * @return PlayerInput|null
     */
    public function progressState(
        string $playerHash,
        State $state
    ): ?PlayerInput {
        $player = $state->getPlayer($playerHash);
        $output = null;


        if (!$state->getAbilityStack()->isEmpty()) {
            $output = $this->getAbilityData($playerHash, $state);
        } else {
            switch ($state->getPhase()) {
                case Phases::PHASE_VANGUARD:
                    if ($player->getField()->getVanguard() === null) {
                        $output = $this->getVanguardPhaseData($playerHash, $state);
                    }
                    break;
                case Phases::PHASE_MULLIGAN1:
                    if ($state->getActivePlayerHash() === $playerHash) {
                        $output = $this->getMulliganPhaseData($playerHash, $state);
                    }
                    break;
                case Phases::PHASE_MULLIGAN2:
                    if ($state->getInactivePlayerHash() === $playerHash) {
                        $output = $this->getMulliganPhaseData($playerHash, $state);
                    }
                    break;
                case Phases::PHASE_ASSIST:
                    if ($state->getActivePlayerHash() === $playerHash) {
                        $output = $this->getAssistPhaseData($playerHash, $state);
                    }
                    break;
                case Phases::PHASE_RIDE:
                    if ($state->getActivePlayerHash() === $playerHash) {
                        $output = $this->getRidePhaseData($playerHash, $state);
                    }
                    break;
                case Phases::PHASE_MAIN:
                    if ($state->getActivePlayerHash() === $playerHash) {
                        $output = $this->getMainPhaseData($playerHash, $state);
                    }
                    break;
                case Phases::PHASE_BATTLE:
                    if ($state->getActivePlayerHash() === $playerHash) {
                        $output = $this->getBattlePhaseData($playerHash, $state);
                    }
                    break;
                case Phases::PHASE_BATTLE_SELECT_DEFENDER:
                    if ($state->getActivePlayerHash() === $playerHash) {
                        $output = $this->getBattleDefenderPhaseData($playerHash, $state);
                    }
                    break;
                case Phases::PHASE_BATTLE_SELECT_GUARDIAN:
                    if ($state->getInactivePlayerHash() === $playerHash) {
                        $output = $this->getBattleGuardianPhaseData($playerHash, $state);
                    }
                    break;
                default:
                    $output = $this->getAbilityData($playerHash, $state);
            }
        }

        if ($output !== null) {
            return (new PlayerInput($playerHash, $output));
        }

        return null;
//
//        const PHASE_BATTLE = 'battle';
//        const PHASE_BATTLE_SELECT_DEFENDER = 'battle_defender';
//        const PHASE_BATTLE_SELECT_GUARDIAN = 'battle_guardian';
    }

    protected function getVanguardPhaseData(string $playerHash, State $state): array
    {
        return ['V-TD01/011#1'];
    }

    protected function getMulliganPhaseData(string $playerHash, State $state): ?array
    {
        return [];
    }

    protected function getAssistPhaseData(string $playerHash, State $state): array
    {
        return ['assist'];
    }

    protected function getRidePhaseData(string $playerHash, State $state): ?array
    {
        $output = [];

        $player = $state->getActivePlayer();
        $vanguardGrade = $player->getField()->getVanguard()->getGrade();
        $desiredGrade = $vanguardGrade + 1;
        $cards = $player->getHand()->getCards();

        foreach ($cards as $card) {
            if ($card->getGrade() === $desiredGrade) {
                $output = [$card->getId()];
                break;
            }
        }

        return $output;
    }

    protected function getMainPhaseData(string $playerHash, State $state): ?array
    {
        $output = [];
        $player = $state->getActivePlayer();
        $vanguardGrade = $player->getField()->getVanguard()->getGrade();
        $cards = $player->getHand()->getCards();
        $output = [];

        $circles = array_values(array_diff(array_keys($player->getField()->getCircles()), [Field::VANGUARD]));
        $handCards = $player->getHand()->getCards();

        $wantedMinGrade = null;
        $wantedMaxGrade = null;
        $frontCircle = false;
        for ($times=0; $times<3; $times++) {
            $chosenCircle = $circles[mt_rand(0, count($circles) - 1)];

            if ($player->getField()->getCircleCard($chosenCircle) !== null) {
                continue;
            }

            if (
                in_array(
                    $chosenCircle,
                    [
                        Field::REARGUARD1,
                        Field::REARGUARD5,
                    ],
                    true
                )
            ) {
                $frontCircle = true;
            }

            $validChoices = [];
            foreach ($handCards as $card) {
                if ($card->getType() === Card::TYPE_BLITZ_ORDER) {
                    continue;
                }
                if (
                    $frontCircle
                    && $card->getGrade() >= 2
                    && $card->getGrade() <= $vanguardGrade
                ) {
                    $validChoices[] = $card->getId();
                } elseif (
                    !$frontCircle
                    && $card->getGrade() < 2
                    && $card->getGrade() >= 0
                    && $card->getGrade() <= $vanguardGrade
                ) {
                    $validChoices[] = $card->getId();
                }
            }

            if (count($validChoices) > 0) {
                $output = [
                    $chosenCircle,
                    $validChoices[mt_rand(0, count($validChoices) - 1)]
                ];

                break;
            }
        }

        return $output;
    }

    protected function getBattlePhaseData(string $playerHash, State $state): ?array
    {
        $output = [];
        $player = $state->getPlayer($playerHash);
        $this->selectedDefender = null;

        $opponent = $state->getOpposingPlayer($playerHash);
        $opponentTargets = [
            $opponent->getField()->getVanguard()
        ];
        if ($opponent->getField()->getCircleCard(Field::REARGUARD1) !== null) {
            $opponentTargets[] = $opponent->getField()->getCircleCard(Field::REARGUARD1);
        }
        if ($opponent->getField()->getCircleCard(Field::REARGUARD5) !== null) {
            $opponentTargets[] = $opponent->getField()->getCircleCard(Field::REARGUARD5);
        }

        $pairs = [
            Field::VANGUARD => Field::REARGUARD3,
            Field::REARGUARD1 => Field::REARGUARD2,
            Field::REARGUARD5 => Field::REARGUARD4,
        ];

        for ($times=0; $times<3; $times++) {
            $selectedTarget = $opponentTargets[mt_rand(0, count($opponentTargets) - 1)];

//            foreach ($pairs as $fieldAttackCircles) {
            foreach ($pairs as $forwardAttacker => $backAttacker) {
                $combinedPower = 0;
                $attackCardIds = [];

                $card = $player->getField()->getCircleCard($forwardAttacker);
                if ($card === null || $card->getState() === Card::STATE_REST) {
                    continue;
                }

                if ($card !== null) {
                    $combinedPower += $card->getPower();
                    $attackCardIds[] = $forwardAttacker;
                }

                $card = $player->getField()->getCircleCard($backAttacker);
                if ($card !== null && $card->getState() !== Card::STATE_REST) {
                    $combinedPower += $card->getPower();
                    $attackCardIds[] = $backAttacker;
                }

                if ($combinedPower > $selectedTarget->getPower()) {
                    $this->selectedDefender = $opponent->getField()->getCircleByCardId($selectedTarget->getId());
                    $output = $attackCardIds;

                    break 2;
                }

            }

        }


        return $output;
    }

    protected function getBattleDefenderPhaseData(string $playerHash, State $state): ?array
    {
        $output = [];

        if ($this->selectedDefender !== null) {
            $output = [$this->selectedDefender];
        }

        return $output;
    }

    protected function getBattleGuardianPhaseData(string $playerHash, State $state): ?array
    {
        $output = [];

        $player = $state->getInactivePlayer();
        $handCards = $player->getHand()->getCards();

        $guardians = [];
        $shouldDefend = false;

        if (
            $player->getDamage()->getCount() > 3
            && $state->getDefender() === Field::VANGUARD
        ) {
            $shouldDefend = true;
        } else {
            $shouldDefend = mt_rand(0, 10) > 5;
        }

        if ($shouldDefend) {
            $validGuardians = [];

            foreach ($handCards as $card) {
                if (
                    $card->getShield() !== null
                    || $card->getType() === Card::TYPE_BLITZ_ORDER
                ) {
                    $validGuardians[] = $card;
                }
            }

            if (count($validGuardians) > 0) {
                $attackerPower = $this->battleAttackHelper->getAttackerPower($state);
                $defenderPower = $this->battleAttackHelper->getDefenderPower($state);

                foreach ($validGuardians as $guardian) {
                    if (
                        (
                            $guardian->getType() === Card::TYPE_BLITZ_ORDER
                            && ($defenderPower + 5) > $attackerPower
                        )
                        || (
                            $defenderPower + $guardian->getShield() > $attackerPower
                        )
                    ) {
                        $output[] = $guardian->getId();
                        break;
                    }
                }

                if (count($output) === 0) {
                    if ($player->getHand()->getCount() > 2) {
                        foreach ($validGuardians as $guardian) {
                            if ($guardian->getShield() === 0) {
                                $output[] = $guardian->getId();
                                break;
                            }
                        }
                    }
                }
            }
        }

        return $output;
    }

    protected function getAbilityData(string $playerHash, State $state): ?array
    {
        $stack = $state->getAbilityStack();
        $output = null;

        if (!$stack->isEmpty()) {
            if (
                $stack->getCostCount() > 0
                || $stack->getEffectCount() > 0
            ) {
                if ($stack->getCostCount() > 0) {
                    $effect = $stack->getCost();
                } else {
                    $effect = $stack->getEffect();
                }
                if ($effect->getPlayerHash() === $playerHash) {
                    $processor = $this->abilityPartManager->getActionAbilityPartProcessor($effect->getType());
                    $validChoices = $processor->getValidChoices($state, $effect);

                    $count = $effect->getOption(AbilityPartOptions::COUNT);
                    $minCount = $effect->getOption(AbilityPartOptions::MIN_COUNT);
                    $maxCount = $effect->getOption(AbilityPartOptions::MAX_COUNT);

                    shuffle($validChoices);
                    if ($count !== null) {
                        $output = array_slice($validChoices, 0, $count);
                    } elseif ($minCount !== null) {
                        $output = array_slice($validChoices, 0, $minCount);
                    } elseif ($maxCount !== null) {
                        $output = array_slice($validChoices, 0, $maxCount);
                    } else {
                        $choiceCount = count($validChoices);
                        $output = array_slice($validChoices, 0, $choiceCount - 1);
                    }
                }
            } else {
                $ability = $stack->getAbility(0);
                if ($ability->getPlayerHash() === $playerHash) {
                    $output = [$ability->getId()];
                }
            }
        }

        return $output;
    }
}
