<?php

declare(strict_types=1);

namespace App\Bot;

use App\Entity\Game\State;
use App\Entity\PlayerInput;

interface BotInterface
{
    public function getName(): string;

    public function getAvatar(): string;

    public function getClan(): string;

    public function getCards(): array;

    public function progressState(
        string $playerHash,
        State $state
    ): ?PlayerInput;

}
