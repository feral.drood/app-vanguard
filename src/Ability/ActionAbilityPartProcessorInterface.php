<?php

declare(strict_types=1);

namespace App\Ability;

use App\Entity\Ability\AbilityPart;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Entity\Update\UpdateDataInterface;

interface ActionAbilityPartProcessorInterface extends AbilityPartProcessorInterface
{
    /**
     * Process user supplied data and return if the data was valid and this cost/effect should be removed from the stack
     *
     * @param State $state
     * @param AbilityPart $abilityPart
     * @param PlayerInput $input
     *
     * @return bool
     */
    public function processPlayerInput(
        State $state,
        AbilityPart $abilityPart,
        PlayerInput $input
    ): bool;

    /**
     * Return DialogUpdate with all texts, action and options for frontend to display
     *
     * @param State $state
     * @param AbilityPart $abilityPart
     * @return UpdateDataInterface|null
     */
    public function getPublicUpdateData(
        State $state,
        AbilityPart $abilityPart
    ): ?UpdateDataInterface;

    /**
     * Return DialogUpdate with all cards and other sensitive info, that should be returned only to
     *
     * @param State $state
     * @param AbilityPart $abilityPart
     * @return UpdateDataInterface|null
     */
    public function getPrivateUpdateData(
        State $state,
        AbilityPart $abilityPart
    ): ?UpdateDataInterface;

    /**
     * Return array of valid card ids or valid field circles to select
     * If there is less or equal number of options for player to choose from, there should be no dialog
     *
     * @param State $state
     * @param AbilityPart $abilityPart
     * @return array
     */
    public function getValidChoices(
        State $state,
        AbilityPart $abilityPart
    ): array;

    /**
     * Check how many valid options there is and progress is there are no choices to be made
     * Return if ability has progressed
     *
     * @param State $state
     * @param AbilityPart $abilityPart
     * @return bool
     */
    public function progressState(
        State $state,
        AbilityPart $abilityPart
    ): bool;
}
