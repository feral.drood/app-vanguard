<?php

declare(strict_types=1);

namespace App\Ability;

use App\Entity\Ability\AbilityPart;
use App\Entity\Ability\AbilityPartOptions;
use App\Entity\Game\Field;
use App\Entity\Game\State;
use App\Entity\Game\Card;
use App\Exception\MisconfiguredAbilityException;

class FilterHelper
{
    public function buildFilterData(State $state, AbilityPart $abilityPartData): FilterData
    {
        $playerOption = $abilityPartData->getOption(AbilityPartOptions::PLAYER);
        $selfPlayer = $state->getPlayer($abilityPartData->getPlayerHash());
        if ($playerOption === AbilityPartOptions::PLAYER_OPPONENT) {
            $player = $state->getOpposingPlayer($abilityPartData->getPlayerHash());
        } else {
            $player = $selfPlayer;
        }

        $filterData = (new FilterData())
            ->setRace($abilityPartData->getOption(AbilityPartOptions::RACE))
            ->setMinGrade($abilityPartData->getOption(AbilityPartOptions::MIN_GRADE))
            ->setMaxGrade($abilityPartData->getOption(AbilityPartOptions::MAX_GRADE))
            ->setPartialName($abilityPartData->getOption(AbilityPartOptions::PARTIAL_NAME))
            ->setIntent($abilityPartData->getOption(AbilityPartOptions::INTENT))
            ->setState($abilityPartData->getOption(AbilityPartOptions::STATE))
            ->setFace($abilityPartData->getOption(AbilityPartOptions::FACE))
            ->setDeleted($abilityPartData->getOption(AbilityPartOptions::DELETED))
        ;

        $name = $abilityPartData->getOption(AbilityPartOptions::NAME);
        if ($name !== null) {
            if ($name === AbilityPartOptions::STORED_VALUE) {
                $filterData->setName($selfPlayer->getBuffer()->getValue($abilityPartData->getSelfCardId()));
            } else {
                $filterData->setName($name);
            }
        }

        $validLocations = $abilityPartData->getOption(AbilityPartOptions::LOCATION_VALID_LOCATIONS);
        if ($validLocations !== null) {
            if ($validLocations === AbilityPartOptions::STORED_VALUE) {
                $validLocations = $selfPlayer->getBuffer()->getValue($abilityPartData->getSelfCardId());
            }
            if (!is_array($validLocations)) {
                $validLocations = [$validLocations];
            }
            $filterData->setValidLocations($validLocations);
        }

        $invalidLocations = $abilityPartData->getOption(AbilityPartOptions::LOCATION_INVALID_LOCATIONS);
        if ($invalidLocations !== null) {
            if ($invalidLocations === AbilityPartOptions::STORED_VALUE) {
                $invalidLocations = $selfPlayer->getBuffer()->getValue($abilityPartData->getSelfCardId());
            }
            if (!is_array($invalidLocations)) {
                $invalidLocations = [$invalidLocations];
            }
            $filterData->setInvalidLocations($invalidLocations);
        }

        $cardGrade = $abilityPartData->getOption(AbilityPartOptions::GRADE);
        if ($cardGrade === AbilityPartOptions::GRADE_CALL) {
            $filterData->setMaxGrade($player->getField()->getVanguard()->getGrade());
        } elseif ($cardGrade !== null) {
            $filterData
                ->setMinGrade($cardGrade)
                ->setMaxGrade($cardGrade)
            ;
        }

        $location = $abilityPartData->getOption(AbilityPartOptions::LOCATION);
        if ($location !== null) {
            $playerCircles = array_keys($player->getField()->getCircles());
            if ($location === AbilityPartOptions::LOCATION_FRONT_REARGUARD) {
                $filterData->setValidLocations(
                    array_diff(
                        $playerCircles,
                        [
                            Field::REARGUARD2,
                            Field::REARGUARD3,
                            Field::REARGUARD4,
                            Field::VANGUARD,
                        ]
                    )
                );

            } elseif ($location === AbilityPartOptions::LOCATION_BOOSTER) {
                if ($state->getBooster() !== null) {
                    $filterData->setValidLocations([$state->getBooster()]);
                } else {
                    $filterData->setValidLocations([]);
                }
            } elseif ($location === AbilityPartOptions::LOCATION_BACK_REARGUARD) {
                $filterData->setValidLocations([
                    Field::REARGUARD2,
                    Field::REARGUARD3,
                    Field::REARGUARD4,
                ]);
            } elseif ($location === AbilityPartOptions::LOCATION_FIELD) {
                $filterData->setValidLocations($playerCircles);
            } elseif ($location === AbilityPartOptions::LOCATION_FRONT_UNIT) {
                $filterData->setValidLocations(
                    array_diff(
                        $playerCircles,
                        [
                            Field::REARGUARD2,
                            Field::REARGUARD3,
                            Field::REARGUARD4,
                        ]
                    )
                );
            } elseif ($location === AbilityPartOptions::LOCATION_BACK) {
                $filterData->setValidLocations([
                    Field::REARGUARD2,
                    Field::REARGUARD3,
                    Field::REARGUARD4,
                ]);
            } elseif ($location === AbilityPartOptions::LOCATION_REARGUARD) {
                $filterData->setValidLocations(
                    array_diff(
                        $playerCircles,
                        [
                            Field::VANGUARD,
                        ]
                    )
                );
            } elseif ($location === AbilityPartOptions::LOCATION_VANGUARD) {
                $filterData->setValidLocations([
                    Field::VANGUARD,
                ]);
            } elseif (
                $location === AbilityPartOptions::LOCATION_COLUMN
                && $playerOption !== AbilityPartOptions::PLAYER_OPPONENT
            ) {
                $currentLocation = $player->getField()->getCircleByCardId($abilityPartData->getSelfCardId());
                $pairs = [
                    Field::REARGUARD1 => [Field::REARGUARD2],
                    Field::REARGUARD2 => [Field::REARGUARD1],
                    Field::VANGUARD => [Field::REARGUARD3],
                    Field::REARGUARD3 => [Field::VANGUARD],
                    Field::REARGUARD4 => [Field::REARGUARD5],
                    Field::REARGUARD5 => [Field::REARGUARD4],
                ];
                if (array_key_exists($currentLocation, $pairs)) {
                    $filterData->setValidLocations($pairs[$currentLocation]);
                } else {
                    $filterData->setValidLocations([]);
                }
            } elseif (
                $location === AbilityPartOptions::LOCATION_COLUMN
                && $playerOption === AbilityPartOptions::PLAYER_OPPONENT
            ) {
                $currentLocation = $state
                    ->getPlayer($abilityPartData->getPlayerHash())
                    ->getField()->getCircleByCardId($abilityPartData->getSelfCardId())
                ;
                $pairs = [
                    Field::REARGUARD1 => [Field::REARGUARD4, Field::REARGUARD5],
                    Field::REARGUARD2 => [Field::REARGUARD4, Field::REARGUARD5],
                    Field::VANGUARD => [Field::REARGUARD3, Field::VANGUARD],
                    Field::REARGUARD3 => [Field::REARGUARD3, Field::VANGUARD],
                    Field::REARGUARD4 => [Field::REARGUARD1, Field::REARGUARD2],
                    Field::REARGUARD5 => [Field::REARGUARD1, Field::REARGUARD2],
                ];

                if (array_key_exists($currentLocation, $pairs)) {
                    $filterData->setValidLocations($pairs[$currentLocation]);
                } elseif ($currentLocation !== null) {
                    $circleId = (int)substr($currentLocation, strlen(Field::REARGUARD));
                    if ($circleId % 2 === 0) {
                        $filterData->setValidLocations([Field::REARGUARD . (++$circleId)]);
                    } else {
                        $filterData->setValidLocations([Field::REARGUARD . (--$circleId)]);
                    }
                } else {
                    $filterData->setValidLocations([]);
                }
            } else {
                throw new MisconfiguredAbilityException(
                    'Invalid location option: ' . $location
                );
            }
        }

        return $filterData;
    }

    public function isCommonFilterPassed(FilterData $filterData, ?Card $card): bool
    {
        if (
            $card === null
            || (
                $filterData->getName() !== null
                && $filterData->getName() !== $card->getName()
            )
            || (
                $filterData->getPartialName() !== null
                && stripos($card->getName(), $filterData->getPartialName()) === false
            )
            || (
                $filterData->getMinGrade() !== null
                && $card->getGrade() < $filterData->getMinGrade()
            )
            || (
                $filterData->getMaxGrade() !== null
                && $card->getGrade() > $filterData->getMaxGrade()
            )
            || (
                $filterData->getRace() !== null
                && $card->getRace() !== null
                && $card->getRace() !== $filterData->getRace()
            )
            || (
                $filterData->getRace() !== null
                && $card->getRace() === null
                && !in_array($filterData->getRace(), $card->getRaces(), true)
            )
            || (
                $filterData->getState() !== null
                && $card->getState() !== $filterData->getState()
            )
            || (
                $filterData->getDeleted() !== null
                && $card->isDeleted() !== $filterData->getDeleted()
            )
            || (
                $filterData->getFace() === AbilityPartOptions::FACE_DOWN
                && !$card->isFaceDown()
            )
            || (
                $filterData->getFace() === AbilityPartOptions::FACE_UP
                && $card->isFaceDown()
            )
        ) {
            return false;
        }

        return true;
    }

    public function isLocationFilterPassed(FilterData $filterData, ?string $location): bool
    {
        return !(
            (
                $filterData->getValidLocations() !== null
                && !in_array($location, $filterData->getValidLocations(), true)
            )
            || (
                $filterData->getInvalidLocations() !== null
                && in_array($location, $filterData->getInvalidLocations(), true)
            )
        );
    }
}
