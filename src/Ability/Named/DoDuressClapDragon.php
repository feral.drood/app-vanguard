<?php

declare(strict_types=1);

namespace App\Ability\Named;

use App\Ability\ActionAbilityPartProcessorInterface;
use App\Entity\Ability\AbilityPart;
use App\Entity\Game\Field;
use App\Entity\Game\PlayerError;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Entity\Update\DialogUpdate;
use App\Entity\Update\UpdateDataInterface;
use App\Service\ChatHelper;

class DoDuressClapDragon implements ActionAbilityPartProcessorInterface
{
    private const PAIRS = [
        Field::REARGUARD1 => Field::REARGUARD2,
        Field::REARGUARD2 => Field::REARGUARD1,
        Field::REARGUARD4 => Field::REARGUARD5,
        Field::REARGUARD5 => Field::REARGUARD4,
    ];

    private $chatHelper;

    public function __construct(
        ChatHelper $chatHelper
    ) {
        $this->chatHelper = $chatHelper;
    }

    public function getName(): string
    {
        return 'do_duress_clap_dragon';
    }

    public function processPlayerInput(State $state, AbilityPart $abilityPart, PlayerInput $input): bool
    {
        $playerChoice = $input->getValue();
        $validChoices = $this->getValidChoices($state, $abilityPart);

        if (
            $playerChoice !== null
            && !in_array($playerChoice, $validChoices, true)
        ) {
            $state->setPlayerError(
                new PlayerError(
                    $abilityPart->getPlayerHash(),
                    'Invalid selection'
                )
            );

            return false;
        }

        if ($playerChoice !== null) {
            $opponent = $state->getOpposingPlayer($abilityPart->getPlayerHash());
            $unit = $opponent->getField()->getCircleCard($playerChoice);
            $opponent->getField()->setCircleCard($playerChoice, null);
            $opponent->getField()->setCircleCard(self::PAIRS[$playerChoice], $unit);
            $state->addChatLog(
                $state->getPlayer($abilityPart->getPlayerHash())->getName()
                . ' repositions opponent\'s '
                . $this->chatHelper->generateCardLink($unit)
                . ' within the column'
            );
        }

        return true;
    }

    public function getPrivateUpdateData(State $state, AbilityPart $abilityPart): ?UpdateDataInterface
    {
        return null;
    }

    public function getPublicUpdateData(State $state, AbilityPart $abilityPart): ?UpdateDataInterface
    {
        return (new DialogUpdate($abilityPart->getPlayerHash()))
            ->setTitle('Select up to one opponent\'s rearguard')
            ->addOption(DialogUpdate::OPTION_MAX_COUNT, 1)
        ;
    }

    public function getValidChoices(State $state, AbilityPart $abilityPart): array
    {
        $opponent = $state->getOpposingPlayer($abilityPart->getPlayerHash());
        $field = $opponent->getField();

        $validChoices = [];
        foreach ($field->getCircles() as $location => $circleUnit) {
            if (
                array_key_exists($location, self::PAIRS)
                && $circleUnit !== null
                && $field->getCircleCard(self::PAIRS[$location]) === null
            ) {
                $validChoices[] = $location;
            }
        }

        return $validChoices;
    }

    public function progressState(State $state, AbilityPart $abilityPart): bool
    {
        $validChoices = $this->getValidChoices($state, $abilityPart);

        return count($validChoices) === 0;
    }
}
