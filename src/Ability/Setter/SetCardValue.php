<?php

declare(strict_types=1);

namespace App\Ability\Setter;

use App\Ability\ActionAbilityPartProcessorInterface;
use App\Ability\ConditionAbilityPartProcessorInterface;
use App\Entity\Ability\AbilityPart;
use App\Entity\Ability\AbilityPartOptions;
use App\Entity\Game\Card;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Entity\Update\UpdateDataInterface;

class SetCardValue implements ConditionAbilityPartProcessorInterface, ActionAbilityPartProcessorInterface
{
    public function getName(): string
    {
        return 'set_card_value';
    }

    public function processPlayerInput(State $state, AbilityPart $abilityPart, PlayerInput $input): bool
    {
        return true;
    }

    public function getPrivateUpdateData(State $state, AbilityPart $abilityPart): ?UpdateDataInterface
    {
        return null;
    }

    public function getPublicUpdateData(State $state, AbilityPart $abilityPart): ?UpdateDataInterface
    {
        return null;
    }

    public function getValidChoices(State $state, AbilityPart $abilityPart): array
    {
        return [];
    }

    private function setValue(State $state, AbilityPart $abilityPart): void
    {
        $playerOption = $abilityPart->getOption(AbilityPartOptions::PLAYER);
        if ($playerOption === AbilityPartOptions::PLAYER_OPPONENT) {
            $targetPlayer = $state->getOpposingPlayer($abilityPart->getPlayerHash());
            $player = $state->getPlayer($abilityPart->getPlayerHash());
        } else {
            $targetPlayer = $state->getPlayer($abilityPart->getPlayerHash());
            $player = $targetPlayer;
        }

        $card = null;
        switch ($abilityPart->getOption(AbilityPartOptions::CARD)) {
            case AbilityPartOptions::LOCATION_ATTACKER:
                if ($state->getAttacker() !== null) {
                    $card = $targetPlayer->getField()->getCircleCard($state->getAttacker());
                }
                break;
            case AbilityPartOptions::LOCATION_DEFENDER:
                if ($state->getDefender() !== null) {
                    $card = $state->getInactivePlayer()->getField()->getCircleCard($state->getDefender());
                }
                break;
            case AbilityPartOptions::LOCATION_TRIGGER:
                $card = $targetPlayer->getTrigger();
                break;
            case AbilityPartOptions::LOCATION_BOOSTER:
                if ($state->getBooster() !== null) {
                    $card = $targetPlayer->getField()->getCircleCard($state->getBooster());
                }
                break;
            case AbilityPartOptions::LOCATION_VANGUARD:
                $card = $targetPlayer->getField()->getVanguard();
                break;
            case AbilityPartOptions::LOCATION_BUFFER:
                $cards = $player->getBuffer()->getCards($abilityPart->getSelfCardId());
                $cardIds = array_keys($cards);
                if (count($cardIds) > 0) {
                    $card = $cards[$cardIds[0]];
                }
                break;
            default:
                $card = $targetPlayer->getCardById($abilityPart->getSelfCardId());
        }

        if ($card === null) {
            return;
        }

        $value = null;
        $option = $abilityPart->getOption('value');
        switch ($option) {
            case 'grade':
                $value = $card->getGrade();
                break;
            case 'name':
                $value = $card->getName();
                break;
            case 'location':
                $value = $targetPlayer->getZoneByCardId($card->getId());
                if ($value === Card::ZONE_FIELD) {
                    $value = $targetPlayer->getField()->getCircleByCardId($card->getId());
                }
                break;
        }

        $player->getBuffer()->setValue($abilityPart->getSelfCardId(), $value);
    }

    public function isPassed(State $state, AbilityPart $abilityPartData): bool
    {
        $this->setValue($state, $abilityPartData);

        return true;
    }

    public function progressState(State $state, AbilityPart $abilityPart): bool
    {
        $this->setValue($state, $abilityPart);

        return true;
    }
}
