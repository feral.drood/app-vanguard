<?php

declare(strict_types=1);

namespace App\Ability\Setter;

use App\Ability\ActionAbilityPartProcessorInterface;
use App\Ability\ConditionAbilityPartProcessorInterface;
use App\Ability\FilterHelper;
use App\Entity\Ability\AbilityPart;
use App\Entity\Ability\AbilityPartOptions;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Entity\Update\UpdateDataInterface;
use App\Exception\MisconfiguredAbilityException;

class SetEmptyFieldLocationCountValue implements ActionAbilityPartProcessorInterface, ConditionAbilityPartProcessorInterface
{
    private $filterHelper;

    public function __construct(
        FilterHelper $filterHelper
    ) {
        $this->filterHelper = $filterHelper;
    }

    public function getName(): string
    {
        return 'set_empty_field_location_count_value';
    }

    public function processPlayerInput(State $state, AbilityPart $abilityPartData, PlayerInput $input): bool
    {
        return true;
    }

    public function getPrivateUpdateData(State $state, AbilityPart $abilityPartData): ?UpdateDataInterface
    {
        return null;
    }

    public function getPublicUpdateData(State $state, AbilityPart $abilityPartData): ?UpdateDataInterface
    {
        return null;
    }

    public function getValidChoices(State $state, AbilityPart $abilityPartData): array
    {
        return [];
    }

    private function calculateValue(State $state, AbilityPart $abilityPartData): void
    {
        $filterData = $this->filterHelper->buildFilterData($state, $abilityPartData);
        $playerOption = $abilityPartData->getOption(AbilityPartOptions::PLAYER);
        if ($playerOption === AbilityPartOptions::PLAYER_OPPONENT) {
            $player = $state->getOpposingPlayer($abilityPartData->getPlayerHash());
        } else {
            $player = $state->getPlayer($abilityPartData->getPlayerHash());
        }

        $circles = $player->getField()->getCircles();
        $count = 0;
        foreach ($circles as $circleId => $circleUnit) {
            if (
                $circleUnit === null
                && $this->filterHelper->isLocationFilterPassed($filterData, $circleId)
            ) {
                $count++;
            }
        }

        $selfPlayer = $state->getPlayer($abilityPartData->getPlayerHash());
        $selfPlayer->getBuffer()->setValue(
            $abilityPartData->getSelfCardId(),
            $count
        );
    }

    public function progressState(State $state, AbilityPart $abilityPartData): bool
    {
        $this->calculateValue($state, $abilityPartData);

        return true;
    }

    public function isPassed(State $state, AbilityPart $abilityPart): bool
    {
        $this->calculateValue($state, $abilityPart);

        return true;
    }
}
