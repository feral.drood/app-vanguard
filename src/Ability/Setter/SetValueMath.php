<?php

declare(strict_types=1);

namespace App\Ability\Setter;

use App\Ability\ActionAbilityPartProcessorInterface;
use App\Ability\ConditionAbilityPartProcessorInterface;
use App\Entity\Ability\AbilityPart;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Entity\Update\UpdateDataInterface;

class SetValueMath implements ActionAbilityPartProcessorInterface, ConditionAbilityPartProcessorInterface
{
    public function getName(): string
    {
        return 'set_value_math';
    }

    public function processPlayerInput(State $state, AbilityPart $abilityPart, PlayerInput $input): bool
    {
        return true;
    }

    public function getPrivateUpdateData(State $state, AbilityPart $abilityPart): ?UpdateDataInterface
    {
        return null;
    }

    public function getPublicUpdateData(State $state, AbilityPart $abilityPart): ?UpdateDataInterface
    {
        return null;
    }

    public function getValidChoices(State $state, AbilityPart $abilityPart): array
    {
        return [];
    }

    private function calcValue(State $state, AbilityPart $abilityPart): void
    {
        $player = $state->getPlayer($abilityPart->getPlayerHash());
        $operation = $abilityPart->getOption('operation');
        $value = $player->getBuffer()->getValue($abilityPart->getSelfCardId()) ?? 0;
        $optionValue = $abilityPart->getOption('value') ?? 0;

        switch ($operation) {
            case 'mul':
                $value = $value * $optionValue;
                break;
            case 'div':
                if ($optionValue > 0) {
                    $value = floor($value / $optionValue);
                }
                break;
            case 'mod':
                if ($optionValue > 0) {
                    $value = $value % $optionValue;
                }
                break;
            case 'add':
                $value = $value + $optionValue;
                break;
            case 'sub':
                $value = $value - $optionValue;
                break;
        }

        $player->getBuffer()->setValue($abilityPart->getSelfCardId(), $value);
    }

    public function progressState(State $state, AbilityPart $abilityPart): bool
    {
        $this->calcValue($state, $abilityPart);

        return true;
    }


    public function isPassed(State $state, AbilityPart $abilityPart): bool
    {
        $this->calcValue($state, $abilityPart);

        return true;
    }
}
