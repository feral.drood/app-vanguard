<?php

declare(strict_types=1);

namespace App\Ability\Setter;

use App\Ability\ActionAbilityPartProcessorInterface;
use App\Ability\ConditionAbilityPartProcessorInterface;
use App\Ability\FilterHelper;
use App\Entity\Ability\AbilityPart;
use App\Entity\Ability\AbilityPartOptions;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Entity\Update\UpdateDataInterface;
use App\Exception\MisconfiguredAbilityException;

class SetListCountValue implements ActionAbilityPartProcessorInterface, ConditionAbilityPartProcessorInterface
{
    private $filterHelper;

    public function __construct(
        FilterHelper $filterHelper
    ) {
        $this->filterHelper = $filterHelper;
    }

    public function getName(): string
    {
        return 'set_list_count_value';
    }

    public function processPlayerInput(State $state, AbilityPart $abilityPartData, PlayerInput $input): bool
    {
        return true;
    }

    public function getPrivateUpdateData(State $state, AbilityPart $abilityPartData): ?UpdateDataInterface
    {
        return null;
    }

    public function getPublicUpdateData(State $state, AbilityPart $abilityPartData): ?UpdateDataInterface
    {
        return null;
    }

    public function getValidChoices(State $state, AbilityPart $abilityPartData): array
    {
        return [];
    }

    private function calculateValue(State $state, AbilityPart $abilityPartData): void
    {
        $filterData = $this->filterHelper->buildFilterData($state, $abilityPartData);
        $playerOption = $abilityPartData->getOption(AbilityPartOptions::PLAYER);
        if ($playerOption === AbilityPartOptions::PLAYER_OPPONENT) {
            $player = $state->getOpposingPlayer($abilityPartData->getPlayerHash());
        } else {
            $player = $state->getPlayer($abilityPartData->getPlayerHash());
        }


        switch ($abilityPartData->getOption(AbilityPartOptions::ZONE)) {
            case AbilityPartOptions::LOCATION_HAND:
                $cards = $player->getHand()->getCards();
                break;
            case AbilityPartOptions::LOCATION_SOUL:
                $cards = $player->getSoul()->getCards();
                break;
            case AbilityPartOptions::LOCATION_DROP:
                $cards = $player->getDrop()->getCards();
                break;
            case AbilityPartOptions::LOCATION_DECK:
                $viewCount = $abilityPartData->getOption('view_count');
                $cards = $player->getDeck()->viewCards($viewCount);
                break;
            case AbilityPartOptions::LOCATION_DAMAGE:
                $cards = $player->getDamage()->getCards();
                break;
            case AbilityPartOptions::LOCATION_FIELD:
                $cards = array_filter($player->getField()->getCircles());
                break;
            case AbilityPartOptions::LOCATION_BUFFER:
                $cards = $player->getBuffer()->getCards($abilityPartData->getSelfCardId());
                break;
            case AbilityPartOptions::LOCATION_GUARDIAN:
                $cards = $player->getGuardians()->getCards();
                break;
            default:
                throw new MisconfiguredAbilityException('Invalid is_list_check location');
        }

        $validCardIds = [];
        foreach ($cards as $id => $card) {
            if ($abilityPartData->getOption(AbilityPartOptions::ZONE) === AbilityPartOptions::LOCATION_FIELD) {
                $cardLocation = $id;
                $cardSelection = $id;
            } else {
                $cardLocation = $player->getZoneByCardId($card->getId());
                $cardSelection = $card->getId();
            }
            if (
                $this->filterHelper->isCommonFilterPassed($filterData, $card)
                && $this->filterHelper->isLocationFilterPassed($filterData, $cardLocation)
            ) {
                $validCardIds[] = $cardSelection;
            }
        }

        $selfPlayer = $state->getPlayer($abilityPartData->getPlayerHash());
        $selfPlayer->getBuffer()->setValue(
            $abilityPartData->getSelfCardId(),
            count($validCardIds)
        );
    }

    public function progressState(State $state, AbilityPart $abilityPartData): bool
    {
        $this->calculateValue($state, $abilityPartData);

        return true;
    }

    public function isPassed(State $state, AbilityPart $abilityPart): bool
    {
        $this->calculateValue($state, $abilityPart);

        return true;
    }
}
