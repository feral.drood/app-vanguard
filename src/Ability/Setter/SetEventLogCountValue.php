<?php

declare(strict_types=1);

namespace App\Ability\Setter;

use App\Ability\ActionAbilityPartProcessorInterface;
use App\Ability\ConditionAbilityPartProcessorInterface;
use App\Entity\Ability\AbilityPart;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Entity\Update\UpdateDataInterface;

class SetEventLogCountValue implements ConditionAbilityPartProcessorInterface, ActionAbilityPartProcessorInterface
{
    public function getName(): string
    {
        return 'set_event_log_count_value';
    }

    private function setValue(State $state, AbilityPart $abilityPart): void
    {
        $events = $abilityPart->getOption('events') ?? [];

        $totalCount = 0;
        $eventLog = $state->getCardEventLog();
        foreach ($events as $event) {
            $totalCount += $eventLog->getCount($event) ?? 0;
        }

        $state->getPlayer($abilityPart->getPlayerHash())->getBuffer()->setValue(
            $abilityPart->getSelfCardId(),
            $totalCount
        );
    }

    public function isPassed(State $state, AbilityPart $abilityPart): bool
    {
        $this->setValue($state, $abilityPart);

        return true;
    }

    public function progressState(State $state, AbilityPart $abilityPart): bool
    {
        $this->setValue($state, $abilityPart);

        return true;
    }

    public function getValidChoices(State $state, AbilityPart $abilityPart): array
    {
        return [];
    }

    public function getPublicUpdateData(State $state, AbilityPart $abilityPart): ?UpdateDataInterface
    {
        return null;
    }

    public function getPrivateUpdateData(State $state, AbilityPart $abilityPart): ?UpdateDataInterface
    {
        return null;
    }

    public function processPlayerInput(State $state, AbilityPart $abilityPart, PlayerInput $input): bool
    {
        return true;
    }
}
