<?php

declare(strict_types=1);

namespace App\Ability;

use App\Entity\Game\State;
use App\Entity\Ability\AbilityPart;
use App\Exception\MisconfiguredAbilityException;

interface ConditionAbilityPartProcessorInterface extends AbilityPartProcessorInterface
{
    /**
     * Return boolean if the condition is met and we should proceed with this ability
     *
     * @param State $state
     * @param AbilityPart $abilityPart
     * @return bool
     */
    public function isPassed(
        State $state,
        AbilityPart $abilityPart
    ): bool;
}
