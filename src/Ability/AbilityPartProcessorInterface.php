<?php
declare(strict_types=1);

namespace App\Ability;

interface AbilityPartProcessorInterface
{
    public function getName(): string;
}
