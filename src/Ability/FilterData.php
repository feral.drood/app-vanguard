<?php

declare(strict_types=1);

namespace App\Ability;

class FilterData
{
    private const RACE = 'race';
    private const MIN_GRADE = 'min_grade';
    private const MAX_GRADE = 'max_grade';
    private const NAME = 'name';
    private const PARTIAL_NAME = 'partial_name';
    private const FACE = 'face';
    private const DELETED = 'deleted';
    private const STATE = 'state';
    private const INTENT = 'intent';
    private const VALID_LOCATIONS = 'valid_locations';
    private const INVALID_LOCATION = 'invalid_locations';

    private $data;

    public function __construct()
    {
        $this->data = [];
    }

    public function setRace(?string $race): self
    {
        $this->data[self::RACE] = $race;

        return $this;
    }

    public function getRace(): ?string
    {
        return $this->data[self::RACE] ?? null;
    }

    public function setMinGrade(?int $grade): self
    {
        $this->data[self::MIN_GRADE] = $grade;

        return $this;
    }

    public function getMinGrade(): ?int
    {
        return $this->data[self::MIN_GRADE] ?? null;
    }

    public function setMaxGrade(?int $grade): self
    {
        $this->data[self::MAX_GRADE] = $grade;

        return $this;
    }

    public function getMaxGrade(): ?int
    {
        return $this->data[self::MAX_GRADE] ?? null;
    }

    public function setName(?string $name): self
    {
        $this->data[self::NAME] = $name;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->data[self::NAME] ?? null;
    }

    public function setPartialName(?string $name): self
    {
        $this->data[self::PARTIAL_NAME] = $name;

        return $this;
    }

    public function getPartialName(): ?string
    {
        return $this->data[self::PARTIAL_NAME];
    }

    public function setIntent(?string $intent): self
    {
        $this->data[self::INTENT] = $intent;

        return $this;
    }

    public function getIntent(): ?string
    {
        return $this->data[self::INTENT] ?? null;
    }

    public function setState(?string $state): self
    {
        $this->data[self::STATE] = $state;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->data[self::STATE] ?? null;
    }

    public function setFace(?string $face): self
    {
        $this->data[self::FACE] = $face;

        return $this;
    }

    public function getFace(): ?string
    {
        return $this->data[self::FACE] ?? null;
    }

    public function setDeleted(?bool $deleted): self
    {
        $this->data[self::DELETED] = $deleted;

        return $this;
    }

    public function getDeleted(): ?bool
    {
        return $this->data[self::DELETED] ?? null;
    }

    public function setValidLocations(?array $validLocations): self
    {
        $this->data[self::VALID_LOCATIONS] = $validLocations;

        return $this;
    }

    public function getValidLocations(): ?array
    {
        return $this->data[self::VALID_LOCATIONS] ?? null;
    }

    public function setInvalidLocations(?array $invalidLocations): self
    {
        $this->data[self::INVALID_LOCATION] = $invalidLocations;

        return $this;
    }

    public function getInvalidLocations(): ?array
    {
        return $this->data[self::INVALID_LOCATION] ?? null;
    }
}
