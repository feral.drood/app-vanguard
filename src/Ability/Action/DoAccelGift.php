<?php

declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\ActionAbilityPartProcessorInterface;
use App\Entity\Ability\AbilityPart;
use App\Entity\Ability\AbilityPartOptions;
use App\Entity\Game\PlayerError;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Entity\Update\DialogUpdate;
use App\Entity\Update\UpdateDataInterface;

/*
Accel I: You gain a permanent rear-guard circle on your front row (represented by the portrait Accel gift marker). The unit on that circle gets [Power]+10000 during your turn.
Accel II: You gain a permanent rear-guard circle on your front row (represented by the landscape Accel gift marker), and draw a card. The unit on that circle gets [Power]+5000 during your turn.
*/

class DoAccelGift implements ActionAbilityPartProcessorInterface
{
    private const ACCEL = 'accel';
    private const ACCEL2 = 'accel2';
    private const ACTIONS = [
        'accel1' => 'Accel I',
        'accel2' => 'Accel II',
    ];

    public function getName(): string
    {
        return 'do_accel_gift';
    }

    public function processPlayerInput(State $state, AbilityPart $abilityPartData, PlayerInput $input): bool
    {
        $player = $state->getPlayer($abilityPartData->getPlayerHash());
        $validChoices = $this->getValidChoices($state, $abilityPartData);
        $choice = $input->getValue();

        if (count($validChoices) > 0) {
            if (!in_array($choice, $validChoices, true)) {
                $state->setPlayerError(
                    new PlayerError(
                        $input->getPlayerHash(),
                        'Invalid choice'
                    )
                );

                return false;
            }

            $player->setGiftType(self::ACCEL, $choice);
            $state->addChatLog($player->getName() . ' chooses ' . self::ACTIONS[$choice]);
        }

        $player->addGiftCircle(self::ACCEL, $player->getField()->addCircle());
        if ($player->getGiftType(self::ACCEL) === self::ACCEL2) {
            $state->getAbilityStack()
                ->addEffect(
                    (new AbilityPart())
                    ->setType('do_draw')
                    ->addOption(AbilityPartOptions::COUNT, 1)
                )
            ;
        }

        return true;
    }

    public function getPrivateUpdateData(State $state, AbilityPart $abilityPartData): ?UpdateDataInterface
    {
        return null;
    }

    public function getPublicUpdateData(State $state, AbilityPart $abilityPartData): ?UpdateDataInterface
    {
        $player = $state->getPlayer($abilityPartData->getPlayerHash());

        if ($player->getGiftType(self::ACCEL) === null) {
            return
                (new DialogUpdate($abilityPartData->getPlayerHash()))
                ->setTitle('Choose gift type')
                ->setActions(self::ACTIONS)
            ;
        }

        return null;
    }

    public function getValidChoices(State $state, AbilityPart $abilityPartData): array
    {
        $player = $state->getPlayer($abilityPartData->getPlayerHash());

        if ($player->getGiftType(self::ACCEL) === null) {
            return array_keys(self::ACTIONS);
        }

        return [];
    }

    public function progressState(State $state, AbilityPart $abilityPartData): bool
    {
        if (count($this->getValidChoices($state, $abilityPartData)) > 0) {
            return false;
        }

        $this->processPlayerInput($state, $abilityPartData, new PlayerInput($abilityPartData->getPlayerHash(), []));

        return true;
    }
}
