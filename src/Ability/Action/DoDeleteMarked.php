<?php

declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\ActionAbilityPartProcessorInterface;
use App\Entity\Ability\AbilityPart;
use App\Entity\Ability\AbilityPartOptions;
use App\Entity\Game\Field;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Entity\Update\UpdateDataInterface;
use App\Exception\MisconfiguredAbilityException;
use App\Service\ChatHelper;

class DoDeleteMarked implements ActionAbilityPartProcessorInterface
{
    private $chatHelper;

    public function __construct(
        ChatHelper $chatHelper
    ) {
        $this->chatHelper = $chatHelper;
    }

    public function getName(): string
    {
        return 'do_delete_marked';
    }

    public function processPlayerInput(State $state, AbilityPart $abilityPart, PlayerInput $input): bool
    {
        return true;
    }

    public function getPrivateUpdateData(State $state, AbilityPart $abilityPart): ?UpdateDataInterface
    {
        return null;
    }

    public function getPublicUpdateData(State $state, AbilityPart $abilityPart): ?UpdateDataInterface
    {
        return null;
    }

    public function getValidChoices(State $state, AbilityPart $abilityPart): array
    {
        return [];
    }

    public function progressState(State $state, AbilityPart $abilityPart): bool
    {
        $playerOption = $abilityPart->getOption(AbilityPartOptions::PLAYER);
        $player = $state->getPlayer($abilityPart->getPlayerHash());
        if ($playerOption === AbilityPartOptions::PLAYER_OPPONENT) {
            $targetPlayer = $state->getOpposingPlayer($abilityPart->getPlayerHash());
        } else {
            $targetPlayer = $player;
        }

        $locations = $player->getBuffer()->getMarked($abilityPart->getSelfCardId()) ?? [];
        if (!is_array($locations)) {
            $locations = [$locations];
        }

        $cards = [];
        foreach ($locations as $location) {
            $card = $targetPlayer->getField()->getCircleCard($location);
            if ($card !== null) {
                $card->setDeleted(true);
                $cards[] = $card;
            }
        }

        $cardCount = count($cards);
        if ($cardCount === 1) {
            $state->addChatLog(
                $player->getName()
                . ' deletes'
                . ($targetPlayer === $player ? '' : ' opponent\'s')
                . ' '
                . $this->chatHelper->generateCardLink($cards[0])
            );
        } elseif ($cardCount > 1) {
            $state->addChatLog(
                $player->getName()
                . ' deletes '
                . $cardCount
                . ($targetPlayer === $player ? '' : ' opponent\'s')
                . ' '
                . $this->chatHelper->plural($cardCount, 'unit')
            );
        }

        return true;
    }
}
