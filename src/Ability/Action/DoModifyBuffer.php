<?php

declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\ActionAbilityPartProcessorInterface;
use App\Entity\Ability\AbilityPart;
use App\Entity\Ability\AbilityPartOptions;
use App\Entity\Game\Card;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Entity\Update\UpdateDataInterface;

class DoModifyBuffer implements ActionAbilityPartProcessorInterface
{
    public function getName(): string
    {
        return 'do_modify_buffer';
    }

    public function processPlayerInput(State $state, AbilityPart $abilityPartData, PlayerInput $input): bool
    {
        return true;
    }

    public function getPrivateUpdateData(State $state, AbilityPart $abilityPartData): ?UpdateDataInterface
    {
        return null;
    }

    public function getPublicUpdateData(State $state, AbilityPart $abilityPartData): ?UpdateDataInterface
    {
        return null;
    }

    public function getValidChoices(State $state, AbilityPart $abilityPartData): array
    {
        return [];
    }

    public function progressState(State $state, AbilityPart $abilityPartData): bool
    {
        $player = $state->getPlayer($abilityPartData->getPlayerHash());
        $length = $abilityPartData->getOption(AbilityPartOptions::LENGTH) ?? Card::BONUS_LENGTH_BATTLE;

        $power = $abilityPartData->getOption(Card::BONUS_POWER);
        if ($power === AbilityPartOptions::STORED_VALUE) {
            $power = $player->getBuffer()->getValue($abilityPartData->getSelfCardId());
        }
        $critical = $abilityPartData->getOption(Card::BONUS_CRITICAL);
        if ($critical === AbilityPartOptions::STORED_VALUE) {
            $critical = $player->getBuffer()->getValue($abilityPartData->getSelfCardId());
        }
        $property = $abilityPartData->getOption(Card::BONUS_PROPERTY);
        $drive = $abilityPartData->getOption(Card::BONUS_DRIVE);
        if ($drive === AbilityPartOptions::STORED_VALUE) {
            $drive = $player->getBuffer()->getValue($abilityPartData->getSelfCardId());
        }
        $state = $abilityPartData->getOption(AbilityPartOptions::STATE);
        $face = $abilityPartData->getOption(AbilityPartOptions::FACE);

        $idValues = $player->getBuffer()->getMarked($abilityPartData->getSelfCardId()) ?? [];
        if (!is_array($idValues)) {
            $idValues = [$idValues];
        }
        $cards = $player->getBuffer()->getCards($abilityPartData->getSelfCardId());

        foreach ($cards as $card) {
            if ($card === null) {
                continue;
            }

            if ($property !== null) {
                $card->addBonusProperty($property);
            }
            if ($power !== null) {
                $card->addBonus($length, Card::BONUS_POWER, $power);
            }
            if ($critical !== null) {
                $card->addBonus($length, Card::BONUS_CRITICAL, $critical);
            }
            if ($drive !== null) {
                $card->addBonus($length, Card::BONUS_DRIVE, $drive);
            }
            if ($state !== null) {
                $card->setState($state);
            }
            if ($face !== null) {
                $card->setFaceDown($face === AbilityPartOptions::FACE_DOWN);
            }
        }

        foreach ($idValues as $cardId) {
            if ($cardId === null) {
                continue;
            }
            $card = $player->getCardById($cardId);
            if ($card === null) {
                continue;
            }

            if ($property !== null) {
                $card->addBonusProperty($property);
            }
            if ($power !== null) {
                $card->addBonus($length, Card::BONUS_POWER, $power);
            }
            if ($critical !== null) {
                $card->addBonus($length, Card::BONUS_CRITICAL, $critical);
            }
            if ($drive !== null) {
                $card->addBonus($length, Card::BONUS_DRIVE, $drive);
            }
            if ($state !== null) {
                $card->setState($state);
            }
            if ($face !== null) {
                $card->setFaceDown($face === AbilityPartOptions::FACE_DOWN);
            }
        }

        return true;
    }
}
