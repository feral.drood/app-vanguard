<?php

declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\ActionAbilityPartProcessorInterface;
use App\Entity\Ability\AbilityPart;
use App\Entity\Ability\AbilityPartOptions;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Entity\Update\UpdateDataInterface;

class DoMarkCard implements ActionAbilityPartProcessorInterface
{
    public function getName(): string
    {
        return 'do_mark_card';
    }

    public function processPlayerInput(State $state, AbilityPart $abilityPartData, PlayerInput $input): bool
    {
        return true;
    }

    public function getPrivateUpdateData(State $state, AbilityPart $abilityPartData): ?UpdateDataInterface
    {
        return null;
    }

    public function getPublicUpdateData(State $state, AbilityPart $abilityPartData): ?UpdateDataInterface
    {
        return null;
    }

    public function getValidChoices(State $state, AbilityPart $abilityPartData): array
    {
        return [];
    }

    public function progressState(State $state, AbilityPart $abilityPartData): bool
    {
        $playerOption = $abilityPartData->getOption(AbilityPartOptions::PLAYER);
        $player = $state->getPlayer($abilityPartData->getPlayerHash());
        if ($playerOption === AbilityPartOptions::PLAYER_OPPONENT) {
            $targetPlayer = $state->getOpposingPlayer($abilityPartData->getPlayerHash());
        } else {
            $targetPlayer = $player;
        }

        $cardId = null;
        switch ($abilityPartData->getOption(AbilityPartOptions::CARD)) {
            case AbilityPartOptions::CARD_SELF:
                $cardId = $abilityPartData->getSelfCardId();
                break;
            case AbilityPartOptions::CARD_EVENT:
                $cardId = $abilityPartData->getEventCardId();
                break;
            case AbilityPartOptions::LOCATION_ATTACKER:
                $attacker = $state->getAttacker();
                if ($attacker !== null) {
                    $attacker = $state->getActivePlayer()->getField()->getCircleCard($attacker);
                    if ($attacker !== null) {
                        $cardId = $attacker->getId();
                    }
                }
                break;
            case AbilityPartOptions::LOCATION_DEFENDER:
                $defender = $state->getDefender();
                if ($defender !== null) {
                    $defender = $state->getInactivePlayer()->getField()->getCircleCard($defender);
                    if ($defender !== null) {
                        $cardId = $defender->getId();
                    }
                }
                break;
            case AbilityPartOptions::LOCATION_BOOSTER:
                $booster = $state->getBooster();
                if ($booster !== null) {
                    $booster = $state->getActivePlayer()->getField()->getCircleCard($booster);
                    if ($booster !== null) {
                        $cardId = $booster->getId();
                    }
                }
                break;
            case AbilityPartOptions::LOCATION_TRIGGER:
                $cardId = $player->getTrigger()->getId();
                break;
            default:
                $cardId = $targetPlayer->getField()->getCircleCard(
                    $abilityPartData->getOption(AbilityPartOptions::CARD)
                )->getId();
        }

        if ($cardId !== null) {
            $player->getBuffer()->setMarked($abilityPartData->getSelfCardId(), $cardId);
        }

        return true;
    }
}
