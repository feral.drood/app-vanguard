<?php

declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\ActionAbilityPartProcessorInterface;
use App\Entity\Ability\AbilityPart;
use App\Entity\Ability\AbilityPartOptions;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Entity\Update\UpdateDataInterface;
use App\Service\ChatHelper;
use App\Service\Game\CardEventDispatcher;

class DoSoulCharge implements ActionAbilityPartProcessorInterface
{
    private $chatHelper;
    private $eventDispatcher;

    public function __construct(
        ChatHelper $chatHelper,
        CardEventDispatcher $eventDispatcher
    ) {
        $this->chatHelper = $chatHelper;
        $this->eventDispatcher = $eventDispatcher;
    }
    public function getName(): string
    {
        return 'do_soul_charge';
    }


    public function processPlayerInput(State $state, AbilityPart $abilityPart, PlayerInput $input): bool
    {
        return true;
    }

    public function getPrivateUpdateData(State $state, AbilityPart $abilityPart): ?UpdateDataInterface
    {
        return null;
    }

    public function getPublicUpdateData(State $state, AbilityPart $abilityPart): ?UpdateDataInterface
    {
        return null;
    }

    public function getValidChoices(State $state, AbilityPart $abilityPart): array
    {
        return [];
    }

    public function progressState(State $state, AbilityPart $abilityPart): bool
    {
        $player = $state->getPlayer($abilityPart->getPlayerHash());
        $count = $abilityPart->getOption(AbilityPartOptions::COUNT) ?? 0;
        $cards = $player->getDeck()->drawCards($count);
        $player->getSoul()->addCards($cards);

        $cardCount = count($cards);
        if ($cardCount > 0) {
            $state->addChatLog(
                $player->getName()
                . ' soul charges '
                . $cardCount
                . ' '
                . $this->chatHelper->plural($cardCount, 'card')
            );
        }

        return true;
    }
}
