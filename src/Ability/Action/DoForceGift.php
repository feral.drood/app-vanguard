<?php

declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\ActionAbilityPartProcessorInterface;
use App\Entity\Ability\AbilityPart;
use App\Entity\Ability\AbilityPartOptions;
use App\Entity\Game\PlayerError;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Entity\Update\DialogUpdate;
use App\Entity\Update\UpdateDataInterface;

/*
Force I: Choose one of your vanguard or rear-guard circles. For the rest of the game, units on that circle get [Power]+10000 during your turn. Put a portrait Force gift marker on that circle, under any units on that circle.
Force II: Choose one of your vanguard or rear-guard circles. For the rest of the game, set the original critical of units on that circle to 2. Put a landscape Force gift marker on that circle, under any units on that circle.
*/

class DoForceGift implements ActionAbilityPartProcessorInterface
{
    private const FORCE = 'force';
    private const FORCE1 = 'force1';
    private const FORCE2 = 'force2';

    public function getName(): string
    {
        return 'do_force_gift';
    }

    public function processPlayerInput(State $state, AbilityPart $abilityPartData, PlayerInput $input): bool
    {
        $player = $state->getPlayer($abilityPartData->getPlayerHash());
        $validChoices = $this->getValidChoices($state, $abilityPartData);
        $choice = $input->getValue();

        if (!in_array($choice, $validChoices, true)) {
            $state->setPlayerError(
                new PlayerError(
                    $input->getPlayerHash(),
                    'Invalid choice'
                )
            );

            return false;
        }

        if ($player->getGiftType(self::FORCE) === null) {
            $player->setGiftType(self::FORCE, $choice);
            $state->addUpdateMessage($this->getPublicUpdateData($state, $abilityPartData));

            return false;
        } else {
            $player->addGiftCircle(self::FORCE, $choice);
        }

        return true;
    }

    public function getPrivateUpdateData(State $state, AbilityPart $abilityPartData): ?UpdateDataInterface
    {
        return null;
    }

    public function getPublicUpdateData(State $state, AbilityPart $abilityPartData): ?UpdateDataInterface
    {
        $player = $state->getPlayer($abilityPartData->getPlayerHash());

        if ($player->getGiftType(self::FORCE) === null) {
            return
                (new DialogUpdate($abilityPartData->getPlayerHash()))
                ->setTitle('Choose gift type')
                ->setActions([
                    self::FORCE1 => 'Force I',
                    self::FORCE2 => 'Force II',
                ])
            ;
        }

        return
            (new DialogUpdate($abilityPartData->getPlayerHash()))
            ->setTitle('Choose field circle to receive Force gift')
            ->addOption(DialogUpdate::OPTION_MIN_COUNT, 1)
            ->addOption(DialogUpdate::OPTION_MAX_COUNT, 1)
        ;
    }

    public function getValidChoices(State $state, AbilityPart $abilityPartData): array
    {
        $player = $state->getPlayer($abilityPartData->getPlayerHash());

        if ($player->getGiftType(self::FORCE) === null) {
            return [self::FORCE1, self::FORCE2];
        }

        return array_keys($player->getField()->getCircles());
    }

    public function progressState(State $state, AbilityPart $abilityPartData): bool
    {
        $player = $state->getPlayer($abilityPartData->getPlayerHash());
        $location = $abilityPartData->getOption(AbilityPartOptions::LOCATION);
        if (
            $location !== null
            && $location === AbilityPartOptions::LOCATION_VANGUARD
            && $player->getGiftType(self::FORCE) !== null
        ) {
            return $this->processPlayerInput(
                $state,
                $abilityPartData,
                new PlayerInput(
                    $abilityPartData->getPlayerHash(),
                    [AbilityPartOptions::LOCATION_VANGUARD]
                )
            );
        }

        return false;
    }
}
