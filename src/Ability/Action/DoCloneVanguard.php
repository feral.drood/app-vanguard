<?php

declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\ActionAbilityPartProcessorInterface;
use App\Entity\Ability\AbilityPart;
use App\Entity\Game\Card;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Entity\Update\UpdateDataInterface;

class DoCloneVanguard implements ActionAbilityPartProcessorInterface
{
    public function getName(): string
    {
        return 'do_clone_vanguard';
    }

    public function processPlayerInput(State $state, AbilityPart $abilityPart, PlayerInput $input): bool
    {
        return true;
    }

    public function getPrivateUpdateData(State $state, AbilityPart $abilityPart): ?UpdateDataInterface
    {
        return null;
    }

    public function getPublicUpdateData(State $state, AbilityPart $abilityPart): ?UpdateDataInterface
    {
        return null;
    }

    public function getValidChoices(State $state, AbilityPart $abilityPart): array
    {
        return [];
    }

    public function progressState(State $state, AbilityPart $abilityPart): bool
    {
        $player = $state->getPlayer($abilityPart->getPlayerHash());
        $card = $player->getCardById($abilityPart->getSelfCardId());
        $vanguard = $player->getField()->getVanguard();

        $modPower = $vanguard->getPower() - $card->getPower();
        $modCritical = $vanguard->getCritical() - $card->getCritical();
        if ($modPower !== 0) {
            $card->addBonus(Card::BONUS_LENGTH_BATTLE, Card::BONUS_POWER, $modPower);
        }
        if ($modCritical !== 0) {
            $card->addBonus(Card::BONUS_LENGTH_BATTLE, Card::BONUS_CRITICAL, $modPower);
        }

        return true;
    }
}
