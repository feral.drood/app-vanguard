<?php

declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\ActionAbilityPartProcessorInterface;
use App\Entity\Ability\AbilityPart;
use App\Entity\Ability\AbilityPartOptions;
use App\Entity\Game\Card;
use App\Entity\Game\Field;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Entity\Update\UpdateDataInterface;
use App\Exception\MisconfiguredAbilityException;
use App\Service\ChatHelper;

class DoBindMarked implements ActionAbilityPartProcessorInterface
{
    private $chatHelper;

    public function __construct(
        ChatHelper $chatHelper
    ) {
        $this->chatHelper = $chatHelper;
    }

    public function getName(): string
    {
        return 'do_bind_marked';
    }

    public function processPlayerInput(State $state, AbilityPart $abilityPart, PlayerInput $input): bool
    {
        return true;
    }

    public function getPrivateUpdateData(State $state, AbilityPart $abilityPart): ?UpdateDataInterface
    {
        return null;
    }

    public function getPublicUpdateData(State $state, AbilityPart $abilityPart): ?UpdateDataInterface
    {
        return null;
    }

    public function getValidChoices(State $state, AbilityPart $abilityPart): array
    {
        return [];
    }

    public function progressState(State $state, AbilityPart $abilityPart): bool
    {
        $playerOption = $abilityPart->getOption(AbilityPartOptions::PLAYER);
        $player = $state->getPlayer($abilityPart->getPlayerHash());
        $faceOption = $abilityPart->getOption(AbilityPartOptions::FACE);
        if ($playerOption === AbilityPartOptions::PLAYER_OPPONENT) {
            $targetPlayer = $state->getOpposingPlayer($abilityPart->getPlayerHash());
        } else {
            $targetPlayer = $player;
        }

        $locations = $player->getBuffer()->getMarked($abilityPart->getSelfCardId()) ?? [];
        if (!is_array($locations)) {
            $locations = [$locations];
        }

        $removedCards = [];
        foreach ($locations as $location) {
            $card = $targetPlayer->getField()->getCircleCard($location);
            if ($card !== null) {
                $originalFace = $card->isFaceDown();
                $card->reset();
                if ($faceOption !== null) {
                    $card->setFaceDown($faceOption === AbilityPartOptions::FACE_DOWN);
                } else {
                    $card->setFaceDown($originalFace);
                }
                $targetPlayer->getField()->setCircleCard($location, null);
                $removedCards[] = $card;
            }
        }

        $removedCount = count($removedCards);
        if ($removedCount === 1) {
            $state->addChatLog(
                $player->getName()
                . ' binds'
                . ($targetPlayer === $player ? '' : ' opponent\'s')
                . ' '
                . $this->chatHelper->generateCardLink($removedCards[0])
            );
        } elseif ($removedCount > 1) {
            $state->addChatLog(
                $player->getName()
                . ' binds '
                . $removedCount
                . ($targetPlayer === $player ? '' : ' opponent\'s')
                . ' '
                . $this->chatHelper->plural($removedCount, 'unit')
            );
        }
        foreach ($removedCards as $removedCard) {
            if ($removedCard->getType() !== Card::TYPE_TOKEN_UNIT) {
                $targetPlayer->getBind()->addCard($removedCard);
            }
        }

        return true;
    }
}
