<?php

declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\ActionAbilityPartProcessorInterface;
use App\Entity\Ability\AbilityPart;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Entity\Update\UpdateDataInterface;
use App\Service\AbilityPartManager;

class DoCondition implements ActionAbilityPartProcessorInterface
{
    private const IF = 'if';
    private const THEN = 'then';
    private const ELSE = 'else';

    private $abilityPartManager;

    public function __construct(
        AbilityPartManager $abilityPartManager
    ) {
        $this->abilityPartManager = $abilityPartManager;
    }

    public function getName(): string
    {
        return 'do_condition';
    }

    public function processPlayerInput(State $state, AbilityPart $abilityPartData, PlayerInput $input): bool
    {
        return true;
    }

    public function getPrivateUpdateData(State $state, AbilityPart $abilityPartData): ?UpdateDataInterface
    {
        return null;
    }

    public function getPublicUpdateData(State $state, AbilityPart $abilityPartData): ?UpdateDataInterface
    {
        return null;
    }

    public function getValidChoices(State $state, AbilityPart $abilityPartData): array
    {
        return [];
    }

    public function progressState(State $state, AbilityPart $abilityPartData): bool
    {
        $abilityStack = $state->getAbilityStack();

        $conditions = $abilityPartData->getOption(self::IF) ?? [];
        $thenEffects = $abilityPartData->getOption(self::THEN) ?? [];
        $elseEffects = $abilityPartData->getOption(self::ELSE) ?? [];

        $conditionsPassed = true;
        foreach ($conditions as $unpackedConditionData) {
            $conditionPartData = (new AbilityPart($unpackedConditionData))
                ->setPlayerHash($abilityPartData->getPlayerHash())
                ->setEventCardId($abilityPartData->getEventCardId())
                ->setSelfCardId($abilityPartData->getSelfCardId())
            ;

            $conditionService = $this->abilityPartManager->getConditionAbilityPartProcessor($conditionPartData->getType());
            $conditionsPassed = $conditionsPassed && $conditionService->isPassed($state, $conditionPartData);
        }

        if ($conditionsPassed) {
            $effects = $thenEffects;
        } else {
            $effects = $elseEffects;
        }
        $effects = array_reverse($effects);
        foreach ($effects as $unpackedEffectData) {
            $effectPartData = (new AbilityPart($unpackedEffectData))
                ->setPlayerHash($abilityPartData->getPlayerHash())
                ->setEventCardId($abilityPartData->getEventCardId())
                ->setSelfCardId($abilityPartData->getSelfCardId())
            ;
            $abilityStack->insertEffect($effectPartData);
        }

        return true;
    }
}
