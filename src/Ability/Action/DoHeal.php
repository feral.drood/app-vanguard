<?php

declare(strict_types=1);

namespace App\Ability\Action;

use App\Entity\Ability\AbilityPartOptions;
use App\Ability\ActionAbilityPartProcessorInterface;
use App\Entity\Ability\AbilityPart;
use App\Entity\Game\PlayerError;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Entity\Update\DialogUpdate;
use App\Entity\Update\UpdateDataInterface;
use App\Service\ChatHelper;

class DoHeal implements ActionAbilityPartProcessorInterface
{
    private $chatHelper;

    public function __construct(
        ChatHelper $chatHelper
    ) {
        $this->chatHelper = $chatHelper;
    }

    public function getName(): string
    {
        return 'do_heal';
    }

    public function processPlayerInput(State $state, AbilityPart $abilityPartData, PlayerInput $input): bool
    {
        $playerChoices = $input->getList();
        $validChoices = $this->getValidChoices($state, $abilityPartData);
        $count = $abilityPartData->getOption(AbilityPartOptions::COUNT);

        $passedChoices = [];
        foreach ($playerChoices as $choice) {
            if (in_array($choice, $validChoices, true)) {
                $passedChoices[] = $choice;
            }
        }

        $passedCount = count($passedChoices);
        if (
            $passedCount !== count($playerChoices)
            || $passedCount > $count
        ) {
            $state->setPlayerError(
                new PlayerError(
                    $input->getPlayerHash(),
                    'Invalid selection'
                )
            );

            return false;
        }

        $player = $state->getPlayer($abilityPartData->getPlayerHash());
        $cards = $player->getDamage()->removeCardsByIds($passedChoices);
        foreach ($cards as $card) {
            $card->reset();
        }
        $player->getDrop()->addCards($cards);
        $cardCount = count($cards);

        if (!$abilityPartData->getOption(AbilityPartOptions::SILENT) && $cardCount > 0) {
            $state->addChatLog(
                $player->getName()
                . ' heals '
                . $cardCount
                . ' '
                . $this->chatHelper->plural($cardCount, 'card')
            );
        }

        return true;
    }

    public function getPublicUpdateData(State $state, AbilityPart $abilityPartData): ?UpdateDataInterface
    {
        $count = $abilityPartData->getOption(AbilityPartOptions::COUNT);
        return (new DialogUpdate($abilityPartData->getPlayerHash()))
            ->setTitle(
                'Select '
                . $count
                . ' damage '
                . $this->chatHelper->plural($count, 'card')
            )
            ->addOption(AbilityPartOptions::MAX_COUNT, $count)
            ->addOption(AbilityPartOptions::MIN_COUNT, $count)
        ;
    }

    public function getPrivateUpdateData(State $state, AbilityPart $abilityPartData): ?UpdateDataInterface
    {
        return null;
    }

    public function getValidChoices(State $state, AbilityPart $abilityPartData): array
    {
        $player = $state->getPlayer($abilityPartData->getPlayerHash());
        $opponent = $state->getOpposingPlayer($abilityPartData->getPlayerHash());
        if ($player->getDamage()->getCount() < $opponent->getDamage()->getCount()) {
            return [];
        }

        $cards = $player->getDamage()->getCards();

        $cardIds = [];
        foreach ($cards as $card) {
            $cardIds[] = $card->getId();
        }

        return $cardIds;
    }

    public function progressState(State $state, AbilityPart $abilityPartData): bool
    {
        $count = $abilityPartData->getOption(AbilityPartOptions::COUNT);
        $validChoices = $this->getValidChoices($state, $abilityPartData);

        if (count($validChoices) === 0) {
            return true;
        }

        if (count($validChoices) <= $count) {
            return $this->processPlayerInput(
                $state,
                $abilityPartData,
                new PlayerInput($abilityPartData->getPlayerHash(), $validChoices)
            );
        }

        return false;
    }
}
