<?php

declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\ActionAbilityPartProcessorInterface;
use App\Entity\Ability\AbilityPart;
use App\Entity\Ability\AbilityPartOptions;
use App\Entity\Game\Card;
use App\Entity\Game\CardEvent;
use App\Entity\Game\CardEventTypes;
use App\Entity\Game\Field;
use App\Entity\Game\PlayerError;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Entity\Update\DialogUpdate;
use App\Entity\Update\UpdateDataInterface;
use App\Service\ChatHelper;
use App\Service\Game\CardEventDispatcher;

class DoCallBuffer implements ActionAbilityPartProcessorInterface
{
    private $chatHelper;
    private $eventDispatcher;

    public function __construct(
        ChatHelper $chatHelper,
        CardEventDispatcher $eventDispatcher
    ) {
        $this->chatHelper = $chatHelper;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function getName(): string
    {
        return 'do_call_buffer';
    }

    public function processPlayerInput(State $state, AbilityPart $abilityPart, PlayerInput $input): bool
    {
        $player = $state->getPlayer($abilityPart->getPlayerHash());
        $buffer = $player->getBuffer();
        $playerChoice = $input->getValue();
        $bufferCards = $buffer->getCards($abilityPart->getSelfCardId());
        $bufferValue = $buffer->getMarked($abilityPart->getSelfCardId());
        $validChoices = $this->getValidChoices($state, $abilityPart);

        if ($bufferValue === null && count($bufferCards) === 1) {
            $bufferValue = array_keys($bufferCards)[0];
        }
        if (is_array($bufferValue)) {
            $bufferValue = $bufferValue[0];
        }

        if (
            $bufferValue === null
            && in_array($playerChoice, $validChoices, true)
        ) {
            $buffer->setMarked($abilityPart->getSelfCardId(), $playerChoice);
            $state->addUpdateMessage(
                $this->getPublicUpdateData($state, $abilityPart)
            );

            return false;
        }

        if (!in_array($playerChoice, $validChoices, true)) {
            $state->setPlayerError(
                new PlayerError(
                    $abilityPart->getPlayerHash(),
                    'Invalid selection'
                )
            );

            return false;
        }

        $card = $bufferCards[$bufferValue];
        $buffer->removeCardById($abilityPart->getSelfCardId(), (string)$bufferValue);

        $oldCard = $player->getField()->getCircleCard($playerChoice);
        if ($oldCard !== null) {
            $oldCard->reset();
            $this->eventDispatcher->dispatch(
                new CardEvent(
                    $state,
                    CardEventTypes::BEFORE_RETIRE,
                    $abilityPart->getPlayerHash(),
                    $oldCard
                )
            );
            $player->getDrop()->addCard($oldCard);
            $player->getField()->setCircleCard($playerChoice, null);
            $this->eventDispatcher->dispatch(
                new CardEvent(
                    $state,
                    CardEventTypes::AFTER_RETIRE,
                    $abilityPart->getPlayerHash(),
                    $oldCard
                )
            );
        }

        $origin = $buffer->getOrigin($abilityPart->getSelfCardId());
        switch ($origin) {
            case AbilityPartOptions::LOCATION_DECK:
                $eventBeforeType = CardEventTypes::BEFORE_CALL_FROM_DECK;
                $eventAfterType = CardEventTypes::AFTER_CALL_FROM_DECK;
                break;
            case AbilityPartOptions::LOCATION_SOUL:
                $eventBeforeType = CardEventTypes::BEFORE_CALL_FROM_SOUL;
                $eventAfterType = CardEventTypes::AFTER_CALL_FROM_SOUL;
                break;
            case AbilityPartOptions::LOCATION_HAND:
                $eventBeforeType = CardEventTypes::BEFORE_CALL_FROM_HAND;
                $eventAfterType = CardEventTypes::AFTER_CALL_FROM_HAND;
                break;
            default:
                $eventBeforeType = CardEventTypes::BEFORE_CALL;
                $eventAfterType = CardEventTypes::AFTER_CALL;
        }
        $this->eventDispatcher->dispatch(
            new CardEvent(
                $state,
                $eventBeforeType,
                $abilityPart->getPlayerHash(),
                $card
            )
        );
        $player->getField()->setCircleCard($playerChoice, $card);
        $this->eventDispatcher->dispatch(
            new CardEvent(
                $state,
                $eventAfterType,
                $abilityPart->getPlayerHash(),
                $card
            )
        );

        $state->addChatLog(
            $player->getName()
            . ' calls '
            . $this->chatHelper->generateCardLink($card)
            . ($origin !== null ? ' from ' . $origin : '')
        );

        return count($buffer->getCards($abilityPart->getSelfCardId())) === 0;
    }

    public function getPrivateUpdateData(State $state, AbilityPart $abilityPartData): ?UpdateDataInterface
    {
        $player = $state->getPlayer($abilityPartData->getPlayerHash());
        $bufferCards = $player->getBuffer()->getCards($abilityPartData->getSelfCardId());
        $bufferValue = $player->getBuffer()->getMarked($abilityPartData->getSelfCardId());

        if ($bufferValue === null && count($bufferCards) === 1) {
            $bufferValue = array_keys($bufferCards)[0];
        }
        if (is_array($bufferValue)) {
            $bufferValue = $bufferValue[0];
        }

        if (
            $bufferValue !== null
            && isset($bufferCards[$bufferValue])
        ) {
            return (new DialogUpdate($abilityPartData->getPlayerHash()))
                ->setTitle('Select rearguard circle to call ' . ($bufferCards[$bufferValue])->getName())
                ->setLoadZone(null)
                ->addOption(DialogUpdate::OPTION_MIN_COUNT, 1)
                ->addOption(DialogUpdate::OPTION_MAX_COUNT, 1)
            ;
        }

        $actions = [];
        foreach ($bufferCards as $card) {
            if ($card instanceof Card) {
                $actions[$card->getId()] = $card->getName();
            }
        }

        return (new DialogUpdate($abilityPartData->getPlayerHash()))
            ->setTitle('Choose card to call')
            ->setLoadZone(null)
            ->setActions($actions)
        ;
    }

    public function getPublicUpdateData(State $state, AbilityPart $abilityPartData): ?UpdateDataInterface
    {
        return (new DialogUpdate($abilityPartData->getPlayerHash()))
            ->setLoadZone('action')
        ;
    }

    public function getValidChoices(State $state, AbilityPart $abilityPartData): array
    {
        $player = $state->getPlayer($abilityPartData->getPlayerHash());
        $bufferValue = $player->getBuffer()->getMarked($abilityPartData->getSelfCardId());
        $bufferCards = $player->getBuffer()->getCards($abilityPartData->getSelfCardId());
        $location = $abilityPartData->getOption(AbilityPartOptions::LOCATION);

        if ($bufferValue !== null || count($bufferCards) === 1) {
            if ($location === AbilityPartOptions::LOCATION_OPEN) {
                $circles = [];
                foreach ($player->getField()->getCircles() as $circleId => $circleUnit) {
                    if ($circleUnit === null) {
                        $circles[] = $circleId;
                    }
                }
            } else {
                $circles = array_keys($player->getField()->getCircles());
            }

            return array_diff($circles, [Field::VANGUARD]);
        }

        return array_keys($player->getBuffer()->getCards($abilityPartData->getSelfCardId()));
    }

    public function progressState(State $state, AbilityPart $abilityPartData): bool
    {
        $player = $state->getPlayer($abilityPartData->getPlayerHash());
        $cards = $player->getBuffer()->getCards($abilityPartData->getSelfCardId());

        if (count($cards) > 0) {
            return false;
//        } elseif (count($cards) === 1) {
//            return $this->processPlayerInput(
//                $state,
//                $abilityPartData,
//                new PlayerInput(
//                    $abilityPartData->getPlayerHash(),
//                    [array_keys($cards)[0]]
//                )
//            );
        }

        return true;
    }
}
