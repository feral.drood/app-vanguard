<?php

declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\ActionAbilityPartProcessorInterface;
use App\Entity\Ability\AbilityPart;
use App\Entity\Ability\AbilityPartOptions;
use App\Entity\Game\Card;
use App\Entity\Game\PlayerError;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Entity\Update\DialogUpdate;
use App\Entity\Update\UpdateDataInterface;
use App\Service\ChatHelper;

class DoRevealBuffer implements ActionAbilityPartProcessorInterface
{
    private $chatHelper;

    public function __construct(
        ChatHelper $chatHelper
    ) {
        $this->chatHelper = $chatHelper;
    }

    public function getName(): string
    {
        return 'do_reveal_buffer';
    }

    public function processPlayerInput(State $state, AbilityPart $abilityPart, PlayerInput $input): bool
    {
        return true;
    }

    public function getPublicUpdateData(State $state, AbilityPart $abilityPart): ?UpdateDataInterface
    {
        return null;
    }

    public function getPrivateUpdateData(State $state, AbilityPart $abilityPart): ?UpdateDataInterface
    {
        return null;
    }

    public function getValidChoices(State $state, AbilityPart $abilityPart): array
    {
        return [];
    }

    public function progressState(State $state, AbilityPart $abilityPart): bool
    {
        $player = $state->getPlayer($abilityPart->getPlayerHash());
        $cards = $player->getBuffer()->getCards($abilityPart->getSelfCardId());
        $cardCount = count($cards);

        if ($cardCount > 0) {
            $state->addChatLog(
                $player->getName()
                . ' reveals '
                . $cardCount
                . ' '
                . $this->chatHelper->plural($cardCount, 'card')
            );
            $state->addUpdateMessage(
                (new DialogUpdate($state->getInactivePlayerHash()))
                    ->setType('view')
                    ->setTitle($player->getName() . ' revealed cards')
                    ->setCards($cards)
                    ->setActions([
                        'close' => 'Close'
                    ])
            );

        }

        return false;
    }
}
