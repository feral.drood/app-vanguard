<?php

declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\ActionAbilityPartProcessorInterface;
use App\Entity\Ability\AbilityPart;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Entity\Update\UpdateDataInterface;

class DoRegisterNamedUsage implements ActionAbilityPartProcessorInterface
{
    public function getName(): string
    {
        return 'do_register_named_usage';
    }

    public function processPlayerInput(State $state, AbilityPart $abilityPartData, PlayerInput $input): bool
    {
        return true;
    }

    public function getPrivateUpdateData(State $state, AbilityPart $abilityPartData): ?UpdateDataInterface
    {
        return null;
    }

    public function getPublicUpdateData(State $state, AbilityPart $abilityPartData): ?UpdateDataInterface
    {
        return null;
    }

    public function getValidChoices(State $state, AbilityPart $abilityPartData): array
    {
        return [];
    }

    public function progressState(State $state, AbilityPart $abilityPartData): bool
    {
        $card = $state->getPlayer($abilityPartData->getPlayerHash())->getCardById($abilityPartData->getSelfCardId());

        $state->getAbilityStack()->addUsedAbility($card->getName());

        return true;
    }
}
