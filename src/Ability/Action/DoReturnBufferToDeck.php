<?php

declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\ActionAbilityPartProcessorInterface;
use App\Entity\Ability\AbilityPart;
use App\Entity\Ability\AbilityPartOptions;
use App\Entity\Game\Card;
use App\Entity\Game\CardEvent;
use App\Entity\Game\CardEventTypes;
use App\Entity\Game\Field;
use App\Entity\Game\PlayerError;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Entity\Update\DialogUpdate;
use App\Entity\Update\UpdateDataInterface;
use App\Service\ChatHelper;
use App\Service\Game\CardEventDispatcher;

class DoReturnBufferToDeck implements ActionAbilityPartProcessorInterface
{
    private $chatHelper;
    private $eventDispatcher;

    public function __construct(
        ChatHelper $chatHelper,
        CardEventDispatcher $eventDispatcher
    ) {
        $this->chatHelper = $chatHelper;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function getName(): string
    {
        return 'do_return_buffer_to_deck';
    }

    public function processPlayerInput(State $state, AbilityPart $abilityPart, PlayerInput $input): bool
    {
        $player = $state->getPlayer($abilityPart->getPlayerHash());
        $buffer = $player->getBuffer();
        $playerChoice = $input->getValue();
        $bufferCards = $buffer->getCards($abilityPart->getSelfCardId());
        $validChoices = $this->getValidChoices($state, $abilityPart);

        if (!in_array($playerChoice, $validChoices, true)) {
            $state->setPlayerError(
                new PlayerError(
                    $abilityPart->getPlayerHash(),
                    'Invalid selection'
                )
            );

            return false;
        }

        $card = $bufferCards[$playerChoice];
        $buffer->removeCardById($abilityPart->getSelfCardId(), $playerChoice);
        $player->getDeck()->addCard($card);

        $state->addChatLog(
            $player->getName()
            . ' puts 1 card to top of the deck'
        );

        return count($buffer->getCards($abilityPart->getSelfCardId())) === 0;
    }

    public function getPrivateUpdateData(State $state, AbilityPart $abilityPartData): ?UpdateDataInterface
    {
        $player = $state->getPlayer($abilityPartData->getPlayerHash());
        $bufferCards = $player->getBuffer()->getCards($abilityPartData->getSelfCardId());

        $cards = [];
        foreach ($bufferCards as $card) {
            $cards[$card->getId()] = $card->serializeForUpdate(Card::ZONE_VIEW);
        }

        return (new DialogUpdate($abilityPartData->getPlayerHash()))
            ->setTitle('Select card to put on top of your deck')
            ->setLoadZone(null)
            ->setCards($cards)
            ->addOption(DialogUpdate::OPTION_MAX_COUNT, 1)
        ;
    }

    public function getPublicUpdateData(State $state, AbilityPart $abilityPartData): ?UpdateDataInterface
    {
        return (new DialogUpdate($abilityPartData->getPlayerHash()))
            ->setLoadZone('action')
        ;
    }

    public function getValidChoices(State $state, AbilityPart $abilityPartData): array
    {
        $player = $state->getPlayer($abilityPartData->getPlayerHash());

        return array_keys($player->getBuffer()->getCards($abilityPartData->getSelfCardId()));
    }

    public function progressState(State $state, AbilityPart $abilityPartData): bool
    {
        $validChoices = $this->getValidChoices($state, $abilityPartData);

        if (count($validChoices) === 0) {
            return true;
        } elseif (count($validChoices) === 1) {
            return $this->processPlayerInput(
                $state,
                $abilityPartData,
                new PlayerInput($abilityPartData->getPlayerHash(), $validChoices)
            );
        }

        return false;
    }
}
