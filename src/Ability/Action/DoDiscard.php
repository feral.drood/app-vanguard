<?php

declare(strict_types=1);

namespace App\Ability\Action;

use App\Entity\Ability\AbilityPartOptions;
use App\Ability\ActionAbilityPartProcessorInterface;
use App\Entity\Ability\AbilityPart;
use App\Entity\Game\PlayerError;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Entity\Update\DialogUpdate;
use App\Entity\Update\UpdateDataInterface;
use App\Service\ChatHelper;

class DoDiscard implements ActionAbilityPartProcessorInterface
{
    private $chatHelper;

    public function __construct(
        ChatHelper $chatHelper
    ) {
        $this->chatHelper = $chatHelper;
    }

    public function getName(): string
    {
        return 'do_discard';
    }

    public function processPlayerInput(State $state, AbilityPart $abilityPartData, PlayerInput $input): bool
    {
        $player = $state->getPlayer($abilityPartData->getPlayerHash());
        $selections = $input->getList();
        $validChoices = $this->getValidChoices($state, $abilityPartData);

        if (count($validChoices) === 0) {
            return true;
        }

        $validSelection = true;
        foreach ($selections as $selection) {
            if (!in_array($selection, $validChoices, true)) {
                $validSelection = false;
                break;
            }
        }

        if (!$validSelection) {
            $state->setPlayerError(
                new PlayerError(
                    $abilityPartData->getPlayerHash(),
                    'Invalid selection'
                )
            );

            return false;
        }


        $cards = $player->getHand()->removeCardsByIds($selections);
        $count = count($cards);
        $state->addChatLog(
            $player->getName()
            . ' discards '
            . $count
            . ' '
            . $this->chatHelper->plural(
                $count,
                'card'
            )
        );
        $player->getDrop()->addCards($cards);

        return true;
    }

    public function getPublicUpdateData(State $state, AbilityPart $abilityPartData): ?UpdateDataInterface
    {
        $count = $abilityPartData->getOption(AbilityPartOptions::COUNT);

        return (new DialogUpdate($abilityPartData->getPlayerHash()))
            ->setTitle(
                'Discard '
                . $count
                . ' '
                . $this->chatHelper->plural($count, 'card')
                . ' from hand'
            )
            ->setLoadZone(null)
            ->addOption(DialogUpdate::OPTION_MIN_COUNT, $count)
            ->addOption(DialogUpdate::OPTION_MAX_COUNT, $count)
            ;
    }

    public function getPrivateUpdateData(State $state, AbilityPart $abilityPartData): ?UpdateDataInterface
    {
        return null;
    }

    public function getValidChoices(State $state, AbilityPart $abilityPartData): array
    {
        $player = $state->getPlayer($abilityPartData->getPlayerHash());
        $cards = $player->getHand()->getCards();

        $choices = [];
        foreach ($cards as $card) {
            $choices[] = $card->getId();
        }

        return $choices;
    }

    public function progressState(State $state, AbilityPart $abilityPartData): bool
    {
        $count = $abilityPartData->getOption(AbilityPartOptions::COUNT);
        $validChoices = $this->getValidChoices($state, $abilityPartData);
        if (count($validChoices) <= $count) {
            $this->processPlayerInput($state, $abilityPartData, new PlayerInput(
                $abilityPartData->getPlayerHash(),
                $validChoices
            ));

            return true;
        }

        return false;
    }
}
