<?php

declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\ActionAbilityPartProcessorInterface;
use App\Entity\Ability\AbilityPart;
use App\Entity\Ability\AbilityPartOptions;
use App\Entity\Game\Card;
use App\Entity\Game\Deck;
use App\Entity\Game\Field;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Entity\Update\UpdateDataInterface;
use App\Exception\MisconfiguredAbilityException;
use App\Service\ChatHelper;

class DoMoveMarked implements ActionAbilityPartProcessorInterface
{
    private const FROM = 'from';
    private const TO = 'to';

    private $chatHelper;

    public function __construct(
        ChatHelper $chatHelper
    ) {
        $this->chatHelper = $chatHelper;
    }

    public function getName(): string
    {
        return 'do_move_marked';
    }

    public function processPlayerInput(State $state, AbilityPart $abilityPart, PlayerInput $input): bool
    {
        return true;
    }

    public function getPrivateUpdateData(State $state, AbilityPart $abilityPart): ?UpdateDataInterface
    {
        return null;
    }

    public function getPublicUpdateData(State $state, AbilityPart $abilityPart): ?UpdateDataInterface
    {
        return null;
    }

    public function getValidChoices(State $state, AbilityPart $abilityPart): array
    {
        return [];
    }

    public function progressState(State $state, AbilityPart $abilityPart): bool
    {
        $player = $state->getPlayer($abilityPart->getPlayerHash());
        $from = $abilityPart->getOption(self::FROM) ?? AbilityPartOptions::LOCATION_BUFFER;
        $to = $abilityPart->getOption(self::TO) ?? AbilityPartOptions::LOCATION_BUFFER;;

        if ($from === $to) {
            return true;
        }

        $cardsIds = $player->getBuffer()->getMarked($abilityPart->getSelfCardId()) ?? [];
        if (!is_array($cardsIds)) {
            $cardsIds = [$cardsIds];
        }
        switch ($from) {
            case AbilityPartOptions::LOCATION_HAND:
                $player->getBuffer()->setOrigin(
                    $abilityPart->getSelfCardId(),
                    AbilityPartOptions::LOCATION_HAND
                );
                $cards = $player->getHand()->removeCardsByIds($cardsIds);
                break;
            case AbilityPartOptions::LOCATION_SOUL:
                $player->getBuffer()->setOrigin(
                    $abilityPart->getSelfCardId(),
                    AbilityPartOptions::LOCATION_SOUL
                );
                $cards = $player->getSoul()->removeCardsByIds($cardsIds);
                break;
            case AbilityPartOptions::LOCATION_DAMAGE:
                $player->getBuffer()->setOrigin(
                    $abilityPart->getSelfCardId(),
                    AbilityPartOptions::LOCATION_DAMAGE
                );
                $cards = $player->getDamage()->removeCardsByIds($cardsIds);
                break;
            case AbilityPartOptions::LOCATION_DROP:
                $player->getBuffer()->setOrigin(
                    $abilityPart->getSelfCardId(),
                    AbilityPartOptions::LOCATION_DROP
                );
                $cards = $player->getDrop()->removeCardsByIds($cardsIds);
                break;
            case AbilityPartOptions::LOCATION_DECK:
                $player->getBuffer()->setOrigin(
                    $abilityPart->getSelfCardId(),
                    AbilityPartOptions::LOCATION_DECK
                );
                $cards = $player->getDeck()->removeCardsByIds($cardsIds);
                break;
            case AbilityPartOptions::LOCATION_FIELD:
                $player->getBuffer()->setOrigin(
                    $abilityPart->getSelfCardId(),
                    AbilityPartOptions::LOCATION_FIELD
                );
                $cards = [];
                foreach ($cardsIds as $location) {
                    if ($location === Field::VANGUARD) {
                        continue;
                    }
                    $card = $player->getField()->getCircleCard($location);
                    if ($card !== null) {
                        $cards[] = $card;
                        $circle = $player->getField()->getCircleByCardId($card->getId());
                        $player->getField()->setCircleCard($circle, null);
                    }

                }
                $cards = array_filter($cards);
                break;
            case AbilityPartOptions::LOCATION_GUARDIAN:
                $player->getBuffer()->setOrigin(
                    $abilityPart->getSelfCardId(),
                    AbilityPartOptions::LOCATION_GUARDIAN
                );
                $cards = $player->getGuardians()->removeCardsByIds($cardsIds);
                break;
            default:
                $cards = [];
                foreach ($cardsIds as $cardId) {
                    $cards[] = $player->getBuffer()->removeCardById($abilityPart->getSelfCardId(), $cardId);
                }
        }

        $cardCount = count($cards);
        $filteredCards = [];
        foreach ($cards as $card) {
            if ($card->getType() !== Card::TYPE_TOKEN_UNIT) {
                $filteredCards[] = $card;
            }
        }
        $cards = $filteredCards;
        $targetZone = null;
        switch ($to) {
            case AbilityPartOptions::LOCATION_HAND:
                $targetZone = 'hand';
                $player->getHand()->addCards($cards);
                break;
            case AbilityPartOptions::LOCATION_SOUL:
                $targetZone = 'soul';
                $player->getSoul()->addCards($cards);
                break;
            case AbilityPartOptions::LOCATION_DAMAGE:
                $player->getDamage()->addCards($cards);
                break;
            case AbilityPartOptions::LOCATION_DROP:
                $targetZone = 'drop';
                $player->getDrop()->addCards($cards);
                break;
            case AbilityPartOptions::LOCATION_DECK:
                $side = $abilityPart->getOption('side');
                if ($side !== null && $side === Deck::BOTTOM) {
                    $targetZone = 'bottom of deck';
                    $side = Deck::BOTTOM;
                } else {
                    $targetZone = 'top of deck';
                    $side = Deck::TOP;
                }
                $player->getDeck()->addCards($cards, $side);
                break;
            case AbilityPartOptions::LOCATION_FIELD:
                throw new MisconfiguredAbilityException('Invalid to location');
                break;
            case AbilityPartOptions::LOCATION_GUARDIAN:
                $player->getGuardians()->addCards($cards);
                break;
            default:
                $player->getBuffer()->setCards($abilityPart->getSelfCardId(), $cards);
        }

        if ($targetZone !== null) {
            $state->addChatLog(
                $player->getName()
                . ' moves '
                . $cardCount
                . ' '
                . $this->chatHelper->plural($cardCount, 'card')
                . ' to '
                . $targetZone
            );
        }

        return true;
    }
}
