<?php

declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\ActionAbilityPartProcessorInterface;
use App\Entity\Ability\Ability;
use App\Entity\Ability\AbilityPart;
use App\Entity\Ability\AbilityPartOptions;
use App\Entity\Game\Card;
use App\Entity\Game\CardEvent;
use App\Entity\Game\CardEventTypes;
use App\Entity\Game\Field;
use App\Entity\Game\PlayerError;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Entity\Update\DialogUpdate;
use App\Entity\Update\UpdateDataInterface;
use App\Service\ChatHelper;
use App\Service\Game\CardEventDispatcher;

class DoCallFlowerFairyToken implements ActionAbilityPartProcessorInterface
{
    private $chatHelper;
    private $eventDispatcher;

    public function __construct(
        ChatHelper $chatHelper,
        CardEventDispatcher $eventDispatcher
    ) {
        $this->chatHelper = $chatHelper;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function getName(): string
    {
        return 'do_call_flower_fairy_token';
    }

    public function processPlayerInput(State $state, AbilityPart $abilityPart, PlayerInput $input): bool
    {
        $player = $state->getPlayer($abilityPart->getPlayerHash());
        $playerChoice = $input->getValue();
        $validChoices = $this->getValidChoices($state, $abilityPart);

        if (!in_array($playerChoice, $validChoices, true)) {
            $state->setPlayerError(
                new PlayerError(
                    $abilityPart->getPlayerHash(),
                    'Invalid selection'
                )
            );

            return false;
        }

        $oldCard = $player->getField()->getCircleCard($playerChoice);
        if ($oldCard !== null) {
            $oldCard->reset();
            $this->eventDispatcher->dispatch(
                new CardEvent(
                    $state,
                    CardEventTypes::BEFORE_RETIRE,
                    $abilityPart->getPlayerHash(),
                    $oldCard
                )
            );
            $player->getDrop()->addCard($oldCard);
            $player->getField()->setCircleCard($playerChoice, null);
            $this->eventDispatcher->dispatch(
                new CardEvent(
                    $state,
                    CardEventTypes::AFTER_RETIRE,
                    $abilityPart->getPlayerHash(),
                    $oldCard
                )
            );
        }

        $ability = (new Ability())
            ->setType(Ability::TYPE_AUTO)
            ->setEvent('before_attack')
            ->addCondition(
                (new AbilityPart())
                ->setType('is_card_check')
                ->addOption('card', 'self')
                ->addOption('location', 'rearguard')
            )
            ->addEffect(
                (new AbilityPart())
                ->setType('do_clone_vanguard')
            )
        ;

        $card = (new Card())
            ->setName('Ahsha\'s Flower Fairy')
            ->setGrade(0)
            ->setSkill(Card::SKILL_BOOST)
            ->setType(Card::TYPE_TOKEN_UNIT)
            ->setPower(13)
            ->setCritical(1)
            ->setId('FlowerFairyToken#' . $player->getNextTokenId())
            ->setImage('https://vignette.wikia.nocookie.net/cardfight/images/4/4a/V-TD12-T01EN_(Sample).png')
            ->setRace('Plant')
            ->setAbilities([
                $ability->getData()
            ])
        ;
        $this->eventDispatcher->dispatch(
            new CardEvent(
                $state,
                CardEventTypes::BEFORE_CALL,
                $abilityPart->getPlayerHash(),
                $card
            )
        );
        $player->getField()->setCircleCard($playerChoice, $card);
        $this->eventDispatcher->dispatch(
            new CardEvent(
                $state,
                CardEventTypes::AFTER_CALL,
                $abilityPart->getPlayerHash(),
                $card
            )
        );

        $state->addChatLog(
            $player->getName()
            . ' calls '
            . $this->chatHelper->generateCardLink($card)
        );

        return true;
    }

    public function getPrivateUpdateData(State $state, AbilityPart $abilityPartData): ?UpdateDataInterface
    {
        return null;
    }

    public function getPublicUpdateData(State $state, AbilityPart $abilityPartData): ?UpdateDataInterface
    {
        return (new DialogUpdate($abilityPartData->getPlayerHash()))
            ->setTitle('Select rearguard circle to call Plant Token')
            ->addOption(DialogUpdate::OPTION_MIN_COUNT, 1)
            ->addOption(DialogUpdate::OPTION_MAX_COUNT, 1)
            ;
    }

    public function getValidChoices(State $state, AbilityPart $abilityPartData): array
    {
        $player = $state->getPlayer($abilityPartData->getPlayerHash());
        $circles = array_keys($player->getField()->getCircles());

        return array_diff($circles, [Field::VANGUARD]);
    }

    public function progressState(State $state, AbilityPart $abilityPartData): bool
    {
        return false;
    }
}
