<?php

declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\ActionAbilityPartProcessorInterface;
use App\Entity\Ability\AbilityPart;
use App\Entity\Ability\AbilityPartOptions;
use App\Entity\Game\PlayerError;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Entity\Update\DialogUpdate;
use App\Entity\Update\UpdateDataInterface;
use App\Service\ChatHelper;

class DoCounterBlast implements ActionAbilityPartProcessorInterface
{
    private $chatHelper;

    public function __construct(
        ChatHelper $chatHelper
    ) {
        $this->chatHelper = $chatHelper;
    }

    public function getName(): string
    {
        return 'do_counter_blast';
    }

    public function processPlayerInput(State $state, AbilityPart $abilityPartData, PlayerInput $input): bool
    {
        $player = $state->getPlayer($abilityPartData->getPlayerHash());
        $count = $abilityPartData->getOption(AbilityPartOptions::COUNT);
        $validChoices = $this->getValidChoices($state, $abilityPartData);
        $selectedChoices = $input->getList();

        $passedChoices = [];
        foreach ($selectedChoices as $choice) {
            if (in_array($choice, $validChoices, true)) {
                $passedChoices[] = $choice;
            }
        }

        $passedCount = count($passedChoices);
        if ($passedCount != $count) {
            $state->setPlayerError(
                new PlayerError(
                    $abilityPartData->getPlayerHash(),
                    'Invalid selection'
                )
            );

            return false;
        }

        $damageCards = $player->getDamage()->getCards();
        foreach ($damageCards as $card) {
            if (in_array($card->getId(), $passedChoices, true)) {
                $card->setFaceDown(true);
            }
        }

        $state->addChatLog(
            $player->getName()
            . ' counter blasts '
            . $passedCount
            . $this->chatHelper->plural($passedCount, 'card')
        );

        return true;
    }

    public function getPrivateUpdateData(State $state, AbilityPart $abilityPartData): ?UpdateDataInterface
    {
        return null;
    }

    public function getPublicUpdateData(State $state, AbilityPart $abilityPartData): ?UpdateDataInterface
    {
        $count = $abilityPartData->getOption(AbilityPartOptions::COUNT);

        return
            (new DialogUpdate($abilityPartData->getPlayerHash()))
            ->setTitle(
                'Choose '
                . $count
                . ' face up damage '
                . $this->chatHelper->plural($count, 'card')
            )
            ->addOption(DialogUpdate::OPTION_MIN_COUNT, $count)
            ->addOption(DialogUpdate::OPTION_MAX_COUNT, $count)
        ;
    }

    public function getValidChoices(State $state, AbilityPart $abilityPartData): array
    {
        $player = $state->getPlayer($abilityPartData->getPlayerHash());
        $cards = $player->getDamage()->getCards();

        $choices = [];
        foreach ($cards as $card) {
            if (!$card->isFaceDown()) {
                $choices[] = $card->getId();
            }
        }

        return $choices;
    }

    public function progressState(State $state, AbilityPart $abilityPartData): bool
    {
        $count = $abilityPartData->getOption(AbilityPartOptions::COUNT);
        $choices = $this->getValidChoices($state, $abilityPartData);

        if (count($choices) === $count) {
            return $this->processPlayerInput(
                $state,
                $abilityPartData,
                new PlayerInput(
                    $abilityPartData->getPlayerHash(),
                    $choices
                )
            );
        }

        return false;
    }
}
