<?php

declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\ActionAbilityPartProcessorInterface;
use App\Entity\Ability\AbilityPart;
use App\Entity\Ability\AbilityPartOptions;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Entity\Update\UpdateDataInterface;

class DoPlayerProperty implements ActionAbilityPartProcessorInterface
{
    public function getName(): string
    {
        return 'do_player_property';
    }

    public function processPlayerInput(State $state, AbilityPart $abilityPart, PlayerInput $input): bool
    {
        return true;
    }

    public function getPrivateUpdateData(State $state, AbilityPart $abilityPart): ?UpdateDataInterface
    {
        return null;
    }

    public function getPublicUpdateData(State $state, AbilityPart $abilityPart): ?UpdateDataInterface
    {
        return null;
    }

    public function getValidChoices(State $state, AbilityPart $abilityPart): array
    {
        return [];
    }

    public function progressState(State $state, AbilityPart $abilityPart): bool
    {
        $playerOption = $abilityPart->getOption(AbilityPartOptions::PLAYER);
        $property = $abilityPart->getOption(AbilityPartOptions::PROPERTY);
        if ($playerOption === AbilityPartOptions::PLAYER_OPPONENT) {
            $player = $state->getOpposingPlayer($abilityPart->getPlayerHash());
        } else {
            $player = $state->getPlayer($abilityPart->getPlayerHash());
        }
        $player->addProperty($property);

        return true;
    }
}
