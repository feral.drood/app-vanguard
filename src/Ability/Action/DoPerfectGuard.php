<?php

declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\ActionAbilityPartProcessorInterface;
use App\Entity\Ability\AbilityPart;
use App\Entity\Game\Card;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Entity\Update\UpdateDataInterface;

class DoPerfectGuard implements ActionAbilityPartProcessorInterface
{
    public function getName(): string
    {
        return 'do_perfect_guard';
    }

    public function processPlayerInput(State $state, AbilityPart $AbilityPart, PlayerInput $input): bool
    {
        return true;
    }

    public function getPrivateUpdateData(State $state, AbilityPart $AbilityPart): ?UpdateDataInterface
    {
        return null;
    }

    public function getPublicUpdateData(State $state, AbilityPart $AbilityPart): ?UpdateDataInterface
    {
        return null;
    }

    public function getValidChoices(State $state, AbilityPart $AbilityPart): array
    {
        return [];
    }

    public function progressState(State $state, AbilityPart $AbilityPart): bool
    {
        $player = $state->getPlayer($AbilityPart->getPlayerHash());
        $player
            ->getField()
            ->getCircleCard($state->getDefender())
            ->addBonusProperty(Card::PROPERTY_CANNOT_BE_HIT)
        ;

        return true;
    }
}
