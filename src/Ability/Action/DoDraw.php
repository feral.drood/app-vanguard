<?php

declare(strict_types=1);

namespace App\Ability\Action;

use App\Entity\Ability\AbilityPartOptions;
use App\Ability\ActionAbilityPartProcessorInterface;
use App\Entity\Ability\AbilityPart;
use App\Entity\Game\CardEvent;
use App\Entity\Game\CardEventTypes;
use App\Entity\Game\PlayerError;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Entity\Update\DialogUpdate;
use App\Entity\Update\UpdateDataInterface;
use App\Service\ChatHelper;
use App\Service\Game\CardEventDispatcher;

class DoDraw implements ActionAbilityPartProcessorInterface
{
    private $chatHelper;
    private $eventDispatcher;

    public function __construct(
        ChatHelper $chatHelper,
        CardEventDispatcher $eventDispatcher
    ) {
        $this->chatHelper = $chatHelper;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function getName(): string
    {
        return 'do_draw';
    }

    public function processPlayerInput(State $state, AbilityPart $abilityPartData, PlayerInput $input): bool
    {
        return true;
    }

    public function getPublicUpdateData(State $state, AbilityPart $abilityPartData): ?UpdateDataInterface
    {
        return null;
    }

    public function getPrivateUpdateData(State $state, AbilityPart $abilityPartData): ?UpdateDataInterface
    {
        return null;
    }

    public function getValidChoices(State $state, AbilityPart $abilityPartData): array
    {
        return [];
    }

    public function progressState(State $state, AbilityPart $abilityPartData): bool
    {
        $player = $state->getPlayer($abilityPartData->getPlayerHash());
        $count = $abilityPartData->getOption(AbilityPartOptions::COUNT);

        $selfCard = $player->getCardById($abilityPartData->getSelfCardId());
//        $this->eventDispatcher->dispatch(
//            new CardEvent(
//                $state,
//                CardEventTypes::BEFORE_DRAW,
//                $abilityPartData->getPlayerHash(),
//                $selfCard
//            )
//        );

        $cards = $player->getDeck()->drawCards($count);
        $player->getHand()->addCards($cards);
        $state->addChatLog(
            $player->getName()
            . ' draws '
            . $count
            . ' '
            . $this->chatHelper->plural($count, 'card')
        );

        $this->eventDispatcher->dispatch(
            new CardEvent(
                $state,
                CardEventTypes::AFTER_DRAW,
                $abilityPartData->getPlayerHash(),
                $selfCard
            )
        );

        return true;
    }
}
