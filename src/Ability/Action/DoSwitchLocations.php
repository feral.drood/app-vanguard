<?php

declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\ActionAbilityPartProcessorInterface;
use App\Entity\Ability\AbilityPart;
use App\Entity\Ability\AbilityPartOptions;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Entity\Update\UpdateDataInterface;

class DoSwitchLocations implements ActionAbilityPartProcessorInterface
{
    private const LOCATIONS = 'locations';

    public function getName(): string
    {
        return 'do_switch_locations';
    }

    public function processPlayerInput(State $state, AbilityPart $abilityPartData, PlayerInput $input): bool
    {
        return true;
    }

    public function getPrivateUpdateData(State $state, AbilityPart $abilityPartData): ?UpdateDataInterface
    {
        return null;
    }

    public function getPublicUpdateData(State $state, AbilityPart $abilityPartData): ?UpdateDataInterface
    {
        return null;
    }

    public function getValidChoices(State $state, AbilityPart $abilityPartData): array
    {
        return [];
    }

    public function progressState(State $state, AbilityPart $abilityPartData): bool
    {
        $playerOption = $abilityPartData->getOption(AbilityPartOptions::PLAYER);
        $selfPlayer = $state->getPlayer($abilityPartData->getPlayerHash());
        if ($playerOption === AbilityPartOptions::PLAYER_OPPONENT) {
            $targetPlayer = $state->getOpposingPlayer($abilityPartData->getPlayerHash());
        } else {
            $targetPlayer = $state->getPlayer($abilityPartData->getPlayerHash());
        }
        $locations = $abilityPartData->getOption(self::LOCATIONS) ?? [];


        if (count($locations) === 2) {
            $unit0 = $targetPlayer->getField()->getCircleCard($locations[0]);
            $unit1 = $targetPlayer->getField()->getCircleCard($locations[1]);

            $targetPlayer->getField()->setCircleCard($locations[0], $unit1);
            $targetPlayer->getField()->setCircleCard($locations[1], $unit0);

            if ($unit0 !== null || $unit1 !== null) {
                $state->addChatLog(
                    $selfPlayer->getName()
                    . ' repositions'
                    . ($selfPlayer === $targetPlayer ? '' : ' opponent\'s')
                    . ' units within a column'
                );
            }
        }

        return true;
    }
}
