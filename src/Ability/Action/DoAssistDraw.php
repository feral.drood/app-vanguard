<?php

declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\ActionAbilityPartProcessorInterface;
use App\Entity\Ability\AbilityPart;
use App\Entity\Ability\AbilityPartOptions;
use App\Entity\Game\Card;
use App\Entity\Game\PlayerError;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Entity\Update\DialogUpdate;
use App\Entity\Update\UpdateDataInterface;
use App\Service\ChatHelper;

class DoAssistDraw implements ActionAbilityPartProcessorInterface
{
    private $chatHelper;

    public function __construct(
        ChatHelper $chatHelper
    ) {
        $this->chatHelper = $chatHelper;
    }

    public function getName(): string
    {
        return 'do_assist_draw';
    }

    public function processPlayerInput(State $state, AbilityPart $abilityPartData, PlayerInput $input): bool
    {
        $player = $state->getPlayer($abilityPartData->getPlayerHash());
        $cardId = $input->getValue();
        $validChoices = $this->getValidChoices($state, $abilityPartData);

        if ($cardId === null) {
            $state->addChatLog(
                $player->getName()
                . ' decides not to take a card'
            );
            
            return true;
        }

        if (!in_array($cardId, $validChoices, true)) {
            $state->setPlayerError(
                new PlayerError(
                    $abilityPartData->getPlayerHash(),
                    'Invalid selection'
                )
            );

            return false;
        }

        $cards = $player->getDeck()->removeCardsByIds([$cardId]);
        $state->addChatLog(
            $player->getName()
            . ' takes '
            . $this->chatHelper->generateCardLink($cards[0])
            . ' to hand'
        );
        $player->getHand()->addCards($cards);

        $state->getAbilityStack()->addEffect(
            (new AbilityPart())
                ->setType('do_discard')
                ->setPlayerHash($abilityPartData->getPlayerHash())
                ->addOption(AbilityPartOptions::COUNT, 2)
        );

        return true;
    }

    public function getPublicUpdateData(State $state, AbilityPart $abilityPartData): ?UpdateDataInterface
    {
        return (new DialogUpdate($abilityPartData->getPlayerHash()))
            ->setLoadZone('action')
        ;
    }

    public function getPrivateUpdateData(State $state, AbilityPart $abilityPartData): ?UpdateDataInterface
    {
        $player = $state->getPlayer($abilityPartData->getPlayerHash());
        $topCards = $player->getDeck()->viewCards(5);
        $cardData = [];
        foreach ($topCards as $card) {
            $cardData[$card->getId()] = $card->serializeForUpdate(Card::ZONE_VIEW);
        }

        return (new DialogUpdate($abilityPartData->getPlayerHash()))
            ->setTitle('Select G Assist card')
            ->setCards($cardData)
            ->setLoadZone(null)
            ->addOption(DialogUpdate::OPTION_MAX_COUNT, 1)
        ;
    }

    public function getValidChoices(State $state, AbilityPart $abilityPartData): array
    {
        $player = $state->getPlayer($abilityPartData->getPlayerHash());
        $topCards = $player->getDeck()->viewCards(5);
        $choices = [];
        $rideGrade = $player->getField()->getVanguard()->getGrade() + 1;

        foreach ($topCards as $card) {
            if ($card->getGrade() === $rideGrade) {
                $choices[] = $card->getId();
            }
        }

        return $choices;
    }

    public function progressState(State $state, AbilityPart $abilityPartData): bool
    {
        return false;
    }
}
