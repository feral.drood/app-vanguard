<?php

declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\ActionAbilityPartProcessorInterface;
use App\Entity\Ability\AbilityPart;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Entity\Update\UpdateDataInterface;
use App\Entity\Game\Buffer;

class DoClearBuffer implements ActionAbilityPartProcessorInterface
{
    private const ID = 'id';
    private const PART = 'part';

    public function getName(): string
    {
        return 'do_clear_buffer';
    }

    public function processPlayerInput(State $state, AbilityPart $abilityPartData, PlayerInput $input): bool
    {
        return true;
    }

    public function getPrivateUpdateData(State $state, AbilityPart $abilityPartData): ?UpdateDataInterface
    {
        return null;
    }

    public function getPublicUpdateData(State $state, AbilityPart $abilityPartData): ?UpdateDataInterface
    {
        return null;
    }

    public function getValidChoices(State $state, AbilityPart $abilityPartData): array
    {
        return [];
    }

    public function progressState(State $state, AbilityPart $abilityPartData): bool
    {
        $id = $abilityPartData->getOption(self::ID);
        $part = $abilityPartData->getOption(self::PART);

        if ($id === null) {
            $id = $abilityPartData->getSelfCardId();
        }

        $buffer = $state
            ->getPlayer(
                $abilityPartData->getPlayerHash()
            )
            ->getBuffer()
        ;

        switch ($part) {
            case Buffer::MARKED:
                $buffer->clearMarked($id);
                break;
            case Buffer::VALUE:
                $buffer->clearValue($id);
                break;
            case Buffer::ORIGIN:
                $buffer->clearOrigin($id);
                break;
            case Buffer::CARDS:
                $buffer->clearCards($id);
                break;
            default:
                $buffer->clear($id);
        }

        return true;
    }
}
