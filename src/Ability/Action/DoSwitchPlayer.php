<?php

declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\ActionAbilityPartProcessorInterface;
use App\Entity\Ability\AbilityPart;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Entity\Update\UpdateDataInterface;

class DoSwitchPlayer implements ActionAbilityPartProcessorInterface
{
    private const PREFIX = 'oppo.';

    public function getName(): string
    {
        return 'do_switch_player';
    }

    public function processPlayerInput(State $state, AbilityPart $abilityPart, PlayerInput $input): bool
    {
        return true;
    }

    public function getPrivateUpdateData(State $state, AbilityPart $abilityPart): ?UpdateDataInterface
    {
        return null;
    }

    public function getPublicUpdateData(State $state, AbilityPart $abilityPart): ?UpdateDataInterface
    {
        return null;
    }

    public function getValidChoices(State $state, AbilityPart $abilityPart): array
    {
        return [];
    }
    public function progressState(State $state, AbilityPart $abilityPart): bool
    {
        $stack = $state->getAbilityStack();
        $opponentHash = $state->getOpposingPlayerHash($abilityPart->getPlayerHash());
        $effects = $abilityPart->getOptions();
        $effects = array_reverse($effects);

        foreach ($effects as $effectData) {
            $effect = (new AbilityPart($effectData))
                ->setPlayerHash($opponentHash)
                ->setSelfCardId(self::PREFIX . $abilityPart->getSelfCardId())
                ->setEventCardId(self::PREFIX . $abilityPart->getEventCardId())
            ;
            $stack->insertEffect($effect);
        }

        return true;
    }
}
