<?php

declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\ActionAbilityPartProcessorInterface;
use App\Ability\FilterHelper;
use App\Entity\Ability\AbilityPart;
use App\Entity\Ability\AbilityPartOptions;
use App\Entity\Game\Card;
use App\Entity\Game\CardEvent;
use App\Entity\Game\CardEventTypes;
use App\Entity\Game\CardProperties;
use App\Entity\Game\PlayerError;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Entity\Update\DialogUpdate;
use App\Entity\Update\UpdateDataInterface;
use App\Exception\MisconfiguredAbilityException;
use App\Service\Game\CardEventDispatcher;

class DoMarkList implements ActionAbilityPartProcessorInterface
{
    const OPTION_TITLE = 'title';

    private $filterHelper;
    private $eventDispatcher;

    public function __construct(
        FilterHelper $filterHelper,
        CardEventDispatcher $eventDispatcher
    ) {
        $this->filterHelper = $filterHelper;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function getName(): string
    {
        return 'do_mark_list';
    }

    public function processPlayerInput(State $state, AbilityPart $abilityPartData, PlayerInput $input): bool
    {
        $validChoices = $this->getValidChoices($state, $abilityPartData);
        $selectedChoices = $input->getList();

        $passedChoices = [];
        foreach ($selectedChoices as $choice) {
            if (in_array($choice, $validChoices, true)) {
                $passedChoices[] = $choice;
            }
        }

        $passedCount = count($passedChoices);
        $count = $abilityPartData->getOption(AbilityPartOptions::COUNT);
        $minCount = $abilityPartData->getOption(AbilityPartOptions::MIN_COUNT);
        $maxCount = $abilityPartData->getOption(AbilityPartOptions::MAX_COUNT);
        if ($count !== null) {
            $minCount = $count;
            $maxCount = $count;
        }
        $minGradeSum = $abilityPartData->getOption('min_grade_sum');
        if (
            (
                $minCount !== null
                && $passedCount < $minCount
                && $passedCount !== count($validChoices)
            )
            || (
                $maxCount !== null
                && $passedCount > $maxCount
            )
            || (
                $passedCount !== count($selectedChoices)
            )
            || (
                $minGradeSum !== null
                && $minGradeSum > $this->getSelectionGradeSum($state, $abilityPartData, $passedChoices)
            )
        ) {
            $state->setPlayerError(
                new PlayerError(
                    $abilityPartData->getPlayerHash(),
                    'Invalid selection'
                )
            );

            return false;
        }

        $player = $state->getPlayer($abilityPartData->getPlayerHash());
        if ($abilityPartData->getOption(AbilityPartOptions::EVENT) !== null) {
            $this->eventDispatcher->dispatch(
                new CardEvent(
                    $state,
                    $abilityPartData->getOption(AbilityPartOptions::EVENT),
                    $abilityPartData->getPlayerHash(),
                    $player->getCardById($abilityPartData->getSelfCardId())
                )
            );
        }
        $player->getBuffer()->setMarked($abilityPartData->getSelfCardId(), $passedChoices);

        return true;
    }

    private function getSelectionGradeSum(State $state, AbilityPart $abilityPart, array $selection): int
    {
        $intent = $abilityPart->getOption(AbilityPartOptions::INTENT);
        $totalGradeSum = 0;
        $playerOption = $abilityPart->getOption(AbilityPartOptions::PLAYER);
        if ($playerOption === AbilityPartOptions::PLAYER_OPPONENT) {
            $player = $state->getOpposingPlayer($abilityPart->getPlayerHash());
        } else {
            $player = $state->getPlayer($abilityPart->getPlayerHash());
        }
        foreach ($selection as $choice) {
            $card = $player->getCardById($choice);
            if (
                $intent === AbilityPartOptions::INTENT_DISCARD
                && $card->hasProperty(CardProperties::DISCARD_AS_GRADE3)
            ) {
                $totalGradeSum += 3;
            } else {
                $totalGradeSum += $card->getGrade();
            }

        }

        return $totalGradeSum;
    }

    public function getPrivateUpdateData(State $state, AbilityPart $abilityPartData): ?UpdateDataInterface
    {
        $title = $abilityPartData->getOption(self::OPTION_TITLE);
        $count = $abilityPartData->getOption(AbilityPartOptions::COUNT);
        $minCount = $abilityPartData->getOption(AbilityPartOptions::MIN_COUNT);
        $maxCount = $abilityPartData->getOption(AbilityPartOptions::MAX_COUNT);
        if ($count !== null) {
            $minCount = $count;
            $maxCount = $count;
        }
        $output = (new DialogUpdate($abilityPartData->getPlayerHash()))
            ->setTitle($title ?? 'Please selected required cards')
            ->setLoadZone(null)
        ;
        if ($minCount !== null) {
            $output->addOption(DialogUpdate::OPTION_MIN_COUNT, $minCount);
        }
        if ($maxCount !== null) {
            $output->addOption(DialogUpdate::OPTION_MAX_COUNT, $maxCount);
        }
        if (
            $abilityPartData->getOption(AbilityPartOptions::ZONE) === AbilityPartOptions::LOCATION_DECK
            || $abilityPartData->getOption(AbilityPartOptions::ZONE) === AbilityPartOptions::LOCATION_BUFFER
        ) {
            $validChoices = $this->getValidChoices($state, $abilityPartData);
            $playerOption = $abilityPartData->getOption(AbilityPartOptions::PLAYER);
            if ($playerOption === AbilityPartOptions::PLAYER_OPPONENT) {
                $player = $state->getOpposingPlayer($abilityPartData->getPlayerHash());
            } else {
                $player = $state->getPlayer($abilityPartData->getPlayerHash());
            }

            $cards = [];
            foreach ($validChoices as $choice) {
                $card = $player->getCardById($choice);
                if ($card !== null) {
                    $cards[$card->getId()] = $card->serializeForUpdate(Card::ZONE_VIEW);
                }
            }

            $output->setCards($cards);
        }

        return $output;
    }

    public function getPublicUpdateData(State $state, AbilityPart $abilityPartData): ?UpdateDataInterface
    {
        return (new DialogUpdate($abilityPartData->getPlayerHash()))
            ->setLoadZone('action')
        ;
    }

    public function getValidChoices(State $state, AbilityPart $abilityPartData): array
    {
        $filterData = $this->filterHelper->buildFilterData($state, $abilityPartData);
        $playerOption = $abilityPartData->getOption(AbilityPartOptions::PLAYER);
        if ($playerOption === AbilityPartOptions::PLAYER_OPPONENT) {
            $player = $state->getOpposingPlayer($abilityPartData->getPlayerHash());
        } else {
            $player = $state->getPlayer($abilityPartData->getPlayerHash());
        }

        switch ($abilityPartData->getOption(AbilityPartOptions::ZONE)) {
            case AbilityPartOptions::LOCATION_HAND:
                $cards = $player->getHand()->getCards();
                break;
            case AbilityPartOptions::LOCATION_SOUL:
                $cards = $player->getSoul()->getCards();
                break;
            case AbilityPartOptions::LOCATION_DROP:
                $cards = $player->getDrop()->getCards();
                break;
            case AbilityPartOptions::LOCATION_DECK:
                $viewCount = $abilityPartData->getOption('view_count');
                if ($viewCount === null) {
                    throw new MisconfiguredAbilityException('Invalid view_count passed to do_mark_list ability');
                }
                $cards = $player->getDeck()->viewCards($viewCount);
                break;
            case AbilityPartOptions::LOCATION_DAMAGE:
                $cards = $player->getDamage()->getCards();
                break;
            case AbilityPartOptions::LOCATION_FIELD:
                $cards = array_filter($player->getField()->getCircles());
                break;
            case AbilityPartOptions::LOCATION_BUFFER:
                $cards = $player->getBuffer()->getCards($abilityPartData->getSelfCardId());
                break;
            case AbilityPartOptions::LOCATION_GUARDIAN:
                $cards = $player->getGuardians()->getCards();
                break;
            default:
                throw new MisconfiguredAbilityException('Invalid is_list_check location');
        }

        $validCardIds = [];
        foreach ($cards as $key => $card) {
            if ($abilityPartData->getOption(AbilityPartOptions::ZONE) === AbilityPartOptions::LOCATION_FIELD) {
                $cardLocation = $key;
                $cardSelection = $key;
            } else {
                $cardLocation = $player->getZoneByCardId($card->getId());
                $cardSelection = $card->getId();
            }
            if (
                $filterData->getIntent() === AbilityPartOptions::INTENT_RETIRE
                && $card->hasProperty(CardProperties::CANNOT_BE_RETIRED_BY_CARD_ABILITIES)
            ) {
                continue;
            }
            if (
                $this->filterHelper->isCommonFilterPassed($filterData, $card)
                && $this->filterHelper->isLocationFilterPassed($filterData, $cardLocation)
            ) {
                $validCardIds[] = $cardSelection;
            }
        }

        return $validCardIds;
    }

    public function progressState(State $state, AbilityPart $abilityPartData): bool
    {
        $validChoices = $this->getValidChoices($state, $abilityPartData);
        $count = $abilityPartData->getOption(AbilityPartOptions::COUNT);
        $minCount = $abilityPartData->getOption(AbilityPartOptions::MIN_COUNT);
        if ($count !== null) {
            $minCount = $count;
        }
        $validCount = count($validChoices);

        if (
            $minCount !== null
            && $validCount <= $minCount
        ) {
            return $this->processPlayerInput(
                $state,
                $abilityPartData,
                new PlayerInput($abilityPartData->getPlayerHash(), $validChoices)
            );
        }

        return false;
    }
}
