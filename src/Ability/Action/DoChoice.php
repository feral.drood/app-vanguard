<?php

declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\ActionAbilityPartProcessorInterface;
use App\Entity\Ability\AbilityPart;
use App\Entity\Game\PlayerError;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Entity\Update\DialogUpdate;
use App\Entity\Update\UpdateDataInterface;

class DoChoice implements ActionAbilityPartProcessorInterface
{
    private const ID = 'id';
    private const LABEL = 'label';
    private const EFFECTS = 'effects';

    public function getName(): string
    {
        return 'do_choice';
    }

    public function processPlayerInput(State $state, AbilityPart $abilityPartData, PlayerInput $input): bool
    {
        $choice = $input->getValue();
        $validChoices = $this->getValidChoices($state, $abilityPartData);

        if (!in_array($choice, $validChoices, true)) {
            $state->setPlayerError(
                new PlayerError(
                    $input->getPlayerHash(),
                    'Invalid selection'
                )
            );

            return false;
        }

        $options = $abilityPartData->getOptions();
        $abilityStack = $state->getAbilityStack();
        foreach ($options as $option) {
            if ($option[self::ID] === $choice) {
                $effects = array_reverse($option[self::EFFECTS]);
                foreach ($effects as $effectData) {
                    $effect = (new AbilityPart($effectData))
                        ->setPlayerHash($abilityPartData->getPlayerHash())
                        ->setSelfCardId($abilityPartData->getSelfCardId())
                        ->setEventCardId($abilityPartData->getEventCardId())
                    ;
                    $abilityStack->insertEffect($effect);
                }

                break;
            }
        }

        return true;
    }

    public function getPrivateUpdateData(State $state, AbilityPart $abilityPartData): ?UpdateDataInterface
    {
        return null;
    }

    public function getPublicUpdateData(State $state, AbilityPart $abilityPartData): ?UpdateDataInterface
    {
        $options = $abilityPartData->getOptions();
        $actions = [];
        foreach ($options as $option) {
            $actions[$option[self::ID]] = $option[self::LABEL];
        }

        return
            (new DialogUpdate($abilityPartData->getPlayerHash()))
            ->setTitle('Choose')
            ->setActions($actions)
        ;
    }

    public function getValidChoices(State $state, AbilityPart $abilityPartData): array
    {
        $options = $abilityPartData->getOptions();
        $choices = [];
        foreach ($options as $option) {
            $choices[] = $option[self::ID];
        }

        return $choices;
    }

    public function progressState(State $state, AbilityPart $abilityPartData): bool
    {
        return false;
    }
}
