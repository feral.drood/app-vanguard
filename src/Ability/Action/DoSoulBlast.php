<?php

declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\ActionAbilityPartProcessorInterface;
use App\Ability\FilterHelper;
use App\Entity\Ability\AbilityPart;
use App\Entity\Ability\AbilityPartOptions;
use App\Entity\Game\PlayerError;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Entity\Update\DialogUpdate;
use App\Entity\Update\UpdateDataInterface;
use App\Service\ChatHelper;

class DoSoulBlast implements ActionAbilityPartProcessorInterface
{
    private $chatHelper;
    private $filterHelper;

    public function __construct(
        ChatHelper $chatHelper,
        FilterHelper $filterHelper
    ) {
        $this->chatHelper = $chatHelper;
        $this->filterHelper = $filterHelper;
    }

    public function getName(): string
    {
        return 'do_soul_blast';
    }

    public function processPlayerInput(State $state, AbilityPart $abilityPartData, PlayerInput $input): bool
    {
        $player = $state->getPlayer($abilityPartData->getPlayerHash());
        $count = $abilityPartData->getOption(AbilityPartOptions::COUNT);
        $validChoices = $this->getValidChoices($state, $abilityPartData);
        $selectedChoices = $input->getList();

        $passedChoices = [];
        foreach ($selectedChoices as $choice) {
            if (in_array($choice, $validChoices, true)) {
                $passedChoices[] = $choice;
            }
        }

        $passedCount = count($passedChoices);
        if ($passedCount != $count) {
            $state->setPlayerError(
                new PlayerError(
                    $abilityPartData->getPlayerHash(),
                    'Invalid selection'
                )
            );

            return false;
        }

        $cards = $player->getSoul()->removeCardsByIds($passedChoices);
        $player->getDrop()->addCards($cards);

        $state->addChatLog(
            $player->getName()
            . ' soul blasts '
            . $passedCount
            . $this->chatHelper->plural($passedCount, 'card')
        );

        return true;
    }

    public function getPrivateUpdateData(State $state, AbilityPart $abilityPartData): ?UpdateDataInterface
    {
        return null;
    }

    public function getPublicUpdateData(State $state, AbilityPart $abilityPartData): ?UpdateDataInterface
    {
        $count = $abilityPartData->getOption(AbilityPartOptions::COUNT);

        return
            (new DialogUpdate($abilityPartData->getPlayerHash()))
            ->setTitle(
                'Choose '
                . $count
                . ' soul '
                . $this->chatHelper->plural($count, 'card')
                . ' to blast'
            )
            ->addOption(DialogUpdate::OPTION_MIN_COUNT, $count)
            ->addOption(DialogUpdate::OPTION_MAX_COUNT, $count)
        ;
    }

    public function getValidChoices(State $state, AbilityPart $abilityPartData): array
    {
        $player = $state->getPlayer($abilityPartData->getPlayerHash());
        $cards = $player->getSoul()->getCards();

        $choices = [];
        foreach ($cards as $card) {
            $choices[] = $card->getId();
        }

        return $choices;
    }

    public function progressState(State $state, AbilityPart $abilityPartData): bool
    {
        $count = $abilityPartData->getOption(AbilityPartOptions::COUNT);
        $choices = $this->getValidChoices($state, $abilityPartData);

        if (count($choices) === $count) {
            return $this->processPlayerInput(
                $state,
                $abilityPartData,
                new PlayerInput(
                    $abilityPartData->getPlayerHash(),
                    $choices
                )
            );
        }

        return false;
    }
}
