<?php

declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\ActionAbilityPartProcessorInterface;
use App\Entity\Ability\AbilityPart;
use App\Entity\Ability\AbilityPartOptions;
use App\Entity\Game\Card;
use App\Entity\Game\Hand;
use App\Entity\Game\Player;
use App\Entity\Game\PlayerError;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Entity\Update\DialogUpdate;
use App\Entity\Update\UpdateDataInterface;

/*
Protect I: Add a pseudo-card named "Protect", represented by the portrait Protect gift marker, to your hand. That pseudo-card has sentinel and the Perfect Guard ability ("[AUTO](GC):When placed, COST [discard a card from your hand], and one of your units cannot be hit until end of that battle.").
Protect II: Choose one of your rear-guard circles. For the rest of the game, units on that circle get [Power]+5000 during both players' turns, and [Shield]+10000 upon intercepting. Put a landscape Protect gift marker on that circle, under any units on that circle.
*/

class DoProtectGift implements ActionAbilityPartProcessorInterface
{
    private const PROTECT = 'protect';
    private const PROTECT1 = 'protect1';
    private const PROTECT2 = 'protect2';

    public function getName(): string
    {
        return 'do_protect_gift';
    }

    public function processPlayerInput(State $state, AbilityPart $abilityPartData, PlayerInput $input): bool
    {
        $player = $state->getPlayer($abilityPartData->getPlayerHash());
        $validChoices = $this->getValidChoices($state, $abilityPartData);
        $choice = $input->getValue();

        if (!in_array($choice, $validChoices, true)) {
            $state->setPlayerError(
                new PlayerError(
                    $input->getPlayerHash(),
                    'Invalid choice'
                )
            );

            return false;
        }

        if (
            $player->getGiftType(self::PROTECT) === null
            && $choice === self::PROTECT1
        ) {
            $player->setGiftType(self::PROTECT, $choice);
            $this->addProtectCard($player->getHand());

            return true;
        } elseif ($player->getGiftType(self::PROTECT) === null) {
            $player->setGiftType(self::PROTECT, $choice);
            $state->addUpdateMessage($this->getPublicUpdateData($state, $abilityPartData));

            return false;
        } else {
            $player->addGiftCircle(self::PROTECT, $choice);
        }

        return true;
    }

    public function getPrivateUpdateData(State $state, AbilityPart $abilityPartData): ?UpdateDataInterface
    {
        return null;
    }

    public function getPublicUpdateData(State $state, AbilityPart $abilityPartData): ?UpdateDataInterface
    {
        $player = $state->getPlayer($abilityPartData->getPlayerHash());

        if ($player->getGiftType(self::PROTECT) === null) {
            return
                (new DialogUpdate($abilityPartData->getPlayerHash()))
                ->setTitle('Choose gift type')
                ->setActions([
                    self::PROTECT1 => 'Protect I',
                    self::PROTECT2 => 'Protect II',
                ])
            ;
        }

        return
            (new DialogUpdate($abilityPartData->getPlayerHash()))
            ->setTitle('Choose rearguard circle to receive Protect gift')
            ->addOption(DialogUpdate::OPTION_MIN_COUNT, 1)
            ->addOption(DialogUpdate::OPTION_MAX_COUNT, 1)
        ;
    }

    public function getValidChoices(State $state, AbilityPart $abilityPartData): array
    {
        $player = $state->getPlayer($abilityPartData->getPlayerHash());

        if ($player->getGiftType(self::PROTECT) === null) {
            return [self::PROTECT1, self::PROTECT2];
        }

        return array_keys($player->getField()->getCircles());
    }

    private function addProtectCard(Hand $playerHand)
    {
        $playerHandCards = $playerHand->getCards();

        $latestId = 0;
        foreach ($playerHandCards as $card) {
            list($pureId, $cardCount) = explode('#', $card->getId());
            if ($pureId === 'Protect') {
                $latestId = max($latestId, (int)$cardCount);
            }
        }
        $latestId++;

        $card = (new Card())
            ->setId('Protect#' . $latestId)
            ->setName('Protect')
            ->setType(Card::TYPE_PROTECT)
            ->setShield(0)
            ->setImage('/images/gift_protect.png')
        ;
        $playerHand->addCard($card);
    }

    public function progressState(State $state, AbilityPart $abilityPartData): bool
    {
        $player = $state->getPlayer($abilityPartData->getPlayerHash());
        $giftType = $player->getGiftType(self::PROTECT);
        if (
            $giftType === null
            || $giftType === self::PROTECT2
        ) {
            return false;
        }
        $this->addProtectCard($player->getHand());

        return true;
    }
}
