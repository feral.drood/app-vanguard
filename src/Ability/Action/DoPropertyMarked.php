<?php

declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\ActionAbilityPartProcessorInterface;
use App\Entity\Ability\AbilityPart;
use App\Entity\Ability\AbilityPartOptions;
use App\Entity\Game\Field;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Entity\Update\UpdateDataInterface;
use App\Exception\MisconfiguredAbilityException;
use App\Service\ChatHelper;

class DoPropertyMarked implements ActionAbilityPartProcessorInterface
{
    private $chatHelper;

    public function __construct(
        ChatHelper $chatHelper
    ) {
        $this->chatHelper = $chatHelper;
    }

    public function getName(): string
    {
        return 'do_property_marked';
    }

    public function processPlayerInput(State $state, AbilityPart $abilityPart, PlayerInput $input): bool
    {
        return true;
    }

    public function getPrivateUpdateData(State $state, AbilityPart $abilityPart): ?UpdateDataInterface
    {
        return null;
    }

    public function getPublicUpdateData(State $state, AbilityPart $abilityPart): ?UpdateDataInterface
    {
        return null;
    }

    public function getValidChoices(State $state, AbilityPart $abilityPart): array
    {
        return [];
    }

    public function progressState(State $state, AbilityPart $abilityPart): bool
    {
        $playerOption = $abilityPart->getOption(AbilityPartOptions::PLAYER);
        $player = $state->getPlayer($abilityPart->getPlayerHash());
        $property = $abilityPart->getOption(AbilityPartOptions::PROPERTY);
        if ($playerOption === AbilityPartOptions::PLAYER_OPPONENT) {
            $targetPlayer = $state->getOpposingPlayer($abilityPart->getPlayerHash());
        } else {
            $targetPlayer = $player;
        }

        $cardIds = $player->getBuffer()->getMarked($abilityPart->getSelfCardId()) ?? [];
        if (!is_array($cardIds)) {
            $cardIds = [$cardIds];
        }

        foreach ($cardIds as $cardId) {
            $card = $targetPlayer->getCardById($cardId);
            if ($card !== null) {
                $card->addBonusProperty(
                    $property
                );
            }
        }

        return true;
    }
}
