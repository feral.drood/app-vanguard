<?php

declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\ActionAbilityPartProcessorInterface;
use App\Entity\Ability\AbilityPart;
use App\Entity\Game\CardEvent;
use App\Entity\Game\CardEventTypes;
use App\Entity\Game\Field;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Entity\Update\UpdateDataInterface;
use App\Service\Game\CardEventDispatcher;

class DoRideBuffer implements ActionAbilityPartProcessorInterface
{
    private $eventDispatcher;

    public function __construct(
        CardEventDispatcher $eventDispatcher
    ) {
        $this->eventDispatcher = $eventDispatcher;
    }

    public function getName(): string
    {
        return 'do_ride_buffer';
    }

    public function processPlayerInput(State $state, AbilityPart $abilityPart, PlayerInput $input): bool
    {
        return true;
    }

    public function getPrivateUpdateData(State $state, AbilityPart $abilityPart): ?UpdateDataInterface
    {
        return null;
    }

    public function getPublicUpdateData(State $state, AbilityPart $abilityPart): ?UpdateDataInterface
    {
        return null;
    }

    public function getValidChoices(State $state, AbilityPart $abilityPart): array
    {
        return [];
    }

    public function progressState(State $state, AbilityPart $abilityPart): bool
    {
        $player = $state->getPlayer($abilityPart->getPlayerHash());
        $cards = $player->getBuffer()->getCards($abilityPart->getSelfCardId());
        $newVanguard = array_pop($cards);

        $this->eventDispatcher->dispatch(
            new CardEvent(
                $state,
                CardEventTypes::BEFORE_RIDE,
                $player->getHash(),
                $newVanguard
            )
        );
        $vanguard = $player->getField()->getVanguard();
        if ($vanguard !== null) {
            $player->getSoul()->addCard($vanguard);
        }

        $player->getField()->setCircleCard(Field::VANGUARD, $newVanguard);
        $this->eventDispatcher->dispatch(
            new CardEvent(
                $state,
                CardEventTypes::AFTER_RIDE,
                $player->getHash(),
                $newVanguard
            )
        );

        return true;
    }
}
