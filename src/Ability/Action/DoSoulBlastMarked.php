<?php

declare(strict_types=1);

namespace App\Ability\Action;

use App\Ability\ActionAbilityPartProcessorInterface;
use App\Entity\Ability\AbilityPart;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Entity\Update\UpdateDataInterface;
use App\Service\ChatHelper;

class DoSoulBlastMarked implements ActionAbilityPartProcessorInterface
{
    private $chatHelper;

    public function __construct(
        ChatHelper $chatHelper
    ) {
        $this->chatHelper = $chatHelper;
    }

    public function getName(): string
    {
        return 'do_soul_blast_marked';
    }

    public function processPlayerInput(State $state, AbilityPart $abilityPartData, PlayerInput $input): bool
    {
        return true;
    }

    public function getPrivateUpdateData(State $state, AbilityPart $abilityPartData): ?UpdateDataInterface
    {
        return null;
    }

    public function getPublicUpdateData(State $state, AbilityPart $abilityPartData): ?UpdateDataInterface
    {
        return null;
    }

    public function getValidChoices(State $state, AbilityPart $abilityPartData): array
    {
        return [];
    }

    public function progressState(State $state, AbilityPart $abilityPartData): bool
    {
        $player = $state->getPlayer($abilityPartData->getPlayerHash());
        $marked = $player->getBuffer()->getMarked($abilityPartData->getSelfCardId());

        if (!is_array($marked)) {
            $marked = [$marked];
        }

        $cards = $player->getSoul()->removeCardsByIds($marked);
        $count = count($cards);

        $player->getDrop()->addCards($cards);

        $state->addChatLog(
            $player->getName()
            . ' soul blasts '
            . $count
            . $this->chatHelper->plural($count, 'card')
        );

        return true;
    }
}
