<?php

declare(strict_types=1);

namespace App\Ability\Action;

use App\Entity\Ability\AbilityPartOptions;
use App\Ability\ActionAbilityPartProcessorInterface;
use App\Entity\Ability\AbilityPart;
use App\Entity\Game\Card;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Entity\Update\UpdateDataInterface;
use App\Service\ChatHelper;

class DoQuickShield implements ActionAbilityPartProcessorInterface
{
    private $chatHelper;

    public function __construct(
        ChatHelper $chatHelper
    ) {
        $this->chatHelper = $chatHelper;
    }

    public function getName(): string
    {
        return 'do_quick_shield';
    }

    public function processPlayerInput(State $state, AbilityPart $abilityPartData, PlayerInput $input): bool
    {
        return true;
    }

    public function getPublicUpdateData(State $state, AbilityPart $abilityPartData): ?UpdateDataInterface
    {
        return null;
    }

    public function getPrivateUpdateData(State $state, AbilityPart $abilityPartData): ?UpdateDataInterface
    {
        return null;
    }

    public function getValidChoices(State $state, AbilityPart $abilityPartData): array
    {
        return [];
    }

    public function progressState(State $state, AbilityPart $abilityPartData): bool
    {
        $player = $state->getPlayer($abilityPartData->getPlayerHash());

        $playerHandCards = $player->getHand()->getCards();
        $latestId = 0;
        foreach ($playerHandCards as $card) {
            list($pureId, $cardCount) = explode('#', $card->getId());
            if ($pureId === 'QuickShield') {
                $latestId = max($latestId, (int)$cardCount);
            }
        }
        $latestId++;

        $card = (new Card())
            ->setId('QuickShield#' . $latestId)
            ->setType(Card::TYPE_BLITZ_ORDER)
            ->setName('Quick Shield')
            ->setGrade(0)
            ->setImage('https://vignette.wikia.nocookie.net/cardfight/images/d/d9/V-PR-0147EN_(Sample).png/revision/latest/scale-to-width/273')
        ;
        $player->getHand()->addCard($card);

        $state->addChatLog(
            $player->getName()
            . ' takes Quick Shield Ticket'
        );

        return true;
    }
}
