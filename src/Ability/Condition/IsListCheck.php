<?php

declare(strict_types=1);

namespace App\Ability\Condition;

use App\Ability\ConditionAbilityPartProcessorInterface;
use App\Ability\FilterHelper;
use App\Entity\Ability\AbilityPart;
use App\Entity\Ability\AbilityPartOptions;
use App\Entity\Game\CardProperties;
use App\Entity\Game\State;
use App\Exception\MisconfiguredAbilityException;

class IsListCheck implements ConditionAbilityPartProcessorInterface
{
    private $filterHelper;

    public function __construct(
        FilterHelper $filterHelper
    ) {
        $this->filterHelper = $filterHelper;
    }

    public function getName(): string
    {
        return 'is_list_check';
    }

    public function isPassed(State $state, AbilityPart $abilityPartData): bool
    {
        $filterData = $this->filterHelper->buildFilterData($state, $abilityPartData);
        $playerOption = $abilityPartData->getOption(AbilityPartOptions::PLAYER);
        if ($playerOption === AbilityPartOptions::PLAYER_OPPONENT) {
            $player = $state->getOpposingPlayer($abilityPartData->getPlayerHash());
        } else {
            $player = $state->getPlayer($abilityPartData->getPlayerHash());
        }

        switch ($abilityPartData->getOption(AbilityPartOptions::ZONE)) {
            case AbilityPartOptions::LOCATION_HAND:
                $cards = $player->getHand()->getCards();
                break;
            case AbilityPartOptions::LOCATION_SOUL:
                $cards = $player->getSoul()->getCards();
                break;
            case AbilityPartOptions::LOCATION_DROP:
                $cards = $player->getDrop()->getCards();
                break;
            case AbilityPartOptions::LOCATION_BIND:
                $cards = $player->getBind()->getCards();
                break;
            case AbilityPartOptions::LOCATION_DECK:
                $cards = $player->getDeck()->getCards();
                break;
            case AbilityPartOptions::LOCATION_DAMAGE:
                $cards = $player->getDamage()->getCards();
                break;
            case AbilityPartOptions::LOCATION_FIELD:
                $cards = array_filter($player->getField()->getCircles());
                break;
            case AbilityPartOptions::LOCATION_BUFFER:
                $cards = $player->getBuffer()->getCards($abilityPartData->getSelfCardId());
                break;
            case AbilityPartOptions::LOCATION_GUARDIAN:
                $cards = $player->getGuardians()->getCards();
                break;
            default:
                throw new MisconfiguredAbilityException('Invalid is_list_check location');
        }

        $filteredCards = [];
        $minGradeSum = $abilityPartData->getOption('min_grade_sum');
        $totalGrade = 0;
        foreach ($cards as $card) {
            if ($abilityPartData->getOption(AbilityPartOptions::ZONE) === AbilityPartOptions::LOCATION_FIELD) {
                $cardLocation = $player->getField()->getCircleByCardId($card->getId());
            } else {
                $cardLocation = $player->getZoneByCardId($card->getId());
            }
            if (
                $filterData->getIntent() === AbilityPartOptions::INTENT_RETIRE
                && $card->hasProperty(CardProperties::CANNOT_BE_RETIRED_BY_CARD_ABILITIES)
            ) {
                continue;
            }
            if (
                $this->filterHelper->isCommonFilterPassed($filterData, $card)
                && $this->filterHelper->isLocationFilterPassed($filterData, $cardLocation)
            ) {
                $filteredCards[] = $card;
                if ($minGradeSum !== null) {
                    if (
                        $filterData->getIntent() === AbilityPartOptions::INTENT_DISCARD
                        && $card->hasProperty(CardProperties::DISCARD_AS_GRADE3)
                    ) {
                        $totalGrade += 3;
                    } else {
                        $totalGrade += $card->getGrade();
                    }
                }
            }
        }

        $count = $abilityPartData->getOption(AbilityPartOptions::COUNT);
        $minCount = $abilityPartData->getOption(AbilityPartOptions::MIN_COUNT);
        $maxCount = $abilityPartData->getOption(AbilityPartOptions::MAX_COUNT);
        $cardCount = count($filteredCards);

        return !(
            (
                $count !== null
                && $cardCount !== $count
            )
            || (
                $minCount !== null
                && $cardCount < $minCount
            )
            || (
                $maxCount !== null
                && $cardCount > $maxCount
            )
            || (
                $minGradeSum !== null
                && $totalGrade < $minGradeSum
            )
        );
    }
}
