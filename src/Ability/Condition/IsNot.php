<?php

declare(strict_types=1);

namespace App\Ability\Condition;

use App\Ability\ConditionAbilityPartProcessorInterface;
use App\Entity\Ability\AbilityPart;
use App\Entity\Game\State;
use App\Service\AbilityPartManager;

class IsNot implements ConditionAbilityPartProcessorInterface
{
    private const OPTION_CONDITION = 'condition';

    private $abilityPartManager;

    public function __construct(
        AbilityPartManager $abilityPartManager
    ) {
        $this->abilityPartManager = $abilityPartManager;
    }

    public function getName(): string
    {
        return 'is_not';
    }

    public function isPassed(State $state, AbilityPart $abilityPartData): bool
    {
        $condition = $abilityPartData->getOption(self::OPTION_CONDITION);
        $conditionPart = (new AbilityPart($condition))
            ->setPlayerHash($abilityPartData->getPlayerHash())
            ->setEventCardId($abilityPartData->getEventCardId())
            ->setSelfCardId($abilityPartData->getSelfCardId())
        ;

        $childCondition = $this->abilityPartManager->getConditionAbilityPartProcessor($conditionPart->getType());
        return !$childCondition->isPassed($state, $conditionPart);
    }
}
