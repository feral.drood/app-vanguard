<?php

declare(strict_types=1);

namespace App\Ability\Condition;

use App\Ability\ConditionAbilityPartProcessorInterface;
use App\Entity\Ability\AbilityPart;
use App\Entity\Ability\AbilityPartOptions;
use App\Entity\Game\Field;
use App\Entity\Game\State;

class IsVanguardGiftCheck implements ConditionAbilityPartProcessorInterface
{
    public function getName(): string
    {
        return 'is_vanguard_gift_check';
    }

    public function isPassed(State $state, AbilityPart $abilityPart): bool
    {
        $player = $state->getPlayer($abilityPart->getPlayerHash());

        $count = $abilityPart->getOption(AbilityPartOptions::COUNT);
        $minCount = $abilityPart->getOption(AbilityPartOptions::MIN_COUNT);
        $maxCount = $abilityPart->getOption(AbilityPartOptions::MAX_COUNT);
        if ($count !== null) {
            $minCount = $count;
            $maxCount = $count;
        }

        $giftCount = $player->getGiftCircleCount(Field::VANGUARD);
        if (
            (
                $minCount !== null
                && $giftCount < $minCount
            )
            || (
                $maxCount !== null
                && $giftCount > $maxCount
            )
        ) {
            return false;
        }

        return true;
    }
}
