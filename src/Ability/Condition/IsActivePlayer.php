<?php

declare(strict_types=1);

namespace App\Ability\Condition;

use App\Ability\ConditionAbilityPartProcessorInterface;
use App\Entity\Ability\AbilityPart;
use App\Entity\Game\State;

class IsActivePlayer implements ConditionAbilityPartProcessorInterface
{
    public function getName(): string
    {
        return 'is_active_player';
    }

    public function isPassed(State $state, AbilityPart $abilityPartData): bool
    {
        return ($state->getActivePlayerHash() === $abilityPartData->getPlayerHash());
    }
}
