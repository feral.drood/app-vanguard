<?php

declare(strict_types=1);

namespace App\Ability\Condition;

use App\Ability\ConditionAbilityPartProcessorInterface;
use App\Ability\FilterHelper;
use App\Entity\Ability\AbilityPart;
use App\Entity\Ability\AbilityPartOptions;
use App\Entity\Game\State;
use App\Entity\Game\Field;

class IsCardCheck implements ConditionAbilityPartProcessorInterface
{
    private $filterHelper;

    public function __construct(
        FilterHelper $filterHelper
    ) {
        $this->filterHelper = $filterHelper;
    }

    public function getName(): string
    {
        return 'is_card_check';
    }

    public function isPassed(State $state, AbilityPart $abilityPartData): bool
    {
        $filterData = $this->filterHelper->buildFilterData($state, $abilityPartData);
        $playerOption = $abilityPartData->getOption(AbilityPartOptions::PLAYER);
        if ($playerOption === AbilityPartOptions::PLAYER_OPPONENT) {
            $player = $state->getOpposingPlayer($abilityPartData->getPlayerHash());
        } else {
            $player = $state->getPlayer($abilityPartData->getPlayerHash());
        }

        $card = null;
        $cardLocation = null;
        switch ($abilityPartData->getOption(AbilityPartOptions::CARD)) {
            case AbilityPartOptions::LOCATION_ATTACKER:
                if ($state->getAttacker() !== null) {
                    $card = $player->getField()->getCircleCard($state->getAttacker());
                }
                break;
            case AbilityPartOptions::LOCATION_DEFENDER:
                if ($state->getDefender() !== null) {
                    $card = $state->getInactivePlayer()->getField()->getCircleCard($state->getDefender());
                    $cardLocation =  $state->getInactivePlayer()->getField()->getCircleByCardId($card->getId());
                }
                break;
            case AbilityPartOptions::LOCATION_TRIGGER:
                $card = $player->getTrigger();
                break;
            case AbilityPartOptions::LOCATION_BOOSTER:
                if ($state->getBooster() !== null) {
                    $card = $player->getField()->getCircleCard($state->getBooster());
                }
                break;
            case AbilityPartOptions::LOCATION_VANGUARD:
                $card = $player->getField()->getVanguard();
                break;
            default:
                $card = $player->getCardById($abilityPartData->getSelfCardId());
        }
        if ($card === null) {
            return false;
        }
        if ($cardLocation === null) {
            $cardLocation = $player->getField()->getCircleByCardId($card->getId());
        }
        $zoneOption = $abilityPartData->getOption(AbilityPartOptions::ZONE);
        if (
            $zoneOption !== null
            && $zoneOption !== $player->getZoneByCardId($card->getId())
        ) {
            return false;
        }

        return (
            $this->filterHelper->isCommonFilterPassed($filterData, $card)
            && $this->filterHelper->isLocationFilterPassed($filterData, $cardLocation)
        );
    }
}
