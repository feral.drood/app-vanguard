<?php

declare(strict_types=1);

namespace App\Ability\Condition;

use App\Ability\ConditionAbilityPartProcessorInterface;
use App\Entity\Ability\AbilityPart;
use App\Entity\Game\State;

class IsAvailableUsage1 implements ConditionAbilityPartProcessorInterface
{
    public function getName(): string
    {
        return 'is_available_usage1';
    }

    public function isPassed(State $state, AbilityPart $abilityPartData): bool
    {
        return !$state->getAbilityStack()->isUsedAbility($abilityPartData->getSelfCardId() . '?1');
    }
}
