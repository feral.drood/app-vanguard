<?php

declare(strict_types=1);

namespace App\Ability\Condition;

use App\Ability\ConditionAbilityPartProcessorInterface;
use App\Entity\Ability\AbilityPart;
use App\Entity\Ability\AbilityPartOptions;
use App\Entity\Game\State;

class IsEventLogCheck implements ConditionAbilityPartProcessorInterface
{
    public function getName(): string
    {
        return 'is_event_log_check';
    }

    public function isPassed(State $state, AbilityPart $abilityPart): bool
    {
        $events = $abilityPart->getOption('events') ?? [];

        $count = $abilityPart->getOption(AbilityPartOptions::COUNT);
        $minCount = $abilityPart->getOption(AbilityPartOptions::MIN_COUNT);
        $maxCount = $abilityPart->getOption(AbilityPartOptions::MAX_COUNT);
        if ($count !== null) {
            $minCount = $count;
            $maxCount = $count;
        }

        $totalCount = 0;
        $eventLog = $state->getCardEventLog();
        foreach ($events as $event) {
            $totalCount += $eventLog->getCount($event) ?? 0;
        }

        if (
            (
                $minCount !== null
                && $totalCount < $minCount
            )
            || (
                $maxCount !== null
                && $totalCount > $maxCount
            )
        ) {
            return false;
        }

        return true;
    }
}
