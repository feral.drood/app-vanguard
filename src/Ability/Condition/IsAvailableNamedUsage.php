<?php

declare(strict_types=1);

namespace App\Ability\Condition;

use App\Ability\ConditionAbilityPartProcessorInterface;
use App\Entity\Ability\AbilityPart;
use App\Entity\Game\State;

class IsAvailableNamedUsage implements ConditionAbilityPartProcessorInterface
{
    public function getName(): string
    {
        return 'is_available_named_usage';
    }

    public function isPassed(State $state, AbilityPart $abilityPartData): bool
    {
        $card = $state->getPlayer($abilityPartData->getPlayerHash())->getCardById($abilityPartData->getSelfCardId());

        return !$state->getAbilityStack()->isUsedAbility($card->getName());
    }
}
