<?php

declare(strict_types=1);

namespace App\Ability\Condition;

use App\Ability\ConditionAbilityPartProcessorInterface;
use App\Entity\Ability\AbilityPart;
use App\Entity\Game\State;
use App\Service\AbilityPartManager;

class IsAnd implements ConditionAbilityPartProcessorInterface
{
    private const OPTION_CONDITIONS = 'conditions';
    private $abilityPartManager;

    public function __construct(
        AbilityPartManager $abilityPartManager
    ) {
        $this->abilityPartManager = $abilityPartManager;
    }

    public function getName(): string
    {
        return 'is_and';
    }

    public function isPassed(State $state, AbilityPart $abilityPartData): bool
    {
        $conditions = $abilityPartData->getOption(self::OPTION_CONDITIONS);

        $conditionsPassed = true;
        foreach ($conditions as $conditionPartData) {
            $conditionPart = (new AbilityPart($conditionPartData))
                ->setPlayerHash($abilityPartData->getPlayerHash())
                ->setEventCardId($abilityPartData->getEventCardId())
                ->setSelfCardId($abilityPartData->getSelfCardId())
            ;

            $conditionProcessor = $this->abilityPartManager->getConditionAbilityPartProcessor(
                $conditionPart->getType()
            );

            if (!$conditionProcessor->isPassed($state, $conditionPart)) {
                $conditionsPassed = false;
                break;
            }
        }

        return $conditionsPassed;
    }
}
