<?php

declare(strict_types=1);

namespace App\Listener;

use App\Entity\Ability\Ability;
use App\Entity\Ability\AbilityPart;
use App\Entity\Game\Card;
use App\Entity\Game\CardEvent;
use App\Entity\Game\CardEventTypes;
use App\Entity\Game\Player;
use App\Entity\Game\State;
use App\Service\AbilityPartManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class CardEventListener implements EventSubscriberInterface
{
    private const CHILD_EVENTS = [
        CardEventTypes::BEFORE_RIDE => [
            CardEventTypes::BEFORE_PLACE,
        ],
        CardEventTypes::AFTER_RIDE => [
            CardEventTypes::AFTER_PLACE,
        ],
        CardEventTypes::BEFORE_PLACE_FROM_HAND => [
            CardEventTypes::BEFORE_PLACE,
        ],
        CardEventTypes::AFTER_PLACE_FROM_HAND => [
            CardEventTypes::AFTER_PLACE,
        ],
        CardEventTypes::BEFORE_PLACE_FROM_DECK => [
            CardEventTypes::BEFORE_PLACE,
        ],
        CardEventTypes::AFTER_PLACE_FROM_DECK => [
            CardEventTypes::AFTER_PLACE,
        ],
        CardEventTypes::BEFORE_PLACE_FROM_SOUL => [
            CardEventTypes::BEFORE_PLACE,
        ],
        CardEventTypes::AFTER_PLACE_FROM_SOUL => [
            CardEventTypes::AFTER_PLACE,
        ],
        CardEventTypes::BEFORE_CALL_FROM_HAND => [
            CardEventTypes::BEFORE_CALL,
            CardEventTypes::BEFORE_PLACE,
            CardEventTypes::BEFORE_PLACE_FROM_HAND,
        ],
        CardEventTypes::AFTER_CALL_FROM_HAND => [
            CardEventTypes::AFTER_CALL,
            CardEventTypes::AFTER_PLACE,
            CardEventTypes::AFTER_PLACE_FROM_HAND,
        ],
        CardEventTypes::BEFORE_CALL_FROM_DECK => [
            CardEventTypes::BEFORE_CALL,
            CardEventTypes::BEFORE_PLACE,
            CardEventTypes::BEFORE_PLACE_FROM_DECK,
        ],
        CardEventTypes::AFTER_CALL_FROM_DECK => [
            CardEventTypes::AFTER_CALL,
            CardEventTypes::AFTER_PLACE,
            CardEventTypes::AFTER_PLACE_FROM_DECK,
        ],
        CardEventTypes::BEFORE_CALL_FROM_SOUL => [
            CardEventTypes::BEFORE_CALL,
            CardEventTypes::BEFORE_PLACE,
            CardEventTypes::BEFORE_PLACE_FROM_SOUL,
        ],
        CardEventTypes::AFTER_CALL_FROM_SOUL => [
            CardEventTypes::AFTER_CALL,
            CardEventTypes::AFTER_PLACE,
            CardEventTypes::AFTER_PLACE_FROM_SOUL,
        ],
    ];

    private $abilityPartManager;

    public function __construct(
        AbilityPartManager $abilityPartManager
    ) {
        $this->abilityPartManager = $abilityPartManager;
    }

    public static function getSubscribedEvents()
    {
        return [
            CardEventTypes::EVENT_PREFIX . CardEventTypes::BEFORE_RIDE => 'onCardEvent',
            CardEventTypes::EVENT_PREFIX . CardEventTypes::AFTER_RIDE => 'onCardEvent',

            CardEventTypes::EVENT_PREFIX . CardEventTypes::AFTER_CALL => 'onCardEvent',
            CardEventTypes::EVENT_PREFIX . CardEventTypes::BEFORE_CALL => 'onCardEvent',

            CardEventTypes::EVENT_PREFIX . CardEventTypes::AFTER_CALL_FROM_DECK => 'onCardEvent',
            CardEventTypes::EVENT_PREFIX . CardEventTypes::BEFORE_CALL_FROM_DECK => 'onCardEvent',

            CardEventTypes::EVENT_PREFIX . CardEventTypes::BEFORE_CALL_FROM_HAND => 'onCardEvent',
            CardEventTypes::EVENT_PREFIX . CardEventTypes::AFTER_CALL_FROM_HAND => 'onCardEvent',

            CardEventTypes::EVENT_PREFIX . CardEventTypes::BEFORE_CALL_FROM_SOUL => 'onCardEvent',
            CardEventTypes::EVENT_PREFIX . CardEventTypes::AFTER_CALL_FROM_SOUL => 'onCardEvent',

            CardEventTypes::EVENT_PREFIX . CardEventTypes::AFTER_PLACE => 'onCardEvent',
            CardEventTypes::EVENT_PREFIX . CardEventTypes::BEFORE_PLACE => 'onCardEvent',

            CardEventTypes::EVENT_PREFIX . CardEventTypes::BEFORE_PLACE_FROM_DECK => 'onCardEvent',
            CardEventTypes::EVENT_PREFIX . CardEventTypes::AFTER_PLACE_FROM_DECK => 'onCardEvent',

            CardEventTypes::EVENT_PREFIX . CardEventTypes::BEFORE_PLACE_FROM_SOUL => 'onCardEvent',
            CardEventTypes::EVENT_PREFIX . CardEventTypes::AFTER_PLACE_FROM_SOUL => 'onCardEvent',

            CardEventTypes::EVENT_PREFIX . CardEventTypes::BEFORE_PLACE_FROM_HAND => 'onCardEvent',
            CardEventTypes::EVENT_PREFIX . CardEventTypes::AFTER_PLACE_FROM_HAND => 'onCardEvent',

            CardEventTypes::EVENT_PREFIX . CardEventTypes::BEFORE_RETIRE => 'onCardEvent',
            CardEventTypes::EVENT_PREFIX . CardEventTypes::AFTER_RETIRE=> 'onCardEvent',

            CardEventTypes::EVENT_PREFIX . CardEventTypes::BEFORE_DRIVE_CHECK => 'onCardEvent',
            CardEventTypes::EVENT_PREFIX . CardEventTypes::AFTER_DRIVE_CHECK=> 'onCardEvent',

            CardEventTypes::EVENT_PREFIX . CardEventTypes::BEFORE_DAMAGE_CHECK => 'onCardEvent',
            CardEventTypes::EVENT_PREFIX . CardEventTypes::AFTER_DAMAGE_CHECK=> 'onCardEvent',

            CardEventTypes::EVENT_PREFIX . CardEventTypes::BEFORE_ATTACK => 'onCardEvent',
            CardEventTypes::EVENT_PREFIX . CardEventTypes::BEFORE_BOOST => 'onCardEvent',

            CardEventTypes::EVENT_PREFIX . CardEventTypes::AFTER_HIT => 'onCardEvent',
            CardEventTypes::EVENT_PREFIX . CardEventTypes::AFTER_MISS => 'onCardEvent',
            CardEventTypes::EVENT_PREFIX . CardEventTypes::AFTER_ATTACK => 'onCardEvent',

            CardEventTypes::EVENT_PREFIX . CardEventTypes::AFTER_LOOK => 'onCardEvent',
            CardEventTypes::EVENT_PREFIX . CardEventTypes::AFTER_REVEAL => 'onCardEvent',
            CardEventTypes::EVENT_PREFIX . CardEventTypes::AFTER_SEARCH => 'onCardEvent',

//            CardEventTypes::EVENT_PREFIX . CardEventTypes::BEFORE_DRAW => 'onCardEvent',
            CardEventTypes::EVENT_PREFIX . CardEventTypes::AFTER_DRAW => 'onCardEvent',

            CardEventTypes::EVENT_PREFIX . CardEventTypes::PHASE_BATTLE_START => 'onCardEvent',
            CardEventTypes::EVENT_PREFIX . CardEventTypes::PHASE_END_START => 'onCardEvent',
        ];
    }

    public function onCardEvent(CardEvent $event)
    {
        $state = $event->getState();
        $eventTypes = [$event->getType()];
        if (isset(self::CHILD_EVENTS[$event->getType()])) {
            $eventTypes = array_merge($eventTypes, self::CHILD_EVENTS[$event->getType()]);
        }
        $otherEventTypes = array_map(function ($element) { return $element . '_other'; }, $eventTypes);

        $state->getCardEventLog()->addEvents($eventTypes);

        foreach ($state->getPlayers() as $player) {
            $fieldCircles = $player->getField()->getCircles();
            foreach ($fieldCircles as $circleId => $circleUnit) {
                if (
                    $circleUnit !== null
                    && !$circleUnit->isDeleted()
                    && $circleUnit->hasAbilities()
                ) {
                    $this->processCardEvents(
                        $state,
                        $player,
                        $circleUnit,
                        $event,
                        $eventTypes,
                        $otherEventTypes
                    );
                }
            }
            $soulCards = $player->getSoul()->getCards();
            foreach ($soulCards as $card) {
                if ($card->hasAbilities()) {
                    $this->processCardEvents(
                        $state,
                        $player,
                        $card,
                        $event,
                        $eventTypes,
                        $otherEventTypes
                    );
                }
            }
            $dropCards = $player->getDrop()->getCards();
            foreach ($dropCards as $card) {
                if ($card->hasAbilities()) {
                    $this->processCardEvents(
                        $state,
                        $player,
                        $card,
                        $event,
                        $eventTypes,
                        $otherEventTypes
                    );
                }
            }
        }
    }

    private function processCardEvents(
        State $state,
        Player $player,
        Card $card,
        CardEvent $event,
        array $eventTypes,
        array $otherEventTypes
    ) {
        $abilities = $card->getAbilities();
        foreach ($abilities as $abilityIndex => $unpackedAbilityData) {
            $abilityData = new Ability($unpackedAbilityData);

            if (
                $abilityData->getType() === Ability::TYPE_AUTO
                && (
                    (
                        $event->getCard() === null
                        && $player->getHash() === $event->getPlayerHash()
                        && in_array($abilityData->getEvent(), $eventTypes, true)
                    )
                    || (
                        $event->getCard() === null
                        && $player->getHash() !== $event->getPlayerHash()
                        && in_array($abilityData->getEvent(), $otherEventTypes, true)
                    )
                    || (
                        $event->getCard() !== null
                        && $card->getId() === $event->getCard()->getId()
                        && $player->getHash() === $event->getPlayerHash()
                        && in_array($abilityData->getEvent(), $eventTypes, true)
                    )
                    || (
                        $event->getCard() !== null
                        && (
                            $card->getId() !== $event->getCard()->getId()
                            || $player->getHash() !== $event->getPlayerHash()
                        )
                        && in_array($abilityData->getEvent(), $otherEventTypes, true)
                    )
                )
            ) {
                $conditions = $abilityData->getConditions();

                $conditionsPassed = true;
                foreach ($conditions as $unpackedConditionPartData) {
                    $conditionPartData = (new AbilityPart($unpackedConditionPartData))
                        ->setPlayerHash($player->getHash())
                        ->setSelfCardId($card->getId())
                    ;
                    if ($event->getCard() !== null) {
                        $conditionPartData->setEventCardId($event->getCard()->getId());
                    } else {
                        $conditionPartData->setEventCardId($card->getId());
                    }

                    $conditionService = $this->abilityPartManager->getConditionAbilityPartProcessor(
                        $conditionPartData->getType()
                    );
                    $conditionsPassed = $conditionsPassed && $conditionService->isPassed(
                            $state,
                            $conditionPartData
                        );
                }

                if ($conditionsPassed) {
                    $abilityData
                        ->setId($card->getId() . '?' . $abilityIndex)
                        ->setName($card->getName() . ''. ' (AUTO)')
                        ->setSelfCardId($card->getId())
                        ->setPlayerHash($player->getHash())
                    ;
                    if ($event->getCard() !== null) {
                        $abilityData->setEventCardId($event->getCard()->getId());
                    } else {
                        $abilityData->setEventCardId($card->getId());
                    }

                    $state->getAbilityStack()->addAbility($abilityData);
                }
            }
        }

    }
}
