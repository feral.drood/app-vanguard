<?php

declare(strict_types=1);

namespace App\Listener;

use App\Entity\Ability\Ability;
use App\Entity\Ability\AbilityPart;
use App\Entity\Ability\AbilityPartOptions;
use App\Entity\Game\Card;
use App\Entity\Game\CardEvent;
use App\Entity\Game\CardEventTypes;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class DamageCheckListener implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return [
            CardEventTypes::EVENT_PREFIX . CardEventTypes::BEFORE_DAMAGE_CHECK => 'handleDamageCheck',
        ];
    }

    public function handleDamageCheck(CardEvent $cardEvent)
    {
        $state = $cardEvent->getState();
        $player = $state->getPlayer($cardEvent->getPlayerHash());

        $trigger = $player->getTrigger();
        switch ($trigger->getTrigger()) {
            case Card::TRIGGER_FRONT:
                $state->getAbilityStack()
                    ->addAbility(
                        (new Ability())
                        ->setPlayerHash($player->getHash())
                        ->setId($trigger->getId() . '?power')
                        ->setName($trigger->getName() . ' (Power)')
                        ->setEventCardId($trigger->getId())
                        ->setSelfCardId($trigger->getId())
                        ->addEffect(
                            (new AbilityPart())
                            ->setType('do_mark_list')
                            ->addOption(AbilityPartOptions::ZONE, AbilityPartOptions::LOCATION_FIELD)
                            ->addOption(AbilityPartOptions::LOCATION, AbilityPartOptions::LOCATION_FRONT_UNIT)
                            ->addOption(AbilityPartOptions::MIN_COUNT, 50)
                        )
                        ->addEffect(
                            (new AbilityPart())
                            ->setType('do_modify_buffer')
                            ->addOption(AbilityPartOptions::LENGTH, Card::BONUS_LENGTH_TURN)
                            ->addOption(Card::BONUS_POWER, 10)
                        )
                        ->addEffect(
                            (new AbilityPart())
                            ->setType('do_clear_buffer')
                        )
                    )
                ;
                break;
            case Card::TRIGGER_DRAW:
                $state->getAbilityStack()
                    ->addAbility(
                        (new Ability())
                            ->setPlayerHash($player->getHash())
                            ->setId($trigger->getId() . '?draw')
                            ->setName($trigger->getName() . ' (Draw)')
                            ->setEventCardId($trigger->getId())
                            ->setSelfCardId($trigger->getId())
                            ->addEffect(
                                (new AbilityPart())
                                ->setType('do_draw')
                                ->addOption(AbilityPartOptions::COUNT, 1)
                            )
                    )
                    ->addAbility(
                        (new Ability())
                        ->setPlayerHash($player->getHash())
                        ->setId($trigger->getId() . '?power')
                        ->setName($trigger->getName() . ' (Power)')
                        ->setEventCardId($trigger->getId())
                        ->setSelfCardId($trigger->getId())
                        ->addEffect(
                            (new AbilityPart())
                            ->setType('do_mark_list')
                            ->addOption(AbilityPartOptions::TITLE, 'Select one unit to receive bonus power')
                            ->addOption(AbilityPartOptions::ZONE, AbilityPartOptions::LOCATION_FIELD)
                            ->addOption(AbilityPartOptions::MAX_COUNT, 1)
                        )
                        ->addEffect(
                            (new AbilityPart())
                            ->setType('do_modify_buffer')
                            ->addOption(AbilityPartOptions::LENGTH, Card::BONUS_LENGTH_TURN)
                            ->addOption(Card::BONUS_POWER, 10)
                        )
                        ->addEffect(
                            (new AbilityPart())
                            ->setType('do_clear_buffer')
                        )
                    )
                ;
                break;
            case Card::TRIGGER_CRITICAL:
                $state->getAbilityStack()
                    ->addAbility(
                        (new Ability())
                        ->setPlayerHash($player->getHash())
                        ->setId($trigger->getId() . '?power')
                        ->setName($trigger->getName() . ' (Power)')
                        ->setEventCardId($trigger->getId())
                        ->setSelfCardId($trigger->getId())
                        ->addEffect(
                            (new AbilityPart())
                            ->setType('do_mark_list')
                            ->addOption(AbilityPartOptions::TITLE, 'Select one unit to receive bonus power')
                            ->addOption(AbilityPartOptions::ZONE, AbilityPartOptions::LOCATION_FIELD)
                            ->addOption(AbilityPartOptions::MAX_COUNT, 1)
                        )
                        ->addEffect(
                            (new AbilityPart())
                            ->setType('do_modify_buffer')
                            ->addOption(AbilityPartOptions::LENGTH, Card::BONUS_LENGTH_TURN)
                            ->addOption(Card::BONUS_POWER, 10)
                        )
                        ->addEffect(
                            (new AbilityPart())
                            ->setType('do_clear_buffer')
                        )
                    )
                ;
                break;
            case Card::TRIGGER_HEAL:
                $state->getAbilityStack()
                    ->addAbility(
                        (new Ability())
                        ->setPlayerHash($player->getHash())
                        ->setId($trigger->getId() . '?heal')
                        ->setName($trigger->getName() . ' (Heal)')
                        ->setEventCardId($trigger->getId())
                        ->setSelfCardId($trigger->getId())
                        ->addEffect(
                            (new AbilityPart())
                            ->setType('do_heal')
                            ->addOption(AbilityPartOptions::TITLE, 'Select one damage card to heal')
                            ->addOption(AbilityPartOptions::COUNT, 1)
                        )
                    )
                ;
                $state->getAbilityStack()
                    ->addAbility(
                        (new Ability())
                        ->setPlayerHash($player->getHash())
                        ->setId($trigger->getId() . '?power')
                        ->setName($trigger->getName() . ' (Power)')
                        ->setEventCardId($trigger->getId())
                        ->setSelfCardId($trigger->getId())
                        ->addEffect(
                            (new AbilityPart())
                                ->setType('do_mark_list')
                                ->addOption(AbilityPartOptions::TITLE, 'Select one unit to receive bonus power')
                                ->addOption(AbilityPartOptions::ZONE, AbilityPartOptions::LOCATION_FIELD)
                                ->addOption(AbilityPartOptions::MAX_COUNT, 1)
                        )
                        ->addEffect(
                            (new AbilityPart())
                                ->setType('do_modify_buffer')
                                ->addOption(AbilityPartOptions::LENGTH, Card::BONUS_LENGTH_TURN)
                                ->addOption(Card::BONUS_POWER, 10)
                        )
                        ->addEffect(
                            (new AbilityPart())
                                ->setType('do_clear_buffer')
                        )
                    )
                ;
                break;
        }
    }
}
