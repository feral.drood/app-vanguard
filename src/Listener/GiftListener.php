<?php

declare(strict_types=1);

namespace App\Listener;

use App\Entity\Ability\Ability;
use App\Entity\Ability\AbilityPart;
use App\Entity\Game\Card;
use App\Entity\Game\CardEvent;
use App\Entity\Game\CardEventTypes;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class GiftListener implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return [
            CardEventTypes::EVENT_PREFIX . CardEventTypes::AFTER_RIDE => 'handleAfterPlace',
        ];
    }

    public function handleAfterPlace(CardEvent $cardEvent)
    {
        $state = $cardEvent->getState();
        $card = $cardEvent->getCard();
        $player = $state->getPlayer($cardEvent->getPlayerHash());
        if (
            $player->getField()->getVanguard()->getId() === $card->getId()
            && $card->getGift() !== null
        ) {
            $abilityPartDataType = 'do_' . $card->getGift() . '_gift';
            $state->getAbilityStack()->addAbility(
                (new Ability())
                ->setPlayerHash($cardEvent->getPlayerHash())
                ->setType(Ability::TYPE_AUTO)
                ->setId($card->getId(). '?gift')
                ->setSelfCardId($card->getId())
                ->setEventCardId($card->getId())
                ->setName($card->getName() . ' (Gift)')
                ->addEffect(
                    (new AbilityPart())
                    ->setType($abilityPartDataType)
                )
            );
        }
    }
}
