<?php

declare(strict_types=1);

namespace App\Listener;

use App\Entity\Ability\Ability;
use App\Entity\Ability\AbilityPart;
use App\Entity\Game\Card;
use App\Entity\Game\CardEvent;
use App\Entity\Game\CardEventTypes;
use App\Entity\Game\Field;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class AhshaListener implements EventSubscriberInterface
{
    private const PREFIX = 'FlowerFairyToken';

    public static function getSubscribedEvents()
    {
        return [
            CardEventTypes::EVENT_PREFIX . CardEventTypes::AFTER_RIDE => 'handleAfterPlace',
            CardEventTypes::EVENT_PREFIX . CardEventTypes::AFTER_PLACE => 'handleAfterPlace',
            CardEventTypes::EVENT_PREFIX . CardEventTypes::AFTER_CALL => 'handleAfterPlace',
            CardEventTypes::EVENT_PREFIX . CardEventTypes::AFTER_CALL_FROM_DECK => 'handleAfterPlace',
            CardEventTypes::EVENT_PREFIX . CardEventTypes::AFTER_CALL_FROM_SOUL => 'handleAfterPlace',
            CardEventTypes::EVENT_PREFIX . CardEventTypes::AFTER_CALL_FROM_HAND => 'handleAfterPlace',
            CardEventTypes::EVENT_PREFIX . CardEventTypes::AFTER_PLACE_FROM_DECK => 'handleAfterPlace',
            CardEventTypes::EVENT_PREFIX . CardEventTypes::AFTER_PLACE_FROM_SOUL => 'handleAfterPlace',
            CardEventTypes::EVENT_PREFIX . CardEventTypes::AFTER_PLACE_FROM_HAND => 'handleAfterPlace',
        ];
    }

    public function handleAfterPlace(CardEvent $cardEvent)
    {
        $state = $cardEvent->getState();
        $player = $state->getPlayer($cardEvent->getPlayerHash());
        $circles = $player->getField()->getCircles();
        $vanguard = $player->getField()->getVanguard();
        $vanguardName = null;
        if ($vanguard !== null) {
            $vanguardName = $vanguard->getName();
        }

        foreach ($circles as $circleId => $circleUnit) {
            if ($circleUnit === null || $circleId === Field::VANGUARD) {
                continue;
            }

            if (
                $circleUnit->getType() === Card::TYPE_TOKEN_UNIT
                && substr($circleUnit->getId(), 0, 16) === self::PREFIX
            ) {
                if (strpos($vanguardName, 'Ahsha') !== false) {
                    $circleUnit->setName($vanguardName);
                } else {
                    $circleUnit->setName('Ahsha\'s Flower Fairy');
                }
            }
        }
    }
}
