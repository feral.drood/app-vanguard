<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * @param string $email
     * @return User|object|null
     */
    public function findOneByEmail(string $email): ?User
    {
        return $this->findOneBy(['email' => $email]);
    }

    /**
     * @param string $id
     * @return User|object|null
     */
    public function findOneByFacebookId(string $id): ?User
    {
        return $this->findOneBy(['facebookId' => $id]);
    }

    /**
     * @param string $id
     * @return User|object|null
     */
    public function findOneByGoogleId(string $id): ?User
    {
        return $this->findOneBy(['googleId' => $id]);
    }

    /**
     * @param string $token
     * @return User|object|null
     */
    public function findOneByToken(string $token): ?User
    {
        return $this->findOneBy(['token' => $token]);
    }
}
