<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\Game;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class GameRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Game::class);
    }

    /**
     * @return object[]|Game[]|null
     */
    public function getActiveGames()
    {
        return $this->createQueryBuilder('g')
            ->where('g.status != :done')
            ->setParameter('done', Game::STATUS_DONE)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param int $gameId
     * @return null|object|Game
     */
    public function getOneById($gameId)
    {
        return $this->findOneBy(['id' => $gameId]);
    }

    /**
     * @param string $userId
     * @return null|Game
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getOneWaiting($userId)
    {
        return $this->createQueryBuilder('g')
            ->where('g.user1Id != :user')
            ->andWhere('g.user2Id IS NULL')
            ->andWhere('g.status = :active')
            ->setParameter('user', $userId)
            ->setParameter('active', Game::STATUS_ACTIVE)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    /**
     * @param int $userId
     * @return Game|object|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getOneActiveByUserId($userId)
    {
        return $this->createQueryBuilder('g')
            ->where('(g.user1Id = :user OR g.user2Id = :user)')
            ->andWhere('(g.status = :active)')
            ->setParameter('user', $userId)
            ->setParameter('active', Game::STATUS_ACTIVE)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    /**
     * @param string $userId
     * @return null|Game
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getOneByUserId($userId)
    {
        return $this->createQueryBuilder('g')
            ->where('(g.user1Id = :user OR g.user2Id = :user)')
            ->andWhere('(g.status != :done)')
            ->setParameter('user', $userId)
            ->setParameter('done', Game::STATUS_DONE)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
