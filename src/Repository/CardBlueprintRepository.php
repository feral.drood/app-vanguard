<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\CardBlueprint;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class CardBlueprintRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CardBlueprint::class);
    }

    /**
     * @param string $cardId
     * @return CardBlueprint|null|object
     */
    public function getOneById(string $cardId): ?CardBlueprint
    {
        return $this->findOneBy(['id' => $cardId]);
    }

    /**
     * @param string $name
     * @return CardBlueprint|null|object
     */
    public function getOneByName(string $name): ?CardBlueprint
    {
        return $this->findOneBy(['name' => $name]);
    }

    /**
     * @param array $idList
     * @return CardBlueprint[]|null|object[]
     */
    public function getAllByIdList(array $idList): ?array
    {
        return $this->findBy(['id' => $idList]);
    }

    /**
     * @param string $clan
     * @return CardBlueprint[]|null|object[]
     */
    public function getAllByClan(string $clan): ?array
    {
        $cards = $this->createQueryBuilder('c')
            ->where('c.clan = :clan OR c.clan IS NULL')
            ->setParameter('clan', $clan)
            ->getQuery()
            ->getResult()
        ;

        return $cards;
    }

    public function getAllClans(): ?array
    {
        $clans = $this->createQueryBuilder('c')
            ->select('c.clan')
            ->where('c.clan IS NOT NULL')
            ->distinct()
            ->getQuery()
            ->getResult()
        ;

        return array_map('reset', $clans);
    }
}
