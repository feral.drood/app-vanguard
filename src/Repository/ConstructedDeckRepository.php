<?php

namespace App\Repository;

use App\Entity\ConstructedDeck;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ConstructedDeckRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ConstructedDeck::class);
    }

    /**
     * @param int $userId
     * @return ConstructedDeck[]
     */
    public function getAllPrivateByUserId(int $userId)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.userId = :user')
            ->setParameter('user', $userId)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param int $userId
     * @return ConstructedDeck[]
     */
    public function getAllPublicByUserId(int $userId)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.userId = :user')
            ->orWhere('d.userId IS NULL')
            ->setParameter('user', $userId)
            ->orderBy('d.userId', 'ASC')
            ->addOrderBy('d.name', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param string $name
     * @return object|null|ConstructedDeck
     */
    public function getOnePublicByName(string $name)
    {
        return $this->findOneBy(
            [
                'name' => $name,
                'userId' => null,
            ]
        );
    }

    /**
     * @param int $id
     * @return null|object|ConstructedDeck
     */
    public function getOneByid(int $id): ?ConstructedDeck
    {
        return $this->findOneBy(['id' => $id]);
    }

    /**
     * @param string $name
     * @param string $userId
     * @return null|object|ConstructedDeck
     */
    public function getOneByNameAndUserId(string $name, string $userId)
    {
        return $this->findOneBy(['name' => $name, 'userId' => $userId]);
    }
}
