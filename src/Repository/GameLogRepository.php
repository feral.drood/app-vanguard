<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\GameLog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class GameLogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GameLog::class);
    }

    public function getLatestByGameId(int $gameId): ?GameLog
    {
        return $this->findOneBy(['gameId' => $gameId], ['id' => 'DESC']);
    }
}
