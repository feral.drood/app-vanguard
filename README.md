## About
A simple application, implementing Cardfight!! Vanguard online server.
Live version is running at https://cfv.feral.lt

## Installing
cp .env.dist .env
cd docker
docker-composer up
docker exec -it vanguard-backend-php bash
Inside container run:
composer install
bin/console doctrine:schema:update --force
