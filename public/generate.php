<?php

const TYPE = 'type';
const FEAT = 'feat';
const WEAPON = 'weapon';
const SPELL = 'spell';
const ABILITY = 'ability';
const NAME = 'name';
const DESCRIPTION = 'description';
const RANGED = 'ranged';
const MELEE = 'melee';
const COST = 'cost';
const RANGE = 'range';
const DURATION = 'duration';
const UPKEEP = 'UP';
const ROUND = 'RND';
const TURN = 'TRN';
const OFFENSIVE = 'offensive';
const AOE = 'aoe';
const POWER = 'power';
const ROF = 'rof';
const STAT = 'stat';
const VALUE = 'value';
const FOCUS = 'focus';

$data = [
    '1' => [
        [
            TYPE => FEAT,
            NAME => 'BLITZ',
            DESCRIPTION => 'Caster and friendly Faction models beginning their activations in caster\'s control range can make one additional ranged or melee attack during their activations. Blitz lasts for one turn.',
        ],
        [
            TYPE => SPELL,
            NAME => 'Defender\'s Ward',
            COST => 3,
            RANGE => 6,
            DURATION => UPKEEP,
            DESCRIPTION => 'Friendly Faction model/unit gains +2 DEF & ARM. Models are not affected while out of formation.',
        ],
        [
            TYPE => SPELL,
            NAME => 'Calamity',
            COST => 3,
            RANGE => 10,
            OFFENSIVE => true,
            DURATION => UPKEEP,
            DESCRIPTION => 'Target model/unit suffers -2 DEF & ARM.',
        ],
        [
            TYPE => SPELL,
            NAME => 'Immolation',
            COST => 2,
            RANGE => 8,
            POWER => 12,
            OFFENSIVE => true,
            DESCRIPTION => 'Immolation causes fire damage. On a critical hit, the model hit suffers the Fire continuous effect.',
        ],
        [
            TYPE => SPELL,
            NAME => 'Dark Guidance',
            COST => 4,
            RANGE => 'SELF',
            AOE => 'CTRL',
            OFFENSIVE => false,
            DURATION => TURN,
            DESCRIPTION => 'While in the spellcaster\'s control range, friendly Faction models gain an additional die on their melee attack rolls this turn.',
        ],
        [
            TYPE => STAT,
            STAT => 'MAT',
            VALUE => '+2',
        ],
        [
            TYPE => FOCUS,
        ],
        [
            TYPE => ABILITY,
            NAME => 'SPRINT',
            DESCRIPTION => 'At the end of an activation in which it destroyed or removed from play one or more enemy models with melee attacks, this model can immediately make a full advance, then its activation ends.',
        ],
        [
            TYPE => WEAPON,
            NAME => 'LOLA',
            RANGE => 2,
            POWER => 8,
        ],
        [
            TYPE => WEAPON,
            NAME => 'SPELLSTORM PISTOL',
        ]
    ],
    '2' => [
        [
            TYPE => FEAT,
            NAME => 'GODSPEED',
        ],
        [
            TYPE => ABILITY,
            NAME => 'RESOURCEFUL',
        ],
        [
            TYPE => WEAPON,
            NAME => 'QUICKSILVER BLAST',
        ],
        [
            TYPE => WEAPON,
            NAME => 'PROVIDENCE',
        ],
        [
            TYPE => STAT,
            STAT => 'RAT',
            VALUE => '+2',
        ],
        [
            TYPE => STAT,
            STAT => 'MAT',
            VALUE => '+2',
        ],
        [
            TYPE => SPELL,
            NAME => 'Teleport',
        ],
        [
            TYPE => SPELL,
            NAME => 'Ashesh to Ashes',
        ],
        [
            TYPE => SPELL,
            NAME => 'Mortality',
        ],
        [
            TYPE => SPELL,
            NAME => 'Arcane Shield',
        ],
    ],
    /*
     *
     * ashes to ashes
3, 8", *, 1-, -, yes
If target model is hit, the d3 nearest enemy models within 4" of it suffer a POW 10 fire damage roll. These additional damage rolls are not considered to have been caused by an attack. Ashes to Ashes damage rolls are simultaneous.
Iron Flesh - Cost 2, RNG 6, Upkeep
Target friendly Faction warrior model/unit gains +2 ARM and does not suffer blast damage. Models are not affected while out of formation.

        [
            TYPE => SPELL,
            NAME => '',
            COST => ?,
            RANGE => ?,
            OFFENSIVE => false,
            DURATION => UPKEEP,
            DESCRIPTION => '',
        ]

W: Lola
W: Spellstorm Pistol
    */
];


/*
3
Divinity
W: Bazooka
W: Fan of Shadows
S: Engine of Destruction
S: Blow the Man Down
S: Batten Down The Hatches
S: Crippling Grasp
Feat: Invincibility
Foc +1
Def +1


4
F: Menoth's wrath
S: Ghost Walk
S: Gallows
S: Deceleration
S: Parasite
W: Rathrok (Blood boon)
True sight
W: Quad Iron
spd +1
Arm +1


5
F: Recalibration
Apparition
W: Dragon Fire
W: Frostfang
Arm +1
Str +2
S: Mage Sight
S: Hellfire
S: Curse of Shadows
S: Iron Flesh

6
F: Roulette
W: Voass (Freeze)
W: Spitfire (14 1 - 12, magical, weapon master)
Arm +1
Foc +1
S: Revive
S: Hex Blast
S: Breath Stealer
S: Roots of the earth
Ability: This model gains stealth and parry)

7/8?
S: Perseverance
S: Disintegration
S: Scourge
S: Rock Wall
Str +2
Foc +1
W: Foecleaver X
W: Oraculus
F: The Vanishing
Future Sight

7/8?
F: Boarding Ac
S: Black spot
S: Stranglehold
S: Synergy
S: Bond of Gristle & ?
Sacred Ward
Rat +2
Spd +1
W: Tritus (Weight of stone)
W: Thrown ragnok?
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Caster draft pack generator</title>
    <style>
        body {
            font-size: 10px;
            font-family: serif;
            margin: 0;
            padding: 0;
        }

        .card {
            background-color: white;
            width: 238px;
            height: 333px;
            border: 5px solid black;
            position: relative;
            float: left;
            margin: 1px 1px 1px 1px;
            overflow: hidden;
        }

        .card .header {
            background-color: white;
            position: relative;
            z-index: 2;
            margin: 5px;
            padding: 2px;
            height: 24px;
            border: 1px solid black;
            margin-bottom: 0px;
            border-radius: 20px;
        }

        .card .header .number {
            margin-left: 2px;
            margin-top: 1px;
            border: 1px solid black;
            border-radius: 16px;
            float: left;
            position: absolute;
            width: 20px;
            height: 20px;
            font-size: 16px;
            text-align: center;
            font-weight: bold;
        }

        .card .header .title {
            text-align: center;
            font-size: 14px;
            font-weight: bold;
        }

        .card .header .subtitle {
            text-align: center;
            font-size: 10px
        }

        .card .body {
            margin: 5px;
            padding: 2px;
            padding-top: 16px;
            border: 1px solid black;
            margin-bottom: 0px;
            border-radius: 20px;
            height: 80%;
            display: block;
        }

        .card .body .text {
            /*margin: 5px;*/
            font-size: 10px;
        }

        .card .body .feat {
            background-color: black;
            color: white;
            text-align: center;
            font-weight: bold;
            font-size: 12px;
        }
        .card .footer {
            position: absolute;
            bottom: 10px;
            right: 10px;
        }

        .card .footer .life {
            border: 1px solid black;
            border-left: 0;
            width: 8px;
            height: 8px;
        }

        .card .footer .last {
            border-left: 1px solid black;
        }

        .left {
            float: left;
        }

        .center {
            margin: auto;
        }

        .right {
            float: right;
        }

        .text-center {
            text-align: center;
        }

        .face {
            z-index: 1;
            position: absolute;
            border: 2px solid black;
            left: -20px;
            top: 80px;
            width: 160px;
            height: 160px;
            border-radius: 100px;
        }

        .focus-increase {
            position: relative;
            margin: auto;
            width: 160px;
            height: 40px;
        }

        .focus {
            position: absolute;
            bottom: -16px;
            left: 64px;
            z-index: 2;
            border: 2px solid black;
            border-radius: 20px;
            width: 32px;
            height: 32px;
            background-color: white;
        }

        .focus-value {
            text-align: center;
            margin: auto;
            margin-top: 6px;
            font-size: 18px;
            font-weight: bold;
        }

        .profile {
            position: relative;
            float: right;
            margin: 5px;
            width: 174px;
            height: 50px;
            border: 1px solid black;
            border-radius: 10px;
            z-index: 2;
            background-color: white;
        }

        .stat {
            margin-top: 10px;
            width: 30px;
            height: 50px;
            border: 1px solid black;
            border-radius: 10px;
            background-color: white;
        }

        .weapon {
            position: relative;
            margin: 5px;
            width: 134px;
            height: 50px;
            border: 1px solid black;
            border-radius: 10px;
            background-color: white;
            z-index: 2;
        }

        .weapon .type {
            border: 2px solid black;
            width: 30px;
            height: 30px;
            position: absolute;
            left: 4px;
            top: 14px;
            z-index: 3;
            background-color: black;
            border-radius: 20px;
        }

        .column {
            /*float: left;*/
            margin-left: 2px;
            width: 22px;
            height: 10px;
        }

        .column-spell {
            margin-left: 2px;
            width: 35px;
            height: 10px;
        }

        .name {
            margin-left: 5px;
            margin-right: 5px;
            font-weight: bold;
            height: 12px;
        }

        .header-row {
            background-color: black;
            color: white;
            height: 10px;
            text-align: center;
            font-size: 8px;
            font-weight: bold;
        }

        .value-row {
            height: 10px;
            text-align: center;
            font-weight: bold;
        }

        .icon {
            border: 2px solid black; height: 16px; width: 16px; border-radius: 10px; float: right;
            background-color: white;
            font-size: 10px;
            text-align: center;
        }

        .weapon .ranged {
            background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAA2vAAANrwFCv+cqAAAAB3RJTUUH3wMeEzcig92i3wAAAAh0RVh0Q29tbWVudAD2zJa/AAAS7klEQVR42u2de1BUdf/H3+eyy4LcHx+1BZSLiiQCik5COqFpNmMoOpm3JmkG0SYsc7wM1Rg2GV0s8SlrxmSehxpLs1TKFLroegGtYREVEfwlrBgsyEVgue7lnN8fewncc3b37AUW4zOzszP7PXvO2e9rP+/P9/L5fg8wYiM2YvxGjFSB3uLj430AhACYBuBpAKGGIgWAAgDXAdyVy+WqESCuhxELYLMBgn+/dwBoM0AxvucBkLsKDDECIz4WQBaApH4Q+MwIpQxAjlwuv+rs+6FGYNgMAwAkAMYZPChUKpU2SqXSFqVSqR4BMvgwuMDMBjBdKpUqlEpl4z9CspZlJoUCiOv38je8+1n5artBWtoM72UAyo5nyxSGAP4fACl2wOCSMRmALGdIGOGmAJIMlZVkQ8ULtfbuRm15683eEEbNjuc6YP78+Xj22WfBsiwKCgrw448/DhoUwk0g+ANINbxiXXktRsvifmUfeu5pwWhZs/KVK1di/fr18PfXO05XVxdKS0uRl5eHsrIyl0MhhhhEnKG5uW6wrtnbqkNbVS/UKsasbNKkSTh06BBIkjQr0+l0yM3NxYEDB6xBOQHgFXubxfQQgUgyBNQnBvO6jJZFV70G2h6Ws3zhwoUmGA0NDVAqlYiNjQVJkqAoCunp6YiMjERmZibUas6GlTG+xRu8xb1bWcsyk+Ki5oYeBvBWv57woFlfG4Pueg20veZAaJrGtm3bTFL19ddfIysrC1euXEF0dLTp89DQUMTExOC3336DVqvlbYFJpdJKe1pe1CCB8I+aG/oegP8OBQgAUKsYdCjU0KgYsOZqhRdffBELFiwAQehVXC6XQy6Xo76+HqdPn8b48eMRFhYGAAgKCsK0adNQWFgIhmF4+yr2QKEGAUaKQVcXDVWsUqsYtN/uQ1+rjjOQT5gwAbt37wZN/63gNTU1uHjxov77ajV++eUXSKVSTJo0CQRBICgoCEFBQTh79qylfoq/VCo9I6TjSA2CV+x1QlvfobjRfluN3mZuGACwZ88eBAcHD/hMo9Hghx9+GPCZTCZDcHAwJk+ebGoEEAQBuVzOBwUArimVSsWQAjH0JQoALB3qJrWluAEAq1evxrJly0xSZTRfX1/k5+ejp6dnwOfnzp1DdHQ0QkJCAADTp09HRUUF7t6965R4QrkARhyAywAmDDUMa3Fj4sSJeP/990FRFGeQr6ysxO3bt83Kzp8/jwULFsDPzw8EQSAhIQEFBQXo6upyWLooJ8NIBXDYBb1rp8cNkUiEzz77DKNHj+buoBEERCIRCgsLzco0Gg1KS0vxzDPPgKZpSCQSRERE4NSpUw5LF+VkGP/tdwNuCwMAduzYgccff9zieaRSKU6dOoXOzk6zstbWVty/fx9z5841BfnGxkZUVVXxQZHY4iWUk2FgOMBISUnBhg0bzOKGWeVQFLq7u1FSUsJZXllZiZiYGISEhIAgCMyYMQMnT540iztCvIT6p8GIjY3ljRtcFhYWhmPHjkGj0XCWX7lyBUuWLIFYLIaHhwcCAgIgk8ns9hLKQRhxhpgx5DKlU7Nou2UZxtixY7F//354e3vbfF5PT090dnbi6lXu8cLOzk50d3eb5G/ixIm4dOkS7t27Z5eXkA42bWXuEsBbK3otwvDy8sK+fft4g7glW7t2rUWIR48eNbXGSJLEpk2b+A4NBbDOMB/jPA8xDJcXuEvT1ppMURSFffv2Ydq0aXZdw9PTE2KxGJcuXeI9pr6+Hk8//TQIgsC4ceNQUlKChoYGwV5ir4dkwcXzFs6CAQDZ2dmYNWuWQ9dasWIFwsPDecuLi4tx5coVk5ekpaXZ5SWkHd6RAuDV4QLj7bffxrx58xy+nkgkwuuvv27xmNzcXLCs/l5mzZqFyMhIrsOMaUYhDkuWQapODOXYlBAYO3fuxOLFi602b221sWPHoqOjAzdu3OAsr6urw5w5czBmzBjTNY0DlA9YG4D/UyqVNxz1kKyhjBuMlkVvq84qDJqmsXv3biQnJzsNhrH3npGRgfHjx/Me8+2335q85KmnnoKPjw+fbD3NJVukAO+IG0qpUqsY3K/sQ1uV5daUWCzG3r17sWjRIqfC6B/g33vvPYhEIs7yM2fO4P79+wAAb29vxMfHC5ItIR6SM9QS1XNPC7WK4YXh7++PTz/9FAkJCS69n8mTJ/PGk56eHhQVFZk8KjExke80/tDnEQuPIYY58LeGQqL62vQjttbiRUREBD755BNERUUNyr1NnjwZarWas8NIkiQWLlwIgiBA0zROnTrFNd0rAaBVKpX59nhIlrtKFAAkJCTg4MGDCA0NHbT7M8aT5ORks7Lr16+jr6/P1BDg6f8YZUuYZBlixxOD6RXGwG1NogBg1apVyMnJ4QueLofy5ptvmkFpbm6GUqkEAIwaNUpQH8iWNKDNg+kVqjtqaFQ6aHtYiyDEYjF27NiBJUuWuCR422oURWHnzp0IDAxEXl6e6fO//voLYWFhoCgKERERNp+PtKHfsc7dvCI8PBwHDx7E0qVLhxTGg/L14YcfmubmJRL9KAnLsrwjxfZ4SKo7eQUAJCcnY9u2bfDy8oI7GUEQmDdvHmbPno3S0lJER0cDALRaLe98ilsBYbQs1B0MVLXWW1DGNv1rr7025BJlSz+l/0xkdXU1Tp8+zddbV9gMxDC8HusOXpGYmIjt27ebpeq4u7W1teGDDz7gnALG32sXbfaQpKH2Cg8PD2zatAnPPfccZwL0UJhxWESj0aClpQXt7e1obGxEc3MzmpubodVqQVEUOjs7cfbsWTQ28mb/tEG/kNRmICnOBtFVr7HZK2JjY5GZmYmJEycOKQCGYdDZ2QmFQoGqqircvn0bCoUCdXV1aG5uFhSwTfGGIjSsjlUAuDvoHiJUnjw8PJCWloYXXnjB5nlvZ3tAW1sbbty4AblcjvLyclRXV6O9vd0p5ydpAh4BFHqatDlcSxZoC/HDbzDlCQAee+wxbNmyRVC73VleUFtbi+LiYhQVFaGiogIqlfNXPZM0AY9ACn4RHqJ/T/ds58pA5fOQOK4K1vWy0HQy6GnWQterTwWkJCQ8R9MQeZOgJARImhDsFb6+vsjIyEBKSsqgxQqWZVFXV4czZ85AJpOhvLycK5PdFTAg9iGNdaywC4ixgnW9DBgNO6CSSZqBRqUDKSJASUh4jaXRWaex2SsWLFiAV199FY888siggOju7saFCxdQWFiIy5cv8y28cRoE2pMw1Y3PBLERhrGOTwgGYm12jtGyUKv+htN3XwdGY90rQkJCsGnTJiQlJbncK1iWxZ07d3Dy5EkUFBRwJR84XOGcwyoc6mFJhSwB8TdWtuqO7TGA0VoHQRAE1qxZg/Xr1wvKj7I3Nly9ehXffPMNzp07B51O59SKH1DhHvxgLNWxIA/R9bJ6mbIBhi0WGxuLzZs3Izo62qW9bYZhUFRUhMOHD+P33393GICDFQ9b47QlIH4AoOnUxwxHTSqVIjU11eVBm2EYXLhwAV999ZW1JcxW9d4FADjrWNBYVk+zlnPFqpeXF/z9/SEWi036zGerVq3CSy+9hFGjRrkURHFxMXJzc3H9+nXHIZjr/aCZRSB8cjV37lxkZWWZBs/Wrl3L+f3t27djxYoVLpMnlmVx7do15Obmori4WDAIkQ/lFhBsBsJnSqUSNE2DIAiEhYVhwoQJZl7y8ssvuwyG0Stzc3P5RlKtesMoqQhiX9ItINgMhJKQIGlzL6moqEBdXR2Cg4MhFovx1ltvYePGjaY2/cyZM5GamuoSGO3t7fjyyy9x5MgR9Pb2Dmtv4LxfS4Weo2nQnuY3rtVq8fnnn5tGPmNiYvDFF1+YMj4yMjKcDoNhGJw8eRJr1qxBXl6eVRgkTUDsQ8JzDA3/SAkCpnjAa5weiLvCsOohIm+St4VRWFiI+Ph40wrWqVOnmjZomTp1qlPlqaKiAvv378cff/whyCPcVZYckCyCV7YA4N1330Vvby9WrVoFkiRBkiRmzJjh1Bv89ddfkZmZ+dCDsCZZ7cYf6TNBDI9AivfHffzxx9i+fTsqKytdMi4UFRUFT09PmwbujNIksXC/bmTtQoCYelViHxK+YWLOWGI0mUyG559/HpcvX3b6XQcFBWHu3LlW44RfxLABYVbHtgBpE3JmiqKwa9cuzJkzx+F40dXVhaNHj5pWKxmzOThvXkTA8980AiI9+o+iDhdrExJDymDYFkOtYtBRo+bdY4qiKOzdu9dSUrFNIGpra5Gfn4/Tp0+jqakJiYmJmD17NgiCQGRkJEiSNJuvYDQstL2sq4Y2hsRDLAGxabR3165dDsFobGzEoUOH8P3335vyYY2fsywLgiAQEBAAb29vdHR0DASiZU0TZf8IIOoO/eQTH4z09HQsWmTfrksMw+DIkSM4cOAA53SpVqs19WWMYB4ysz2GHM+WKRgt22FpO7yZM2ciLS3NropqampCRkYGPvroI9656/5zJRqNZoD39A/qlIQcjjDaj2fLFIJ66r0tuut83uHh4YE33njDrqF0hUKBtLQ0q5282NhYE+z6+nrOnjntScBzND0cgcgEdQzj4+N9Oqr7WD7vWL16tWm/KCHW0NCADRs2oKWlxeqxS5f+vdVWaWkpbytL5D0sPeSE0J56iKabpVmdORA/Pz+kpqYKvgOVSoWtW7faBGP9+vWmNeE6nQ75+fm8ckVJiIfKQ/j+XtNYHcu5f8ny5cvtmgs/f/48KisrrR63bNkypKenm+Tqp59+Qm1tLa9cDcPhkat88cOSh/R/oMnf9EgSS5Yssesu5s+fD4Zh8N1333Gu846IiMC6detM21MAwL1795CTk/OwydX/LBXSHPEjEAMfaGKymJgYBAUF2XUXnp6eSE5OxuLFi1FTU4M///wTTU1N8PX1RVhYGKZOnTqgkdDa2opXXnnFrO/xEMiVMCDQ7xHIaQkJCXa1rDo6OuDj4wOCIECSJCIiInjTRVmWRVlZGbKyslBXV8d9054ERklFw1Gu8o5ny9qEApkGnpyhmJgYu4ZF3nnnHdA0jZUrVyI6OhokSZr1XxiGQXV1NY4ePYpjx46ZJr+4vEPkQ0HsOyzlyupaf9rW+BEcHGxXU9foISUlJfj5558RHh6OKVOmIDw8HP7+/uju7kZtbS0qKipQUVFh8TzGYXafCeLh6B3njmfLyuwBwhk/4uLiTPufCzGCIAakAFVXV6O6ulrweTiSlYebZdn0O209W1hY2ICtuIXYmDFjHPolDwGMc8ezZTKnA7F3AY0jc+wPAQxAwFp/m//yxjwse2zGjBmgaZrv8Q68IIxz5A+k8Q8322dL7BDsIY7k5I4bN860btsaBLEPCUkgNSB9ZxjDuAOB+8TY7CEdHR12z0uQJImUlBTeBOjhlswmRKqs9Tvs9pCamhq711cAwJNPPsm7RSsp0me3/CtaMiyS2QRI1QnBf15bD7x165ZNqZuWhk7WrFnDWcZo9HudOGsdihvYVdi5pRUXEAU4MiLKy8tNW9fZa8uXL0dgYKA5EC0LjUoHdQfzMMBoB5AiVKosASkAx+rQlpYWi+tAbDFvb2+kp6dzlml79E9QG+Ze0g4gydLwuj1AroMnZ8i4UbAjlpKSwrkhsTGDRNc7rIFsFtLEtRUI7xLVkpISh9dy0zSNjRs38sYSTeewla0Xj2fL/ufoScyAyOXyVr44cvPmTb5nLQmyJ554AlKplFO2epq1/1gYllpZnHGEYRibVyxZMoqiMGXKFF7ZGmYxw2kwLAHhjSP5+fkONX8B/RxJa2vrw9CaSnImDEtA7vLJVlNTEw4ePOjQRWtqalBeXs7ZYx8miW9XAcQ5GsA51YPrQ6VSqZZKpe0AZoNjSresrAxhYWEIDw8XPJTS2dmJrVu3cj2BBqJRJLyDxO6evLAPQOrxbFmDK07OO54ulUpbAEyHfsLKLCXozJkzCAgIQFRUlE1QWJbFzZs3sWXLFty6dYvzGJG3fnUsJXbLYZM7hniRU3lR0euqi/ACMXiJwgBkHBeUoqIilJaWQiKRwM/PD2KxeMCosFarRVtbG0pLS5Gbm4s9e/bwxg6SJiD2o+A1jgZBEu7oFatcIVEPmtVfHh8fnwT982zjLB336KOPIjExETExMfD29oZKpUJVVRUuXryIa9euWQ5k7jsJdc4ZnT1nA/EB8B/o92B0+oNc3BTGOQBZtk67DioQA5RY6Ecvk5wKhYDKw4+6HxAlGe8mMPIA5AymR9gcQx6IJ41SqbSyHwwJHH92YRuAs7pedkPgo5JMAI0AHoGFRD0XNmHfB7D6eLbscOVFRcNQ/iMERU+DfMVDvx98HHhShqxAUPR7z5HL5QMewGHYgDPJIJFJcP5zEtuhzz4/AUDmyMjskAPhAWOEwgfnQQgFhpGAu1zbpD5oBkBx/V7+hnc/Gyq+zHDdMuPL3QA4BcgDYEKgTz/lzHi0B8KIjdiIjRi3/T92pdQV94oxxQAAAABJRU5ErkJggg==);
            background-size: 100% 100%;
            background-position: 0px 0px;
            background-repeat: no-repeat;
            display: inline-block;
        }

        .weapon .melee {
            background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAA2vAAANrwFCv+cqAAAAB3RJTUUH3wMeEzUOgzOsvgAAABl0RVh0Q29tbWVudABDcmVhdGVkIHdpdGggR0lNUFeBDhcAABmPSURBVHja7Z17UFNn3se/5yQBAgk3ASVykWprtdvKrtXZLVtFWstr7cgt3O8XYe2Vzs47u/2P97/tTMdq1U5VUHGcaV/rKGJtacsqha6rnWYr9kW5ShAJ4SIkASEhOee8f5CcTUhOcoII4X39zTAZ48k5J88nv+vze55D4Im4lM2bN0sBRAJ4HsB/AFjj4DAlgAYAvwLoVygUE/O5FvFkuF3C2ASg0gwh0Op1rmjMUCyvBxQKRasDoM8CaOcC9wSIaxhVAOI5IHCJBkATgA/N/66wAhlo/n87cMsGyJWqHWsAxFr9BZpfA1x8VAvgpvnL37T8JVRdVT5GGHM1Bi60qglAlQUK4cEA4gEkm18DFvgSWvNA1AFomgtoAWC4C64OwLsKhWKC8CAIgQCKzH+bFvnyrQBOATj1n5d00YsIwyJNAN5SKBS3hR4AItbsNAuX8DY2AfhYpaM+jg4S3OvXUEE0A6mjA6Ojo5Gfn48XXngBAoEA4+PjaG5uxldffYWxsbH5Xj/Q7PCXDsiVqh3x5l/idk/QUJWOwvcdBgxP0lE04/gYqVSKpKQkvPzyy/D19QVJkvDx8QFJkjCZTLyuIxAIQFHU3LfXmMPp/xYukUYc8BQQ1jB6HlCYNjqmERwcjPLyciQlJUEkErHve3l5QaVSYWpqyuV1Vq9ejbi4OJw9e9aRhqwBAOEiggg0a8R7nhRA8IERHh6OsrIyvPHGGxAIBOz7DMNgbGwMvb29LjUkJCQE+/btw/bt2x0BYUW4SDCSzVoRvdxgBAYGOoRhEZ1Oh8nJSafXEYlEyM7Oxs6dOx2eY9GAeKpW8IUhlUpRWlrKCQMAhoeHodfrOa8jFouRk5ODjIwMlzAeKxBzLlG3BCHsgsGoqKhAWloa50BOTU1BqVRyAgkMDMS+ffuwe/dueHt787o34WOCEWuOrQOWI4ygoCCUlJQgNTXVxoHPlcnJSQwMDDj0H2KxGJWVlXj99ddBkiTv+yMfA4yi5QzD29sbBQUFeOONN+Dl5eUyhNVoNHZhLEmSKCkpcRvGgmuIGcZJTyzH8IHh5+eH/Px8pKamws/PzyaaYpjZz5hMJuh0OhgMBrS2tuLu3bugaZo9NjQ0FNnZ2UhPT3cbxoICWe4wpFIpioqKkJGRAbFYDKPRCK1WC51Oh6mpKUxPT2NkZAT37t3DyMgIJicn0d3djaGhIZssvqKiAjt27HBq6h47kOUOIygoCMXFxUhJSYFYLMbMzAxaW1tx48YNdHV1YWhoCBqNBjqdDkajkdWauXlGWVkZXnnlFV7R1GMDYpV5L1szVVhYiKSkJIjFYgDA9PQ0vvrqKzQ3N0Ov18NkMtkBsBZfX1/k5uZi+/bt8zJTCwbEHNouWwculUrxpz/9CXK5nP1VMwyDyclJdHV1YWLC9SxseHg4ioqKkJiYCLFYDIIglgaIOemrW86hbVlZmQ0Mi4yOjjpN9iwSFhaGd955Bzt37uQNwjoAWGgNqVquSZ+/vz/Ky8uRmppqB4OmaQwNDbkEIhKJkJeXh1dffZU3DK1Wizt37iw8EHNtalmWQ3x9fVFQUICkpCSHztdgMEClUrHO2+GgCYXIyclBSkoKb59hMBjw5Zdfor6+3tF/s9O9wnmaqgPLEYa3tzeKi4uRnZ3NmfRNTU3h3r17nBoilUpRWFiI9PR0NghwJRRF4dy5c6itrcX09LSjQ5SYbSGal4ZUYRlWbSUSCfLz85GVleW0rjQ1NYWBgQHMzMw4jMjefvttJCUlQSjkN3RGoxHnzp3D8ePHuWBYNORXt0sn5hB32c1nCAQC5ObmIjk52emvmmEYkCSJiYkJu3IIQRDIysrCnj17eMOgKAr19fU4efKksxK9xVz1z0dDDiw3GD4+PsjLy0N2djYkEonTqIeiKHR2dmJkZMTO1KWnpyMrK4t3Bm4ymXD27FlUV1dDp9M5g9GE2d6sCbeAmOfAty83GPn5+cjLy4Ofnx8YhoHJZIJGo8Hk5CQePnyIiYkJjI6OYmxsDOPj47h16xa0Wq1NBp6Tk4PU1FSHQB2JXq/HuXPnUF1d7UozmmDVk+WuhlQtt6TP4jN8fX1hNBrR3d2NO3fuoLe3F8PDwxgZGcHAwAB0Oh1bQrfOyAMCApCTk4OcnBzeZmpiYgIXLlxAdXW1s3l2hzB4AzH7ju3LSTNyc3Mhl8vh6+vLDlRtbS2uXbsGiqJAURQYhgFN0w7LIiKRCK+//rpbPmN6ehr19fU4c+YMJwySwATNOIbhjoZULqc8o6ysDNnZ2ay9ZxgGIyMj6Orq4tUdIhaLkZGRgYyMDAQEBPCGcfbsWdTU1HBeQywiECYhx/vGKYcweAEx5x2FywGGn58fSktL7UwMTdNQKpW8eqf8/PxQXl6O5ORkmzkRPknfyZMnncJYu0KAneu9o2T+gr4EheNz8Ql7i5aLmSooKEBWVpadidHr9RgcHHRZDiEIAjk5OcjMzOQNY2ZmBvX19Th16hSnA7eCAZm/wOmYejwQvhm4ZSAdZeAmk8lldwgAJCcnIysri/d8BkVRuHz5Mk6cOMEZ2jqAMX8g5vL6Jk+G4eXlhdzcXBQVFXGGpQaDAf39/ZyZslgsRn5+Pt58800EBATwKhZSFIWLFy/i8OHDdnmLCxgAsMk8tm5rSPxygFFQUMCZgVvmN9RqtcPSt0Qiwb59+1BeXo7AQH7N7jRNo76+HkeOHLHJWXjCcDq2roAkeyoMgiAgl8uRnp4OiUTi9Fc9NTXl0L4TBIHMzEy2UOhKMyxhckNDA2pqah4FBufYCj1NQ/jAEIlEyMrKQk5ODkJDQ10O4ODgoF30YzmHu+WQ+vp6nDhxwqa5YR4wOMeWcOE/ej0xmsrMzERBQQGbI1jadGZmZqDVaqHRaDA2Ngaj0YiRkRE0NjZCoVCwJmvlypVsoZBvnmEwGHDhwgUcP378UTXDWmLmrt5ypiGxnhhNZWZmori4GBKJBEajESqVCiqVCmNjYxgaGoJarcbAwADUajUmJyeh1Wpt8o/Q0FAUFxcjNTWV9+SSpenh6NGjnPPs84BhGWPPA8LXgWdmZiInJwcSiQQMw2B0dBRnzpxBY2MjZmZmYDKZHC2GsTlHSkoKdu/eDYIgwDCMS78xMzODhoYGnDhxYqFhWMa4zqOAuNPemZ+fz9amKIpCe3s7/vWvf/HqDvH19UVOTo5bM30GgwF1dXU4fvw4NBrNQsNwOMbOgAR6imYUFhaioKAAPj4+Ng62p6eHc6Csxd/fn+1CtwDl48Dr6upw9OhRd5M+dyTQHSC8NcRgYqCZZqCeoNA+PGuvnw0TYpVUgEAxAW8hMS8YQqEQmZmZyM/Pt5t2pSiKV3eIQCBAcXEx0tLSQJIkLzNFURS++eYb1NTUPE4YbmtIAN9fecvdGYxPMZg2Mhifno1kVFoaYhGBIF8CLz/lZXPTfGFkZGQgNzfXoYkxmUwYHR2FwWBwen/p6emQy+UsDD4Z+HfffYeamhrOVbULBMPhGM+7L8tgYnBfS+HHuzMOB3baOOtcxToCeiPD3jzfPEMul2Pv3r2QSqUO8wudTofh4WHO+/P29oZcLkdJSQkLlI9mfPvttzh06NB8yiELIsL52v6WuzNQaWmMT9OcAzsLhkHPAwroMGBrlBd+ujfjMgNPTU1FaWmpQxgWGRwc5PQf/v7+KCsrQ3JyMm+fQdM0GhsbcfDgQTx48GBJYMwLCJ9fOBeUAZ0eeiPj9HMpKSnIy8tzmbANDAzg4cOHDn1Gbm4u0tPTIRKJePkMmqbR1NSETz/9dElhuA3EYGLQcnfGLRjWUJx9hiAIpKenIzc3F+Hh4S5rShqNxq53ysvLC+np6UhLS+NdDqEois0zBgYGlhSG20A00wzrvLnE4jz5OFBrn5GWlobS0lIEBQXZ/XppmsbDhw8xNjYGk8kEpVKJa9eu2QAJCgqCXC6HXC63qdo60w6j0YhLly7h2LFjGB0dXXIYbgNRTzjXjNjYWGzZsgXt7e1oaWnhdU6BQIDU1FTs3bsX/v7+bLPzyMgIHj58iMnJSQwPD6O/v599z1IesUhISAgKCwshl8t5a4bRaERDQwM+++yzxYimHg+Q9mETG9bO1QqZTIa8vDxs27YNt2/fxtDQEDo7O51fXChkHXhAQABomkZ/fz+++eYb/POf/8SDBw+g1WqdtWBCJBIhOTnZ5YrZuSHz999/j2PHjnkUDMD5fIhdSZPLXInFYmRlZWHbtm0gSRIbN25EZWUloqKiOE8ukUiQmZmJvXv3Ijg4mPUNCoUCX3/9Ndra2qBWq53CsPiM9PR0lytmrWE0NDTgyJEjGBwcXGoYWneA3Jz7RpAvAbGIcFjzuXXrFoaHh9n+2C1btuCDDz5AdLR9X7ZUKkVJSQnKy8tZn8EwDCiKQm9vL1QqFa/aVFFREUpKShAcHMxr2pVhGFy+fBmffPLJQsxnLITcdAeIXZD/bJgQQWLS4a/uu+++w4cffgi1Ws2Gmi+++CI++OADPPfcc3aDGRcXB19fXzYAIAgCMzMzGBkZcTm4AoEAhYWFKC4uRmBgIC8Yljzj6NGjnmSmNI+kIaukAocaYpGWlhacOHECGo2GHeTNmzfj7bffRkxMzL9N3/g4zp8/b1MnYhgG09PTGBsbcxmh7d69G5mZmbx9BsMwaGlpQU1NDWd2v0Q+w26MOa9cGB8TCCDLxoEKgL5xCuPTDEwcS+W6u7sxMjKCZ555Bv7+/iAIAuHh4Vi5ciXa2trYVv87d+5Ap9Nh48aNbA9Ub28vGhoanFZwExMT8eabb7J+hy+Mjz/+GEql0qMcOICDtU3Kdr5A9JjTQiokCQT7khifojFhcAyFpml0dXVBqVTiueeeY01KVFQUwsPD0dHRAa1WC4Zh0NnZiXv37mHDhg0ICAjAr7/+iqamJofdfz4+PpDL5XjrrbeczqPPhdHc3IyPPvqI0y8tIQwA+KC2SanhBaS2SakpjI95H4CPjUP2JhEmdQ4FAO7fv4+hoSHExsayXSFRUVGIiIiAUqnEgwcPwDAM+vr6oNFoEBsbi87OTty4ccOupC4UCpGWloZ9+/bxngOnaRrXrl3D/v37PRWGNqHq6l95myyzlvweszsxYy6UtSFCDE1QLqFMTk7imWeegUQiAUmSWLlyJUJDQ3H79m12pm9sbAyjo6Po6+tDX1+fTQZOkiQSExPZaIovjObmZnz22Wfo7e31RBgA8G1tk/ILd4H4gKN/yFtIuNQUhmFw9+5dDAwMIDo6GsHBwRCJRIiMjERYWBg6Ojqg0+mg1+vR3d0NlUoFvV7POnWRSIRdu3ahqKgIERERvKIpS8R3+PBhT/QZ1vJhbZPyprtANHCyFIGP+bLkFmq1GjExMVixYgUEAgFiYmIQHh6Orq4uaDQa0DQNo9HIwggNDUVKSgoKCwsRGRnJG0ZjY6OnJH2u5P25/sMlELMfSQGw6lGgAEB/fz8mJyexbt06SKVSCAQCREdHIyoqCu3t7RgfH7c5PiEhAe+99x7v9k6TyYTm5mYcOXLEU32GtbQmVF39m7ulE4uccnWAzH/2S65d4TxP+fHHH9HY2Mg6bYIgsHXrVrzzzjt2JXeRSMR7L1yKolgY9+/f93QYTsd0QYBYoMSv83aYyVvEz88P0dHRNus3SJJEXFwc/vKXv0Amk7HvX7lyhY2QnCWKNE3j73//O/bv34++vr7lAMPpmLq8u9ompb4wPiYGPLpQtHoGd4ZMmDAwnBm2XC6Hj4+PjU8gCAIRERGIjIxEV1cXxsfHYTAY0NHRgd7eXjz//PN24a6l5PLDDz/g4MGDUKvVywVGbULV1S8eRUMAHuvTDSYG1/tmHJbngdmdc7Zt28bZZU6SJF566SW8++67WL16Nfv+9evXcfz4cbvudZqm8fPPP+PUqVPLwYHzHkted1nbpFQXxsfsgONH/QCYLan83G/iBPLaa69hz549Tre1IEkSERERCA4Oxp07d1gIPT090Ov12LRpE7y8vEDTNH766Sd88sknaG9vX04wfuBy5m4BMYfASnAsxVLpKDR1z0Clox1GWU8//TRKS0sRGRnp8joEQSAmJsYOSnt7O/R6PdasWYOenh589NFH6O7uXk4wAKC4tokjObJ8f3fOdqVqRxPmrFd31YXi7++PP//5z0hMTORc701RFAwGA2iaZp86YDKZcOXKFRw7doxN8EQiETZs2ACj0Yj29naHzt6DYfyQUHU13tVB7rYBVQL4hS8MANi1axe2bNniFMbw8DAuXryI6elpZGVlITw8HEKhEHFxcaAoCocOHcLw8DCMRiNu3brF2YXowTAAnmv93bprsy8JAvB7PjA2b96M8vJyREREOAShUqlQV1eHL7/8Eo2Njbh9+zZGR0fx1FNPITAwEF5eXlizZg1CQkLQ1tbG9mFxhcFSHxLbn/LCuhChp8E4mFB19dSCAzH7kuv3NVRhY6dB6gzGmjVr8P7772PDhg02URXDMJiYmMCVK1dw+vRpXLx4EX19fTAajTCZTOjq6mKhBAcHQyAQYN26dVixYgVu377tsDnO2v4aTAzCpCSk3qSnwOgDkFXbpNQ/FiD/M7FifecotW1kko5w9vCTiooKbNu2zQYGTdNoa2vD+fPncfr0aXR1dTlcGdvb2wuDwYD169dDKpWCIAisXbsWMpmMndhyWD6hgQkDg/Ep2pOgFCdUXb3J92DeQDZv3iyVyWQvAfir3si8aKJt50lYOy4Wo6yszG7TFoqi8PPPP6O6uhoNDQ0ulxH09/eDpmmsX7+efaxQdHQ0QkJC0NrayrmFhQXKtJHB0yFCCEliqU2VW3uMCXjC2ATgv8xhr+U5gg5LIxUVFXZ7jej1ely6dAmHDh3izBscFQs7OzuhVqvxwgsvQCwWgyRJxMTEYPXq1fjll1+cQhGLSMQECyHxXjIgrQCK+Joql0DMGrFOJpMlAHgb/34GrEPN8PHxQUVFBbKzs20WU6rValRXV+Pzzz/nbNd0BqW7uxvDw8P47W9/C19fXxAEgejoaKxatQptbW1O9xcJlZBYJV2SaEsLID6h6qra3Q8KXGhELoBXnWkFMNv0lpOTg7y8PJtOkAcPHuDAgQO4ePGiy4U1zqSnpwc0TSM2NhZCoZDVlIiICKjVaoedJCYaIAngN6tESwWjfT4fFnD5CbNGPIvZuRAfrhOEh4ejoKAAaWlpbPeIZUHNp59+isuXLzu9gdk9pASQepMw0eCcT+np6QFFUVi7di1bD4uKisLY2BgUCoVdKGwxWy9GLjqQfQlVVxvm+2HhHK2oNGvDGvBY9Pnss8+ioKAA8fHxbCsnwzC4f/8+Tp486RSGWEQgSExCFkDi99Gzn23q5s5rpqamcPr0aYyOjrJtQB0dHejo6OBMEoN8F91/FPPNN5xqiNWzX536CWt/sXXrVlRWVmLr1q02O7fdvXsXR44cwffff8+5z7l1Rr01ygsr/EgEil3PPNI0je7ubnaJwuHDh6FQKByuTQ+TCLAlSrSYPuSRYQAAYX7W9yeYbWZwqRVhYWFITU3Fq6++iqioKBsHrtPpcPjwYVy4cIEzm3ZV3pgwMDh/a9rlNn4SiYRdL+JI1q4QIuV5H6yUPvZcRAugciFgWExWpCsTRRAERCIRtmzZgoyMDGzatMlubyq9Xo/z58/j0qVLLmGkviCGlCMclXoT2LneG+hwbr6c7Z1oMVeBYmIxYMS7k/jxAfK8Mxh+fn546aWX8Lvf/Q5/+MMfIJPJ7PYIsfRBffHFF5yb2FtrhtRFbmCZo4ebaxmtr/PyU14O18cvcJ6RzOfZ7O4CsfgNu1pUaGgo/vjHPyIxMRErVqxwONNH0zQ6Ojpw5syZBV0WNh8oi1jtPQigKqHqqmahTyzkMlevvPIKEhISsH79eqcVVksne1dXF9c1ND4iouOVp72jZP6CcHduzh0oiwSjz+wv6h7XBTjr1GNjYzaNBY60g2EY3LhxA19//TWXqdIAaBqfoqsiAgV9mMdjWC1QfObsFjFtZNjQmWvHiOWiFXOBKM0DZ6MljY2NCA0Ntdv0xQKCIAh0d3fj888/d5aFKwEcVCgUreZ9aiuvVO04BTcf3y3zFyD5Nz42+6mMTzEI8iVc7qmyAPKDWStuYhFEiNkHicRiTpvPxMQE6urqEBcXZ7cCytKC849//MPZI3w0mF2QYrNlsPmLxbv7gHtvIYGVUgIrpSQ2yRYl+/7BrBFNi5lZCjH7IBGHajg8PIxz584hPDwcQUFBrNkiCAIqlQrXr193BqMJVo9hmCvmLxpv3le+Eh6we7ZZagEcWCyNcASkn8tsAUBDw2xZZs+ePdi4cSPbxnP58mW0tra6NFWubsD8xYuuVO2oxGx5vwiLv1dwK2a7CU89bh/hMlOfUzqJdwSFJEmEhYUhJSUFu3btglqtxv79+7nmNjSY3bbuXS7tcCXmDTjjzdWDeCz8o/m0Zg2uA9C00LnEIwMxQ4kH8DGctIwGBASwz3ltaWnhat+8CeB9hUKxYLbXDCjW6i/Q/BrAY+BvWvmzmwBuehIAZ0Dcqmk58R2PpB3/34UN2gcHB2dkMpnSnCg6nQNxIu0APlQoFJ1PhnZ+YlOUMjvhKrN91cxDO+zC3CcyTw2x0pQhmUzWbmW2fHhoiyXM/ZtCobj3ZFgXwIfMFbNP2WzODyz1Luu6l8YqXFaac47WJ0P6mIDMAROJ2TK9dWVYac7yfwXQ/8SJP5H/k/K/YAMc+zsUYXwAAAAASUVORK5CYII=);
            background-size: 100% 100%;
            background-position: 0px 0px;
            background-repeat: no-repeat;
            display: inline-block;
        }

        .picture1 {
            display: block;
            position: relative;
            background: url('https://static1.squarespace.com/static/540fb2cae4b0da480523257b/549126c8e4b0de7a08c58f8f/559f312ee4b0789ce5f5b059/1503789978775/NICHOLASKAYZERKOVAGUARD2.jpg');
            background-position: -180px 0px;
            background-size: 600px;
            background-repeat: no-repeat;
        }
    </style>
</head>
<>
<?php

if (isset($_GET['count'])) {
    $count = intval($_GET['count']);
    if ($count < 4) {
        $count = 4;
    }
    if ($count > 20) {
        $count = 20;
    }
    $setCount = ceil($count / 4);

    for ($sc=1; $sc<=$setCount; $sc++) {
        /*
        for ($cc=1; $cc<=4; $cc++) {
            ?>
            <div class="card">
                <div class="header">
                    <div class="title">'CASTER</div>
                </div>
                <div class="face">
                    <div class="focus">
                        <div class="focus-value">6</div>
                    </div>
                </div>
                <div class="profile right">
                    <div class="name">CASTER</div>
                    <div class="header-row">
                        <div class="column left">SPD</div>
                        <div class="column left">STR</div>
                        <div class="column left">MAT</div>
                        <div class="column left">RAT</div>
                        <div class="column left">DEF</div>
                        <div class="column left">ARM</div>
                        <div class="column left">CMD</div>
                    </div>
                    <div class="value-row">
                        <div class="column left">6</div>
                        <div class="column left">6</div>
                        <div class="column left">6</div>
                        <div class="column left">6</div>
                        <div class="column left">15</div>
                        <div class="column left">15</div>
                        <div class="column left">8</div>
                    </div>
                    <div class="icon">
                        30
                    </div>
                </div>
                <div class="weapon right">
                    <div class="name">PISTOL</div>
                    <div class="header-row">
                        <div class="column right">POW</div>
                        <div class="column right">AOE</div>
                        <div class="column right">ROF</div>
                        <div class="column right">RNG</div>
                    </div>
                    <div class="value-row">
                        <div class="column right">12</div>
                        <div class="column right">-</div>
                        <div class="column right">1</div>
                        <div class="column right">8</div>
                    </div>
                    <div class="type ranged">
                    </div>
                </div>
                <div class="weapon right">
                    <div class="name">SWORD</div>
                    <div class="header-row">
                        <div class="column right">P+S</div>
                        <div class="column right">POW</div>
                        <div class="column right">RNG</div>
                    </div>
                    <div class="value-row">
                        <div class="column right">9</div>
                        <div class="column right">3</div>
                        <div class="column right">0.5</div>
                    </div>
                    <div class="type melee">
                    </div>
                </div>
                <div class="footer">
                    <div class="right life"></div>
                    <div class="right life"></div>
                    <div class="right life"></div>
                    <div class="right life"></div>
                    <div class="right life last"></div>
                    <div class="right">&nbsp;</div>
                    <div class="right life"></div>
                    <div class="right life"></div>
                    <div class="right life"></div>
                    <div class="right life"></div>
                    <div class="right life last"></div>
                    <div class="right">&nbsp;</div>
                    <div class="right life"></div>
                    <div class="right life"></div>
                    <div class="right life"></div>
                    <div class="right life"></div>
                    <div class="right life last"></div>
                    <div class="right">&nbsp;</div>
                    <div class="right life last"></div>
                </div>
            </div>
            <?php
        }
        */

        foreach ($data as $setId => $setCards) {
            foreach ($setCards as $setCard) {
                switch ($setCard[TYPE]) {
                    case FEAT:
                        ?>
                        <div class="card">
                            <div class="header">
                                <div class="number"><?=$setId;?></div>
                                <div class="title"><b>FEAT</b></div>
                            </div>
                            <div class="body">
                                <div class="feat">FEAT: <?=$setCard[NAME];?></div>
                                <br/>
                                <div class="text"><?=$setCard[DESCRIPTION];?></div>
                            </div>
                        </div>
                        <?php
                        break;
                    case SPELL:
                        ?>
                        <div class="card">
                            <div class="header">
                                <div class="number"><?=$setId;?></div>
                                <div class="title"><b>SPELL</b></div>
                            </div>
                            <div class="body">
                                <div class="spell">
                                    <div class="name"><?=$setCard[NAME];?></div>
                                    <div class="header-row">
                                        <div class="column-spell left">COST</div>
                                        <div class="column-spell left">RNG</div>
                                        <div class="column-spell left">AOE</div>
                                        <div class="column-spell left">POW</div>
                                        <div class="column-spell left">DUR</div>
                                        <div class="column-spell left">OFF</div>
                                    </div>
                                    <div class="value-row">
                                        <div class="column-spell left"><?=$setCard[COST];?></div>
                                        <div class="column-spell left"><?=$setCard[RANGE];?></div>
                                        <div class="column-spell left"><?=$setCard[AOE]??'-';?></div>
                                        <div class="column-spell left"><?=$setCard[POWER]??'-';?></div>
                                        <div class="column-spell left"><?=$setCard[DURATION]??'-';?></div>
                                        <div class="column-spell left"><?=($setCard[OFFENSIVE] ? 'YES' : 'NO');?></div>
                                    </div>
                                </div>
                                <br/>
                                <div class="text"><?=$setCard[DESCRIPTION];?></div>
                            </div>
                        </div>
                        <?php
                        break;
                    case WEAPON:
                        ?>
                        <div class="card">
                            <div class="header">
                                <div class="number"><?=$setId;?></div>
                                <div class="title">WEAPON</div>
                            </div>
                            <div class="body">
                                <div class="weapon" style="margin: auto;">
                                    <div class="name"><?=$setCard[NAME];?></div>
                                    <?php
                                    if (isset($setCard[ROF])) {
                                        ?>
                                        <div class="header-row">
                                            <div class="column right">POW</div>
                                            <div class="column right">AOE</div>
                                            <div class="column right">ROF</div>
                                            <div class="column right">RNG</div>
                                        </div>
                                        <div class="value-row">
                                            <div class="column right"><?=$setCard[POWER];?></div>
                                            <div class="column right"><?=$setCard[AOE]??'-';?></div>
                                            <div class="column right"><?=$setCard[ROF];?></div>
                                            <div class="column right"><?=$setCard[RANGE];?></div>
                                        </div>
                                        <div class="type ranged">
                                        </div>
                                        <?php
                                    } else {
                                        ?>
                                        <div class="header-row">
                                            <div class="column right">P+S</div>
                                            <div class="column right">POW</div>
                                            <div class="column right">RNG</div>
                                        </div>
                                        <div class="value-row">
                                            <div class="column right"><?=($setCard[POWER] + 6);?></div>
                                            <div class="column right"><?=$setCard[POWER];?></div>
                                            <div class="column right"><?=$setCard[RANGE];?></div>
                                        </div>
                                        <div class="type melee">
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <?php
                        break;
                    case ABILITY:
                        ?>
                        <div class="card">
                            <div class="header">
                                <div class="number"><?=$setId;?></div>
                                <div class="title"><b>ABILITY</b></div>
                            </div>
                            <div class="body">
                                <div class="text"><?php
                                    if (isset($setCard[NAME])) {
                                        echo '<b>'.$setCard[NAME].'</b> - ';
                                    }
                                    ?><?=$setCard[DESCRIPTION];?></div>
                            </div>
                        </div>
                        <?php
                        break;
                    case STAT:
                        ?>
                        <div class="card">
                            <div class="header">
                                <div class="number"><?=$setId;?></div>
                                <div class="title">STAT</div>
                            </div>
                            <div class="body">
                                <div class="text text-center"><?=$setCard[DESCRIPTION];?></div>
                                <div class="stat center">
                                    <div class="name"></div>
                                    <div class="header-row">
                                        <div class="column"><?=$setCard[STAT];?></div>
                                    </div>
                                    <div class="value-row">
                                        <div class="column"><?=$setCard[VALUE];?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                        break;
                    case FOCUS:
                        ?>
                        <div class="card">
                            <div class="header">
                                <div class="number"><?=$setId;?></div>
                                <div class="title">STAT</div>
                            </div>
                            <div class="body">
                                <div class="text text-center">Increase your 'caster's</div>
                                <div class="text text-center">FOCUS or FURY by 1.</div>
                                <div class="focus-increase center">
                                    <div class="focus">
                                        <div class="focus-value">+1</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                        break;
                }
            }
        }
    }

} else {
?>
    <form method="get" action="">
        Player count: <input type="text" name="count" size="2" maxlength="2"><input type="submit" value="Generate">
    </form>
<?php
}
?>
</body>
</html>