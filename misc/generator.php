<?php
//$html = file_get_contents('http://cardfight.wikia.com/wiki/V_Booster_Set_01:_Unite!_Team_Q4');
//$html = file_get_contents('http://cardfight.wikia.com/wiki/V_Extra_Booster_01:_The_Destructive_Roar');
//$html = file_get_contents('https://cardfight.fandom.com/wiki/V_Booster_Set_02:_Strongest!_Team_AL4');
//$html = file_get_contents('https://cardfight.fandom.com/wiki/V_Booster_Set_03:_Miyaji_Academy_Cardfight_Club');
//$html = file_get_contents('https://cardfight.fandom.com/wiki/V_Booster_Set_04:_Vilest!_Deletor');
//$html = file_get_contents('https://cardfight.fandom.com/wiki/V_Trial_Deck_04:_Ren_Suzugamori');
//$html = file_get_contents('https://cardfight.fandom.com/wiki/V_Extra_Booster_02:_Champions_of_the_Asia_Circuit');
//$html = file_get_contents('https://cardfight.fandom.com/wiki/V_Extra_Booster_04:_The_Answer_of_Truth');
//$html = file_get_contents('https://cardfight.fandom.com/wiki/V_Extra_Booster_05:_Primary_Melody');
//$html = file_get_contents('https://cardfight.fandom.com/wiki/V_Extra_Booster_06:_Light_of_Salvation,_Logic_of_Destruction');
//$html = file_get_contents('https://cardfight.fandom.com/wiki/V_Extra_Booster_07:_The_Heroic_Evolution');
//$html = file_get_contents('https://cardfight.fandom.com/wiki/V_Trial_Deck_05:_Misaki_Tokura');
//$html = file_get_contents('https://cardfight.fandom.com/wiki/V_Trial_Deck_06:_Naoki_Ishida');
//$html = file_get_contents('https://cardfight.fandom.com/wiki/V_Trial_Deck_07:_Kouji_Ibuki');
//$html = file_get_contents('https://cardfight.fandom.com/wiki/V_Trial_Deck_10:_Chronojet');
//$html = file_get_contents('https://cardfight.fandom.com/wiki/V_Trial_Deck_11:_Altmile');
$html = file_get_contents('https://cardfight.fandom.com/wiki/V_Trial_Deck_12:_Ahsha');
$outputFile = 'v-td12.set.json';

$cardSet = [];

$dom = new DOMDocument;
libxml_use_internal_errors(true);
$result = $dom->loadHTML($html, LIBXML_NOERROR );
if ($result === false) {
    die('failed to load html');
}
foreach ($dom->getElementsByTagName('a') as $node) {
    $path = $node->getNodePath();
    if (
        stripos($path, 'table') === false
        || substr($path, -7) !== 'td[2]/a'
    ) {
        continue;
    }


    $cardUrl = 'http://cardfight.wikia.com' .  $node->attributes->getNamedItem('href')->textContent;
    echo $cardUrl.PHP_EOL;
//    if ($cardUrl !== 'http://cardfight.wikia.com/wiki/Archbird_(V_Series)') {
//        continue;
//    }
    $cardContent = file_get_contents($cardUrl);
//    $cardDom = new DOMDocument();
//    $cardDom->loadHTML($cardContent);

    $card = [
        'url' => $cardUrl,
    ];
//    $cardUrl = 'http://cardfight.wikia.com/wiki/Blaster_Blade_(V_Series)';
//    echo $cardUrl.PHP_EOL;
//    $cardContent = file_get_contents($cardUrl);
//file_put_contents('card.html', $cardContent);

//    $cardContent = file_get_contents('card.html');
    $cardDom = new DOMDocument();
    $cardDom->loadHTML($cardContent);

    $cardXPath = new DOMXpath($cardDom);

    $elements = $cardXPath->query('//div[@class="info-main"]');

    foreach ($elements as $element) {
        $content = $element->textContent;
        $arr = explode("\n", $content);
//    print_r($arr);

        foreach ($arr as $line) {
            $count = 1;
            while ($count > 0) {
                $line = str_replace('  ', ' ', $line, $count);
            }
            $line = trim($line);
            $expl = explode(' ', $line);
            switch ($expl[0]) {
                case 'Name':
                    echo "name: " . join(' ', array_slice($expl, 1)) . PHP_EOL;
                    $card['name'] = join(' ', array_slice($expl, 1));
                    break;
                case 'Grade':
                    echo "grade: ".intval($expl[4]).PHP_EOL;
                    $card['grade'] = intval($expl[4]);
                    echo "skill: ".strtolower($expl[6]).PHP_EOL;
                    $card['skill'] = strtolower($expl[6]);
                    break;
                case 'Imaginary Gift':
                    $card['gift'] = '?';
                    break;
                case 'Power':
                    echo "power: ".(intval($expl[1]) / 1000).PHP_EOL;
                    $card['power'] = (intval($expl[1]) / 1000);
                    break;
                case 'Critical':
                    echo "critical: ".intval($expl[1]).PHP_EOL;
                    $card['critical'] = intval($expl[1]);
                    break;
                case 'Shield':
                    echo "shield: ".(intval($expl[1]) / 1000).PHP_EOL;
                    $card['shield'] = (intval($expl[1]) / 1000);
                    break;
                case 'Clan':
                    echo "clan: " . join(' ', array_slice($expl, 1)) . PHP_EOL;
                    $card['clan'] = join(' ', array_slice($expl, 1));
                    break;
                case 'Race':
                    echo "race: " . join(' ', array_slice($expl, 1)) . PHP_EOL;
                    $card['race'] = join(' ', array_slice($expl, 1));
                    break;
                case 'Trigger':
                    echo "trigger: ".strtolower($expl[2]).PHP_EOL;
                    $card['trigger'] = strtolower($expl[2]);
                    break;
                default:
                    echo "unknown: ".join(' ', $expl).PHP_EOL;
            }
        }
    }

    $elements = $cardXPath->query('//div[@class="info-extra"]');
    foreach ($elements as $element) {
        $content = $element->textContent;
        $arr = explode("\n", $content);
        print_r($arr);

        $stage = null;
        $cardIds = [];
        $cardEffects = [];

        foreach ($arr as $line) {
            $count = 1;
            while ($count > 0) {
                $line = str_replace('  ', ' ', $line, $count);
            }
            $line = trim($line);
            $expl = explode(' ', $line);
            if (count($expl) < 2) {
                continue;
            }
            switch ($expl[1]) {
                case 'Set(s)':
                    echo 'starting sets'.PHP_EOL;
                    $stage = 'set';
                    break;
                case 'Flavor(s)':
                    $stage = 'flavor';
                    echo 'starting flavour'.PHP_EOL;
                    break;
                case 'Effect(s)':
                    $stage = 'effect';
                    echo 'starting effects'.PHP_EOL;
                    break;
                case 'Status':
                    $stage = 'status';
                    echo 'starting status'.PHP_EOL;
                    break;
                case 'Tips':
                    $stage = 'tips';
                    echo 'end'.PHP_EOL;
                    break;
                default:
                    if ($stage === 'set') {
                        $setExpl = explode(' - ', $line);
                        $cardIds = array_merge($cardIds, array_slice($setExpl, 1));
                    }
                    if ($stage === 'effect') {
                        $cardEffects[] = $line;
                    }
                    echo "unknown: ".join(' ', $expl).PHP_EOL;
            }
        }

        echo "ids: ".PHP_EOL;
        print_r($cardIds);
        if (count($cardIds) > 1) {
            $card['id'] = $cardIds;
        } else {
            $card['id'] = $cardIds[0];
        }
        echo "effects: ".PHP_EOL;
        print_r($cardEffects);
        $card['description'] = join(' ', $cardEffects);
    }


//    $cardXPath = new DOMXpath($cardDom);

//    $elements = $cardXPath->query("");
//    print_r($elements);
//    die;

//    foreach ($cardDom->getElementsByTagName('table') as $tableNode) {
//        if ($tableNode->getAttribute('class') !== 'effect') {
//            continue;
//        }
//        fwrite($fp, $cardDom->saveHtml($tableNode)."\n");
//    }
//    die;
//    echo $dom->saveHtml($node), PHP_EOL;
    $cardSet[] = $card;
}

$fp = fopen($outputFile, 'w');
fwrite($fp, json_encode(['type' => 'set', 'cards' => $cardSet], JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
fclose($fp);

echo "done";
/*
 * <tr style="text-align: center;">
  <td>V-BT01/044</td>
  <td><a href="/wiki/Herculean_Knight,_Allobrox" title="Herculean Knight, Allobrox">Herculean Knight, Allobrox</a></td>
  <td>2</td>
  <td><a href="/wiki/Royal_Paladin" title="Royal Paladin">Royal Paladin</a></td>
  <td></td>
  <td>C</td>
 </tr>
 */

//$content = file_get_contents('http://cardfight.wikia.com/wiki/Ravenous_Dragon,_Gigarex_(V_Series)');
//$xml = simplexml_load_string($content);
//
//$result = $xml->xpath('div');
//print_r($result);
////foreach ($r)
////while(list( , $node) = each($result)) {
////    echo '/a/b/c: ',$node,"\n";
////}


