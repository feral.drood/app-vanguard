# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased
### Changed
- lowered php version requirement to 7.2
### Added
- Tests for Update entities
- FilterData entity
- FilterHelper service 

## 2.1.1 - 2020-04-09
### Added
- Added additional libraries for phpstan
### Fixed
- Fixed phpstan error with undefined class
### Changed
- Renamed App\Game\Updates namespace to App\Game\Update
- Split phpstan and php-cs-fixer to separate jobs

## 2.1.0 - 2020-04-09
### Added
- Ability to fight bots
- Added gitlab ci integration
- Card event entity transferred to symfony listener
- Gift abilities, since player has to choose between I and II
- Action rollback command added
- Reload card abilities command added

### Changed
- Refactored abilities to pass only data entity between services
- Update messages moved to State entity
- Changed all services to require State instead of Game entity
- Refactored AbilityStackProcessor to process and unpack abilities
- Refactored player buffer to always require bufferId
- ChatHelper changes from static service to instantiated and injected one

### Removed
- Removed GameProvider and similar services and their traits
- Removed battle traits from phases and changed to injected services  


## 2.0.0 - 2020-03-19
### Changed
- Restarted project, pushed all intermediate changes. Some where lost :(
- Most of the frontend messages changed, switching major version due to incompatibilities
 
## 1.0.0 - very long time ago
### Added
- Added app:import-deck command to import trial decks
### Changes
- Card description can be null (for those cards that have no text)
