<?php

declare(strict_types=1);

namespace App\Tests\Entity\Ability;

use App\Entity\Ability\AbilityPart;
use PHPUnit\Framework\TestCase;

class AbilityPartDataTest extends TestCase
{

    public function testSettersAndGetters()
    {
        $abilityPartData = new AbilityPart();

        $abilityPartData
            ->setType('type')
            ->setSelfCardId('self')
            ->setEventCardId('event')
            ->setPlayerHash('hash')
            ->addOption('test1', 1)
            ->addOption('test2', 2)
        ;

        $this->assertEquals($abilityPartData->getType(), 'type');
        $this->assertEquals($abilityPartData->getSelfCardId(), 'self');
        $this->assertEquals($abilityPartData->getEventCardId(), 'event');
        $this->assertEquals($abilityPartData->getPlayerHash(), 'hash');
        $this->assertEquals($abilityPartData->getOption('test1'), 1);
        $this->assertEquals($abilityPartData->getOption('test2'), 2);

        $this->assertEquals(
            $abilityPartData->getData(),
            [
                'type' => 'type',
                'self_card' => 'self',
                'event_card' => 'event',
                'player_hash' => 'hash',
                'options' => [
                    'test1' => 1,
                    'test2' => 2,
                ]
            ]
        );
    }
}
