<?php

declare(strict_types=1);

namespace App\Tests\Entity\Ability;

use App\Entity\Ability\Ability;
use PHPUnit\Framework\TestCase;

class AbilityDataTest extends TestCase
{
    public function testSettersAndGetters()
    {
        $abilityData = new Ability();
        $abilityData
            ->setType('type')
            ->setId('id')
            ->setName('name')
            ->setPlayerHash('hash')
            ->setSelfCardId('self')
            ->setEventCardId('event')
        ;

        $this->assertEquals($abilityData->getType(), 'type');
        $this->assertEquals($abilityData->getId(), 'id');
        $this->assertEquals($abilityData->getName(), 'name');
        $this->assertEquals($abilityData->getPlayerHash(), 'hash');
        $this->assertEquals($abilityData->getSelfCardId(), 'self');
        $this->assertEquals($abilityData->getEventCardId(), 'event');

        $this->assertEquals(
            $abilityData->getData(),
            [
                'type' => 'type',
                'id' => 'id',
                'name' => 'name',
                'player_hash' => 'hash',
                'self_card_id' => 'self',
                'event_card_id' => 'event',
            ]
        );

        $array1 = ['test1' => 'much_wow'];
        $array2 = ['test2' => 'less_wow'];

        $abilityData->setConditions([$array1]);
        $this->assertEquals($abilityData->getConditions(), [$array1]);
        $abilityData->addCondition($array2);
        $this->assertEquals($abilityData->getConditions(), [$array1, $array2]);

        $abilityData->setCosts([$array1]);
        $this->assertEquals($abilityData->getCosts(), [$array1]);
        $abilityData->addCost($array2);
        $this->assertEquals($abilityData->getCosts(), [$array1, $array2]);

        $abilityData->setEffects([$array1]);
        $this->assertEquals($abilityData->getEffects(), [$array1]);
        $abilityData->addEffect($array2);
        $this->assertEquals($abilityData->getEffects(), [$array1, $array2]);

        $this->assertEquals(
            $abilityData->getData(),
            [
                'type' => 'type',
                'id' => 'id',
                'name' => 'name',
                'player_hash' => 'hash',
                'self_card_id' => 'self',
                'event_card_id' => 'event',
                'conditions' => [
                    $array1,
                    $array2,
                ],
                'costs' => [
                    $array1,
                    $array2,
                ],
                'effects' => [
                    $array1,
                    $array2,
                ]
            ]
        );
    }
}
