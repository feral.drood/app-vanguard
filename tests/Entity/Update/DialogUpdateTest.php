<?php

declare(strict_types=1);

namespace App\Tests\Entity\Update;

use App\Entity\Update\DialogUpdate;
use PHPUnit\Framework\TestCase;

class DialogUpdateTest extends TestCase
{
    public function testEntity()
    {
        $entity = new DialogUpdate(
            'hash'
        );

        $this->assertEquals($entity->getPlayerHash(), 'hash');

        $entity->setLoadZone('zone1');
        $data = $entity->getData();
        $this->assertEquals($data['data']['load_zone'] ?? null, 'zone1');

        $entity->setTitle('title1');
        $data = $entity->getData();
        $this->assertEquals($data['data']['title'] ?? null, 'title1');

        $entity->setType('type1');
        $data = $entity->getData();
        $this->assertEquals($data['data']['type'] ?? null, 'type1');

        $entity->setCards(['cards']);
        $data = $entity->getData();
        $this->assertEquals($data['data']['cards'] ?? null, ['cards']);

        $entity->setActions(['actions1']);
        $data = $entity->getData();
        $this->assertEquals($data['data']['actions'] ?? null, ['actions1']);

        $entity->addOption('option1', 'value1');
        $data = $entity->getData();
        $this->assertEquals($data['data']['options']['option1'] ?? null, 'value1');
    }
}
