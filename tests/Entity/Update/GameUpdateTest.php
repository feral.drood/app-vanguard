<?php

declare(strict_types=1);

namespace App\Tests\Entity\Update;

use App\Entity\Update\GameUpdate;
use PHPUnit\Framework\TestCase;

class GameUpdateTest extends TestCase
{
    public function test()
    {
        $entity = new GameUpdate(
            'type1',
            'hash1',
            'data1'
        );
        $data = $entity->getData();

        $this->assertEquals($entity->getPlayerHash(), 'hash1');
        $this->assertEquals($data['player'], 'hash1');
        $this->assertEquals($data['type'], 'type1');
        $this->assertEquals($data['data'], 'data1');
    }
}
