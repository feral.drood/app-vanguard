<?php

declare(strict_types=1);

namespace App\Tests\Entity\Game;

use App\Entity\Game\Buffer;
use App\Entity\Game\Card;
use PHPUnit\Framework\TestCase;

class BufferTest extends TestCase
{
    public function test()
    {
        $entity = new Buffer();

        $card = $this->createMock(Card::class);
        $card->method('getId')->willReturn('id1');

        $entity->setCards('test1', [$card]);
        $this->assertEquals($entity->getCards('test1'), ['id1' => $card]);

        $entity->setMarked('test2', 'data2');
        $this->assertEquals('data2', $entity->getMarked('test2'));

        $entity->clearCards('test1');
        $this->assertEquals($entity->getCards('test1'), []);

        $entity->clearMarked('test2');
        $this->assertEquals($entity->getMarked('test2'), null);

        $entity->setOrigin('card1', 'hand');
        $this->assertEquals('hand', $entity->getOrigin('card1'));

        $databaseData = $entity->serializeForDatabase();
        $entity2 = (new Buffer())->deserializeFromDatabase($databaseData);
        $this->assertEquals($entity, $entity2);
    }
}
