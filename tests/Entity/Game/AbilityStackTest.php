<?php

declare(strict_types=1);

namespace App\Tests\Entity\Game;

use App\Entity\Game\AbilityStack;
use PHPUnit\Framework\TestCase;

class AbilityStackTest extends TestCase
{
    public function testSerialize()
    {
        $data = [
            'queue' => [
                [
                    'ability1',
                    'ability2',
                    'ability3'
                ],
                [
                    'ability4',
                    'ability5'
                ]
            ],
            'costs' => [
                'cost1',
                'cost2'
            ],
            'effects' => [
                'effect1',
                'effect2'
            ],
            'used_abilities' => []
        ];

        $abilityStack = new AbilityStack();
        $modifiedData = $abilityStack->deserializeFromDatabase($data)->serializeForDatabase();

        $this->assertEquals($data, $modifiedData);
    }

    public function testEmpty()
    {
        $abilityStack = new AbilityStack();
        $this->assertEquals($abilityStack->isEmpty(), true);

        $abilityStack = new AbilityStack();
        $this->assertEquals($abilityStack->getEffectCount(), 0);
        $abilityStack->addEffect(['type' => 'ACT']);
        $this->assertEquals($abilityStack->isEmpty(), false);
        $this->assertEquals($abilityStack->getEffectCount(), 1);

        $abilityStack = new AbilityStack();
        $this->assertEquals($abilityStack->getCostCount(), 0);
        $abilityStack->addCost(['type' => 'ACT']);
        $this->assertEquals($abilityStack->isEmpty(), false);
        $this->assertEquals($abilityStack->getCostCount(), 1);

        $abilityStack = new AbilityStack();
        $abilityStack->addAbility(['type' => 'ACT']);
        $this->assertEquals($abilityStack->isEmpty(), false);
    }

    public function testAbilityGroups()
    {
        $abilityStack = new AbilityStack();

        $abilityStack->addUsedAbility('test1');
        $this->assertEquals($abilityStack->isUsedAbility('test1'), true);
        $abilityStack->resetUsedAbilities();
        $this->assertEquals($abilityStack->isUsedAbility('test1'), false);

        $abilityStack->addAbility(['type' => 'ACT1']);
        $abilityStack->addAbility(['type' => 'ACT2']);
        $abilityStack->addAbility(['type' => 'ACT3']);
        $abilityStack->removeAbility(1);

        $this->assertEquals($abilityStack->serializeForDatabase(), [
            'queue' => [
                [
                    ['type' => 'ACT1'],
                    ['type' => 'ACT3']
                ],
                []
            ],
            'costs' => [],
            'effects' => [],
            'used_abilities' => [],
        ]);

        $abilityStack->removeAbility(1);
        $this->assertEquals($abilityStack->serializeForDatabase(), [
            'queue' => [
                [
                    ['type' => 'ACT1']
                ],
                []
            ],
            'costs' => [],
            'effects' => [],
            'used_abilities' => [],
        ]);

        $abilityStack->addAbility(['type' => 'ACT4']);
        $abilityStack->removeAbility(0);
        $abilityStack->addAbility(['type' => 'ACT5']);
        $this->assertEquals($abilityStack->serializeForDatabase(), [
            'queue' => [
                [],
                [
                    ['type' => 'ACT4']
                ],
                [
                    ['type' => 'ACT5']
                ]
            ],
            'costs' => [],
            'effects' => [],
            'used_abilities' => [],
        ]);

        $this->assertEquals($abilityStack->getAbilities(), [['type' => 'ACT4']]);

        $this->assertEquals($abilityStack->serializeForDatabase(), [
            'queue' => [
                [
                    ['type' => 'ACT4']
                ],
                [
                    ['type' => 'ACT5']
                ]
            ],
            'costs' => [],
            'effects' => [],
            'used_abilities' => [],
        ]);
    }
}