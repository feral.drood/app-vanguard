<?php

declare(strict_types=1);

namespace App\Tests\Entity\Game;

use App\Entity\Game\CardEventLog;
use PHPUnit\Framework\TestCase;

class CardEventLogTest extends TestCase
{
    public function testEntity()
    {
        $entity = (new CardEventLog());

        $this->assertEquals(null, $entity->getCount('test1'));

        $entity->addEvents(['test1', 'test2']);
        $this->assertEquals(1, $entity->getCount('test1'));
        $this->assertEquals(1, $entity->getCount('test2'));

        $entity->clear();
        $this->assertEquals(null, $entity->getCount('test1'));
        $this->assertEquals(null, $entity->getCount('test2'));
    }
}
