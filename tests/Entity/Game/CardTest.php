<?php

declare(strict_types=1);

namespace App\Tests\Entity\Game;

use App\Entity\Game\Card;
use PHPUnit\Framework\TestCase;

class CardTest extends TestCase
{

    public function testSetImage()
    {
        $card = (new Card())
            ->setImage('image1')
        ;

        $this->assertEquals('image1', $card->getImage());
    }

    public function testSetAttacker()
    {
        $card = (new Card())
            ->setAttacker(true)
        ;

        $this->assertEquals(true, $card->isAttacker());
    }

    public function testAddBonusProperty()
    {
        $card = (new Card())
            ->addBonusProperty('property1')
        ;

        $this->assertEquals(true, $card->hasProperty('property1'));
    }

    public function testSetPower()
    {
        $card = (new Card())
            ->setPower(1)
        ;

        $this->assertEquals(1, $card->getPower());
    }

    public function testSetFaceDown()
    {
        $card = (new Card())
            ->setFaceDown(true)
        ;

        $this->assertEquals(true, $card->isFaceDown());

    }

    public function testSetDescription()
    {
        $card = (new Card())
            ->setDescription('description1')
        ;

        $this->assertEquals('description1', $card->getDescription());
    }

    public function testSetGrade()
    {
        $card = (new Card())
            ->setGrade(1)
        ;

        $this->assertEquals(1, $card->getGrade());
    }

    public function testResetBonus()
    {
        $card = (new Card())
            ->setPower(1)
        ;

        $this->assertEquals(1, $card->getPower());
        $card->addBonus(Card::BONUS_POWER, Card::BONUS_POWER, 1);
        $this->assertEquals(2, $card->getPower());
    }

    public function testSetClan()
    {
        $card = (new Card())
            ->setClan('clan1')
        ;

        $this->assertEquals('clan1', $card->getClan());
    }

    public function testSetAbilities()
    {
        $card = (new Card())
            ->setAbilities(['abilities1'])
        ;

        $this->assertEquals(['abilities1'], $card->getAbilities());
    }

    public function testSetId()
    {
        $card = (new Card())
            ->setId('id1')
        ;

        $this->assertEquals('id1', $card->getId());
    }

    public function testSetTrigger()
    {
        $card = (new Card())
            ->setTrigger('trigger1')
        ;

        $this->assertEquals('trigger1', $card->getTrigger());
    }

    public function testSetRace()
    {
        $card = (new Card())
            ->setRace('race1')
        ;

        $this->assertEquals('race1', $card->getRace());
    }

    public function testSetRaces()
    {
        $card = (new Card())
            ->setRace('race1 / race2')
        ;

        $this->assertEquals(['race1', 'race2'], $card->getRaces());
    }

    public function testSetShield()
    {
        $card = (new Card())
            ->setShield(12)
        ;

        $this->assertEquals(12, $card->getShield());
    }

    public function testSetState()
    {
        $card = (new Card())
            ->setState('state1')
        ;

        $this->assertEquals('state1', $card->getState());
    }

    public function testSetName()
    {
        $card = (new Card())
            ->setName('name1')
        ;

        $this->assertEquals('name1', $card->getName());
    }

    public function testSetBooster()
    {
        $card = (new Card())
            ->setBooster(true)
        ;

        $this->assertEquals(true, $card->isBooster());
    }

    public function testSetCritical()
    {
        $card = (new Card())
            ->setCritical(2)
        ;

        $this->assertEquals(2, $card->getCritical());
    }

    public function testAddBonus()
    {
        $card = (new Card())
            ->setPower(12)
            ->setShield(32)
            ->setCritical(4)
            ->setSkill(Card::SKILL_INTERCEPT)
        ;

        $this->assertEquals(12, $card->getPower());
        $this->assertEquals(32, $card->getShield());
        $this->assertEquals(4, $card->getCritical());
        $this->assertEquals(4, $card->getDamage());
        $this->assertEquals(1, $card->getDrive());

        $card
            ->addBonus(Card::BONUS_LENGTH_BATTLE, Card::BONUS_POWER, 1)
            ->addBonus(Card::BONUS_LENGTH_BATTLE,Card::BONUS_SHIELD, 3)
            ->addBonus(Card::BONUS_LENGTH_BATTLE,Card::BONUS_CRITICAL, 2)
            ->addBonus(Card::BONUS_LENGTH_BATTLE,Card::BONUS_DAMAGE, 4)
            ->addBonus(Card::BONUS_LENGTH_BATTLE,Card::BONUS_DRIVE, 5)
        ;

        $this->assertEquals(13, $card->getPower());
        $this->assertEquals(35, $card->getShield());
        $this->assertEquals(6, $card->getCritical());
        $this->assertEquals(10, $card->getDamage());
        $this->assertEquals(6, $card->getDrive());
    }

    public function testSetDefender()
    {
        $card = (new Card())
            ->setDefender(true)
        ;

        $this->assertEquals(true, $card->isDefender());
    }

    public function testSetSkill()
    {
        $card = (new Card())
            ->setSkill(Card::SKILL_TWIN_DRIVE)
        ;

        $this->assertEquals(Card::SKILL_TWIN_DRIVE, $card->getSkill());
    }

    public function testSetGift()
    {

        $card = (new Card())
            ->setGift(Card::GIFT_ACCEL)
        ;

        $this->assertEquals(Card::GIFT_ACCEL, $card->getGift());
    }
}
