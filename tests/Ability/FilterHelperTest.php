<?php

declare(strict_types=1);

namespace App\Tests\Ability;

use App\Ability\FilterData;
use App\Ability\FilterHelper;
use App\Entity\Ability\AbilityPart;
use App\Entity\Ability\AbilityPartOptions;
use App\Entity\Game\Card;
use App\Entity\Game\Field;
use App\Entity\Game\Player;
use App\Entity\Game\State;
use App\Exception\MisconfiguredAbilityException;
use PHPUnit\Framework\TestCase;

class FilterHelperTest extends TestCase
{
    /**
     * @var State
     */
    protected $state;

    protected function setUp(): void
    {
        parent::setUp();

        $state = new State();
        $player1 = (new Player())
            ->setHash('hash1')
        ;
        $player1->getField()->addCircle();
        $vanguard1 = (new Card())
            ->setId('card1')
            ->setName('Small Vanguard')
            ->setGrade(2)
            ->setState(AbilityPartOptions::STATE_REST)
            ->setFaceDown(true)
            ->setRace('Dog')
        ;
        $card2 = (new Card())
            ->setId('card2')
            ->setName('Rearguard2')
            ->setGrade(2)
            ->setRace('Cat')
        ;
        $card3 = (new Card())
            ->setId('card3')
            ->setGrade(3)
            ->setName('Rearguard3')
            ->setRace('race1 / race2')
        ;
        $player1->getField()->setCircleCard(Field::VANGUARD, $vanguard1);
        $player1->getField()->setCircleCard(Field::REARGUARD . '6', $card2);
        $player1->getField()->setCircleCard(Field::REARGUARD . '7', $card3);
        $player2 = (new Player())
            ->setHash('hash2')
        ;
        $state
            ->addPlayer($player1)
            ->addPlayer($player2)
            ->setActivePlayerHash('hash1')
        ;

        $this->state = $state;
    }

    public function testBuildFilterData(): void
    {
        $service = new FilterHelper();

        $abilityPartData = (new AbilityPart())
            ->setPlayerHash('hash1')
            ->setSelfCardId('card1')
            ->setOptions([
                AbilityPartOptions::RACE => 'race1',
                AbilityPartOptions::MIN_GRADE => 1,
                AbilityPartOptions::MAX_GRADE => 3,
                AbilityPartOptions::NAME => 'name1',
                AbilityPartOptions::PARTIAL_NAME => 'me',
                AbilityPartOptions::STATE => AbilityPartOptions::STATE_STAND,
                AbilityPartOptions::FACE => AbilityPartOptions::FACE_DOWN,
                AbilityPartOptions::LOCATION_VALID_LOCATIONS => [Field::VANGUARD],
                AbilityPartOptions::LOCATION_INVALID_LOCATIONS => [Field::REARGUARD2],
            ])
        ;
        $filterData = $service->buildFilterData($this->state, $abilityPartData);
        $this->assertEquals($filterData->getRace(), 'race1');
        $this->assertEquals($filterData->getMinGrade(), 1);
        $this->assertEquals($filterData->getMaxGrade(), 3);
        $this->assertEquals($filterData->getName(), 'name1');
        $this->assertEquals($filterData->getPartialName(), 'me');
        $this->assertEquals($filterData->getState(), AbilityPartOptions::STATE_STAND);
        $this->assertEquals($filterData->getFace(), AbilityPartOptions::FACE_DOWN);
        $this->assertEquals($filterData->getValidLocations(), [Field::VANGUARD]);
        $this->assertEquals($filterData->getInvalidLocations(), [Field::REARGUARD2]);


        $abilityPartData->setOptions([
            AbilityPartOptions::LOCATION => AbilityPartOptions::LOCATION_VANGUARD,
        ]);
        $filterData = $service->buildFilterData($this->state, $abilityPartData);
        $this->assertEquals(
            $filterData->getValidLocations(),
            [
                Field::VANGUARD,
            ]
        );

        $abilityPartData->setOptions([
            AbilityPartOptions::GRADE => AbilityPartOptions::GRADE_CALL,
        ]);
        $filterData = $service->buildFilterData($this->state, $abilityPartData);
        $this->assertEquals($filterData->getMinGrade(), null);
        $this->assertEquals($filterData->getMaxGrade(), 2);

        $abilityPartData->setOptions([
            AbilityPartOptions::LOCATION => AbilityPartOptions::LOCATION_BACK
        ]);
        $filterData = $service->buildFilterData($this->state, $abilityPartData);
        $this->assertEquals(
            $filterData->getValidLocations(),
            [
                Field::REARGUARD2,
                Field::REARGUARD3,
                Field::REARGUARD4,
            ]
        );

        $abilityPartData->setOptions([
            AbilityPartOptions::LOCATION => AbilityPartOptions::LOCATION_FRONT_REARGUARD
        ]);
        $filterData = $service->buildFilterData($this->state, $abilityPartData);
        $this->assertEqualsCanonicalizing(
            $filterData->getValidLocations(),
            [
                Field::REARGUARD1,
                Field::REARGUARD5,
                Field::REARGUARD . '6',
                Field::REARGUARD . '7',
            ]
        );

        $abilityPartData->setOptions([
            AbilityPartOptions::LOCATION => AbilityPartOptions::LOCATION_FRONT_UNIT
        ]);
        $filterData = $service->buildFilterData($this->state, $abilityPartData);
        $this->assertEqualsCanonicalizing(
            $filterData->getValidLocations(),
            [
                Field::VANGUARD,
                Field::REARGUARD1,
                Field::REARGUARD5,
                Field::REARGUARD . '6',
                Field::REARGUARD . '7',
            ]
        );

        $abilityPartData->setOptions([
            AbilityPartOptions::LOCATION => AbilityPartOptions::LOCATION_BACK
        ]);
        $filterData = $service->buildFilterData($this->state, $abilityPartData);
        $this->assertEquals(
            $filterData->getValidLocations(),
            [
                Field::REARGUARD2,
                Field::REARGUARD3,
                Field::REARGUARD4,
            ]
        );

        $abilityPartData->setOptions([
            AbilityPartOptions::LOCATION => AbilityPartOptions::LOCATION_REARGUARD
        ]);
        $filterData = $service->buildFilterData($this->state, $abilityPartData);
        $this->assertEqualsCanonicalizing(
            $filterData->getValidLocations(),
            [
                Field::REARGUARD1,
                Field::REARGUARD2,
                Field::REARGUARD3,
                Field::REARGUARD4,
                Field::REARGUARD5,
                Field::REARGUARD . '6',
                Field::REARGUARD . '7',
            ]
        );

        $abilityPartData->setOptions([
            AbilityPartOptions::LOCATION => AbilityPartOptions::LOCATION_COLUMN
        ]);
        $filterData = $service->buildFilterData($this->state, $abilityPartData);
        $this->assertEquals(
            $filterData->getValidLocations(),
            [
                Field::REARGUARD3,
            ]
        );

        $abilityPartData->setOptions([
            AbilityPartOptions::PLAYER => AbilityPartOptions::PLAYER_OPPONENT,
            AbilityPartOptions::LOCATION => AbilityPartOptions::LOCATION_COLUMN
        ]);
        $filterData = $service->buildFilterData($this->state, $abilityPartData);
        $this->assertEquals(
            $filterData->getValidLocations(),
            [
                Field::REARGUARD3,
                Field::VANGUARD,
            ]
        );

        $abilityPartData->setOptions([
            AbilityPartOptions::PLAYER => AbilityPartOptions::PLAYER_OPPONENT,
            AbilityPartOptions::LOCATION => AbilityPartOptions::LOCATION_COLUMN
        ])
        ->setSelfCardId('card2');
        $filterData = $service->buildFilterData($this->state, $abilityPartData);
        $this->assertEquals(
            $filterData->getValidLocations(),
            [
                Field::REARGUARD . '7',
            ]
        );

        $abilityPartData->setOptions([
            AbilityPartOptions::PLAYER => AbilityPartOptions::PLAYER_OPPONENT,
            AbilityPartOptions::LOCATION => AbilityPartOptions::LOCATION_COLUMN
        ])
            ->setSelfCardId('card3');
        $filterData = $service->buildFilterData($this->state, $abilityPartData);
        $this->assertEquals(
            $filterData->getValidLocations(),
            [
                Field::REARGUARD . '6',
            ]
        );
    }

    /**
     * @dataProvider getCommonFilterData
     * @param string $playerHash
     * @param string $cardId
     * @param array $options
     * @param bool $result
     * @throws MisconfiguredAbilityException
     */
    public function testIsCommonFilterPassed(string $playerHash, string $cardId, array $options, bool $result): void
    {
        $service = new FilterHelper();

        $card = $this->state->getPlayer($playerHash)->getCardById($cardId);
        $this->assertNotNull($card);

        $abilityPartData = (new AbilityPart())
            ->setPlayerHash($playerHash)
            ->setSelfCardId($cardId)
            ->setOptions($options)
        ;
        $filterData = $service->buildFilterData($this->state, $abilityPartData);
        $this->assertEquals($service->isCommonFilterPassed($filterData, $card), $result);
    }

    public function testIsLocationFilterPassed()
    {
        $service = new FilterHelper();

        $filterData = (new FilterData())
            ->setValidLocations([
                Field::VANGUARD,
                Field::REARGUARD1
            ])
        ;
        $this->assertEquals($service->isLocationFilterPassed($filterData, Field::VANGUARD), true);
        $this->assertEquals($service->isLocationFilterPassed($filterData, Field::REARGUARD5), false);

        $filterData = (new FilterData())
            ->setInvalidLocations([
                Field::VANGUARD,
                Field::REARGUARD1
            ])
        ;
        $this->assertEquals($service->isLocationFilterPassed($filterData, Field::VANGUARD), false);
        $this->assertEquals($service->isLocationFilterPassed($filterData, Field::REARGUARD5), true);

        $filterData = new FilterData();
        $this->assertEquals($service->isLocationFilterPassed($filterData, Field::VANGUARD), true);
    }

    public function getCommonFilterData(): array
    {
        return [
            [
                'hash1',
                'card1',
                [
                    AbilityPartOptions::RACE => 'Dog'
                ],
                true
            ],
            [
                'hash1',
                'card1',
                [
                    AbilityPartOptions::RACE => 'Cat'
                ],
                false
            ],
            [
                'hash1',
                'card3',
                [
                    AbilityPartOptions::RACE => 'Cat'
                ],
                false
            ],
            [
                'hash1',
                'card3',
                [
                    AbilityPartOptions::RACE => 'race1'
                ],
                true
            ],
            [
                'hash1',
                'card1',
                [
                    AbilityPartOptions::MIN_GRADE => 3
                ],
                false
            ],
            [
                'hash1',
                'card1',
                [
                    AbilityPartOptions::MIN_GRADE => 2
                ],
                true
            ],
            [
                'hash1',
                'card1',
                [
                    AbilityPartOptions::MAX_GRADE => 1
                ],
                false
            ],
            [
                'hash1',
                'card1',
                [
                    AbilityPartOptions::MAX_GRADE => 3
                ],
                true
            ],
            [
                'hash1',
                'card1',
                [
                    AbilityPartOptions::NAME => 'Small Vanguard'
                ],
                true
            ],
            [
                'hash1',
                'card1',
                [
                    AbilityPartOptions::NAME => 'Small Rearguard'
                ],
                false
            ],
            [
                'hash1',
                'card1',
                [
                    AbilityPartOptions::PARTIAL_NAME => 'Small'
                ],
                true
            ],
            [
                'hash1',
                'card1',
                [
                    AbilityPartOptions::PARTIAL_NAME => 'Large'
                ],
                false
            ],
            [
                'hash1',
                'card1',
                [
                    AbilityPartOptions::STATE => AbilityPartOptions::STATE_STAND,
                ],
                false
            ],
            [
                'hash1',
                'card1',
                [
                    AbilityPartOptions::STATE => AbilityPartOptions::STATE_REST,
                ],
                true
            ],
            [
                'hash1',
                'card1',
                [],
                true
            ],
        ];
    }
}
