<?php

declare(strict_types=1);

namespace App\Tests\Ability\Action;

use App\Ability\Action\DoDeckShuffle;
use App\Entity\Ability\AbilityPart;
use App\Entity\Game\Card;
use App\Entity\Game\Player;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use PHPUnit\Framework\TestCase;

class DoDeckShuffleTest extends TestCase
{

    public function testProcessor()
    {
        $service = new DoDeckShuffle();
        $abilityPartData = (new AbilityPart())
            ->setPlayerHash('hash')
        ;
        $playerInput = $this->createMock(PlayerInput::class);

        $state = new State();
        $player = new Player();
        $player->setHash('hash');
        $state->addPlayer($player);

        $cards = [];
        for ($c=0; $c< 10; $c++) {
            $cards[] = (new Card())
                ->setId('card' . $c)
            ;
        }
        $player->getDeck()->setCards($cards);

        $this->assertEquals('do_deck_shuffle', $service->getName());
        $this->assertEquals(true, $service->processPlayerInput($state, $abilityPartData, $playerInput));
        $this->assertEquals(null, $service->getPublicUpdateData($state, $abilityPartData));
        $this->assertEquals(null, $service->getPrivateUpdateData($state, $abilityPartData));
        $this->assertEquals($cards, $player->getDeck()->getCards());
        $service->progressState($state, $abilityPartData);
        $this->assertNotEquals($cards, $player->getDeck()->getCards());
    }
}
