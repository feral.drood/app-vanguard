<?php

declare(strict_types=1);

namespace App\Tests\Ability\Action;

use App\Ability\Action\DoMoveMarked;
use App\Entity\Ability\AbilityPart;
use App\Entity\Game\Card;
use App\Entity\Game\Player;
use App\Entity\Game\State;
use App\Service\ChatHelper;
use PHPUnit\Framework\TestCase;

class DoMoveMarkedTest extends TestCase
{

    public function testProcessor()
    {
        $chatHelper = $this->createMock(ChatHelper::class);
        $service = new DoMoveMarked($chatHelper);
        $state = new State();
        $player = (new Player())
            ->setHash('hash')
        ;
        $state->addPlayer($player);
        $card1 = (new Card())
            ->setId('card1')
        ;
        $card2 = (new Card())
            ->setId('card2')
        ;
        $card3 = (new Card())
            ->setId('card3')
        ;
        $player->getHand()->setCards([$card1, $card2, $card3]);

        $this->assertEquals('do_move_marked', $service->getName());
        $this->assertEquals(3, $player->getHand()->getCount());

        $player->getBuffer()->setMarked('card_id', ['card1', 'card2']);
        $abilityPart = (new AbilityPart())
            ->setSelfCardId('card_id')
            ->setPlayerHash('hash')
            ->setOptions([
                'from' => 'hand',
                'to' => 'buffer',
            ])
        ;
        $service->progressState($state, $abilityPart);
        $this->assertEquals(1, $player->getHand()->getCount());

        $player->getBuffer()->setMarked('card_id', ['card1', 'card2']);
        $abilityPart = (new AbilityPart())
            ->setSelfCardId('card_id')
            ->setPlayerHash('hash')
            ->setOptions([
                'from' => 'buffer',
                'to' => 'drop',
            ])
        ;
        $service->progressState($state, $abilityPart);
        $this->assertEquals(2, $player->getDrop()->getCount());

        $player->getBuffer()->setMarked('card_id', ['card1', 'card2']);
        $abilityPart = (new AbilityPart())
            ->setSelfCardId('card_id')
            ->setPlayerHash('hash')
            ->setOptions([
                'from' => 'drop',
                'to' => 'soul',
            ])
        ;
        $service->progressState($state, $abilityPart);
        $this->assertEquals(0, $player->getDrop()->getCount());
        $this->assertEquals(2, $player->getSoul()->getCount());

        $player->getBuffer()->setMarked('card_id', ['card1', 'card2']);
        $abilityPart = (new AbilityPart())
            ->setSelfCardId('card_id')
            ->setPlayerHash('hash')
            ->setOptions([
                'from' => 'soul',
                'to' => 'damage',
            ])
        ;
        $service->progressState($state, $abilityPart);
        $this->assertEquals(2, $player->getDamage()->getCount());
    }
}
