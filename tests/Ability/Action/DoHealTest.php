<?php

declare(strict_types=1);

namespace App\Tests\Ability\Action;

use App\Ability\Action\DoHeal;
use App\Entity\Ability\AbilityPart;
use App\Entity\Ability\AbilityPartOptions;
use App\Entity\Game\Card;
use App\Entity\Game\Player;
use App\Entity\Game\State;
use App\Service\ChatHelper;
use PHPUnit\Framework\TestCase;

class DoHealTest extends TestCase
{
    public function testProcessor()
    {
        $chatHelper = $this->createMock(ChatHelper::class);
        $service = new DoHeal($chatHelper);

        $this->assertEquals('do_heal', $service->getName());

        $state = new State();
        $player1 = (new Player())
            ->setHash('hash1')
        ;
        $player2 = (new Player())
            ->setHash('hash2')
        ;
        $state->addPlayer($player1);
        $state->addPlayer($player2);

        $abilityPart = (new AbilityPart())
            ->setPlayerHash('hash1')
            ->addOption(AbilityPartOptions::COUNT, 1)
        ;

        $player1->getDamage()->addCard((new Card())->setId('id'));
        $this->assertEquals(0, $player1->getDrop()->getCount());
        $this->assertEquals(1, $player1->getDamage()->getCount());
        $service->progressState($state, $abilityPart);
        $this->assertEquals(1, $player1->getDrop()->getCount());
        $this->assertEquals(0, $player1->getDamage()->getCount());
    }
}
