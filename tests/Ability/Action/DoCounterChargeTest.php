<?php

declare(strict_types=1);

namespace App\Tests\Ability\Action;

use App\Ability\Action\DoCounterCharge;
use App\Entity\Ability\AbilityPart;
use App\Entity\Ability\AbilityPartOptions;
use App\Entity\Game\Card;
use App\Entity\Game\Player;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Service\ChatHelper;
use PHPUnit\Framework\TestCase;

class DoCounterChargeTest extends TestCase
{
    public function testProcessor()
    {
        $chatHelper = $this->createMock(ChatHelper::class);
        $service = new DoCounterCharge($chatHelper);

        $state = new State();
        $player = (new Player())
            ->setHash('hash')
        ;
        $state->addPlayer($player);
        $card1 = (new Card())->setId('card1')->setFaceDown(true);
        $player->getDamage()->setCards([
            $card1,
            (new Card())->setId('card2')->setFaceDown(true),
            (new Card())->setId('card3'),
        ]);
        $abilityPart = (new AbilityPart())
            ->setPlayerHash('hash')
            ->addOption(AbilityPartOptions::COUNT, 1)
        ;

        $playerInput = new PlayerInput('hash', ['card3']);
        $this->assertEquals('do_counter_charge', $service->getName());
        $this->assertEquals(['card1', 'card2'], $service->getValidChoices($state, $abilityPart));
        $this->assertEquals(false, $service->processPlayerInput($state, $abilityPart, $playerInput));
        $playerInput = new PlayerInput('hash', ['card1']);
        $this->assertEquals(true, $card1->isFaceDown());
        $this->assertEquals(true, $service->processPlayerInput($state, $abilityPart, $playerInput));
        $this->assertEquals(false, $card1->isFaceDown());
    }
}
