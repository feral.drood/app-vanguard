<?php

declare(strict_types=1);

namespace App\Tests\Ability\Action;

use App\Ability\Action\DoMarkList;
use App\Ability\FilterHelper;
use App\Service\Game\CardEventDispatcher;
use PHPUnit\Framework\TestCase;

class DoMarkListTest extends TestCase
{
    public function testGetName()
    {
        $filterHelper = $this->createMock(FilterHelper::class);
        $eventDispatcher = $this->createMock(CardEventDispatcher::class);
        $service = new DoMarkList($filterHelper, $eventDispatcher);

        $this->assertEquals('do_mark_list', $service->getName());
    }
}
