<?php

declare(strict_types=1);

namespace App\Tests\Ability\Action;

use App\Ability\Action\DoForceGift;
use App\Entity\Ability\AbilityPart;
use App\Entity\Game\Field;
use App\Entity\Game\Player;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use PHPUnit\Framework\TestCase;

class DoForceGiftTest extends TestCase
{

    public function testGetPrivateUpdateData()
    {
        $service = new DoForceGift();
        $state = $this->createMock(State::class);
        $abilityPartData = new AbilityPart();

        $this->assertEquals($service->getPrivateUpdateData($state, $abilityPartData), null);
    }

    public function testProcessPlayerInput()
    {
        $state = new State();
        $player = (new Player())->setHash('hash1');
        $state->addPlayer($player);
        $abilityPartData = new AbilityPart();
        $abilityPartData->setPlayerHash('hash1');

        $service = new DoForceGift();

        $this->assertEquals(false, $service->progressState($state, $abilityPartData));
        $this->assertEquals(['force1', 'force2'], $service->getValidChoices($state, $abilityPartData));


        $playerInput = new PlayerInput(
            'hash1',
            ['force3']
        );
        $this->assertEquals(false, $service->processPlayerInput($state, $abilityPartData, $playerInput));

        $playerInput = new PlayerInput(
            'hash1',
            ['force1']
        );
        $this->assertEquals(false, $service->processPlayerInput($state, $abilityPartData, $playerInput));
        $this->assertEquals('force1', $player->getGiftType('force'));
        $playerInput = new PlayerInput(
            'hash1',
            [Field::VANGUARD]
        );
        $this->assertEquals(true, $service->processPlayerInput($state, $abilityPartData, $playerInput));
        $this->assertEquals('force', $player->getGiftCircleType(Field::VANGUARD));
        $this->assertEquals(1, $player->getGiftCircleCount(Field::VANGUARD));
    }

    public function testGetPublicUpdateData()
    {
        $state = new State();
        $player = (new Player())->setHash('hash1');
        $state->addPlayer($player);
        $abilityPartData = new AbilityPart();
        $abilityPartData->setPlayerHash('hash1');

        $service = new DoForceGift();

        $dialogue1 = $service->getPublicUpdateData($state, $abilityPartData);
        $this->assertEquals(
            $dialogue1->getData(),
            [
                'type' => 'action.dialog',
                'player' => 'hash1',
                'data' => [
                    'type' => 'action',
                    'title' => 'Choose gift type',
                    'actions' => [
                        'force1' => 'Force I',
                        'force2' => 'Force II',
                    ]
                ]
            ]
        );
        $player->setGiftType('force', 'force1');
        $dialogue2 = $service->getPublicUpdateData($state, $abilityPartData);
        $this->assertEquals(
            $dialogue2->getData(),
            [
                'type' => 'action.dialog',
                'player' => 'hash1',
                'data' => [
                    'type' => 'action',
                    'title' => 'Choose field circle to receive Force gift',
                    'actions' => [
                        'send' => 'Done',
                    ],
                    'options' => [
                        'min_count' => 1,
                        'max_count' => 1,
                    ]
                ]
            ]
        );

    }

    public function testGetName()
    {
        $service = new DoForceGift();

        $this->assertEquals($service->getName(), 'do_force_gift');
    }
}
