<?php

declare(strict_types=1);

namespace App\Tests\Ability\Action;

use App\Ability\Action\DoProtectGift;
use App\Entity\Ability\AbilityPart;
use App\Entity\Game\Field;
use App\Entity\Game\Player;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use PHPUnit\Framework\TestCase;

class DoProtectGiftTest extends TestCase
{

    public function testGetPrivateUpdateData()
    {
        $service = new DoProtectGift();
        $state = $this->createMock(State::class);
        $abilityPartData = new AbilityPart();

        $this->assertEquals($service->getPrivateUpdateData($state, $abilityPartData), null);
    }

    public function testProcessPlayerInput()
    {
        $state = new State();
        $player = (new Player())->setHash('hash1');
        $state->addPlayer($player);
        $abilityPartData = new AbilityPart();
        $abilityPartData->setPlayerHash('hash1');

        $service = new DoProtectGift();

        $this->assertEquals(false, $service->progressState($state, $abilityPartData));
        $this->assertEquals(['protect1', 'protect2'], $service->getValidChoices($state, $abilityPartData));


        $playerInput = new PlayerInput(
            'hash1',
            ['protect3']
        );
        $this->assertEquals(false, $service->processPlayerInput($state, $abilityPartData, $playerInput));

        $playerInput = new PlayerInput(
            'hash1',
            ['protect1']
        );
        $this->assertEquals(true, $service->processPlayerInput($state, $abilityPartData, $playerInput));
        $this->assertEquals('protect1', $player->getGiftType('protect'));
        $this->assertEquals(1, $player->getHand()->getCount());

        $state = new State();
        $player = (new Player())->setHash('hash1');
        $state->addPlayer($player);
        $playerInput = new PlayerInput(
            'hash1',
            ['protect2']
        );
        $this->assertEquals(false, $service->processPlayerInput($state, $abilityPartData, $playerInput));
        $this->assertEquals('protect2', $player->getGiftType('protect'));
        $playerInput = new PlayerInput(
            'hash1',
            [Field::REARGUARD1]
        );
        $this->assertEquals(true, $service->processPlayerInput($state, $abilityPartData, $playerInput));
        $this->assertEquals('protect', $player->getGiftCircleType(Field::REARGUARD1));
    }

    public function testGetPublicUpdateData()
    {
        $state = new State();
        $player = (new Player())->setHash('hash1');
        $state->addPlayer($player);
        $abilityPartData = new AbilityPart();
        $abilityPartData->setPlayerHash('hash1');

        $service = new DoProtectGift();

        $dialogue1 = $service->getPublicUpdateData($state, $abilityPartData);
        $this->assertEquals(
            $dialogue1->getData(),
            [
                'type' => 'action.dialog',
                'player' => 'hash1',
                'data' => [
                    'type' => 'action',
                    'title' => 'Choose gift type',
                    'actions' => [
                        'protect1' => 'Protect I',
                        'protect2' => 'Protect II',
                    ]
                ]
            ]
        );
        $player->setGiftType('protect', 'protect1');
        $dialogue2 = $service->getPublicUpdateData($state, $abilityPartData);
        $this->assertEquals(
            $dialogue2->getData(),
            [
                'type' => 'action.dialog',
                'player' => 'hash1',
                'data' => [
                    'type' => 'action',
                    'title' => 'Choose rearguard circle to receive Protect gift',
                    'actions' => [
                        'send' => 'Done',
                    ],
                    'options' => [
                        'min_count' => 1,
                        'max_count' => 1,
                    ]
                ]
            ]
        );

    }

    public function testGetName()
    {
        $service = new DoProtectGift();

        $this->assertEquals($service->getName(), 'do_protect_gift');
    }
}
