<?php

declare(strict_types=1);

namespace App\Tests\Ability\Action;

use App\Ability\Action\DoSoulBlastMarked;
use App\Entity\Ability\AbilityPart;
use App\Entity\Game\Card;
use App\Entity\Game\Player;
use App\Entity\Game\State;
use App\Service\ChatHelper;
use PHPUnit\Framework\TestCase;

class DoSoulBlastMarkedTest extends TestCase
{
    public function testProcessor()
    {
        $chatHelper = $this->createMock(ChatHelper::class);
        $service = new DoSoulBlastMarked($chatHelper);

        $state = new State();
        $player = (new Player())
            ->setHash('hash1')
        ;
        $state->addPlayer($player);
        $player->getSoul()->addCards([
            (new Card())->setId('id1'),
            (new Card())->setId('id2'),
            (new Card())->setId('id3'),
            (new Card())->setId('id4'),
        ]);
        $player->getBuffer()->setMarked('card1', ['id2', 'id3', 'id4']);

        $this->assertEquals('do_soul_blast_marked', $service->getName());

        $abilityPart = (new AbilityPart())
            ->setSelfCardId('card1')
            ->setPlayerHash('hash1')
        ;

        $this->assertEquals(4, $player->getSoul()->getCount());
        $this->assertEquals(0, $player->getDrop()->getCount());
        $this->assertEquals(true, $service->progressState($state, $abilityPart));
        $this->assertEquals(1, $player->getSoul()->getCount());
        $this->assertEquals(3, $player->getDrop()->getCount());
    }
}
