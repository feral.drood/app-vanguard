<?php

declare(strict_types=1);

namespace App\Tests\Ability\Action;

use App\Ability\Action\DoRegisterUsage1;
use App\Entity\Ability\AbilityPart;
use App\Entity\Game\AbilityStack;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use PHPUnit\Framework\TestCase;

class DoRegisterUsage1Test extends TestCase
{
    public function testProcessor()
    {
        $service = new DoRegisterUsage1();
        $state = $this->createMock(State::class);
        $abilityStack = $this->createMock(AbilityStack::class);
        $abilityStack
            ->method('addUsedAbility')
            ->with('card1?1')
        ;

        $state->method('getAbilityStack')->willReturn($abilityStack);
        $abilityPart = (new AbilityPart())
            ->setSelfCardId('card1')
        ;
        $playerInput = $this->createMock(PlayerInput::class);

        $this->assertEquals('do_register_usage1', $service->getName());
        $this->assertEquals(true, $service->processPlayerInput($state, $abilityPart, $playerInput));
        $this->assertEquals(null, $service->getPublicUpdateData($state, $abilityPart));
        $this->assertEquals(null, $service->getPrivateUpdateData($state, $abilityPart));
        $this->assertEquals([], $service->getValidChoices($state, $abilityPart));
        $this->assertEquals(true, $service->progressState($state, $abilityPart));
    }
}
