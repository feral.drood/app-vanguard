<?php

declare(strict_types=1);

namespace App\Tests\Ability\Action;

use App\Ability\Action\DoAssistDraw;
use App\Entity\Ability\AbilityPart;
use App\Entity\Game\Card;
use App\Entity\Game\Field;
use App\Entity\Game\Player;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Service\ChatHelper;
use PHPUnit\Framework\TestCase;

class DoAssistDrawTest extends TestCase
{
    public function testProcessor(): void
    {
        $chatHelper = $this->createMock(ChatHelper::class);
        $service = new DoAssistDraw($chatHelper);


        $state = new State();
        $player = (new Player())
            ->setHash('hash')
        ;
        $state->addPlayer($player);
        $player->getField()->setCircleCard(
            Field::VANGUARD,
            (new Card())
            ->setGrade(0)
        );
        $player->getDeck()->setCards([
            (new Card())->setGrade(1)->setId('card1'),
            (new Card())->setGrade(0)->setId('card2'),
            (new Card())->setGrade(3)->setId('card3'),
            (new Card())->setGrade(2)->setId('card4'),
            (new Card())->setGrade(1)->setId('card5'),
            (new Card())->setGrade(0)->setId('card6'),
            (new Card())->setGrade(2)->setId('card7'),
            (new Card())->setGrade(1)->setId('card8'),
            (new Card())->setGrade(3)->setId('card9'),
            (new Card())->setGrade(0)->setId('card10'),
        ]);


        $abilityPart = (new AbilityPart())
            ->setPlayerHash('hash')
        ;

        $this->assertEquals('do_assist_draw', $service->getName());
        $this->assertEquals(['card1', 'card5'], $service->getValidChoices($state, $abilityPart));
        $playerInput = new PlayerInput('hash', ['card8']);
        $this->assertEquals(false, $service->processPlayerInput($state, $abilityPart, $playerInput));
        $playerInput = new PlayerInput('hash', ['card5']);
        $this->assertEquals(0, $player->getHand()->getCount());
        $this->assertEquals(true, $service->processPlayerInput($state, $abilityPart, $playerInput));
        $this->assertEquals(1, $player->getHand()->getCount());
    }
}
