<?php

declare(strict_types=1);

namespace App\Tests\Ability\Action;

use App\Ability\Action\DoMarkCard;
use App\Entity\Ability\AbilityPart;
use App\Entity\Ability\AbilityPartOptions;
use App\Entity\Game\Card;
use App\Entity\Game\Field;
use App\Entity\Game\Player;
use App\Entity\Game\State;
use PHPUnit\Framework\TestCase;

class DoMarkCardTest extends TestCase
{
    public function testProcessor()
    {
        $service = new DoMarkCard();

        $state = new State();
        $player = (new Player())
            ->setHash('hash')
        ;
        $player2 = (new Player())
            ->setHash('hash2')
        ;
        $state->addPlayer($player);
        $state->addPlayer($player2);
        $trigger = (new Card())->setId('trigger1');
        $vanguard = (new Card())->setId('vanguard1');
        $rearguard1 = (new Card())->setId('rearguard1');
        $rearguard2 = (new Card())->setId('rearguard2');
        $player->setTrigger($trigger);
        $player->getField()->setCircleCard(Field::VANGUARD, $vanguard);
        $player2->getField()->setCircleCard(Field::REARGUARD1, $rearguard1);
        $player->getField()->setCircleCard(Field::REARGUARD2, $rearguard2);
        $state->setAttacker(Field::VANGUARD);
        $state->setDefender(Field::REARGUARD1);
        $state->setBooster(Field::REARGUARD2);
        $state->setActivePlayerHash('hash');


        $this->assertEquals('do_mark_card', $service->getName());
        $this->assertEquals(null, $player->getBuffer()->getMarked('vanguard1'));

        $abilityPart = (new AbilityPart())
            ->setPlayerHash('hash')
            ->setSelfCardId('vanguard1')
            ->setEventCardId('rearguard1')
            ->addOption(AbilityPartOptions::CARD, AbilityPartOptions::LOCATION_VANGUARD)
        ;
        $service->progressState($state, $abilityPart);
        $this->assertEquals('vanguard1', $player->getBuffer()->getMarked('vanguard1'));

        $abilityPart = (new AbilityPart())
            ->setPlayerHash('hash')
            ->setSelfCardId('vanguard1')
            ->setEventCardId('rearguard1')
            ->addOption(AbilityPartOptions::CARD, AbilityPartOptions::CARD_EVENT)
        ;
        $service->progressState($state, $abilityPart);
        $this->assertEquals('rearguard1', $player->getBuffer()->getMarked('vanguard1'));

        $abilityPart = (new AbilityPart())
            ->setPlayerHash('hash')
            ->setSelfCardId('vanguard1')
            ->setEventCardId('rearguard1')
            ->addOption(AbilityPartOptions::CARD, AbilityPartOptions::CARD_SELF)
        ;
        $service->progressState($state, $abilityPart);
        $this->assertEquals('vanguard1', $player->getBuffer()->getMarked('vanguard1'));

        $abilityPart = (new AbilityPart())
            ->setPlayerHash('hash')
            ->setSelfCardId('vanguard1')
            ->setEventCardId('rearguard1')
            ->addOption(AbilityPartOptions::CARD, AbilityPartOptions::LOCATION_TRIGGER)
        ;
        $service->progressState($state, $abilityPart);
        $this->assertEquals('trigger1', $player->getBuffer()->getMarked('vanguard1'));

        $abilityPart = (new AbilityPart())
            ->setPlayerHash('hash')
            ->setSelfCardId('vanguard1')
            ->setEventCardId('rearguard1')
            ->addOption(AbilityPartOptions::CARD, AbilityPartOptions::LOCATION_ATTACKER)
        ;
        $service->progressState($state, $abilityPart);
        $this->assertEquals('vanguard1', $player->getBuffer()->getMarked('vanguard1'));

        $abilityPart = (new AbilityPart())
            ->setPlayerHash('hash')
            ->setSelfCardId('vanguard1')
            ->setEventCardId('rearguard1')
            ->addOption(AbilityPartOptions::CARD, AbilityPartOptions::LOCATION_BOOSTER)
        ;
        $service->progressState($state, $abilityPart);
        $this->assertEquals('rearguard2', $player->getBuffer()->getMarked('vanguard1'));

        $abilityPart = (new AbilityPart())
            ->setPlayerHash('hash')
            ->setSelfCardId('vanguard1')
            ->setEventCardId('rearguard1')
            ->addOption(AbilityPartOptions::CARD, AbilityPartOptions::LOCATION_DEFENDER)
        ;
        $service->progressState($state, $abilityPart);
        $this->assertEquals('rearguard1', $player->getBuffer()->getMarked('vanguard1'));
    }
}
