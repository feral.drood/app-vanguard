<?php

declare(strict_types=1);

namespace App\Tests\Ability\Action;

use App\Ability\Action\DoQuickShield;
use App\Entity\Ability\AbilityPart;
use App\Entity\Game\Player;
use App\Entity\Game\State;
use App\Service\ChatHelper;
use PHPUnit\Framework\TestCase;

class DoQuickShieldTest extends TestCase
{

    public function testProcessPlayerInput()
    {
        $chatHelper = $this->createMock(ChatHelper::class);
        $service = new DoQuickShield($chatHelper);

        $state = new State();
        $player = (new Player())
            ->setHash('hash')
            ->setName('Player')
        ;
        $state
            ->addPlayer($player)
        ;

        $abilityPart = (new AbilityPart())
            ->setPlayerHash('hash')
        ;

        $this->assertEquals('do_quick_shield', $service->getName());
        $this->assertEquals(0, $player->getHand()->getCount());
        $this->assertEquals(true, $service->progressState($state, $abilityPart));
        $this->assertEquals(1, $player->getHand()->getCount());
    }
}
