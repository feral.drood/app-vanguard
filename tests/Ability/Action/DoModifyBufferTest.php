<?php

declare(strict_types=1);

namespace App\Tests\Ability\Action;

use App\Ability\Action\DoModifyBuffer;
use App\Entity\Ability\AbilityPart;
use App\Entity\Game\Card;
use App\Entity\Game\Field;
use App\Entity\Game\Player;
use App\Entity\Game\State;
use PHPUnit\Framework\TestCase;

class DoModifyBufferTest extends TestCase
{
    public function testProcessor()
    {
        $service = new DoModifyBuffer();

        $state = new State();
        $player = (new Player())
            ->setHash('hash')
        ;
        $state->addPlayer($player);
        $card1 = (new Card())
            ->setId('card1')
            ->setPower(1)
            ->setCritical(1)
        ;
        $card2 = (new Card())
            ->setId('card2')
        ;
        $player->getField()->setCircleCard(Field::VANGUARD, $card1);
        $player->getHand()->addCard($card2);

        $this->assertEquals('do_modify_buffer', $service->getName());
        $abilityData = (new AbilityPart())
            ->setPlayerHash('hash')
            ->setSelfCardId('card3')
            ->addOption(Card::BONUS_POWER, 1)
        ;
        $player->getBuffer()->setMarked('card3', 'someting_pretty');
        $player->getBuffer()->setCards('card3', [$card1]);
        $this->assertEquals(1, $card1->getPower());
        $service->progressState($state, $abilityData);
        $this->assertEquals(2, $card1->getPower());


//        $abilityData->setOptions([
//            Card::BONUS_CRITICAL => 1,
//        ]);
//        $player->getBuffer()->setValue('card3', 'card1');
//        $player->getBuffer()->setCards('card3', []);
//        $this->assertEquals(1, $card1->getCritical());
//        $service->progressState($state, $abilityData);
//        $this->assertEquals(2, $card1->getPower());

    }
}
