<?php

declare(strict_types=1);

namespace App\Tests\Ability\Action;

use App\Ability\Action\DoCondition;
use App\Ability\ConditionAbilityPartProcessorInterface;
use App\Entity\Ability\AbilityPart;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Service\AbilityPartManager;
use PHPUnit\Framework\TestCase;

class DoConditionTest extends TestCase
{

    public function testProcessor()
    {
        $abilityPartManager = $this->createMock(AbilityPartManager::class);
        $childConditionProcessor = $this->createMock(ConditionAbilityPartProcessorInterface::class);
        $abilityPartManager->method('getConditionAbilityPartProcessor')->willReturn($childConditionProcessor);
        $childConditionProcessor->method('isPassed')->willReturn(true, true, true, false);
        $service = new DoCondition($abilityPartManager);
        $state = new State();
        $playerInput = $this->createMock(PlayerInput::class);

        $abilityPart = (new AbilityPart())
            ->setOptions([
                'if' => [
                    ['type' => 'condition1'],
                    ['type' => 'condition2'],
                ],
                'then' => [
                    ['then_condition'],
                ],
                'else' => [
                    ['else_condition'],
                ],
            ])
        ;

        $this->assertEquals('do_condition', $service->getName());
        $this->assertEquals(null, $service->getPublicUpdateData($state, $abilityPart));
        $this->assertEquals(null, $service->getPrivateUpdateData($state, $abilityPart));
        $this->assertEquals(true, $service->processPlayerInput($state, $abilityPart, $playerInput));
        $this->assertEquals([], $service->getValidChoices($state, $abilityPart));
        $service->progressState($state, $abilityPart);
        $this->assertEquals(true, in_array('then_condition', $state->getAbilityStack()->getEffect()->getData(), true));
        $this->assertEquals(false, in_array('else_condition', $state->getAbilityStack()->getEffect()->getData(), true));
        $state->getAbilityStack()->removeEffect();
        $service->progressState($state, $abilityPart);
        $this->assertEquals(false, in_array('then_condition', $state->getAbilityStack()->getEffect()->getData(), true));
        $this->assertEquals(true, in_array('else_condition', $state->getAbilityStack()->getEffect()->getData(), true));
    }
}
