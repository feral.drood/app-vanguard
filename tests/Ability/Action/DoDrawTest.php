<?php

declare(strict_types=1);

namespace App\Tests\Ability\Action;

use App\Ability\Action\DoDraw;
use App\Entity\Ability\AbilityPart;
use App\Entity\Ability\AbilityPartOptions;
use App\Entity\Game\Player;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Service\ChatHelper;
use App\Service\Game\CardEventDispatcher;
use PHPUnit\Framework\TestCase;
use App\Entity\Game\Card;

class DoDrawTest extends TestCase
{
    public function testService()
    {
        $chatHelper = $this->createMock(ChatHelper::class);
        $eventDispatcher = $this->createMock(CardEventDispatcher::class);
        $chatHelper->method('plural')->willReturn('card');
        $service = new DoDraw($chatHelper, $eventDispatcher);

        $state = new State();
        $player = (new Player())
            ->setName('Player')
            ->setHash('hash')
        ;
        $state->addPlayer($player);
        $player->getDeck()->addCard((new Card())->setId('card1'));
        $abilityPartData = (new AbilityPart())
            ->setSelfCardId('card1')
            ->setPlayerHash('hash')
            ->addOption(AbilityPartOptions::COUNT, 1)
        ;
        $playerInput = new PlayerInput('hash', []);

        $this->assertEquals('do_draw', $service->getName());
        $this->assertEquals(0, $player->getHand()->getCount());
        $this->assertEquals(true, $service->processPlayerInput($state, $abilityPartData, $playerInput));
        $this->assertEquals(null, $service->getPublicUpdateData($state, $abilityPartData));
        $this->assertEquals(null, $service->getPrivateUpdateData($state, $abilityPartData));
        $this->assertEquals([], $service->getValidChoices($state, $abilityPartData));
        $this->assertEquals(true, $service->progressState($state, $abilityPartData));
        $this->assertEquals(1, $player->getHand()->getCount());
    }
}
