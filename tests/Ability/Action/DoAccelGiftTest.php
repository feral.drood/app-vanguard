<?php

declare(strict_types=1);

namespace App\Tests\Ability\Action;

use App\Ability\Action\DoAccelGift;
use App\Entity\Ability\AbilityPart;
use App\Entity\Game\Field;
use App\Entity\Game\Player;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use PHPUnit\Framework\TestCase;

class DoAccelGiftTest extends TestCase
{

    public function testGetPrivateUpdateData()
    {
        $service = new DoAccelGift();
        $state = $this->createMock(State::class);
        $abilityPartData = new AbilityPart();

        $this->assertEquals($service->getPrivateUpdateData($state, $abilityPartData), null);
    }

    public function testProcessPlayerInput()
    {
        $state = new State();
        $player = (new Player())->setHash('hash1');
        $state->addPlayer($player);
        $abilityPartData = new AbilityPart();
        $abilityPartData->setPlayerHash('hash1');

        $service = new DoAccelGift();

        $this->assertEquals(false, $service->progressState($state, $abilityPartData));
        $this->assertEquals(['accel1', 'accel2'], $service->getValidChoices($state, $abilityPartData));


        $playerInput = new PlayerInput(
            'hash1',
            ['accel3']
        );
        $this->assertEquals(false, $service->processPlayerInput($state, $abilityPartData, $playerInput));

        $playerInput = new PlayerInput(
            'hash1',
            ['accel1']
        );
        $this->assertEquals(false, $player->getField()->circleExists(Field::REARGUARD . 6));
        $this->assertEquals(true, $service->processPlayerInput($state, $abilityPartData, $playerInput));
        $this->assertEquals('accel1', $player->getGiftType('accel'));
        $this->assertEquals(true, $player->getField()->circleExists(Field::REARGUARD . 6));
    }

    public function testGetPublicUpdateData()
    {
        $state = new State();
        $player = (new Player())->setHash('hash1');
        $state->addPlayer($player);
        $abilityPartData = new AbilityPart();
        $abilityPartData->setPlayerHash('hash1');

        $service = new DoAccelGift();

        $dialogue1 = $service->getPublicUpdateData($state, $abilityPartData);
        $this->assertEquals(
            $dialogue1->getData(),
            [
                'type' => 'action.dialog',
                'player' => 'hash1',
                'data' => [
                    'type' => 'action',
                    'title' => 'Choose gift type',
                    'actions' => [
                        'accel1' => 'Accel I',
                        'accel2' => 'Accel II',
                    ]
                ]
            ]
        );
        $player->setGiftType('accel', 'accel1');
        $this->assertEquals(null, $service->getPublicUpdateData($state, $abilityPartData));
    }

    public function testGetName()
    {
        $service = new DoAccelGift();

        $this->assertEquals($service->getName(), 'do_accel_gift');
    }
}
