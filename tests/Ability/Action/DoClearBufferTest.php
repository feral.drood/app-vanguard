<?php

declare(strict_types=1);

namespace App\Tests\Ability\Action;

use App\Ability\Action\DoClearBuffer;
use App\Entity\Ability\AbilityPart;
use App\Entity\Game\Card;
use App\Entity\Game\Player;
use App\Entity\Game\State;
use PHPUnit\Framework\TestCase;

class DoClearBufferTest extends TestCase
{
    public function testProcessor()
    {
        $service = new DoClearBuffer();

        $state = new State();
        $player = (new Player())
            ->setHash('hash')
        ;
        $state->addPlayer($player);
        $abilityPart = (new AbilityPart())
            ->setPlayerHash('hash')
            ->setSelfCardId('card1')
        ;

        $this->assertEquals('do_clear_buffer', $service->getName());
        $this->assertEquals(null, $player->getBuffer()->getMarked('card1'));
        $card = (new Card())->setId('id');
        $player->getBuffer()->setCards('card1', [$card]);
        $this->assertEquals(['id' => $card], $player->getBuffer()->getCards('card1'));
        $player->getBuffer()->setMarked('card1', 'test_data');
        $this->assertEquals('test_data', $player->getBuffer()->getMarked('card1'));
        $this->assertEquals(true, $service->progressState($state, $abilityPart));
        $this->assertEquals([], $player->getBuffer()->getCards('card1'));
        $this->assertEquals(null, $player->getBuffer()->getMarked('card1'));
    }
}
