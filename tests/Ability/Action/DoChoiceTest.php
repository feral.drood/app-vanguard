<?php

declare(strict_types=1);

namespace App\Tests\Ability\Action;

use App\Ability\Action\DoChoice;
use App\Entity\Ability\AbilityPart;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use PHPUnit\Framework\TestCase;

class DoChoiceTest extends TestCase
{

    public function testProcessor()
    {
        $service = new DoChoice();

        $state = $this->createMock(State::class);

        $abilityPart = (new AbilityPart())
            ->setOptions([
                [
                    'id' => 'choice1',
                    'label' => 'Choose door1',
                    'effects' => [],
                ],
                [
                    'id' => 'choice2',
                    'label' => 'Choose door2',
                    'effects' => [],
                ]
            ])
        ;

        $this->assertEquals('do_choice', $service->getName());
        $this->assertNotEquals(null, $service->getPublicUpdateData($state, $abilityPart));
        $this->assertEquals(null, $service->getPrivateUpdateData($state, $abilityPart));
        $this->assertEquals(['choice1', 'choice2'], $service->getValidChoices($state, $abilityPart));
        $this->assertEquals(false, $service->progressState($state, $abilityPart));
        $playerInput = new PlayerInput('hash', ['choice3']);
        $this->assertEquals(false, $service->processPlayerInput($state, $abilityPart, $playerInput));
        $playerInput = new PlayerInput('hash', ['choice1']);
        $this->assertEquals(true, $service->processPlayerInput($state, $abilityPart, $playerInput));
    }
}
