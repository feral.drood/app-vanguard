<?php

declare(strict_types=1);

namespace App\Tests\Ability\Action;

use App\Ability\Action\DoCallBuffer;
use App\Entity\Ability\AbilityPart;
use App\Entity\Game\Card;
use App\Entity\Game\Field;
use App\Entity\Game\Player;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Service\ChatHelper;
use App\Service\Game\CardEventDispatcher;
use PHPUnit\Framework\TestCase;

class DoCallBufferTest extends TestCase
{

    public function testProcessor()
    {
        $chatHelper = $this->createMock(ChatHelper::class);
        $eventDispatcher = $this->createMock(CardEventDispatcher::class);

        $service = new DoCallBuffer($chatHelper, $eventDispatcher);

        $this->assertEquals('do_call_buffer', $service->getName());


        $state = new State();
        $player = (new Player())
            ->setHash('hash')
        ;
        $state->addPlayer($player);
        $card1 = (new Card())
            ->setId('card1')
            ->setName('Card 1')
        ;
        $card2 = (new Card())
            ->setId('card2')
            ->setName('Card 2')
        ;

        $abilityPart = (new AbilityPart())
            ->setPlayerHash('hash')
            ->setSelfCardId('id')
            ->setOptions([

            ])
        ;

        $player->getBuffer()->setMarked('id', null);
        $player->getBuffer()->setCards('id', [$card1, $card2]);
        $this->assertEquals(['card1', 'card2'], $service->getValidChoices($state, $abilityPart));
        $player->getBuffer()->setMarked('id', 'card1');
        $this->assertEqualsCanonicalizing(
            [
                Field::REARGUARD1,
                Field::REARGUARD2,
                Field::REARGUARD3,
                Field::REARGUARD4,
                Field::REARGUARD5,
            ],
            $service->getValidChoices($state, $abilityPart)
        );

        $player->getBuffer()->setMarked('id', null);
        $player->getBuffer()->setCards('id', [$card1, $card2]);
        $playerInput = new PlayerInput('hash', ['card3']);
        $this->assertEquals(false, $service->processPlayerInput($state, $abilityPart, $playerInput));
        $this->assertEquals(null, $player->getBuffer()->getMarked('id'));

        $player->getBuffer()->setMarked('id', null);
        $player->getBuffer()->setCards('id', [$card1, $card2]);
        $playerInput = new PlayerInput('hash', ['card1']);
        $this->assertEquals(false, $service->processPlayerInput($state, $abilityPart, $playerInput));
        $this->assertEquals('card1', $player->getBuffer()->getMarked('id'));

        $player->getBuffer()->setMarked('id', 'card1');
        $player->getBuffer()->setCards('id', [$card1, $card2]);
        $playerInput = new PlayerInput('hash', [Field::REARGUARD1]);
        $this->assertEquals(false, $service->processPlayerInput($state, $abilityPart, $playerInput));
        $this->assertEquals(1, count($player->getBuffer()->getCards('id')));
        $this->assertEquals($card1, $player->getField()->getCircleCard(Field::REARGUARD1));

        $player->getBuffer()->setMarked('id', 'card2');
        $playerInput = new PlayerInput('hash', [Field::REARGUARD2]);
        $this->assertEquals(true, $service->processPlayerInput($state, $abilityPart, $playerInput));

        $player->getBuffer()->setMarked('id', 'card2');
        $playerInput = new PlayerInput('hash', [Field::REARGUARD2]);
        $abilityPart->addOption('location','open');
        $this->assertEquals(false, $service->processPlayerInput($state, $abilityPart, $playerInput));

        $player->getBuffer()->setMarked('id', 'card2');
        $player->getBuffer()->setCards('id', [$card2]);
        $playerInput = new PlayerInput('hash', [Field::REARGUARD3]);
        $this->assertEquals(true, $service->processPlayerInput($state, $abilityPart, $playerInput));
    }
}
