<?php

declare(strict_types=1);

namespace App\Tests\Ability\Action;

use App\Ability\Action\DoSwitchPlayer;
use App\Entity\Ability\AbilityPart;
use App\Entity\Game\Player;
use App\Entity\Game\State;
use PHPUnit\Framework\TestCase;

class DoSwitchPlayerTest extends TestCase
{
    public function testProcessPlayerInput()
    {
        $service = new DoSwitchPlayer();

        $state = new State();
        $player1 = (new Player())
            ->setHash('hash1')
        ;
        $player2 = (new Player())
            ->setHash('hash2')
        ;
        $state->addPlayer($player1);
        $state->addPlayer($player2);

        $this->assertEquals('do_switch_player', $service->getName());

        $abilityPart = (new AbilityPart())
            ->setPlayerHash('hash1')
            ->setSelfCardId('id1')
            ->setEventCardId('id1')
            ->setOptions([
                [
                ]
            ])
        ;
        $this->assertEquals(true, $service->progressState($state, $abilityPart));
        $childEffect = $state->getAbilityStack()->getEffect();
        $this->assertEquals('hash2', $childEffect->getPlayerHash());
    }
}
