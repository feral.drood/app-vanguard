<?php

declare(strict_types=1);

namespace App\Tests\Ability\Action;

use App\Ability\Action\DoDiscard;
use App\Entity\Ability\AbilityPart;
use App\Entity\Ability\AbilityPartOptions;
use App\Entity\Game\Card;
use App\Entity\Game\Player;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use App\Service\ChatHelper;
use PHPUnit\Framework\TestCase;

class DoDiscardTest extends TestCase
{
    public function testProcessor(): void
    {
        $chatHelper = $this->createMock(ChatHelper::class);
        $service = new DoDiscard($chatHelper);

        $state = new State();
        $player = (new Player())
            ->setHash('hash')
        ;
        $state->addPlayer($player);
        $player->getHand()->addCards([
            (new Card())
            ->setId('card1'),
            (new Card())
            ->setId('card2'),
        ]);

        $abilityPart = (new AbilityPart())
            ->setPlayerHash('hash')
            ->addOption(AbilityPartOptions::COUNT, 1)
        ;

        $this->assertEquals('do_discard', $service->getName());
        $this->assertEquals(['card1', 'card2'], $service->getValidChoices($state, $abilityPart));
        $playerInput = new PlayerInput('hash', ['card3']);
        $this->assertEquals(false, $service->processPlayerInput($state, $abilityPart, $playerInput));
        $playerInput = new PlayerInput('hash', ['card1']);
        $this->assertEquals(2, $player->getHand()->getCount());
        $this->assertEquals(true, $service->processPlayerInput($state, $abilityPart, $playerInput));
        $this->assertEquals(1, $player->getHand()->getCount());
    }
}
