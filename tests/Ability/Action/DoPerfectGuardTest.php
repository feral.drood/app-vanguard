<?php

declare(strict_types=1);

namespace App\Tests\Ability\Action;

use App\Ability\Action\DoPerfectGuard;
use App\Entity\Ability\AbilityPart;
use App\Entity\Ability\AbilityPartOptions;
use App\Entity\Game\Card;
use App\Entity\Game\Field;
use App\Entity\Game\Player;
use App\Entity\Game\State;
use App\Entity\PlayerInput;
use PHPUnit\Framework\TestCase;

class DoPerfectGuardTest extends TestCase
{
    public function testProcessor(): void
    {
        $service = new DoPerfectGuard();

        $state = new State();
        $player = (new Player())
            ->setName('Player')
            ->setHash('hash')
        ;
        $state->addPlayer($player);
        $state->setDefender(Field::VANGUARD);
        $card = new Card();
        $player->getField()->setCircleCard(Field::VANGUARD, $card);
        $abilityPartData = (new AbilityPart())
            ->setPlayerHash('hash')
            ->addOption(AbilityPartOptions::COUNT, 1)
        ;
        $playerInput = new PlayerInput('hash', []);

        $this->assertEquals('do_perfect_guard', $service->getName());
        $this->assertEquals(true, $service->processPlayerInput($state, $abilityPartData, $playerInput));
        $this->assertEquals(null, $service->getPublicUpdateData($state, $abilityPartData));
        $this->assertEquals(null, $service->getPrivateUpdateData($state, $abilityPartData));
        $this->assertEquals([], $service->getValidChoices($state, $abilityPartData));
        $this->assertEquals(false, $card->hasProperty(Card::PROPERTY_CANNOT_BE_HIT));
        $this->assertEquals(true, $service->progressState($state, $abilityPartData));
        $this->assertEquals(true, $card->hasProperty(Card::PROPERTY_CANNOT_BE_HIT));

    }
}
