<?php

declare(strict_types=1);

namespace App\Tests\Ability;

use App\Ability\FilterData;
use PHPUnit\Framework\TestCase;

class FilterDataTest extends TestCase
{
    public function testState(): void
    {
        $entity = (new FilterData())
            ->setState('data1')
        ;
        $this->assertEquals($entity->getState(), 'data1');
    }

    public function testMinGrade(): void
    {
        $entity = (new FilterData())
            ->setMinGrade(123)
        ;
        $this->assertEquals($entity->getMinGrade(), 123);
    }

    public function testRace(): void
    {
        $entity = (new FilterData())
            ->setRace('race1')
        ;
        $this->assertEquals($entity->getRace(), 'race1');
    }

    public function testValidLocations(): void
    {
        $entity = (new FilterData())
            ->setValidLocations(['location1'])
        ;
        $this->assertEquals($entity->getValidLocations(), ['location1']);
    }

    public function testInvalidLocations(): void
    {
        $entity = (new FilterData())
            ->setInvalidLocations(['location1'])
        ;
        $this->assertEquals($entity->getInvalidLocations(), ['location1']);
    }

    public function testName(): void
    {
        $entity = (new FilterData())
            ->setName('name1')
        ;
        $this->assertEquals($entity->getName(), 'name1');
    }

    public function testMaxGrade(): void
    {
        $entity = (new FilterData())
            ->setMaxGrade(321)
        ;
        $this->assertEquals($entity->getMaxGrade(), 321);
    }

    public function testFace(): void
    {
        $entity = (new FilterData())
            ->setFace('down')
        ;
        $this->assertEquals($entity->getFace(), 'down');
    }

    public function testPartialName(): void
    {
        $entity = (new FilterData())
            ->setPartialName('name2')
        ;
        $this->assertEquals($entity->getPartialName(), 'name2');
    }
}
