<?php

declare(strict_types=1);

namespace App\Tests\Ability\Setter;

use App\Ability\Setter\SetValueMath;
use App\Entity\Ability\AbilityPart;
use App\Entity\Game\Player;
use App\Entity\Game\State;
use PHPUnit\Framework\TestCase;

class SetValueMathTest extends TestCase
{
    public function testProcessor()
    {
        $service = new SetValueMath();

        $state = new State();
        $player = (new Player())
            ->setHash('hash1')
        ;
        $state->addPlayer($player);

        $abilityPart = (new AbilityPart())
            ->setPlayerHash('hash1')
            ->setSelfCardId('card1')
        ;

        $this->assertEquals('set_value_math', $service->getName());

        $abilityPart->setOptions([
            'operation' => 'mul',
            'value' => 3,
        ]);
        $player->getBuffer()->setValue('card1', 3);
        $this->assertEquals(true, $service->progressState($state, $abilityPart));
        $this->assertEquals(9, $player->getBuffer()->getValue('card1'));

        $abilityPart->setOptions([
            'operation' => 'div',
            'value' => 2,
        ]);
        $player->getBuffer()->setValue('card1', 7);
        $this->assertEquals(true, $service->isPassed($state, $abilityPart));
        $this->assertEquals(3, $player->getBuffer()->getValue('card1'));

        $abilityPart->setOptions([
            'operation' => 'mod',
            'value' => 2,
        ]);
        $player->getBuffer()->setValue('card1', 7);
        $this->assertEquals(true, $service->progressState($state, $abilityPart));
        $this->assertEquals(1, $player->getBuffer()->getValue('card1'));

        $abilityPart->setOptions([
            'operation' => 'add',
            'value' => 3,
        ]);
        $player->getBuffer()->setValue('card1', 2);
        $this->assertEquals(true, $service->isPassed($state, $abilityPart));
        $this->assertEquals(5, $player->getBuffer()->getValue('card1'));

        $abilityPart->setOptions([
            'operation' => 'sub',
            'value' => 6,
        ]);
        $player->getBuffer()->setValue('card1', 2);
        $this->assertEquals(true, $service->progressState($state, $abilityPart));
        $this->assertEquals(-4, $player->getBuffer()->getValue('card1'));
    }
}
