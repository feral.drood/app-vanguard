<?php

declare(strict_types=1);

namespace App\Tests\Ability\Condition;

use App\Ability\Condition\IsCardCheck;
use App\Ability\FilterData;
use App\Ability\FilterHelper;
use App\Entity\Ability\AbilityPart;
use App\Entity\Ability\AbilityPartOptions;
use App\Entity\Game\Card;
use App\Entity\Game\Field;
use App\Entity\Game\Player;
use App\Entity\Game\State;
use PHPUnit\Framework\TestCase;

class IsCardCheckTest extends TestCase
{
    public function testAbilityPart(): void
    {
        $filterData = (new FilterData());
        $filterHelper = $this->createMock(FilterHelper::class);
        $filterHelper->method('buildFilterData')->willReturn($filterData);
        $filterHelper->method('isCommonFilterPassed')->willReturn(true, true, false, false);
        $filterHelper->method('isLocationFilterPassed')->willReturn(true, false, true, false);

        $service = new IsCardCheck($filterHelper);

        $state = new State();
        $player1 = (new Player())
            ->setHash('hash1')
        ;
        $state
            ->addPlayer($player1)
        ;
        $card = (new Card())
            ->setId('card1')
            ->setName('Much Wow')
            ->setRace('doge')
        ;
        $player1->getField()->setCircleCard(Field::VANGUARD, $card);

        $this->assertEquals($service->getName(), 'is_card_check');

        $checkData = (new AbilityPart())
            ->setPlayerHash('hash1')
            ->setSelfCardId('card1')
        ;

        $player1->getField()->setCircleCard(Field::VANGUARD, $card);
        $this->assertEquals($service->isPassed($state, $checkData), true);
        $this->assertEquals($service->isPassed($state, $checkData), false);
        $this->assertEquals($service->isPassed($state, $checkData), false);
        $this->assertEquals($service->isPassed($state, $checkData), false);
    }
}