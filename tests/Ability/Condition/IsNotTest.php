<?php

declare(strict_types=1);

namespace App\Tests\Ability\Condition;

use App\Ability\Condition\IsNot;
use App\Ability\ConditionAbilityPartProcessorInterface;
use App\Entity\Ability\AbilityPart;
use App\Entity\Game\State;
use App\Service\AbilityPartManager;
use PHPUnit\Framework\TestCase;

class IsNotTest extends TestCase
{
    public function testService()
    {
        $abilityPartManager = $this->createMock(AbilityPartManager::class);
        $service = new IsNot($abilityPartManager);
        $childAbility = $this->createMock(ConditionAbilityPartProcessorInterface::class);
        $childAbility->method('isPassed')->willReturn(true, false);
        $abilityPartManager->method('getConditionAbilityPartProcessor')->willReturn($childAbility);
        $state = $this->createMock(State::class);
        $childAbilityPartData = (new AbilityPart())
            ->setType('test')
        ;
        $abilityPartData = (new AbilityPart())
            ->addOption('condition', $childAbilityPartData->getData())
        ;

        $this->assertEquals('is_not', $service->getName());
        $this->assertEquals(false, $service->isPassed($state, $abilityPartData));
        $this->assertEquals(true, $service->isPassed($state, $abilityPartData));
    }
}
