<?php

declare(strict_types=1);

namespace App\Tests\Ability\Condition;

use App\Ability\Condition\IsActivePlayer;
use App\Entity\Ability\AbilityPart;
use App\Entity\Game\State;
use PHPUnit\Framework\TestCase;

class IsActivePlayerTest extends TestCase
{
    public function testProcessor()
    {
        $state = $this->createMock(State::class);
        $state->method('getActivePlayerHash')->willReturn('hash1');

        $abilityPart = (new AbilityPart())
            ->setPlayerHash('hash1')
        ;

        $service = new IsActivePlayer();

        $this->assertEquals(true, $service->isPassed($state, $abilityPart));

        $abilityPart->setPlayerHash('hash2');
        $this->assertEquals(false, $service->isPassed($state, $abilityPart));
    }
}
