<?php

declare(strict_types=1);

namespace App\Tests\Ability\Condition;

use App\Ability\Condition\IsListCheck;
use App\Ability\FilterData;
use App\Ability\FilterHelper;
use App\Entity\Ability\AbilityPart;
use App\Entity\Ability\AbilityPartOptions;
use App\Entity\Game\Card;
use App\Entity\Game\Field;
use App\Entity\Game\Player;
use App\Entity\Game\State;
use PHPUnit\Framework\TestCase;

class IsListCheckTest extends TestCase
{
    public function testGetName(): void
    {
        $filterHelper = $this->createMock(FilterHelper::class);
        $service = new IsListCheck($filterHelper);

        $this->assertEquals($service->getName(), 'is_list_check');
    }

    public function testIsPassed(): void
    {
        $filterHelper = $this->createMock(FilterHelper::class);
        $service = new IsListCheck($filterHelper);

        $filterData = (new FilterData());
        $filterHelper->method('buildFilterData')->willReturn($filterData);
        $filterHelper->method('isCommonFilterPassed')->willReturn(true);
        $filterHelper->method('isLocationFilterPassed')->willReturn(true);

        $state = new State();
        $player = (new Player())
            ->setHash('hash1')
        ;
        $state->addPlayer($player);

        $abilityPartData = (new AbilityPart())
            ->setPlayerHash('hash1')
            ->setSelfCardId('hand_card')
            ->setOptions([
                AbilityPartOptions::ZONE => AbilityPartOptions::LOCATION_HAND,
                AbilityPartOptions::MIN_COUNT => 1,
            ])
        ;
        $this->assertEquals($service->isPassed($state, $abilityPartData), false);

        $player->getHand()->addCard(
            (new Card())
            ->setId('hand_card')
            ->setName('Hand')
        );
        $abilityPartData->setOptions([
            AbilityPartOptions::ZONE => AbilityPartOptions::LOCATION_HAND,
            AbilityPartOptions::MIN_COUNT => 1,
        ]);
        $this->assertEquals($service->isPassed($state, $abilityPartData), true);

        $abilityPartData->setOptions([
            AbilityPartOptions::ZONE => AbilityPartOptions::LOCATION_SOUL,
            AbilityPartOptions::MAX_COUNT => 0,
        ]);
        $this->assertEquals($service->isPassed($state, $abilityPartData), true);
        $player->getSoul()->addCard(
            (new Card())
                ->setId('soul_card')
        );
        $this->assertEquals($service->isPassed($state, $abilityPartData), false);

        $abilityPartData->setOptions([
            AbilityPartOptions::ZONE => AbilityPartOptions::LOCATION_GUARDIAN,
            AbilityPartOptions::COUNT => 1,
        ]);
        $this->assertEquals($service->isPassed($state, $abilityPartData), false);
        $player->getGuardians()->addCard(
            (new Card())
            ->setId('guardian_card')
            ->setName('test')
        );
        $this->assertEquals($service->isPassed($state, $abilityPartData), true);

        $abilityPartData->setOptions([
            AbilityPartOptions::ZONE => AbilityPartOptions::LOCATION_DROP,
            AbilityPartOptions::COUNT => 1,
        ]);
        $this->assertEquals($service->isPassed($state, $abilityPartData), false);
        $player->getDrop()->addCard(
            (new Card())
            ->setId('drop_card')
        );
        $this->assertEquals($service->isPassed($state, $abilityPartData), true);

        $abilityPartData->setOptions([
            AbilityPartOptions::ZONE => AbilityPartOptions::LOCATION_DECK,
            AbilityPartOptions::COUNT => 1,
        ]);
        $this->assertEquals($service->isPassed($state, $abilityPartData), false);
        $player->getDeck()->addCard(
            (new Card())
            ->setId('deck_card')
        );
        $this->assertEquals($service->isPassed($state, $abilityPartData), true);

        $abilityPartData->setOptions([
            AbilityPartOptions::ZONE => AbilityPartOptions::LOCATION_FIELD,
            AbilityPartOptions::COUNT => 1,
        ]);
        $this->assertEquals($service->isPassed($state, $abilityPartData), false);
        $player->getField()->setCircleCard(
            Field::VANGUARD,
            (new Card())
            ->setId('vanguard_card')
        );
        $this->assertEquals($service->isPassed($state, $abilityPartData), true);

        $abilityPartData->setOptions([
            AbilityPartOptions::ZONE => AbilityPartOptions::LOCATION_BUFFER,
            AbilityPartOptions::COUNT => 1,
        ]);
        $this->assertEquals($service->isPassed($state, $abilityPartData), false);
        $player->getBuffer()->addCard(
            'hand_card',
            (new Card())
            ->setId('buffer_card')
        );
        $this->assertEquals($service->isPassed($state, $abilityPartData), true);

        $abilityPartData->setOptions([
            AbilityPartOptions::ZONE => AbilityPartOptions::LOCATION_DAMAGE,
            AbilityPartOptions::COUNT => 1,
        ]);
        $this->assertEquals($service->isPassed($state, $abilityPartData), false);
        $player->getDamage()->addCard(
            (new Card())
                ->setId('damage_card')
        );
        $this->assertEquals($service->isPassed($state, $abilityPartData), true);
    }
}
