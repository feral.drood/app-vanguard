<?php

declare(strict_types=1);

namespace App\Tests\Ability\Condition;

use App\Ability\Condition\IsOr;
use App\Ability\ConditionAbilityPartProcessorInterface;
use App\Entity\Ability\AbilityPart;
use App\Entity\Game\State;
use App\Service\AbilityPartManager;
use PHPUnit\Framework\TestCase;

class IsOrTest extends TestCase
{

    public function testProcessor()
    {
        $abilityPartManager = $this->createMock(AbilityPartManager::class);
        $service = new IsOr($abilityPartManager);
        $childAbility = $this->createMock(ConditionAbilityPartProcessorInterface::class);
        $childAbility->method('isPassed')->willReturn(false, true, false, false);
        $abilityPartManager->method('getConditionAbilityPartProcessor')->willReturn($childAbility);
        $state = $this->createMock(State::class);
        $childAbilityPartData = (new AbilityPart())
            ->setType('test')
        ;
        $abilityPartData = (new AbilityPart())
            ->addOption('conditions', [
                $childAbilityPartData->getData(),
                $childAbilityPartData->getData(),
            ])
        ;

        $this->assertEquals('is_or', $service->getName());
        $this->assertEquals(true, $service->isPassed($state, $abilityPartData));
        $this->assertEquals(false, $service->isPassed($state, $abilityPartData));

    }
}
