<?php

declare(strict_types=1);

namespace App\Tests\Ability\Condition;

use App\Ability\Condition\IsEventLogCheck;
use App\Entity\Ability\AbilityPart;
use App\Entity\Game\CardEventLog;
use App\Entity\Game\State;
use PHPUnit\Framework\TestCase;

class IsEventLogCheckTest extends TestCase
{
    public function testProcessor()
    {
        $service = new IsEventLogCheck();

        $state = $this->createMock(State::class);
        $eventLog = $this->createMock(CardEventLog::class);
        $eventLog->method('getCount')->willReturn(1, 0, null, 3, 2, 2);
        $state->method('getCardEventLog')->willReturn($eventLog);


        $this->assertEquals('is_event_log_check', $service->getName());

        $abilityPart = (new AbilityPart())
            ->setOptions([
                'events' => ['test1', 'test2'],
                'min_count', 1
            ])
        ;
        $this->assertEquals(true, $service->isPassed($state, $abilityPart));

        $abilityPart = (new AbilityPart())
            ->setOptions([
                'events' => ['test1', 'test2'],
                'max_count' => 2,
            ])
        ;
        $this->assertEquals(false, $service->isPassed($state, $abilityPart));

        $abilityPart = (new AbilityPart())
            ->setOptions([
                'events' => ['test1', 'test2'],
                'count' => 4,
            ])
        ;
        $this->assertEquals(true, $service->isPassed($state, $abilityPart));

    }
}
