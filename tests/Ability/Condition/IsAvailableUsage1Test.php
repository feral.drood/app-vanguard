<?php

declare(strict_types=1);

namespace App\Tests\Ability\Condition;

use App\Ability\Condition\IsAvailableUsage1;
use App\Entity\Ability\AbilityPart;
use App\Entity\Game\AbilityStack;
use App\Entity\Game\State;
use PHPUnit\Framework\TestCase;

class IsAvailableUsage1Test extends TestCase
{
    public function testService(): void
    {
        $service = new IsAvailableUsage1();

        $state = $this->createMock(State::class);
        $abilityStack = $this->createMock(AbilityStack::class);
        $abilityStack->method('isUsedAbility')->willReturn(true, false);
        $state->method('getAbilityStack')->willReturn($abilityStack);

        $abilityPartData = $this->createMock(AbilityPart::class);
        $abilityPartData->method('getSelfCardId')->willReturn('card1');

        $this->assertEquals($service->getName(), 'is_available_usage1');
        $this->assertEquals(false, $service->isPassed($state, $abilityPartData));
        $this->assertEquals(true, $service->isPassed($state, $abilityPartData));
    }
}
